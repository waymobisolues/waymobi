package br.com.servicoemcasa.foto;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.CRC32;

public class IOUtils {

    private static final int BUFFER_SIZE = 8192;

    public static byte[] readStream(InputStream in) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        echo(in, bout);
        return bout.toByteArray();
    }
    
    public static void echo(InputStream in, OutputStream out) throws IOException {
        echo(in, out, BUFFER_SIZE);
    }

    public static void echo(InputStream in, OutputStream out, int bufferSize) throws IOException {
        byte[] buf = new byte[BUFFER_SIZE];
        int length = 0;
        while ((length = in.read(buf)) > 0) {
            out.write(buf, 0, length);
        }
    }

    public static String readString(InputStream is) throws IOException {
        byte[] b = readStream(is);
        return new String(b);
    }

	public static String readString(InputStream is, String charset) throws IOException {
        byte[] b = readStream(is);
        return new String(b, charset);
    }

    public static String createHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            int v = b & 0xFF;
            sb.append(Integer.toHexString(v >>> 4)).append(Integer.toHexString(v & 0xF));
        }
        return sb.toString();
    }

    public static byte[] getBytesFromHexString(String string) {
        byte[] bytes = new byte[string.length() / 2];
        for (int i = 0, j = 0; i < string.length();) {
            int i1 = Character.digit(string.charAt(i++), 16);
            int i2 = Character.digit(string.charAt(i++), 16);
            bytes[j++] = (byte) ((i1 << 4) + (i2 & 0xF));
        }
        return bytes;
    }

    public static byte[] createChecksum(InputStream in, String algorithm) throws NoSuchAlgorithmException, IOException {
        if (algorithm == null) {
            throw new NullPointerException();
        }
        if (algorithm.equals("CRC32")) {
            return String.valueOf(createCRC32Checksum(in)).getBytes();
        }
        byte[] buffer = new byte[BUFFER_SIZE];
        MessageDigest digest = MessageDigest.getInstance(algorithm);
        int length;
        while ((length = in.read(buffer)) > -1) {
            digest.update(buffer, 0, length);
        }
        in.close();
        return digest.digest();
    }

    public static byte[] createMD5Checksum(InputStream in) throws IOException {
        try {
            return createChecksum(in, "MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] createSHA1Checksum(InputStream in) throws IOException {
        try {
            return createChecksum(in, "SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static long createCRC32Checksum(InputStream in) throws IOException {
        byte[] buf = new byte[BUFFER_SIZE];
        int length = -1;
        CRC32 crc = new CRC32();
        while ((length = in.read(buf)) > 0) {
            crc.update(buf, 0, length);
        }
        long checksum = crc.getValue();
        return checksum;
    }

    public static long createCRC32Checksum(File file) throws IOException {
        InputStream in = new FileInputStream(file);
        try {
            return createCRC32Checksum(in);
        } finally {
            in.close();
        }
    }

    public static long createCRC32Checksum(URL url) throws IOException {
        InputStream in = url.openStream();
        try {
            return createCRC32Checksum(in);
        } finally {
            in.close();
        }
    }
}
