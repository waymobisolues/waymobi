package br.com.servicoemcasa.foto;

import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = { "/*" })
public class Foto extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestUri = req.getRequestURI();
        requestUri = requestUri.substring(getServletContext().getContextPath().length() + 1);
        resp.setContentType("image/png");
        if (!requestUri.endsWith(".png")) {
            requestUri += ".png";
        }
        IOUtils.echo(new FileInputStream("/home/thiago/Projects/servicoemcasa/FotoPrestador/fotos/" + requestUri),
                resp.getOutputStream());
    }

}
