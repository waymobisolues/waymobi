package br.com.servicoemcasa.fragments;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import br.com.servicoemcasa.infra.AddressDescription;
import br.com.servicoemcasa.infra.GoogleServices;
import br.com.servicoemcasa.infra.LocationAware;
import br.com.servicoemcasa.infra.MapService;
import br.com.servicoemcasa.lib.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocationAwareMapFragment extends SupportMapFragment {

    private MapService mapService;
    private GoogleMap map;
    private Marker markerActualLocation;
    private Marker markerSelectedLocation;
    private LatLng latlngActualLocation;
    private LatLng latlngSelectedLocation;
    private AddressDescription addressDescription;

    public LocationAwareMapFragment() {
    }

    @Override
    public void onPause() {
        mapService.close();
        super.onPause();
    }

    @Override
    public void onStop() {
        mapService.close();
        super.onStop();
    }

    @Override
    public void onResume() {
        if (GoogleServices.checkPlayServices(getActivity())) {
            startMapService();
            startListeners();
            startWithLastKnownLocationOrSelected();
        }
        super.onResume();
    }

    public void setManualSelection(AddressDescription addressDescription) {
        this.addressDescription = addressDescription;
    }

    private void startWithLastKnownLocationOrSelected() {
        if (addressDescription != null) {
            selectLocation(addressDescription);
        } else {
            LatLng latlng = mapService.getLastKnownLatLng();
            if (latlng != null) {
                setActualLocation(latlng);
            }
        }
    }

    private void startListeners() {
        map.setOnMapClickListener(new OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                selectLocation(latLng);
            }
        });
        map.setOnMarkerClickListener(new OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTitle().equals(getActivity().getResources().getString(R.string.posicao_atual))) {
                    setAddress(latlngActualLocation);
                } else {
                    setAddress(latlngSelectedLocation);
                }
                return false;
            }
        });
    }

    private void startMapService() {
        map = getMap();
        mapService = new MapService(getActivity());
        mapService.requestUpdates(new ActualLocationListener());
    }

    public void stop() {
        mapService.close();
    }

    public class ActualLocationListener implements LocationListener {
        @Override
        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        }

        @Override
        public void onProviderEnabled(String arg0) {
        }

        @Override
        public void onProviderDisabled(String arg0) {
        }

        @Override
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng local = new LatLng(latitude, longitude);
            setActualLocation(local);
        }
    }

    private void selectLocation(LatLng latlng) {
        addMarkerForSelectedLocation(latlng);
        setAddress(latlng);
    }

    private void addMarkerForSelectedLocation(LatLng latlng) {
        removeSelectedLocationMarker();
        markerSelectedLocation = addMarker(getActivity().getResources().getString(R.string.posicao_selecionada),
                BitmapDescriptorFactory.HUE_AZURE, latlng);
        latlngSelectedLocation = latlng;
    }

    private void removeSelectedLocationMarker() {
        if (markerSelectedLocation != null) {
            markerSelectedLocation.remove();
        }
    }

    public void selectLocation(AddressDescription addressDesciption) {
        addMarkerForSelectedLocation(addressDesciption.latlng);
        animateCameraTo(addressDesciption.latlng);
    }

    private Marker addMarker(String title, float hue, LatLng latlng) {
        Marker marker = map.addMarker(new MarkerOptions().title(title).icon(BitmapDescriptorFactory.defaultMarker(hue))
                .position(latlng));
        return marker;
    }

    private void setActualLocation(LatLng latlng) {
        addMarkerForActualLocation(latlng);
        if (markerSelectedLocation == null) {
            setAddress(latlng);
        }
    }

    private void addMarkerForActualLocation(LatLng latlng) {
        removeActualLocationMarker();
        markerActualLocation = addMarker(getActivity().getResources().getString(R.string.posicao_atual),
                BitmapDescriptorFactory.HUE_ORANGE, latlng);
        latlngActualLocation = latlng;
    }

    private void removeActualLocationMarker() {
        if (markerActualLocation != null) {
            markerActualLocation.remove();
        }
    }

    private void setAddress(LatLng latlng) {
        ((LocationAware) getActivity()).setLatLng(mapService, latlng);
        animateCameraTo(latlng);
    }

    private void animateCameraTo(LatLng latlng) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 15));
    }

}
