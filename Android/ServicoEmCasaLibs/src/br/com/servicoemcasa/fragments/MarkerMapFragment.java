package br.com.servicoemcasa.fragments;

import android.os.Bundle;
import br.com.servicoemcasa.lib.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MarkerMapFragment extends SupportMapFragment {

    private GoogleMap map;
    private int zoom = 16;

    public MarkerMapFragment() {
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    @Override
    public void onResume() {
        Bundle extras = getActivity().getIntent().getExtras();
        double latitude, longitude;
        if (extras == null) {
            latitude = longitude = 0;
        } else {
            latitude = extras.getDouble("latitude", 0);
            longitude = extras.getDouble("longitude", 0);
        }
        startMapService(new LatLng(latitude, longitude));
        super.onResume();
    }

    private void startMapService(LatLng latlng) {
        map = getMap();
        map.getUiSettings().setAllGesturesEnabled(false);
        map.getUiSettings().setZoomControlsEnabled(false);
        selectLocation(latlng);
    }

    public void selectLocation(LatLng latlng) {
        map.clear();
        addMarker(getActivity().getString(R.string.posicao_selecionada), BitmapDescriptorFactory.HUE_AZURE, latlng);
    }

    private Marker addMarker(String title, float hue, LatLng latlng) {
        Marker marker = map.addMarker(new MarkerOptions().title(title).icon(BitmapDescriptorFactory.defaultMarker(hue))
                .position(latlng));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, zoom));
        return marker;
    }

}
