package br.com.servicoemcasa.activities;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import br.com.servicoemcasa.lib.R;
import br.com.servicoemcasa.widget.Cartao;

public abstract class BaseListaActivity<T> extends BaseActivity {

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.lista_simples_layout);
    }

    private interface OpcaoConfig<T> {

        String getEmptyMessage();

        void onClick(T obj);

        boolean isValid(T obj);

    }

    protected interface OpcaoSimplesConfig<T> extends OpcaoConfig<T> {

        String getText(T obj);

    }

    protected interface OpcaoLayoutConfig<T> extends OpcaoConfig<T> {

        View getView(LayoutInflater inflater, T obj);

    }

    protected void setListaOpcoes(final OpcaoConfig<T> config, final List<T> param, final Comparator<T> comparator) {
        ordenaLista(param, comparator);
        adicionaCartoes(config, param, true);
    }

    protected void setListaOpcoes(final OpcaoConfig<T> config, final List<T> param, final Comparator<T> comparator,
            final boolean removeViews) {

        ordenaLista(param, comparator);
        adicionaCartoes(config, param, removeViews);
    }

    private void adicionaCartoes(final OpcaoConfig<T> config, final List<T> param, final boolean removeViews) {
        LayoutInflater inflater = getInflater();
        LinearLayout frame = getFrame(removeViews);
        int c = 0;
        for (final T p : param) {
            if (adicionaCartao(-1, config, inflater, frame, p)) {
                c++;
            }
        }
        if (c == 0) {
            adicionaCartaoMensagem(-1, inflater, frame, config);
        }
    }

    protected void limpaCartoes() {
        LinearLayout frame = findLinearLayout(R.id.lista);
        frame.removeAllViews();
    }

    private LinearLayout getFrame(boolean removeViews) {
        if (removeViews) {
            limpaCartoes();
        }
        return findLinearLayout(R.id.lista);
    }

    private LayoutInflater getInflater() {
        LayoutInflater inflater = LayoutInflater.from(this);
        return inflater;
    }

    protected boolean adicionaCartao(final OpcaoConfig<T> config, final T param) {
        return adicionaCartao(config, param, true);
    }

    protected boolean adicionaCartao(final OpcaoConfig<T> config, final T param, final boolean removeViews) {
        return adicionaCartao(-1, (OpcaoConfig<T>) config, param, removeViews);
    }

    protected boolean adicionaCartao(final int index, final OpcaoConfig<T> config, final T param, final boolean removeViews) {
        LayoutInflater inflater = getInflater();
        LinearLayout frame = getFrame(removeViews);
        return adicionaCartao(index, config, inflater, frame, param);
    }

    @SuppressLint("InflateParams")
    private boolean adicionaCartao(final int index, final OpcaoConfig<T> config, LayoutInflater inflater, LinearLayout frame, final T param) {
        if (!config.isValid(param)) {
            return false;
        }
        View view = inflater.inflate(getLayoutCartao(config), null);
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                config.onClick(param);
            }
        });
        Cartao cartao = findCartao(view, R.id.cartao);
        if (isSimples(config)) {
            findTextView(cartao, R.id.descricao).setText(Html.fromHtml(((OpcaoSimplesConfig<T>) config).getText(param)));
        } else {
            View conteudo = ((OpcaoLayoutConfig<T>) config).getView(inflater, param);
            findFrameLayout(cartao, R.id.conteudo).addView(conteudo);
        }
        if (index > -1) {
            frame.addView(view, index);
        } else {
            frame.addView(view);
        }
        cartao.setMargin(0, 0, 0, 10);
        return true;
    }

    @SuppressLint("InflateParams")
    protected void adicionaCartaoMensagem(final int index, final LayoutInflater inflater, final LinearLayout frame,
            final OpcaoConfig<T> config) {

        View view = inflater.inflate(R.layout.opcao_simples_layout, null);
        Cartao cartao = findCartao(view, R.id.cartao);
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                config.onClick(null);
            }
        });
        findTextView(cartao, R.id.descricao).setText(Html.fromHtml(config.getEmptyMessage()));
        if (index > -1) {
            frame.addView(view, index);
        } else {
            frame.addView(view);
        }
        cartao.setMargin(0, 0, 0, 10);
    }

    private int getLayoutCartao(OpcaoConfig<T> config) {
        return !isSimples(config) ? R.layout.opcao_layout : R.layout.opcao_simples_layout;
    }

    private boolean isSimples(OpcaoConfig<T> config) {
        return (config instanceof OpcaoSimplesConfig);
    }

    private void ordenaLista(List<T> param, Comparator<T> comparator) {
        if (comparator != null) {
            Collections.sort(param, comparator);
        }
    }

}
