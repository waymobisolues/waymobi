package br.com.servicoemcasa.activities;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.servicoemcasa.lib.R;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AdvertisingActivity extends BaseActivity {

    private List<String> adUnitId = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.advertising_progress_dialog_layout);

        for (String s : adUnitId) {
            loadAd(s);
        }
    }

    private void loadAd(String adUnitId) {
        AdView adView = new AdView(this);
        adView.setAdUnitId(adUnitId);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources()
                .getDisplayMetrics());
        adView.setLayoutParams(params);
        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        adView.loadAd(adRequest);
        findLinearLayout(R.id.ad_container).addView(adView);
    }

    protected void addAdUnitId(String adUnitId) {
        this.adUnitId.add(adUnitId);
    }

    protected void setMessage(int message, Object... params) {
        String msg = getString(message);
        if (params != null && params.length > 0) {
            msg = String.format(msg, params);
        }
        ((TextView) findViewById(R.id.texto)).setText(msg);
    }

}
