package br.com.servicoemcasa.activities;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import br.com.servicoemcasa.dialogs.AlertDialogBuilder;
import br.com.servicoemcasa.lib.R;
import br.com.servicoemcasa.widget.Cartao;
import br.com.servicoemcasa.widget.InvalidValueException;
import br.com.servicoemcasa.widget.RegexEditText;

public abstract class BaseActivity extends FragmentActivity {

    private boolean displayHome;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("displayHome", displayHome);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.displayHome = savedInstanceState.getBoolean("displayHome");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        ActionBar actionBar = getActionBar();
        // =-setHideOnContentScroll(actionBar);
        setDisplayHome(actionBar, true);
    }

    // @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    // private void setHideOnContentScroll(ActionBar actionBar) {
    // int sdk = android.os.Build.VERSION.SDK_INT;
    // if (sdk >= android.os.Build.VERSION_CODES.LOLLIPOP) {
    // actionBar.setHideOnContentScrollEnabled(true);
    // }
    // }

    protected void setDisplayHome(ActionBar actionBar, boolean b) {
        if (actionBar != null) {
            displayHome = b;
            actionBar.setDisplayHomeAsUpEnabled(b);
        }
    }

    public void disableDisplayHome() {
        ActionBar actionBar = getActionBar();
        setDisplayHome(actionBar, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            if (displayHome) {
                finish();
            }
            break;
        default:
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private String parseMessage(int mensagem, Object... params) {
        String msg = getString(mensagem);
        if (params != null && params.length > 0) {
            msg = String.format(msg, params);
        }
        return msg;
    }

    protected void showToast(int mensagem, Object... params) {
        showToast(parseMessage(mensagem, params));
    }

    protected void showToast(String mensagem) {
        Toast.makeText(this, mensagem, Toast.LENGTH_SHORT).show();
    }

    protected void showLongToast(int mensagem, Object... params) {
        showLongToast(parseMessage(mensagem, params));
    }

    protected void showLongToast(String mensagem) {
        Toast.makeText(this, mensagem, Toast.LENGTH_LONG).show();
    }

    protected void showAlert(int mensagem, Object... params) {
        showAlert(parseMessage(mensagem, params));
    }

    protected void showAlert(String mensagem) {
        AlertDialogBuilder.show(this, mensagem);
    }

    protected void showAlert(int mensagem, OnClickListener ocl) {
        AlertDialogBuilder.show(this, mensagem, ocl);
    }

    public void showError(int errorCode) {
        showError(errorCode, null);
    }

    public void showError(int errorCode, OnClickListener ocl) {
        int resourceId = 0;
        switch (errorCode) {
        case 401: // NOT_AUTHORIZED
            resourceId = R.string.erro401;
            break;
        case 400: // BAD_REQUEST
            resourceId = R.string.erro400;
            break;
        case 403: // FORBIDDEN
            resourceId = R.string.erro403;
            break;
        case 404: // NOT FOUND
            resourceId = R.string.erro404;
            break;
        case 409: // CONFLICT
            resourceId = R.string.erro409;
            break;
        case 500:
            resourceId = R.string.erro500;
            break;
        default:
            resourceId = R.string.erro_de_conexao_com_servidor_verifique_a_conexao_com_a_internet;
        }
        if (ocl != null) {
            showAlert(resourceId, ocl);
        } else {
            showAlert(resourceId);
        }
    }

    public ScrollView findScrollView(int id) {
        return (ScrollView) findViewById(id);
    }

    public EditText findEditText(int id) {
        return (EditText) findViewById(id);
    }

    public CheckBox findCheckBox(int id) {
        return (CheckBox) findViewById(id);
    }

    public RegexEditText findRegexEditText(int id) {
        return (RegexEditText) findViewById(id);
    }

    public ToggleButton findToggleButton(int id) {
        return (ToggleButton) findViewById(id);
    }

    public TextView findTextView(int id) {
        return (TextView) findViewById(id);
    }

    public TextView findTextView(View context, int id) {
        return (TextView) context.findViewById(id);
    }

    public RatingBar findRatingBar(int id) {
        return (RatingBar) findViewById(id);
    }

    public RatingBar findRatingBar(View context, int id) {
        return (RatingBar) context.findViewById(id);
    }

    public Button findButton(int id) {
        return (Button) findViewById(id);
    }

    public Cartao findCartao(View context, int id) {
        return (Cartao) context.findViewById(id);
    }

    public Cartao findCartao(int id) {
        return (Cartao) findViewById(id);
    }

    public Switch findSwitch(int id) {
        return (Switch) findViewById(id);
    }

    public Switch findSwitch(View context, int id) {
        return (Switch) context.findViewById(id);
    }

    public Spinner findSpinner(int id) {
        return (Spinner) findViewById(id);
    }

    public Spinner findSpinner(View context, int id) {
        return (Spinner) context.findViewById(id);
    }

    public FrameLayout findFrameLayout(int id) {
        return (FrameLayout) findViewById(id);
    }

    public FrameLayout findFrameLayout(View context, int id) {
        return (FrameLayout) context.findViewById(id);
    }

    public LinearLayout findLinearLayout(int id) {
        return (LinearLayout) findViewById(id);
    }

    public ImageView findImageView(int id) {
        return (ImageView) findViewById(id);
    }

    public String getEditTextText(int id) {
        View view = findViewById(id);
        if (view == null) {
            return null;
        }
        if (view instanceof RegexEditText) {
            return ((RegexEditText) view).getValidText().toString();
        } else {
            return ((EditText) view).getText().toString();
        }
    }

    public <T extends Enum<T>> T getEnumValue(String valor, Class<T> enumType) {
        if (valor != null) {
            return Enum.valueOf(enumType, valor);
        }
        return null;
    }

    public <T extends Comparable<T>> void setSpinnerArrayAdapter(Spinner spinner, List<T> array, T selectedValue) {
        Collections.sort(array);
        ArrayAdapter<T> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, array);
        int selectedIndex = 0;
        if (selectedValue != null) {
            selectedIndex = Collections.binarySearch(array, selectedValue);
        }
        setSpinnerAdapter(adapter, spinner, selectedIndex);
    }

    public <T extends Comparable<T>> void setSpinnerArrayAdapter(Spinner spinner, T[] array, T selectedValue) {
        Arrays.sort(array);
        ArrayAdapter<T> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, array);
        int selectedIndex = 0;
        if (selectedValue != null) {
            selectedIndex = Arrays.binarySearch(array, selectedValue);
        }
        setSpinnerAdapter(adapter, spinner, selectedIndex);

    }

    private <T> void setSpinnerAdapter(ArrayAdapter<T> adapter, Spinner spinner, int selectedIndex) {
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (selectedIndex > -1) {
            spinner.setSelection(selectedIndex);
        }
    }

    public class DatePickerClickListener implements android.view.View.OnClickListener, Serializable {

        private static final long serialVersionUID = 1L;
        private Calendar selectedDate = Calendar.getInstance();
        private boolean userSelectedDate = false;

        public DatePickerClickListener() {
            this(null);
        }

        public DatePickerClickListener(Date userSelectedDate) {
            if (userSelectedDate != null) {
                this.userSelectedDate = true;
                selectedDate.setTime(userSelectedDate);
            }
        }

        @Override
        public void onClick(final View view) {
            new DatePickerDialog(view.getContext(), new OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0, int year, int month, int dayOfMonth) {
                    userSelectedDate = true;
                    selectedDate.set(Calendar.YEAR, year);
                    selectedDate.set(Calendar.MONTH, month);
                    selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    ((EditText) view).setText(DateFormat.getDateFormat(view.getContext())
                            .format(selectedDate.getTime()));
                }
            }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH),
                    selectedDate.get(Calendar.DAY_OF_MONTH)).show();
        }

        public Date getDate() {
            return userSelectedDate ? selectedDate.getTime() : null;
        }

        public Date getValidDate(Context context, int message) {
            Date date = getDate();
            if (date == null) {
                throw new InvalidValueException(context.getString(message));
            }
            return date;
        }

    }

    public class DateTimePickerClickListener extends AlertDialog implements android.view.View.OnClickListener,
            Serializable, OnClickListener {

        private static final long serialVersionUID = 1L;
        private Calendar selectedDate = Calendar.getInstance();
        private boolean userSelectedDate = false;
        private View view;
        private NumberPicker hora;
        private NumberPicker minuto;
        private CalendarView calendarView;
        private EditText editText;

        public DateTimePickerClickListener(Context context) {
            this(context, null);
        }

        @SuppressLint("InflateParams")
        public DateTimePickerClickListener(Context context, Date userSelectedDate) {
            super(context);
            if (userSelectedDate != null) {
                this.userSelectedDate = true;
                selectedDate.setTime(userSelectedDate);
            }
            final LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.date_time_picker_layout, null);
            calendarView = (CalendarView) view.findViewById(R.id.calendario);
            calendarView.setDate(selectedDate.getTimeInMillis());
            configureNumberPickers();
            setView(view);
            setButton(BUTTON_POSITIVE, context.getString(R.string.confirmar), this);
            setButton(BUTTON_NEGATIVE, context.getString(R.string.cancelar), this);
        }

        private void configureNumberPickers() {
            hora = (NumberPicker) view.findViewById(R.id.hora);
            hora.setMinValue(0);
            hora.setMaxValue(23);
            hora.setWrapSelectorWheel(true);
            hora.setDisplayedValues(getResources().getStringArray(R.array.arr_hora));
            hora.setValue(selectedDate.get(Calendar.HOUR_OF_DAY) - 1);
            minuto = (NumberPicker) view.findViewById(R.id.minuto);
            minuto.setMinValue(0);
            minuto.setMaxValue(1);
            minuto.setWrapSelectorWheel(true);
            minuto.setDisplayedValues(getResources().getStringArray(R.array.arr_minuto));
            minuto.setValue(selectedDate.get(Calendar.MINUTE) >= 30 ? 1 : 0);
        }

        @Override
        public void onClick(final View view) {
            this.editText = (EditText) view;
            show();
        }

        public Date getDate() {
            return userSelectedDate ? selectedDate.getTime() : null;
        }

        public Date getValidDate(Context context, int message) {
            Date date = getDate();
            if (date == null) {
                throw new InvalidValueException(context.getString(message));
            }
            return date;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (which == Dialog.BUTTON_POSITIVE) {
                userSelectedDate = true;
                selectedDate.setTime(new Date(calendarView.getDate()));
                selectedDate.set(Calendar.HOUR_OF_DAY, hora.getValue() + 1);
                selectedDate.set(Calendar.MINUTE, minuto.getValue() == 0 ? 0 : 30);
                editText.setText(DateFormat.getDateFormat(editText.getContext()).format(selectedDate.getTime()) + " "
                        + DateFormat.getTimeFormat(editText.getContext()).format(selectedDate.getTime()));
            }
        }
    }

}
