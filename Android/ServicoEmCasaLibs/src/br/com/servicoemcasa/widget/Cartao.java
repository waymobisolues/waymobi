package br.com.servicoemcasa.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import br.com.servicoemcasa.lib.R;

public class Cartao extends LinearLayout {

    private static final int PADDING = 15;

    public Cartao(Context context) {
        super(context);
        preparaCartao();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Cartao(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        preparaCartao();
    }

    public Cartao(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        preparaCartao();
    }

    public Cartao(Context context, AttributeSet attrs) {
        super(context, attrs);
        preparaCartao();
    }

    private void preparaCartao() {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            setBackground();
        } else {
            setBackgroundDrawable();
        }
        int dp = pxToDip(PADDING);
        setPadding(dp, dp, dp, dp);
    }

    private int pxToDip(int px) {
        Resources res = getResources();
        int dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, res.getDisplayMetrics());
        return dp;
    }

    public void setMargin(int marginLeft, int marginTop, int marginRight, int marginBottom) {
        android.view.ViewGroup.LayoutParams lp = getLayoutParams();
        if (lp instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) lp;
            marginLeft = marginLeft < 0 ? layoutParams.leftMargin : marginLeft;
            marginTop = marginTop < 0 ? layoutParams.topMargin : marginTop;
            marginRight = marginRight < 0 ? layoutParams.rightMargin : marginRight;
            marginBottom = marginBottom < 0 ? layoutParams.bottomMargin : marginBottom;
            layoutParams.setMargins(pxToDip(marginLeft), pxToDip(marginTop), pxToDip(marginRight),
                    pxToDip(marginBottom));
        }
    }

    public void setMarginTop(int margin) {
        setMargin(-1, margin, -1, -1);
    }

    public void setMarginBottom(int margin) {
        setMargin(-1, -1, -1, margin);
    }

    public void setOnClickListener(final OnClickListener onClickListener) {
        setClickable(true);
        super.setOnClickListener(onClickListener);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setBackground() {
        super.setBackground(getResources().getDrawable(R.drawable.cartao));
    }

    @SuppressWarnings("deprecation")
    private void setBackgroundDrawable() {
        super.setBackgroundDrawable(getResources().getDrawable(R.drawable.cartao));
    }

}
