package br.com.servicoemcasa.widget;

import java.util.regex.Pattern;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;
import br.com.servicoemcasa.lib.R;
import br.com.servicoemcasa.util.StringUtils;

public class RegexEditText extends EditText {

    private final String invalidMessage;
    private final Pattern validationPattern;

    public RegexEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.validationPattern = Pattern.compile("");
        this.invalidMessage = context.getString(R.string.valor_invalido);
    }

    public RegexEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Pattern validationPattern = null;
        String message = context.getString(R.string.valor_invalido);
        if (attrs != null) {
            TypedArray a = null;
            try {
                a = getContext().obtainStyledAttributes(attrs, R.styleable.RegexEditText);
                message = a.getString(R.styleable.RegexEditText_invalid_message);
                String pattern = a.getString(R.styleable.RegexEditText_validation_pattern);
                if (!StringUtils.isNullOrEmpty(pattern)) {
                    validationPattern = Pattern.compile(pattern);
                } else {
                    validationPattern = Pattern.compile("");
                }
            } finally {
                if (a != null) {
                    a.recycle();
                }
            }
        }
        this.validationPattern = validationPattern;
        this.invalidMessage = message;
    }

    public RegexEditText(Context context) {
        super(context);
        this.validationPattern = Pattern.compile("");
        this.invalidMessage = context.getString(R.string.valor_invalido);
    }

    public Editable getValidText() {
        Editable result = super.getText();
        if (validationPattern.matcher(result).matches()) {
            return result;
        } else {
            throw new InvalidValueException(invalidMessage);
        }
    }

}
