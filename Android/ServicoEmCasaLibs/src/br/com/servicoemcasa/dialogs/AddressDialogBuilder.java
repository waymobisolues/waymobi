package br.com.servicoemcasa.dialogs;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ArrayAdapter;
import br.com.servicoemcasa.infra.AddressDescription;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.lib.R;

public class AddressDialogBuilder {

    public static void show(final Activity activity, final List<AddressDescription> addressDescriptions,
            final Callback<AddressDescription> callback) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final ArrayAdapter<AddressDescription> adapter = new ArrayAdapter<AddressDescription>(activity,
                android.R.layout.simple_list_item_1, addressDescriptions);
        builder.setTitle(R.string.selecione_o_endereco).setAdapter(adapter, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callback.callback(addressDescriptions.get(which));
            }
        });
        builder.create().show();
    }

}
