package br.com.servicoemcasa.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import br.com.servicoemcasa.lib.R;

public class AlertDialogBuilder {

    public static void show(final Context context, final int mensagem) {
        show(context, R.string.alerta, getMensagem(context, mensagem), null);
    }

    public static void show(final Context context, final int titulo, final int mensagem, final OnClickListener callback) {
        show(context, titulo, getMensagem(context, mensagem), callback);
    }

    public static void show(final Context context, final int mensagem, final OnClickListener callback) {
        show(context, R.string.alerta, getMensagem(context, mensagem), callback);
    }

    public static void show(final Context context, final int titulo, final int mensagem) {
        show(context, titulo, getMensagem(context, mensagem), null);
    }

    private static String getMensagem(final Context context, final int mensagem) {
        return context.getApplicationContext().getString(mensagem);
    }

    public static void show(final Context context, final String mensagem) {
        show(context, R.string.alerta, mensagem, null);
    }

    public static void show(final Context context, final int titulo, final String mensagem) {
        show(context, titulo, mensagem, null);
    }

    public static void show(final Context context, final int titulo, final String mensagem,
            final OnClickListener callback) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo).setMessage(mensagem).setPositiveButton(android.R.string.ok, callback);
        builder.create().show();
    }

    public static void showOkCancel(final Context context, final int mensagem) {
        showOkCancel(context, R.string.alerta, getMensagem(context, mensagem), null, null);
    }

    public static void showOkCancel(final Context context, final int titulo, final int mensagem,
            final OnClickListener callbackOk, final OnClickListener callbackCancel) {

        showOkCancel(context, titulo, getMensagem(context, mensagem), callbackOk, callbackCancel);
    }

    public static void showOkCancel(final Context context, final int mensagem, final OnClickListener callbackOk,
            final OnClickListener callbackCancel) {

        showOkCancel(context, R.string.alerta, getMensagem(context, mensagem), callbackOk, callbackCancel);
    }

    public static void showOkCancel(final Context context, final int titulo, final int mensagem) {
        showOkCancel(context, titulo, getMensagem(context, mensagem), null, null);
    }

    public static void showOkCancel(final Context context, final String mensagem) {
        showOkCancel(context, R.string.alerta, mensagem, null, null);
    }

    public static void showOkCancel(final Context context, final int titulo, final String mensagem) {
        showOkCancel(context, titulo, mensagem, null, null);
    }

    public static void showOkCancel(final Context context, final int titulo, final String mensagem,
            final OnClickListener callbackOk, final OnClickListener callbackCancel) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo).setMessage(mensagem).setPositiveButton(android.R.string.ok, callbackOk)
                .setNegativeButton(android.R.string.cancel, callbackCancel);
        builder.create().show();
    }

}
