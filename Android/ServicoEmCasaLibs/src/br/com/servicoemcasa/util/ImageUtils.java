package br.com.servicoemcasa.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import br.com.servicoemcasa.infra.Callback;

public class ImageUtils {

    private static final String TAG = ImageUtils.class.getSimpleName();

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static void getImageFromURL(URL src, final Callback<Bitmap> callback) {
        new AsyncTask<URL, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(URL... src) {
                HttpURLConnection connection = null;
                try {
                    connection = (HttpURLConnection) src[0].openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                    return bitmap;
                } catch (IOException e) {
                    Log.e(TAG, "Erro carregando a imagem: " + e.toString());
                    return null;
                } finally {
                    try {
                        connection.disconnect();
                    } catch (Exception e) {
                        Log.e(TAG, "Erro no disconnect");
                    }
                }
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                if (result != null) {
                    callback.callback(result);
                }
            }

        }.execute(src);

    }

    public static void saveImageToFile(Bitmap bitmap, File file) throws IOException {
        bitmap.compress(CompressFormat.PNG, 100, new FileOutputStream(file));
    }

    public static Bitmap getImageFromFile(File file) {
        Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
        switch (getImageOrientation(file)) {
        case ExifInterface.ORIENTATION_ROTATE_90:
            return rotateBitmap(bm, 90);
        case ExifInterface.ORIENTATION_ROTATE_180:
            return rotateBitmap(bm, 180);
        case ExifInterface.ORIENTATION_ROTATE_270:
            return rotateBitmap(bm, 270);
        }
        return bm;
    }

    public static Bitmap getScaledImageFromFile(File file, int width, int height) {
        Bitmap bm = getImageFromFile(file);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bm, width, height, true);
        return scaledBitmap;
    }

    public static Bitmap getScaledImageFromFile(File file) {
        return getScaledImageFromFile(file, 150, 150);
    }

    public static int getImageOrientation(File file) {
        try {
            ExifInterface ei = new ExifInterface(file.getAbsolutePath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            return orientation;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Intent createImageCropIntent(File bitmapFile) {
        return createImageCropIntent(bitmapFile, 300, 300);
    }

    public static Intent createImageCropIntent(File bitmapFile, int width, int height) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(Uri.fromFile(bitmapFile), "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", width);
        cropIntent.putExtra("outputY", height);
        cropIntent.putExtra("return-data", true);
        return cropIntent;
    }

}
