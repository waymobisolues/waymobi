package br.com.servicoemcasa.util;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.Toast;
import br.com.servicoemcasa.dialogs.AddressDialogBuilder;
import br.com.servicoemcasa.infra.AddressDescription;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.MapService;
import br.com.servicoemcasa.lib.R;

public class MapUtils {

    public static void getAddressDescription(final Activity activity, final String addressLine,
            final Callback<AddressDescription> addressCallback) {

        getAddressDescription(activity, addressLine, false, addressCallback);
    }

    public static void getAddressDescription(final Activity activity, final String addressLine,
            final boolean alwaysShowDialog, final Callback<AddressDescription> addressCallback) {

        MapService mapService = new MapService(activity);
        try {
            final ProgressDialog progressDialog = ActivityUtils
                    .showProgressDialog(activity, R.string.buscando_endereco);
            mapService.getLatLngFromAddressDescription(addressLine, new Callback<List<AddressDescription>>() {
                @Override
                public void callback(List<AddressDescription> param) {
                    ActivityUtils.hideProgressDialog(progressDialog);
                    if (param != null && param.size() > 0) {
                        if (alwaysShowDialog || param.size() > 1) {
                            AddressDialogBuilder.show(activity, param, new Callback<AddressDescription>() {
                                @Override
                                public void callback(AddressDescription param) {
                                    addressCallback.callback(param);
                                }
                            });
                        } else {
                            AddressDescription description = param.get(0);
                            addressCallback.callback(description);
                        }
                    } else {
                        Toast.makeText(activity, R.string.endereco_nao_encontrado, Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } finally {
            mapService.close();
        }
    }

}
