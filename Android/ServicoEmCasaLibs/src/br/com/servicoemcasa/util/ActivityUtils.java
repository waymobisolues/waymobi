package br.com.servicoemcasa.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.view.inputmethod.InputMethodManager;

public class ActivityUtils {

    private static boolean dialog = false;

    public static void hideKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static ProgressDialog showProgressDialog(Context context, int mensagem, Object... params) {
        if (dialog) {
            return null;
        }
        String msg = context.getString(mensagem);
        if (params != null && params.length > 0) {
            msg = String.format(msg, params);
        }
        final ProgressDialog progress = ProgressDialog.show(context, "Aguarde", msg, true, false);
        progress.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialog = false;
            }
        });
        dialog = true;
        return progress;
    }

    public static void hideProgressDialog(final ProgressDialog progressDialog) {
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
        }
    }

}
