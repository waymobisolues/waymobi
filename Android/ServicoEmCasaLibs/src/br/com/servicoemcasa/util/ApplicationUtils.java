package br.com.servicoemcasa.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;

public class ApplicationUtils {

    public static int getAppVersion(final Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static void startMarket(final Context context) {
        final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static boolean isDebuggable(Context context) {
        return (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
    }

    public static String getMetaDataAsString(final Context context, final String key) {
        Bundle bundle = getApplicationMetaData(context);
        if (isDebuggable(context) && bundle.containsKey(key + ".debug")) {
            return bundle.getString(key + ".debug");
        } else {
            return bundle.getString(key);
        }
    }

    public static int getMetaDataAsInt(final Context context, final String key) {
        Bundle bundle = getApplicationMetaData(context);
        if (isDebuggable(context) && bundle.containsKey(key + ".debug")) {
            return bundle.getInt(key + ".debug");
        } else {
            return bundle.getInt(key);
        }
    }

    public static long getMetaDataAsLong(final Context context, final String key) {
        String value = getMetaDataAsString(context, key);
        return Long.parseLong(value.substring(1));
    }

    private static Bundle getApplicationMetaData(final Context context) {
        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            return bundle;
        } catch (NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
