package br.com.servicoemcasa.util;

import java.text.DateFormat;
import java.util.Date;

public class StringUtils {

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.trim().length() == 0;
    }

    public static boolean isValidEmail(String s) {
        return s.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    public static String toString(Object s, String padrao) {
        return s == null ? padrao : s.toString();
    }

    public static String toString(Date date) {
        if (date != null) {
            return DateFormat.getDateInstance().format(date);
        } else {
            return "";
        }
    }
    
    public static String toDateTimeString(Date date) {
        if (date != null) {
            return DateFormat.getDateTimeInstance().format(date);
        } else {
            return "";
        }
    }

}
