package br.com.servicoemcasa;

import android.app.Application;
import br.com.servicoemcasa.activities.BaseActivity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.Versao;

public class BaseApplication extends Application {

    private boolean versionChecked = false;

    public void checkVersion(final BaseActivity activity, final Callback<SimpleCallback<Void>> callback) {
        if (!versionChecked) {
            versionChecked = true;
            final Versao versao = new Versao(activity);
            versao.validaVersao(callback);
        } else {
            callback.callback(new SimpleCallback<Void>(0, null));
        }
    }

}
