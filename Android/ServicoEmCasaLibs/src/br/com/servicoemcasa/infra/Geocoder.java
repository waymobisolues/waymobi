package br.com.servicoemcasa.infra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.com.servicoemcasa.infra.WebClient.WebResponse;

import com.google.android.gms.maps.model.LatLng;

class Geocoder {

    private static final String TAG = Geocoder.class.getCanonicalName();;
    private static final Cache<LatLng, AddressDescription> latlngCache = new SimpleCache<LatLng, AddressDescription>();
    private static final String fromLocationUrl = "http://maps.googleapis.com/maps/api/geocode";
    private static final String STATUS_OK = "OK";
    private WebClient webClient;

    public Geocoder(Context context) {
        this.webClient = WebClient.newInstance(context, fromLocationUrl, false);
    }

    public void getFromLocation(final LatLng latlng, final Callback<AddressDescription> callback) {
        if (latlngCache.containsKey(latlng)) {
            Log.i(TAG, "Cache hit");
            callback.callback(latlngCache.get(latlng));
        } else {
            Log.i(TAG, "Cache miss");
            webClient.get(
                    "/json",
                    String.format("sensor=true&latlng=%s,%s&language=%s", String.valueOf(latlng.latitude),
                            String.valueOf(latlng.longitude), Locale.getDefault().getLanguage()),
                    new Callback<WebClient.WebResponse>() {
                        @Override
                        public void callback(WebResponse param) {
                            AddressDescription addressDescription = null;
                            if (param.responseCode == HttpStatus.SC_OK) {
                                List<AddressDescription> addresses = listAddressesAndAddToCache(param.responseBody);
                                if (addresses != null && addresses.size() > 0) {
                                    addressDescription = addresses.get(0);
                                }
                            }
                            callback.callback(addressDescription);
                        }
                    });
        }
    }

    public void getFromLocation(final double latitude, final double longitude,
            final Callback<AddressDescription> callback) {

        getFromLocation(new LatLng(latitude, longitude), callback);
    }

    public void getFromLocationName(final String locationName, final Callback<List<AddressDescription>> callback) {
        webClient.get(
                "/json",
                String.format("sensor=false&language=%s&address=%s", Locale.getDefault().getLanguage(),
                        webClient.encode(locationName)), new Callback<WebClient.WebResponse>() {
                    @Override
                    public void callback(WebResponse param) {
                        if (param.responseCode == HttpStatus.SC_OK) {
                            List<AddressDescription> addresses = listAddressesAndAddToCache(param.responseBody);
                            callback.callback(addresses);
                        } else {
                            callback.callback(Collections.<AddressDescription>emptyList());
                        }
                    }
                });
    }

    private List<AddressDescription> listAddressesAndAddToCache(String json) {
        try {
            final JSONObject o = new JSONObject(json);
            final String status = o.getString("status");
            if (status.equals(STATUS_OK)) {
                final List<AddressDescription> result = new ArrayList<AddressDescription>();
                final JSONArray a = o.getJSONArray("results");
                for (int i = 0; i < a.length(); i++) {
                    final JSONObject item = a.getJSONObject(i);
                    final JSONObject location = item.getJSONObject("geometry").getJSONObject("location");
                    final AddressDescription ad = new AddressDescription(new LatLng(location.getDouble("lat"),
                            location.getDouble("lng")), item.getString("formatted_address"));
                    latlngCache.put(ad.latlng, ad);
                    result.add(ad);
                }
                return result;
            } else {
                return Collections.<AddressDescription>emptyList();
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

}
