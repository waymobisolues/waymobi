package br.com.servicoemcasa.infra;

public class SimpleCallback<T> {

    public final int errorCode;
    public final String errorDescription;
    public final T object;

    public SimpleCallback(int errorCode, String errorDescription) {
        this(errorCode, errorDescription, null);
    }

    public SimpleCallback(T object) {
        this(0, null, object);
    }

    public SimpleCallback(int errorCode, String errorDescription, T object) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
        this.object = object;
    }

}
