package br.com.servicoemcasa.infra;

public abstract class GcmException extends Exception {

    private static final long serialVersionUID = 1L;
    
    protected GcmException() {
        super();
    }
    
    protected GcmException(Exception origin) {
        super(origin);
    }
    
    protected GcmException(String message) {
        super(message);
    }
    
}
