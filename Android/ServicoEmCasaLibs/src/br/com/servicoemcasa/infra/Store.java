package br.com.servicoemcasa.infra;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Store {

    private SharedPreferences sharedPreferences;

    public Store(Context context, Class<?> storeClass) {
        context = context.getApplicationContext() == null ? context : context.getApplicationContext();
        sharedPreferences = context.getSharedPreferences(storeClass.getSimpleName(), Context.MODE_PRIVATE);
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public Editor getEditor() {
        return sharedPreferences.edit();
    }

}
