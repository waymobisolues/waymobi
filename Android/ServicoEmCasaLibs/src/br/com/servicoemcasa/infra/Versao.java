package br.com.servicoemcasa.infra;

import java.util.List;

import org.apache.http.HttpStatus;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import br.com.servicoemcasa.dialogs.AlertDialogBuilder;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.lib.R;
import br.com.servicoemcasa.util.ApplicationUtils;
import br.com.servicoemcasa.util.JsonParser;

import com.google.gson.internal.LinkedTreeMap;

public class Versao {

    private Context context;

    public Versao(final Context context) {
        this.context = context;
    }

    public void validaVersao(final Callback<SimpleCallback<Void>> callback) {
        final String app = context.getPackageName();
        WebClient webClient = WebClient.newInstance(context);
        webClient.get("data/versao.json", new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    parseVersao(param.responseBody, app, callback);
                } else {
                    callback.callback(new SimpleCallback<Void>(param.responseCode, param.getErrorDescription()));
                }
            }
        });
    }

    private void parseVersao(final String json, final String app, final Callback<SimpleCallback<Void>> callback) {
        LinkedTreeMap<?, ?> map = JsonParser.toMap(json);
        LinkedTreeMap<?, ?> appMap = (LinkedTreeMap<?, ?>) map.get(app);
        if (!verificaObsoletas(appMap, callback)) {
            verificaVersao(appMap, callback);
        }
    }

    private void verificaVersao(LinkedTreeMap<?, ?> appMap, final Callback<SimpleCallback<Void>> callback) {
        Store store = new Store(context, this.getClass());
        int ultimaVersaoVerificada = store.getInt("versao");
        int versaoInstalada = ApplicationUtils.getAppVersion(context);
        if (ultimaVersaoVerificada != versaoInstalada) {
            Integer versaoAtual = ((Double) appMap.get("versao")).intValue();
            if (versaoAtual > versaoInstalada) {
                AlertDialogBuilder.showOkCancel(context, R.string.existe_uma_nova_versao_da_aplicacao,
                        R.string.deseja_atualizar_agora, new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ApplicationUtils.startMarket(context);
                            }
                        }, new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                callback.callback(new SimpleCallback<Void>(0, null));
                            }
                        });
            } else {
                callback.callback(new SimpleCallback<Void>(0, null));
            }
            store.getEditor().putInt("versao", versaoInstalada).commit();
        } else {
            callback.callback(new SimpleCallback<Void>(0, null));
        }
    }

    private boolean verificaObsoletas(LinkedTreeMap<?, ?> appMap, final Callback<SimpleCallback<Void>> callback) {
        @SuppressWarnings("unchecked")
        List<Double> obsoletas = (List<Double>) appMap.get("obsoletas");
        int versao = ApplicationUtils.getAppVersion(context);
        for (Double versaoObsoleta : obsoletas) {
            if (versao == versaoObsoleta.intValue()) {
                AlertDialogBuilder.show(context, R.string.existe_uma_nova_versao_da_aplicacao,
                        R.string.e_necessario_atualizar_para_continuar, new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ApplicationUtils.startMarket(context);
                            }
                        });
                return true;
            }
        }
        return false;
    }
}