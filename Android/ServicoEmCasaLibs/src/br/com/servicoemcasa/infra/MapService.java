package br.com.servicoemcasa.infra;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;

public class MapService implements Closeable {

    private final LocationManager locationManager;
    private final List<LocationListener> listeners = new ArrayList<LocationListener>();
    private MapServiceLocationListener mapServiceLocationListener;
    private Location lastKnownLocation;
    private Geocoder geocoder;

    public MapService(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        lastKnownLocation = getLastKnownLocation();
        this.geocoder = new Geocoder(context);
    }

    public LatLng getLastKnownLatLng() {
        return lastKnownLocation == null ? null : getLatLng(lastKnownLocation);
    }

    private Location getLastKnownLocation() {
        Location lastKnownLocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Location lastKnownLocationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (lastKnownLocationGps == null && lastKnownLocationNetwork == null) {
            return null;
        }
        if (lastKnownLocationGps != null && lastKnownLocationNetwork != null) {
            if (isBetterLocation(lastKnownLocationGps, lastKnownLocationNetwork)) {
                return lastKnownLocationGps;
            } else {
                return lastKnownLocationNetwork;
            }
        } else {
            if (lastKnownLocationGps != null) {
                return lastKnownLocationGps;
            } else {
                return lastKnownLocationNetwork;
            }
        }
    }

    /**
     * Determines whether one Location reading is better than the current
     * Location fix
     * 
     * @param location
     *            The new Location that you want to evaluate
     * @param currentBestLocation
     *            The current Location fix, to which you want to compare the new
     *            one
     */
    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isNewer = timeDelta > 0;

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and
        // accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private LatLng getLatLng(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public void getLatLngFromAddressDescription(String addressDescription, Callback<List<AddressDescription>> callback) {
        geocoder.getFromLocationName(addressDescription, callback);
    }

    public void getAddressFromLatLng(LatLng latlng, Callback<AddressDescription> callback) {
        geocoder.getFromLocation(latlng, callback);
    }

    private class MapServiceLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            if (isBetterLocation(location, lastKnownLocation)) {
                lastKnownLocation = location;
                for (LocationListener listener : listeners) {
                    listener.onLocationChanged(location);
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            for (LocationListener listener : listeners) {
                listener.onProviderDisabled(provider);
            }
        }

        @Override
        public void onProviderEnabled(String provider) {
            for (LocationListener listener : listeners) {
                listener.onProviderEnabled(provider);
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle b) {
            for (LocationListener listener : listeners) {
                listener.onStatusChanged(s, i, b);
            }
        }

    }

    public void requestUpdates(LocationListener listener) {
        listeners.add(listener);
        if (mapServiceLocationListener == null) {
            mapServiceLocationListener = new MapServiceLocationListener();
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 20, mapServiceLocationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60000, 10,
                    mapServiceLocationListener);
        }
    }

    public void close() {
        if (mapServiceLocationListener != null) {
            locationManager.removeUpdates(mapServiceLocationListener);
            mapServiceLocationListener = null;
        }
    }

}
