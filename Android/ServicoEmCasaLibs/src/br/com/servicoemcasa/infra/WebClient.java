package br.com.servicoemcasa.infra;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.Header;
import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import br.com.servicoemcasa.lib.R;
import br.com.servicoemcasa.util.ActivityUtils;
import br.com.servicoemcasa.util.ApplicationUtils;
import br.com.servicoemcasa.util.JsonParser;

public class WebClient {

    private static final int DATA_TIMEOUT = 10000;
    private static final int CONNECTION_TIMEOUT = 3000;
    private static final String TAG = WebClient.class.getSimpleName();
    private static final String WEBCLIENT_DEFAULT_URL = "br.com.servicoemcasa.infra.WebClient.";
    private DefaultHttpClient client;
    private String serverURL;
    private Context context;
    private boolean showWaitDialog;

    public static WebClient newInstance(Context context) {
        return newInstance(context, true);
    }

    public static WebClient newInstance(Context context, boolean showWaitDialog) {
        return new WebClient(context, showWaitDialog);
    }

    public static WebClient newInstance(Context context, String serverURL) {
        return newInstance(context, serverURL, true);
    }

    public static WebClient newInstance(Context context, String serverURL, boolean showWaitDialog) {
        return new WebClient(context, serverURL, showWaitDialog);
    }

    private WebClient(Context context, String serverURL, boolean showWaitDialog) {
        this.context = context;
        this.showWaitDialog = showWaitDialog;
        this.serverURL = serverURL;
        createDefaultHttpClient();
    }

    private WebClient(Context context, boolean showWaitDialog) {
        this.context = context;
        this.showWaitDialog = showWaitDialog;
        this.serverURL = getURL(context, "default");
        createDefaultHttpClient();
    }

    private void createDefaultHttpClient() {
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParameters, DATA_TIMEOUT);
        client = new DefaultHttpClient(httpParameters);
    }

    public static String getURL(Context context, String key) {
        return ApplicationUtils.getMetaDataAsString(context, WEBCLIENT_DEFAULT_URL + key);
    }

    public class WebResponse {
        public final int responseCode;
        public final String responseDescription;
        public final String location;
        public final String responseBody;
        public final boolean hasValidResponseFormat;

        public WebResponse(final int responseCode, final String responseDescription, final String location,
                final String responseBody, final boolean hasValidResponseFormat) {

            this.responseCode = responseCode;
            this.responseDescription = responseDescription;
            this.location = location;
            this.responseBody = responseBody;
            this.hasValidResponseFormat = hasValidResponseFormat;
        }

        public String getErrorDescription() {
            return hasValidResponseFormat ? responseBody : null;
        }

        @Override
        public String toString() {
            return "WebResponse [responseCode=" + responseCode + ", responseDescription=" + responseDescription
                    + ", hasValidResponseFormat=" + hasValidResponseFormat + "]";
        }
    }

    private class WebClientTask extends AsyncTask<HttpMessage, Void, WebResponse> {

        private Callback<WebResponse> callback;
        private ProgressDialog showProgressDialog;

        public WebClientTask(Callback<WebResponse> callback) {
            this.callback = callback;
        }

        @Override
        protected WebResponse doInBackground(HttpMessage... params) {
            try {
                HttpMessage message = params[0];
                message.setHeader("Accept", "application/json");
                if (!message.containsHeader("Content-type")) {
                    message.setHeader("Content-type", "application/json");
                }
                if (message instanceof HttpPost) {
                    return backgroundPost((HttpPost) message);
                } else if (message instanceof HttpDelete) {
                    return backgroundDelete((HttpDelete) message);
                } else if (message instanceof HttpPut) {
                    return backgroundPut((HttpPut) message);
                } else {
                    return backgroundGet((HttpGet) message);
                }
            } catch (Exception e) {
                Log.e(TAG, Log.getStackTraceString(e));
                return new WebResponse(-1, e.toString(), null, e.getMessage() != null ? e.getMessage() : e.toString(),
                        false);
            }
        }

        private WebResponse backgroundGet(HttpGet httpGet) throws ClientProtocolException, IOException {
            HttpResponse response = client.execute(httpGet);
            return responseToWebResponse(response);
        }

        private WebResponse backgroundPost(HttpPost httpPost) throws ClientProtocolException, IOException {
            HttpResponse response = client.execute(httpPost);
            return responseToWebResponse(response);
        }

        private WebResponse backgroundPut(HttpPut httpPut) throws ClientProtocolException, IOException {
            HttpResponse response = client.execute(httpPut);
            return responseToWebResponse(response);
        }

        private WebResponse backgroundDelete(HttpDelete httpDelete) throws ClientProtocolException, IOException {
            HttpResponse response = client.execute(httpDelete);
            return responseToWebResponse(response);
        }

        private WebResponse responseToWebResponse(HttpResponse response) throws IOException {
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK
                    && response.getStatusLine().getStatusCode() != HttpStatus.SC_MOVED_TEMPORARILY) {

                Log.e(TAG, "Service Response: " + response.getStatusLine().toString());
            }
            Header[] locations = response.getHeaders("Location");
            String location = locations != null && locations.length > 0 ? locations[0].getValue() : null;
            Header[] contentType = response.getHeaders("Content-Type");
            boolean hasValidResponseFormat = contentType.length > 0
                    && contentType[0].getValue().contains("application/json");
            String body = EntityUtils.toString(response.getEntity());
            return new WebResponse(response.getStatusLine().getStatusCode(),
                    response.getStatusLine().getReasonPhrase(), location, body, hasValidResponseFormat);
        }

        @Override
        protected void onPreExecute() {
            if (showWaitDialog) {
                showProgressDialog = ActivityUtils.showProgressDialog(context, R.string.aguarde);
            }
        }

        @Override
        protected void onPostExecute(WebResponse result) {
            try {
                if (showWaitDialog) {
                    ActivityUtils.hideProgressDialog(showProgressDialog);
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
            callback.callback(result);
            super.onPostExecute(result);
        }

    }

    public void post(String path, Object param, Callback<WebResponse> callback) {
        post(path, JsonParser.toJson(param), "application/json", callback);
    }

    public void post(String path, String param, Callback<WebResponse> callback) {
        post(path, param, "application/json", callback);
    }

    public void postForm(String path, String param, Callback<WebResponse> callback) {
        post(path, param, "application/x-www-form-urlencoded", callback);
    }

    private void post(String path, String param, String contentType, Callback<WebResponse> callback) {
        try {
            HttpPost post = new HttpPost(checkPath(path));
            StringEntity entity = new StringEntity(param);
            post.setEntity(entity);
            post.setHeader("Content-type", contentType);
            new WebClientTask(callback).execute(post);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void put(String path, Object param, Callback<WebResponse> callback) {
        put(path, JsonParser.toJson(param), "application/json", callback);
    }

    public void put(String path, String param, Callback<WebResponse> callback) {
        put(path, param, "application/json", callback);
    }

    public void putForm(String path, String param, Callback<WebResponse> callback) {
        post(path, param, "application/x-www-form-urlencoded", callback);
    }

    private void put(String path, String param, String contentType, Callback<WebResponse> callback) {
        try {
            HttpPut put = new HttpPut(checkPath(path));
            StringEntity entity;
            entity = new StringEntity(param);
            put.setEntity(entity);
            put.setHeader("Content-type", contentType);
            new WebClientTask(callback).execute(put);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void get(String path, Callback<WebResponse> callback) {
        get(path, null, callback);
    }

    public void get(String path, String param, Callback<WebResponse> callback) {
        String url = checkPath(path);
        if (param != null) {
            url += "?" + param;
        }
        HttpGet get = new HttpGet(url);
        new WebClientTask(callback).execute(get);
    }

    public void delete(String path, Callback<WebResponse> callback) {
        String url = checkPath(path);
        HttpDelete delete = new HttpDelete(url);
        new WebClientTask(callback).execute(delete);
    }

    private String checkPath(String path) {
        if (path == null) {
            return serverURL;
        }
        if (path.startsWith("http")) {
            return path;
        }
        if (!path.startsWith("/")) {
            return serverURL + "/" + path;
        }
        String fullPath = serverURL + path;
        Log.i(TAG, "URL: " + fullPath);
        return fullPath;
    }

    public Object encode(String locationName) {
        try {
            return URLEncoder.encode(locationName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, Log.getStackTraceString(e));
            throw new RuntimeException(e);
        }
    }

}
