package br.com.servicoemcasa.infra;

public class GcmClientServiceException extends GcmException {

    private static final long serialVersionUID = 1L;
    
    public GcmClientServiceException() {
        super();
    }
    
    public GcmClientServiceException(Exception origin) {
        super(origin);
    }
    
    public GcmClientServiceException(String message) {
        super(message);
    }
    
}
