package br.com.servicoemcasa.infra;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;

public class AddressDescription implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public final LatLng latlng;
    public final String addressDescription;

    public AddressDescription(LatLng latlng, String addressDescription) {
        this.latlng = latlng;
        this.addressDescription = addressDescription;
    }

    @Override
    public String toString() {
        return addressDescription;
    }

}
