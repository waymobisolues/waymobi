package br.com.servicoemcasa.infra;

public abstract class Callback<Result> {

    public abstract void callback(Result param);

    public void onProgress(Integer percent) {
    }

}
