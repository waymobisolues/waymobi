package br.com.servicoemcasa.infra;

import com.google.android.gms.maps.model.LatLng;

public interface LocationAware {

    public void setLatLng(LatLng latlng);
    public void setLatLng(MapService mapService, LatLng latlng);

}
