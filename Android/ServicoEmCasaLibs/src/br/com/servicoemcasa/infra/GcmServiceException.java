package br.com.servicoemcasa.infra;

public class GcmServiceException extends GcmException {

    private static final long serialVersionUID = 1L;
    
    public GcmServiceException() {
        super();
    }
    
    public GcmServiceException(Exception origin) {
        super(origin);
    }


}
