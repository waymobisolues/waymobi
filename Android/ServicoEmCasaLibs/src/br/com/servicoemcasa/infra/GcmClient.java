package br.com.servicoemcasa.infra;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import br.com.servicoemcasa.util.ApplicationUtils;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmClient {

    public static final String GCM_PROJECT_ID = "br.com.servicoemcasa.infra.GcmClient.project_id";

    private final static String TAG = GcmClient.class.getCanonicalName();

    private String projectId;
    private GoogleCloudMessaging gcm;
    private Context context;
    private Activity activity;

    public GcmClient(Activity activity) {
        this.context = activity.getApplicationContext();
        this.activity = activity;
        startGcmService();
    }

    private void startGcmService() {
        if (GoogleServices.checkPlayServices(activity)) {
            gcm = GoogleCloudMessaging.getInstance(context);
        } else {
            Log.e(TAG, "No valid Google Play Services APK found.");
        }
    }

    public class GcmCallback {
        public final GcmException exception;
        public final String gcmKey;

        public GcmCallback(GcmException exception) {
            this.exception = exception;
            this.gcmKey = null;
        }

        public GcmCallback(String gcmKey) {
            this.exception = null;
            this.gcmKey = gcmKey;
        }

    }

    public void register(Callback<GcmCallback> callback) {
        registerInBackground(callback);
    }

    public String getProjectId() {
        if (projectId == null) {
            projectId = String.valueOf(ApplicationUtils.getMetaDataAsLong(activity, GCM_PROJECT_ID));
        }
        return projectId;
    }

    private void registerInBackground(final Callback<GcmCallback> callback) {
        Log.i(TAG, "registerInBackground()");
        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void... params) {
                try {
                    Log.i(TAG, "Registering, projectId: " + getProjectId());
                    String regid = gcm.register(getProjectId());
                    Log.i(TAG, "Registro GCM: " + regid);
                    return regid;
                } catch (IOException ex) {
                    Log.e(TAG, "registerInBackground", ex);
                    return null;
                }
            }

            protected void onPostExecute(String regid) {
                Log.i(TAG, "RegisterGcm::onPostExecute(" + regid + ")");
                if (regid != null) {
                    callback.callback(new GcmCallback(regid));
                } else {
                    callback.callback(new GcmCallback(new GcmServiceException()));
                }
            };
        }.execute();
    }

}
