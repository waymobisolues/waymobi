package br.com.servicoemcasa.adapter;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

public abstract class SimpleAdapter<T> extends BaseAdapter implements ListAdapter {

    private List<T> list;
    private ItemId<T> itemId;
    private Text<T> text;

    public SimpleAdapter(List<T> list, ItemId<T> itemId, Text<T> text) {
        this.list = list;
        this.itemId = itemId;
        this.text = text;
    }

    public static interface ItemId<T> {
        long getItemId(T object);
    }

    public static interface Text<T> {
        String getText(T object);
    }
    
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.itemId.getItemId(list.get(i));
    }

    public String getItemText(int i) {
        return this.text.getText(list.get(i));
    }

    @Override
    public abstract View getView(int i, View view, ViewGroup viewGroup);

}
