package br.com.servicoemcasa.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import br.com.servicoemcasa.lib.R;

public class CheckBoxAdapter<T> extends SimpleAdapter<T> {

    private Activity activity;
    private Select<T> select;

    public CheckBoxAdapter(Activity activity, List<T> list, ItemId<T> itemId, Text<T> text, Select<T> select) {
        super(list, itemId, text);
        this.activity = activity;
        this.select = select;
    }

    public static interface Select<T> {
        boolean isSelected(T object);

        void select(T object);

        void remove(T object);
    }

    @SuppressWarnings("unchecked")
    @SuppressLint({ "ViewHolder", "InflateParams" })
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View linha = activity.getLayoutInflater().inflate(R.layout.checkbox_adapter_layout, null);
        CheckBox chk = (CheckBox) linha.findViewById(R.id.checkbox);
        final T object = (T) getItem(i);
        chk.setText(getItemText(i));
        chk.setChecked(select.isSelected(object));
        chk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select.isSelected(object)) {
                    select.remove(object);
                } else {
                    select.select(object);
                }
            }
        });
        return linha;
    }

}
