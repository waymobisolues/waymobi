package br.com.servicoemcasa.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import br.com.servicoemcasa.lib.R;

public class TextImageAdapter<T> extends SimpleAdapter<T> {

    private Activity activity;
    private int drawable;

    public TextImageAdapter(Activity activity, List<T> list, int drawable, ItemId<T> itemId, Text<T> text) {
        super(list, itemId, text);
        this.activity = activity;
        this.drawable = drawable;
    }

    @SuppressLint({ "ViewHolder", "InflateParams" })
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View linha = activity.getLayoutInflater().inflate(R.layout.text_image_adapter_layout, null);
        TextView txt = (TextView) linha.findViewById(R.id.text_and_image);
        txt.setText(getItemText(i));
        txt.setCompoundDrawablesWithIntrinsicBounds(this.drawable, 0, 0, 0);
        return linha;
    }

}
