package br.com.servicoemcasa.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class PeriodoAtendimento implements Serializable {

    private static final long serialVersionUID = 1L;

    private int codigo;
    private String descricao;

    public int getCodigo() {
        return codigo;
    }

    public PeriodoAtendimento setId(int codigo) {
        this.codigo = codigo;
        return this;
    }

    public String getDescricao() {
        return descricao;
    }

    public PeriodoAtendimento setDescricao(String descricao) {
        this.descricao = descricao;
        return this;
    }
    
    @Override
    public String toString() {
        return descricao;
    }

}
