package br.com.servicoemcasa.model;

import java.io.Serializable;

public class Telefone implements Serializable {

    private static final long serialVersionUID = 1L;

    private String ddd;
    private String numeroTelefone;

    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    public Telefone setNumeroTelefone(String numero) {
        this.numeroTelefone = numero;
        return this;
    }

    public String getDDD() {
        return ddd;
    }

    public Telefone setDDD(String ddd) {
        this.ddd = ddd;
        return this;
    }

}
