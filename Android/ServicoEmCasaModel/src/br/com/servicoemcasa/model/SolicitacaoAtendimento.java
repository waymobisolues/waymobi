package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class SolicitacaoAtendimento implements Serializable {

    private static final long serialVersionUID = 1L;

    private long codigo;
    private Cliente cliente;
    private LocalAtendimento localAtendimento;
    private Endereco endereco;
    private TipoPrestador tipoPrestador;
    private Date data;
    private List<SolicitacaoAtendimentoPrestador> prestadores;
    private String observacoes;
    private boolean encerrado;
    private Date dataInclusao;
    private Agendamento agendamento;

    public SolicitacaoAtendimento setCliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public SolicitacaoAtendimento setEndereco(Endereco endereco) {
        this.endereco = endereco;
        return this;
    }

    public SolicitacaoAtendimento setTipoPrestador(TipoPrestador tipoPrestador) {
        this.tipoPrestador = tipoPrestador;
        return this;
    }

    public SolicitacaoAtendimento setData(Date data) {
        this.data = data;
        return this;
    }

    public long getCodigo() {
        return codigo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public TipoPrestador getTipoPrestador() {
        return tipoPrestador;
    }

    public Date getData() {
        return data;
    }

    public LocalAtendimento getLocalAtendimento() {
        return localAtendimento;
    }

    public SolicitacaoAtendimento setLocalAtendimento(LocalAtendimento localAtendimento) {
        this.localAtendimento = localAtendimento;
        return this;
    }

    public boolean isEncerrado() {
        return encerrado;
    }

    public List<SolicitacaoAtendimentoPrestador> getPrestadores() {
        return prestadores;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public SolicitacaoAtendimento setObservacoes(String observacoes) {
        this.observacoes = observacoes;
        return this;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public Agendamento getAgendamento() {
        return agendamento;
    }

    @Override
    public String toString() {
        return "SolicitacaoAtendimento [codigo=" + codigo + ", cliente=" + cliente + ", endereco=" + endereco
                + ", tipoPrestador=" + tipoPrestador + ", data=" + data + "]";
    }
}
