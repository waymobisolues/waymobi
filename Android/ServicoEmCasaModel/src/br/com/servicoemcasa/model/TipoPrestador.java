package br.com.servicoemcasa.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class TipoPrestador implements Serializable {

    private static final long serialVersionUID = 1L;

    private int codigo;
    private String descricao;
    private GrupoTipoPrestador grupoTipoPrestador;

    public int getCodigo() {
        return codigo;
    }

    public TipoPrestador setCodigo(int codigo) {
        this.codigo = codigo;
        return this;
    }

    public String getDescricao() {
        return descricao;
    }

    public TipoPrestador setDescricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public GrupoTipoPrestador getGrupoTipoPrestador() {
        return grupoTipoPrestador;
    }

    public TipoPrestador setGrupoTipoPrestador(GrupoTipoPrestador grupoTipoPrestador) {
        this.grupoTipoPrestador = grupoTipoPrestador;
        return this;
    }

    @Override
    public String toString() {
        return descricao;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + codigo;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TipoPrestador other = (TipoPrestador) obj;
        if (codigo != other.codigo)
            return false;
        return true;
    }

}
