package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

public class AutorizacaoCadastro implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idAutorizacao;
    private Email email;
    private Gcm gcm;
    private Gcm gcmAutorizado;
    private boolean liberado;
    private Date dataInclusao;
    private Date dataAlteracao;

    AutorizacaoCadastro() {
    }

    public String getIdAutorizacao() {
        return idAutorizacao;
    }

    public Email getEmail() {
        return email;
    }

    public Gcm getGcmAutorizado() {
        return gcmAutorizado;
    }

    public Gcm getGcm() {
        return gcm;
    }

    public boolean isLiberado() {
        return liberado;
    }

    public void setLiberado(boolean liberado) {
        this.liberado = liberado;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

}
