package br.com.servicoemcasa.model;

import java.io.Serializable;

import android.util.Log;
import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.CPFValidator;
import br.com.caelum.stella.validation.InvalidStateException;

public class Documento implements Serializable {

    private static final long serialVersionUID = 1L;

    private TipoDocumento tipoDocumento;
    private Long numeroDocumento;

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public Documento setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
        return this;
    }

    public Long getNumeroDocumento() {
        return numeroDocumento;
    }

    public Documento setNumeroDocumento(Long numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
        return this;
    }

    public boolean validaDigito() {
        Log.i("Documento",
                String.valueOf(new CNPJValidator(false).isEligible(String.format("%1$014d", numeroDocumento))));
        try {
            if (tipoDocumento.equals(TipoDocumento.CNPJ)) {
                new CNPJValidator(false).assertValid(String.format("%1$014d", numeroDocumento));
            } else {
                new CPFValidator(false).assertValid(String.format("%1$011d", numeroDocumento));
            }
            return true;
        } catch (InvalidStateException e) {
            return false;
        }
    }

}
