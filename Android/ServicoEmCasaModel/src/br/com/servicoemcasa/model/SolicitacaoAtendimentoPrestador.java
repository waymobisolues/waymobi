package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class SolicitacaoAtendimentoPrestador implements Serializable {

    private static final long serialVersionUID = 1L;

    private long codigo;
    private SolicitacaoAtendimento solicitacaoAtendimento;
    private Prestador prestador;
    private List<Conversa> conversa;
    private int distanciaParaEnderecoDeAtendimento;
    private boolean aceito;

    public long getCodigo() {
        return codigo;
    }

    public SolicitacaoAtendimento getSolicitacaoAtendimento() {
        return solicitacaoAtendimento;
    }

    public Prestador getPrestador() {
        return prestador;
    }

    public List<Conversa> getConversa() {
        return conversa;
    }

    public int getDistanciaParaEnderecoDeAtendimento() {
        return distanciaParaEnderecoDeAtendimento;
    }

    public boolean isAceito() {
        return aceito;
    }

    public SolicitacaoAtendimentoPrestador setConversa(List<Conversa> conversa) {
        this.conversa = conversa;
        return this;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (codigo ^ (codigo >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SolicitacaoAtendimentoPrestador other = (SolicitacaoAtendimentoPrestador) obj;
        if (codigo != other.codigo)
            return false;
        return true;
    }

}
