package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Conversa implements Serializable {

    private static final long serialVersionUID = 1L;

    private long codigo;
    private SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador;
    private Gcm remetente;
    private Date data;
    private String mensagem;

    public long getCodigo() {
        return codigo;
    }

    public SolicitacaoAtendimentoPrestador getSolicitacaoAtendimentoPrestador() {
        return solicitacaoAtendimentoPrestador;
    }

    public Gcm getRemetente() {
        return remetente;
    }

    public Date getData() {
        return data;
    }

    public String getMensagem() {
        return mensagem;
    }

    public Conversa setRemetente(Gcm remetente) {
        this.remetente = remetente;
        return this;
    }

    public Conversa setMensagem(String mensagem) {
        this.mensagem = mensagem;
        return this;
    }
    
    public Conversa setData(Date data) {
        this.data = data;
        return this;
    }
    
    public Conversa setSolicitacaoAtendimentoPrestador(SolicitacaoAtendimentoPrestador prestador) {
        this.solicitacaoAtendimentoPrestador = prestador;
        return this;
    }

}
