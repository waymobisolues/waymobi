package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

public class RG implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Long rg;
    private Date dataExpedicao;
    private String orgaoEmissorRG;
    private UF uf = new UF();

    public Long getRg() {
        return rg;
    }

    public RG setRg(Long rg) {
        this.rg = rg;
        return this;
    }

    public String getOrgaoEmissorRG() {
        return orgaoEmissorRG;
    }

    public RG setOrgaoEmissorRG(String orgaoEmissorRG) {
        this.orgaoEmissorRG = orgaoEmissorRG;
        return this;
    }

    public Date getDataExpedicao() {
        return dataExpedicao;
    }

    public RG setDataExpedicao(Date dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
        return this;
    }

    public UF getUf() {
        return uf;
    }

    public RG setUf(UF uf) {
        this.uf = uf;
        return this;
    }

}
