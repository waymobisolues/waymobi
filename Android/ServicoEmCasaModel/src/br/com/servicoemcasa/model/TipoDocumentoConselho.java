package br.com.servicoemcasa.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class TipoDocumentoConselho implements Serializable, Comparable<TipoDocumentoConselho> {

    private static final long serialVersionUID = 1L;

    private String tipoDocumentoConselho;
    private String descricaoTipoDocumentoConselho;

    public TipoDocumentoConselho() {
    }

    public String getTipoDocumentoConselho() {
        return tipoDocumentoConselho;
    }

    public String getDescricaoTipoDocumentoConselho() {
        return descricaoTipoDocumentoConselho;
    }

    public TipoDocumentoConselho setTipoDocumentoConselho(String tipoDocumentoConselho) {
        this.tipoDocumentoConselho = tipoDocumentoConselho;
        return this;
    }

    public TipoDocumentoConselho setDescricaoTipoDocumentoConselho(String descricao) {
        this.descricaoTipoDocumentoConselho = descricao;
        return this;
    }

    @Override
    public String toString() {
        return (tipoDocumentoConselho == null ? "" : tipoDocumentoConselho + " - ") + descricaoTipoDocumentoConselho;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((descricaoTipoDocumentoConselho == null) ? 0 : descricaoTipoDocumentoConselho.hashCode());
        result = prime * result + ((tipoDocumentoConselho == null) ? 0 : tipoDocumentoConselho.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TipoDocumentoConselho other = (TipoDocumentoConselho) obj;
        if (descricaoTipoDocumentoConselho == null) {
            if (other.descricaoTipoDocumentoConselho != null)
                return false;
        } else if (!descricaoTipoDocumentoConselho.equals(other.descricaoTipoDocumentoConselho))
            return false;
        if (tipoDocumentoConselho == null) {
            if (other.tipoDocumentoConselho != null)
                return false;
        } else if (!tipoDocumentoConselho.equals(other.tipoDocumentoConselho))
            return false;
        return true;
    }

    @Override
    public int compareTo(TipoDocumentoConselho arg0) {
        return tipoDocumentoConselho.compareTo(arg0.tipoDocumentoConselho);
    }

}
