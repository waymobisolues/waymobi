package br.com.servicoemcasa.model;

import java.io.Serializable;

public class DocumentoConselho implements Serializable {

    private static final long serialVersionUID = 1L;

    private String numeroDocumentoConselho;
    private TipoDocumentoConselho tipoDocumentoConselho = new TipoDocumentoConselho();

    public DocumentoConselho() {
    }

    public String getNumeroDocumentoConselho() {
        return numeroDocumentoConselho;
    }

    public TipoDocumentoConselho getTipoDocumentoConselho() {
        return tipoDocumentoConselho;
    }

    public DocumentoConselho setNumeroDocumentoConselho(String numero) {
        this.numeroDocumentoConselho = numero;
        return this;
    }

    public DocumentoConselho setTipoDocumentoConselho(TipoDocumentoConselho tipo) {
        this.tipoDocumentoConselho = tipo;
        return this;
    }
}
