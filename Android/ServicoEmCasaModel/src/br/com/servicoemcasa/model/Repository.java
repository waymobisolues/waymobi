package br.com.servicoemcasa.model;

import br.com.servicoemcasa.util.JsonParser;

class Repository {

    protected String toJson(Object o) {
        return JsonParser.toJson(o);
    }

    protected <T> T fromJson(String json, Class<T> classOf) {
        return JsonParser.fromJson(json, classOf);
    }

}
