package br.com.servicoemcasa.model;

import java.io.Serializable;

public class RegiaoAtendimento implements Serializable {

    private static final long serialVersionUID = 1L;

    private int maximaDistanciaAtendimento;

    private float latitudeRegiao;
    private float longitudeRegiao;

    public int getMaximaDistanciaAtendimento() {
        return maximaDistanciaAtendimento;
    }

    public RegiaoAtendimento setMaximaDistanciaAtendimento(int maximaDistanciaAtendimento) {
        this.maximaDistanciaAtendimento = maximaDistanciaAtendimento;
        return this;
    }

    public float getLatitudeRegiao() {
        return latitudeRegiao;
    }

    public RegiaoAtendimento setLatitudeRegiao(float latitudeRegiao) {
        this.latitudeRegiao = latitudeRegiao;
        return this;
    }

    public float getLongitudeRegiao() {
        return longitudeRegiao;
    }

    public RegiaoAtendimento setLongitudeRegiao(float longitudeRegiao) {
        this.longitudeRegiao = longitudeRegiao;
        return this;
    }

}
