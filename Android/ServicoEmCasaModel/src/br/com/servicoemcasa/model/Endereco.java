package br.com.servicoemcasa.model;

import java.io.Serializable;

public class Endereco implements Serializable {

    private static final long serialVersionUID = 6533051685886480032L;

    private String descricao;
    private String complemento;
    private double latitude;
    private double longitude;

    @Override
    public String toString() {
        return descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public Endereco setDescricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public String getComplemento() {
        return complemento;
    }

    public Endereco setComplemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public Endereco setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public Endereco setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

}
