package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Agendamento implements Serializable {

    private static final long serialVersionUID = 7967799849459286147L;

    public static final String RESOURCE = Agendamento.class.getSimpleName();

    private long codigo;
    private Date dataInclusao;
    private SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador;
    private boolean encerrado;
    private Boolean avaliacaoPositiva;

    public long getId() {
        return codigo;
    }

    public Agendamento setId(long id) {
        this.codigo = id;
        return this;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public SolicitacaoAtendimentoPrestador getSolicitacaoAtendimentoPrestador() {
        return solicitacaoAtendimentoPrestador;
    }

    public boolean isEncerrado() {
        return encerrado;
    }

    public Boolean getAvaliacaoPositiva() {
        return avaliacaoPositiva;
    }

}
