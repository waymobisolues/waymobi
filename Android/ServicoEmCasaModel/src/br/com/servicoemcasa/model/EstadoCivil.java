package br.com.servicoemcasa.model;

public enum EstadoCivil {

    Solteiro, Casado, Divorciado, Viuvo;

}
