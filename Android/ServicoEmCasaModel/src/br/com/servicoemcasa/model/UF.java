package br.com.servicoemcasa.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class UF implements Serializable, Comparable<UF> {

    private static final long serialVersionUID = 1L;

    private String sigla;
    private String nome;

    public String getSigla() {
        return sigla;
    }

    public UF setSigla(String sigla) {
        this.sigla = sigla;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public UF setNome(String nome) {
        this.nome = nome;
        return this;
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public int compareTo(UF arg0) {
        return nome.compareTo(arg0.nome);
    }

}
