package br.com.servicoemcasa.model.message;

import java.io.Serializable;
import java.util.Date;

import br.com.servicoemcasa.model.Gcm;

public class SolicitacaoAtendimentoMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private long codigo;
    private long codigoSolicitacao;
    private Gcm gcmPrestador;
    private Date data;
    private String tipoServico;
    private int distancia;

    public long getCodigo() {
        return codigo;
    }

    public long getCodigoSolicitacao() {
        return codigoSolicitacao;
    }

    public Date getData() {
        return data;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public int getDistancia() {
        return distancia;
    }

    public Gcm getGcmPrestador() {
        return gcmPrestador;
    }

}
