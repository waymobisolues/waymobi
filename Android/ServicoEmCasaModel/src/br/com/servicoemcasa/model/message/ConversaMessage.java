package br.com.servicoemcasa.model.message;

import java.io.Serializable;

public class ConversaMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private long codigo;
    private long codigoSolicitacao;
    private long codigoSolicitacaoPrestador;
    private String nomeOrigem;
    private String mensagem;

    public long getCodigo() {
        return codigo;
    }

    public long getCodigoSolicitacao() {
        return codigoSolicitacao;
    }

    public long getCodigoSolicitacaoPrestador() {
        return codigoSolicitacaoPrestador;
    }

    public String getNomeOrigem() {
        return nomeOrigem;
    }

    public String getMensagem() {
        return mensagem;
    }

}