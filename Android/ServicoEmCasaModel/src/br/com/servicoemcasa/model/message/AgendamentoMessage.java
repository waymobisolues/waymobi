package br.com.servicoemcasa.model.message;

import java.io.Serializable;
import java.util.Date;

public class AgendamentoMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private long codigo;
    private long codigoSolicitacao;
    private long codigoSolicitacaoPrestador;
    private Date data;
    private String tipoServico;
    private boolean encerrado;

    public long getCodigo() {
        return codigo;
    }

    public Date getData() {
        return data;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public long getCodigoSolicitacao() {
        return codigoSolicitacao;
    }

    public long getCodigoSolicitacaoPrestador() {
        return codigoSolicitacaoPrestador;
    }

    public boolean isEncerrado() {
        return encerrado;
    }

}
