package br.com.servicoemcasa.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class GrupoTipoPrestador implements Serializable, Comparable<GrupoTipoPrestador> {

    private static final long serialVersionUID = 1L;

    private int codigoGrupo;
    private String descricaoGrupo;

    public int getCodigoGrupo() {
        return codigoGrupo;
    }

    public GrupoTipoPrestador setCodigoGrupo(int codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
        return this;
    }

    public String getDescricaoGrupo() {
        return descricaoGrupo;
    }

    public GrupoTipoPrestador setDescricaoGrupo(String descricaoGrupo) {
        this.descricaoGrupo = descricaoGrupo;
        return this;
    }

    @Override
    public int compareTo(GrupoTipoPrestador arg0) {
        return descricaoGrupo.compareTo(arg0.descricaoGrupo);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + codigoGrupo;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GrupoTipoPrestador other = (GrupoTipoPrestador) obj;
        if (codigoGrupo != other.codigoGrupo)
            return false;
        return true;
    }

}
