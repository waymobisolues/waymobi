package br.com.servicoemcasa.model;

import java.io.Serializable;

public class AvaliacaoPrestador implements Serializable {

    private static final long serialVersionUID = 1L;

    private long quantidadeAvaliacoes;
    private long pontuacao;

    public long getQuantidadeAvaliacoes() {
        return quantidadeAvaliacoes;
    }

    public AvaliacaoPrestador setQuantidadeAvaliacoes(int quantidadeAvaliacoes) {
        this.quantidadeAvaliacoes = quantidadeAvaliacoes;
        return this;
    }

    public long getPontuacao() {
        return pontuacao;
    }

    public AvaliacaoPrestador setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
        return this;
    }

}
