package br.com.servicoemcasa.model;

import java.io.Serializable;

public class Email implements Serializable {

    private static final long serialVersionUID = 1L;

    private String email;

    public String getEmail() {
        return this.email;
    }

    public Email setEmail(String email) {
        this.email = email;
        return this;
    }

}
