package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    private int codigo;
    private Gcm gcm;
    private Email email;
    private String nome;
    private Telefone telefone;
    private Sexo sexo;
    private EstadoCivil estadoCivil;
    private Date dataNascimento;
    private Date dataInclusao;
    private Date dataAlteracao;

    public int getCodigo() {
        return codigo;
    }

    public Cliente setCodigo(int codigo) {
        this.codigo = codigo;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public Cliente setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Email getEmail() {
        return email;
    }

    public Cliente setEmail(Email email) {
        this.email = email;
        return this;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public Cliente setTelefone(Telefone telefone) {
        this.telefone = telefone;
        return this;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public Cliente setSexo(Sexo sexo) {
        this.sexo = sexo;
        return this;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public Cliente setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
        return this;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public Cliente setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
        return this;
    }

    public Gcm getGcm() {
        return gcm;
    }

    public Cliente setGcm(Gcm gcm) {
        this.gcm = gcm;
        return this;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

}
