package br.com.servicoemcasa.model;

import java.io.Serializable;

import br.com.servicoemcasa.util.Hash;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Gcm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String hashedGcmKey;
    private String gcmKey;

    public String getHashedGcmKey() {
        return hashedGcmKey;
    }

    public String getGcmKey() {
        return gcmKey;
    }

    public Gcm setGcmKey(String gcmKey) {
        this.hashedGcmKey = Hash.createMD5Hash(gcmKey);
        this.gcmKey = gcmKey;
        return this;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((hashedGcmKey == null) ? 0 : hashedGcmKey.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Gcm other = (Gcm) obj;
        if (hashedGcmKey == null) {
            if (other.hashedGcmKey != null)
                return false;
        } else if (!hashedGcmKey.equals(other.hashedGcmKey))
            return false;
        return true;
    }

}
