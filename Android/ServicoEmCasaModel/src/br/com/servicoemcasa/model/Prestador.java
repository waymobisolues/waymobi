package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Prestador implements Serializable {
    private static final long serialVersionUID = 1L;

    private int codigo;
    private Cliente cliente;
    private Email email;
    private String nomeFantasia;
    private List<TipoPrestador> tipo;
    private Documento documento;
    private Endereco endereco;
    private RegiaoAtendimento regiaoAtendimento;
    private AvaliacaoPrestador avaliacao;
    private String resumoProfissional;
    private DocumentoConselho documentoConselho;
    private RG rg;
    private Date dataInclusao;
    private Date dataAlteracao;

    @Override
    public String toString() {
        return cliente.getNome();
    }

    public int getCodigo() {
        return codigo;
    }

    public Prestador setCodigo(int codigo) {
        this.codigo = codigo;
        return this;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Prestador setCliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public Prestador setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
        return this;
    }

    public Email getEmail() {
        return email;
    }

    public Prestador setEmail(Email email) {
        this.email = email;
        return this;
    }

    public Telefone getTelefone() {
        return cliente.getTelefone();
    }

    public List<TipoPrestador> getTipo() {
        return tipo;
    }

    public Prestador setTipo(List<TipoPrestador> lista) {
        this.tipo = lista;
        return this;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public Prestador setEndereco(Endereco endereco) {
        this.endereco = endereco;
        return this;
    }

    public RegiaoAtendimento getRegiaoAtendimento() {
        return regiaoAtendimento;
    }

    public Prestador setRegiaoAtendimento(RegiaoAtendimento regiaoAtendimento) {
        this.regiaoAtendimento = regiaoAtendimento;
        return this;
    }

    public AvaliacaoPrestador getAvaliacao() {
        return avaliacao;
    }

    public Prestador setAvaliacao(AvaliacaoPrestador avaliacao) {
        this.avaliacao = avaliacao;
        return this;
    }

    public Prestador setResumoProfissional(String resumo) {
        this.resumoProfissional = resumo;
        return this;
    }

    public String getResumoProfissional() {
        return resumoProfissional;
    }

    public Documento getDocumento() {
        return documento;
    }

    public Prestador setDocumento(Documento documento) {
        this.documento = documento;
        return this;
    }

    public DocumentoConselho getDocumentoConselho() {
        return documentoConselho;
    }

    public Prestador setDocumentoConselho(DocumentoConselho documentoConselho) {
        this.documentoConselho = documentoConselho;
        return this;
    }

    public RG getRg() {
        return rg;
    }

    public Prestador setRg(RG rg) {
        this.rg = rg;
        return this;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public Gcm getGcm() {
        return getCliente().getGcm();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + codigo;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Prestador other = (Prestador) obj;
        if (codigo != other.codigo)
            return false;
        return true;
    }

}
