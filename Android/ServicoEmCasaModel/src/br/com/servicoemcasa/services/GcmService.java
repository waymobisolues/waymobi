package br.com.servicoemcasa.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.GcmClient;
import br.com.servicoemcasa.infra.GcmClient.GcmCallback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.Store;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.util.ApplicationUtils;
import br.com.servicoemcasa.util.JsonParser;

public class GcmService {

    private static final String TAG = GcmService.class.getSimpleName();
    private static final String PATH = "/Gcm/";
    private static final String STORE_GCM_KEY = "GCM_KEY";
    private static final String STORE_GCM_HASHED_KEY = "GCM_HASHED_KEY";
    private static final String STORE_GCM_APP_VERSION = "GCM_APP_VERSION";
    private final GcmClient gcmClient;
    private final Store store;
    private WebClient webClient;
    private Activity activity;

    public GcmService(Activity activity) {
        this.activity = activity;
        gcmClient = new GcmClient(activity);
        store = new Store(activity, this.getClass());
        webClient = WebClient.newInstance(activity);
    }

    private interface GcmRegister {

        void execute(String gcmKey);

    }

    private void register(final GcmRegister command, final Callback<SimpleCallback<Gcm>> callback) {
        gcmClient.register(new Callback<GcmClient.GcmCallback>() {
            @Override
            public void callback(GcmCallback param) {
                if (param.exception == null) {
                    command.execute(param.gcmKey);
                } else {
                    Log.e(TAG, Log.getStackTraceString(param.exception));
                    callback.callback(new SimpleCallback<Gcm>(500, null));
                }
            }
        });
    }

    private int getAppVersion() {
        return ApplicationUtils.getAppVersion(activity);
    }

    private boolean appVersionChanged() {
        return getAppVersion() != store.getInt(STORE_GCM_APP_VERSION);
    }

    public static Gcm getActualRegisteredGcm(Context context) {
        Store store = new Store(context, GcmService.class);
        String id = store.getString(STORE_GCM_KEY);
        if (id.length() > 0) {
            return new Gcm().setGcmKey(id);
        }
        return null;
    }

    private Gcm getRegisteredGcm() {
        return getActualRegisteredGcm(activity);
    }

    private boolean isRegistered() {
        return store.getString(STORE_GCM_KEY).length() > 0;
    }

    public void getGcm(final Callback<SimpleCallback<Gcm>> callback) {
        if (appVersionChanged() || !isRegistered()) {
            register(callback);
        } else {
            callback.callback(new SimpleCallback<Gcm>(getRegisteredGcm()));
        }
    }

    private void register(final Callback<SimpleCallback<Gcm>> callback) {
        Gcm registeredGcm = getRegisteredGcm();
        if (registeredGcm == null) {
            newRegister(callback);
        } else {
            changeRegister(registeredGcm, callback);
        }
    }

    private void newRegister(final Callback<SimpleCallback<Gcm>> callback) {
        register(new GcmRegister() {
            @Override
            public void execute(String gcmKey) {
                post(gcmKey, callback);
            }
        }, callback);
    }

    private void changeRegister(final Gcm gcm, final Callback<SimpleCallback<Gcm>> callback) {
        register(new GcmRegister() {
            @Override
            public void execute(String gcmKey) {
                put(gcm, gcmKey, callback);
            }
        }, callback);
    }

    private class SendToServerCallback extends Callback<WebClient.WebResponse> {

        private final Callback<SimpleCallback<Gcm>> callback;

        SendToServerCallback(final Callback<SimpleCallback<Gcm>> callback) {
            this.callback = callback;
        }

        @Override
        public void callback(WebResponse param) {
            if (param.responseCode == HttpStatus.SC_OK || param.responseCode == HttpStatus.SC_CREATED) {
                Gcm gcm = JsonParser.fromJson(param.responseBody, Gcm.class);
                Editor editor = store.getEditor();
                editor.putString(STORE_GCM_HASHED_KEY, gcm.getHashedGcmKey());
                editor.putString(STORE_GCM_KEY, gcm.getGcmKey());
                editor.putInt(STORE_GCM_APP_VERSION, getAppVersion());
                editor.commit();
                callback.callback(new SimpleCallback<Gcm>(gcm));
            } else {
                callback.callback(new SimpleCallback<Gcm>(param.responseCode, param.getErrorDescription()));
            }
        }
    }

    private void post(final String gcmKey, final Callback<SimpleCallback<Gcm>> callback) {
        webClient.postForm(PATH, "regId=" + webClient.encode(gcmKey), new SendToServerCallback(callback));
    }

    private void put(final Gcm gcmAntigo, final String gcmKeyNovo, final Callback<SimpleCallback<Gcm>> callback) {
        webClient.putForm(PATH + webClient.encode(gcmAntigo.getHashedGcmKey()), "regId=" + webClient.encode(gcmKeyNovo),
                new SendToServerCallback(callback));
    }

}
