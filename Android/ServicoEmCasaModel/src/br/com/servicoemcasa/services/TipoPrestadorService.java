package br.com.servicoemcasa.services;

import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.GrupoTipoPrestador;
import br.com.servicoemcasa.model.LocalAtendimento;
import br.com.servicoemcasa.model.TipoPrestador;
import br.com.servicoemcasa.util.JsonParser;

import com.google.android.gms.maps.model.LatLng;

public class TipoPrestadorService {

    private static final String PATH = "/TipoPrestador";

    private WebClient webClient;

    public TipoPrestadorService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void listaTipoPrestador(final GrupoTipoPrestador grupoTipoPrestador, final Callback<SimpleCallback<List<TipoPrestador>>> callback) {
        listaTipoPrestador(String.format("grupo=%d", grupoTipoPrestador.getCodigoGrupo()), callback);
    }

    public void listaTipoPrestador(final LatLng latlng, final LocalAtendimento localAtendimento,
            final GrupoTipoPrestador grupoTipoPrestador, final Callback<SimpleCallback<List<TipoPrestador>>> callback) {

        listaTipoPrestador(String.format("grupo=%d&latitude=%f&longitude=%f&localAtendimento=%s", grupoTipoPrestador.getCodigoGrupo(),
                latlng.latitude, latlng.longitude, localAtendimento.toString()), callback);
    }

    private void listaTipoPrestador(final String params, final Callback<SimpleCallback<List<TipoPrestador>>> callback) {
        webClient.get(PATH, params, new Callback<WebClient.WebResponse>() {

            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    TipoPrestador[] tipos = JsonParser.fromJson(param.responseBody, TipoPrestador[].class);
                    callback.callback(new SimpleCallback<List<TipoPrestador>>(Arrays.asList(tipos)));
                } else {
                    callback.callback(new SimpleCallback<List<TipoPrestador>>(param.responseCode, param.getErrorDescription()));
                }
            }
        });
    }

}
