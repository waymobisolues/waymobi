package br.com.servicoemcasa.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.PeriodoAtendimento;
import br.com.servicoemcasa.util.JsonParser;

public class PeriodoAtendimentoService {

    private static final String PATH = "/PeriodoAtendimento";
    private WebClient webClient;

    public PeriodoAtendimentoService(final Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void listaPeriodosAtendimento(final Callback<SimpleCallback<PeriodoAtendimento[]>> callback) {
        webClient.get(PATH, new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode != HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<PeriodoAtendimento[]>(param.responseCode, param
                            .getErrorDescription()));
                } else {
                    callback.callback(new SimpleCallback<PeriodoAtendimento[]>(JsonParser.fromJson(param.responseBody,
                            PeriodoAtendimento[].class)));
                }
            }
        });
    }

}
