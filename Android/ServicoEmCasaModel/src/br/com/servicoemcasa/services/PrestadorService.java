package br.com.servicoemcasa.services;

import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.util.ImageUtils;

public class PrestadorService {
    
    private Context context;

    public PrestadorService(Context context) {
        this.context = context;
    }

    public void recuperaFotoPretador(Prestador prestador, Callback<Bitmap> callback) {
        try {
            URL url = new URL(WebClient.getURL(context, "foto") + prestador.getCodigo() + ".png");
            ImageUtils.getImageFromURL(url, callback);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
    
}
