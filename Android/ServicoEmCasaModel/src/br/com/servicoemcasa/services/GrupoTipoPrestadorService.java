package br.com.servicoemcasa.services;

import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpStatus;

import android.annotation.SuppressLint;
import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.GrupoTipoPrestador;
import br.com.servicoemcasa.model.LocalAtendimento;
import br.com.servicoemcasa.util.JsonParser;

import com.google.android.gms.maps.model.LatLng;

public class GrupoTipoPrestadorService {

    private static final String PATH = "/GrupoTipoPrestador";

    private WebClient webClient;

    public GrupoTipoPrestadorService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    @SuppressLint("DefaultLocale")
    public void listaGrupoTipoPrestador(final LatLng latlng, final LocalAtendimento localAtendimento,
            final Callback<SimpleCallback<List<GrupoTipoPrestador>>> callback) {

        String params = null;
        if (latlng != null && localAtendimento != null) {
            params = String.format("latitude=%f&longitude=%f&localAtendimento=%s", latlng.latitude, latlng.longitude,
                    localAtendimento.toString());
        }
        webClient.get(PATH, params, new Callback<WebClient.WebResponse>() {

            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    GrupoTipoPrestador[] tipos = JsonParser.fromJson(param.responseBody, GrupoTipoPrestador[].class);
                    Arrays.sort(tipos);
                    callback.callback(new SimpleCallback<List<GrupoTipoPrestador>>(Arrays.asList(tipos)));
                } else {
                    callback.callback(new SimpleCallback<List<GrupoTipoPrestador>>(param.responseCode, param.getErrorDescription()));
                }
            }
        });
    }
}
