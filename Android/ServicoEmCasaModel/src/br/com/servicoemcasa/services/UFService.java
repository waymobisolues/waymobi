package br.com.servicoemcasa.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.UF;
import br.com.servicoemcasa.util.JsonParser;

public class UFService {

    private static final String PATH = "/UF";
    private WebClient webClient;

    public UFService(final Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void listaUF(final Callback<SimpleCallback<UF[]>> callback) {
        webClient.get(PATH, new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode != HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<UF[]>(param.responseCode, param.getErrorDescription()));
                } else {
                    callback.callback(new SimpleCallback<UF[]>(JsonParser.fromJson(param.responseBody, UF[].class)));
                }
            }
        });
    }
}
