package br.com.servicoemcasa.comum.services.gcm;

import android.content.Context;

public interface MessageIntent {

    public MessageIntentInfo getMessageIntentInfo(String message, int notificationId, Context context);

}
