package br.com.servicoemcasa.comum.services.gcm;

import android.content.Intent;

public class MessageIntentInfo {

    public final Intent intent;
    public final boolean autoCancel;
    public final String localBroadcastAction;
    public final int titulo;
    public final int mensagem;
    public final String[] params;
    public final int notificationId;

    public MessageIntentInfo(Intent intent, boolean autoCancel, int titulo, int mensagem, String... params) {
        this(intent, autoCancel, null, 0, titulo, mensagem, params);
    }

    public MessageIntentInfo(Intent intent, boolean autoCancel, String localBroadcastName, int notificationId,
            int titulo, int mensagem, String... params) {

        this.intent = intent;
        this.autoCancel = autoCancel;
        this.localBroadcastAction = localBroadcastName;
        this.notificationId = notificationId;
        this.titulo = titulo;
        this.mensagem = mensagem;
        this.params = params;
    }

}
