package br.com.servicoemcasa.comum.services.gcm;

import android.content.Context;
import android.content.Intent;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.comum.activities.ChatActivity;
import br.com.servicoemcasa.model.message.ConversaMessage;
import br.com.servicoemcasa.util.JsonParser;

class ConversaMessageIntent implements MessageIntent {

    @Override
    public MessageIntentInfo getMessageIntentInfo(String json, int notificationId, Context context) {
        ConversaMessage conversa = JsonParser.fromJson(json, ConversaMessage.class);
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(ConversaMessage.class.getSimpleName(), conversa);
        intent.putExtra("notificationId", (int) -conversa.getCodigoSolicitacaoPrestador());
        return new MessageIntentInfo(intent, true, ChatActivity.class.getName(),
                (int) -conversa.getCodigoSolicitacaoPrestador(), R.string.nova_mensagem, R.string.mensagem_recebida,
                conversa.getNomeOrigem(), conversa.getMensagem());
    }

}
