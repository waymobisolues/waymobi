package br.com.servicoemcasa.comum.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.Conversa;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.util.JsonParser;

import com.fasterxml.jackson.core.type.TypeReference;

public class MensagemService {

    private static final String PATH = "/Mensagem/";
    private WebClient webClient;

    public MensagemService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void listaMensagens(final SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador,
            final Callback<SimpleCallback<List<Conversa>>> callback) {

        webClient.get(PATH + solicitacaoAtendimentoPrestador.getCodigo(), new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebClient.WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<List<Conversa>>(JsonParser.fromJson(param.responseBody,
                            new TypeReference<ArrayList<Conversa>>() {
                            })));
                } else {
                    callback.callback(new SimpleCallback<List<Conversa>>(param.responseCode, param
                            .getErrorDescription()));
                }
            }
        });
    }

    public void enviaMensagem(final Conversa conversa, final Callback<SimpleCallback<List<Conversa>>> callback) {
        webClient.post(PATH + conversa.getSolicitacaoAtendimentoPrestador().getCodigo(), conversa,
                new Callback<WebClient.WebResponse>() {
                    @Override
                    public void callback(WebResponse param) {
                        if (param.responseCode == HttpStatus.SC_OK) {
                            callback.callback(new SimpleCallback<List<Conversa>>(JsonParser.fromJson(
                                    param.responseBody, new TypeReference<ArrayList<Conversa>>() {
                                    })));
                        } else {
                            callback.callback(new SimpleCallback<List<Conversa>>(param.responseCode, param
                                    .getErrorDescription()));
                        }
                    }
                });
    }

}
