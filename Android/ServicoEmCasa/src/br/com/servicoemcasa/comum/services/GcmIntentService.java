package br.com.servicoemcasa.comum.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.comum.services.gcm.MessageIntent;
import br.com.servicoemcasa.comum.services.gcm.MessageIntentInfo;
import br.com.servicoemcasa.comum.services.gcm.MessageRegistry;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService {

    public static int notificationId = 100;
    private NotificationManager mNotificationManager;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                processaMessagem(extras);
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void processaMessagem(Bundle extras) {
        String json = extras.getString("message");
        String type = extras.getString("type");
        int notificationId = ++GcmIntentService.notificationId;
        MessageIntent messageIntent = MessageRegistry.getMessageIntentInfo(type);
        MessageIntentInfo messageIntentInfo = messageIntent.getMessageIntentInfo(json, notificationId, this);
        criaNotificacao(messageIntentInfo, notificationId);
        criaLocalBroadcast(messageIntentInfo);
    }

    private void criaLocalBroadcast(MessageIntentInfo messageIntentInfo) {
        if (messageIntentInfo.localBroadcastAction != null) {
            Intent intent = new Intent(messageIntentInfo.localBroadcastAction);
            intent.putExtras(messageIntentInfo.intent.getExtras());
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

    private void criaNotificacao(final MessageIntentInfo messageIntentInfo, int notificationId) {
        notificationId = messageIntentInfo.notificationId != 0 ? messageIntentInfo.notificationId : notificationId;
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        messageIntentInfo.intent.putExtra("notificationId", notificationId);

        PendingIntent contentIntent = PendingIntent.getActivity(this, notificationId, messageIntentInfo.intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setStyle(
                        new NotificationCompat.BigTextStyle().bigText(getResources()
                                .getString(messageIntentInfo.titulo)))
                .setContentText(
                        String.format(getResources().getString(messageIntentInfo.mensagem),
                                (Object[]) messageIntentInfo.params)).setSound(uri)
                .setAutoCancel(messageIntentInfo.autoCancel);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(notificationId, mBuilder.build());
    }
}
