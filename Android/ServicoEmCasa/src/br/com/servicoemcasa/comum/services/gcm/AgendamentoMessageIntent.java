package br.com.servicoemcasa.comum.services.gcm;

import android.content.Context;
import android.content.Intent;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.activities.AvaliacaoAtendimentoActivity;
import br.com.servicoemcasa.model.message.AgendamentoMessage;
import br.com.servicoemcasa.prestador.activities.DetalheClienteActivity;
import br.com.servicoemcasa.util.JsonParser;

class AgendamentoMessageIntent implements MessageIntent {

    @Override
    public MessageIntentInfo getMessageIntentInfo(String json, int notificationId, Context context) {
        AgendamentoMessage agendamento = JsonParser.fromJson(json, AgendamentoMessage.class);
        return agendamento.isEncerrado() ? getMessageIntentInfoEncerrado(notificationId, context, agendamento)
                : getMessageIntentInfoNovo(context, agendamento);
    }

    private MessageIntentInfo getMessageIntentInfoEncerrado(int notificationId, Context context,
            AgendamentoMessage agendamento) {

        Intent intent = new Intent(context, AvaliacaoAtendimentoActivity.class);
        intent.putExtra(AgendamentoMessage.class.getSimpleName(), agendamento);
        return new MessageIntentInfo(intent, true, R.string.app_name, R.string.avaliar_atendimento);
    }

    private MessageIntentInfo getMessageIntentInfoNovo(Context context, AgendamentoMessage agendamento) {
        Intent intent = new Intent(context, DetalheClienteActivity.class);
        intent.putExtra(AgendamentoMessage.class.getSimpleName(), agendamento);
        return new MessageIntentInfo(intent, true, R.string.app_name, R.string.novo_agendamento);
    }

}
