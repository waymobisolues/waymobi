package br.com.servicoemcasa.comum.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimento;
import br.com.servicoemcasa.util.JsonParser;

import com.fasterxml.jackson.core.type.TypeReference;

public class SolicitacaoAtendimentoService {

    private static final String PATH = "/SolicitacaoAtendimento/";
    private WebClient webClient;

    public SolicitacaoAtendimentoService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void solicitaAtendimento(final SolicitacaoAtendimento solicitacaoAtendimento,
            final Callback<SimpleCallback<SolicitacaoAtendimento>> callback) {

        webClient.post(PATH, solicitacaoAtendimento, new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_PARTIAL_CONTENT) {
                    callback.callback(new SimpleCallback<SolicitacaoAtendimento>(JsonParser.fromJson(
                            param.responseBody, SolicitacaoAtendimento.class)));
                } else {
                    callback.callback(new SimpleCallback<SolicitacaoAtendimento>(param.responseCode, param
                            .getErrorDescription()));
                }
            }
        });
    }

    public void listaSolicitacoes(final Prestador prestador,
            final Callback<SimpleCallback<List<SolicitacaoAtendimento>>> callback) {

        webClient.get(PATH, String.format("codigoPrestador=%d", prestador.getCodigo()),
                listaSolicitacoesCallback(callback));
    }
    
    public void listaSolicitacoes(final Cliente cliente,
            final Callback<SimpleCallback<List<SolicitacaoAtendimento>>> callback) {

        webClient
                .get(PATH, String.format("codigoCliente=%d", cliente.getCodigo()), listaSolicitacoesCallback(callback));
    }

    private Callback<WebResponse> listaSolicitacoesCallback(
            final Callback<SimpleCallback<List<SolicitacaoAtendimento>>> callback) {

        return new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<List<SolicitacaoAtendimento>>(
                            (ArrayList<SolicitacaoAtendimento>) JsonParser.fromJson(param.responseBody,
                                    new TypeReference<ArrayList<SolicitacaoAtendimento>>() {
                                    })));
                } else {
                    callback.callback(new SimpleCallback<List<SolicitacaoAtendimento>>(param.responseCode, param
                            .getErrorDescription()));
                }
            }
        };
    }

    public void recuperaRespostas(final SolicitacaoAtendimento solicitacaoAtendimento,
            final Callback<SimpleCallback<SolicitacaoAtendimento>> callback) {

        webClient.get(PATH + solicitacaoAtendimento.getCodigo(), new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<SolicitacaoAtendimento>(JsonParser.fromJson(
                            param.responseBody, SolicitacaoAtendimento.class)));
                } else {
                    callback.callback(new SimpleCallback<SolicitacaoAtendimento>(param.responseCode, param
                            .getErrorDescription()));
                }
            }
        });
    }

}
