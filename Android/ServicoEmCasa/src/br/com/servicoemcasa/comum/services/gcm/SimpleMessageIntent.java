package br.com.servicoemcasa.comum.services.gcm;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.util.JsonParser;

class SimpleMessageIntent implements MessageIntent {

    class Message {
        String mensagem;
        String url;
    }

    @Override
    public MessageIntentInfo getMessageIntentInfo(String json, int notificationId, Context context) {
        Message message = JsonParser.fromJson(json, Message.class);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setData(Uri.parse(message.url));
        return new MessageIntentInfo(browserIntent, false, R.string.app_name, R.string.unknown, message.mensagem);
    }

}
