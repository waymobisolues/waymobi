package br.com.servicoemcasa.comum.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.util.JsonParser;

public class SolicitacaoAtendimentoPrestadorService {

    private static final String PATH = "/SolicitacaoAtendimentoPrestador/";
    private WebClient webClient;

    public SolicitacaoAtendimentoPrestadorService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void recuperaSolicitacao(final long codigo,
            final Callback<SimpleCallback<SolicitacaoAtendimentoPrestador>> callback) {

        webClient.get(PATH + codigo, new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<SolicitacaoAtendimentoPrestador>(JsonParser.fromJson(
                            param.responseBody, SolicitacaoAtendimentoPrestador.class)));
                } else {
                    callback.callback(new SimpleCallback<SolicitacaoAtendimentoPrestador>(param.responseCode, param
                            .getErrorDescription()));
                }
            }
        });
    }

}
