package br.com.servicoemcasa.comum.services.gcm;

import java.util.HashMap;
import java.util.Map;

public class MessageRegistry {

    private static final Map<String, MessageIntent> map = new HashMap<String, MessageIntent>();

    static {
        map.put("SimpleMessage", new SimpleMessageIntent());
        map.put("Conversa", new ConversaMessageIntent());
        map.put("SolicitacaoAtendimentoPrestador", new SolicitacaoAtendimentoMessageIntent());
        map.put("Agendamento", new AgendamentoMessageIntent());
    }

    public static MessageIntent getMessageIntentInfo(String type) {
        return map.get(type);
    }

}
