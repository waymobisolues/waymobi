package br.com.servicoemcasa.comum.services.gcm;

import android.content.Context;
import android.content.Intent;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.activities.DetalhePrestadorActivity;
import br.com.servicoemcasa.model.message.SolicitacaoAtendimentoMessage;
import br.com.servicoemcasa.prestador.activities.DetalheClienteActivity;
import br.com.servicoemcasa.services.GcmService;
import br.com.servicoemcasa.util.JsonParser;

class SolicitacaoAtendimentoMessageIntent implements MessageIntent {

    class Message {
        String mensagem;
        String url;
    }

    @Override
    public MessageIntentInfo getMessageIntentInfo(String json, int notificationId, Context context) {
        SolicitacaoAtendimentoMessage solicitacao = JsonParser.fromJson(json, SolicitacaoAtendimentoMessage.class);
        if (solicitacao.getGcmPrestador().equals(GcmService.getActualRegisteredGcm(context))) {
            Intent intent = new Intent(context, DetalheClienteActivity.class);
            intent.putExtra(SolicitacaoAtendimentoMessage.class.getSimpleName(), solicitacao);
            return new MessageIntentInfo(intent, true, R.string.app_name, R.string.nova_solicitacao_de_antendimento);
        } else {
            Intent intent = new Intent(context, DetalhePrestadorActivity.class);
            intent.putExtra(SolicitacaoAtendimentoMessage.class.getSimpleName(), solicitacao);
            return new MessageIntentInfo(intent, true, null, (int) -solicitacao.getCodigoSolicitacao(),
                    R.string.app_name, R.string.nova_resposta);
        }
    }

}
