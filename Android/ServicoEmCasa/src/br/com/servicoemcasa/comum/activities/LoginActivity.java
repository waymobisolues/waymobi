package br.com.servicoemcasa.comum.activities;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.activities.CadastroClienteActivity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.AutorizacaoCadastro;
import br.com.servicoemcasa.prestador.services.LoginService;

public class LoginActivity extends ServicoEmCasaBaseActivity {

    private static final int CADASTRO_CLIENTE = 1;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.login_activity_layout);
        setResult(RESULT_CANCELED);
    }

    public void novo(View view) {
        Intent intent = new Intent(this, CadastroClienteActivity.class);
        startActivityForResult(intent, CADASTRO_CLIENTE);
    }

    public void login(View view) {
        LoginService loginService = new LoginService(this);
        loginService.login(getEditTextText(R.id.email), new Callback<SimpleCallback<AutorizacaoCadastro>>() {
            @Override
            public void callback(SimpleCallback<AutorizacaoCadastro> param) {
                if (param.errorCode != 0) {
                    showError(param.errorCode);
                    return;
                }
                if (param.object == null) {
                    showAlert(R.string.email_nao_encontrado);
                } else if (param.object.isLiberado()) {
                    vaiParaEnderecoActivity();
                } else {
                    showAlert(R.string.email_autorizacao, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case CADASTRO_CLIENTE:
            if (resultCode == RESULT_FIRST_USER) {
                vaiParaEnderecoActivity();
            }
            break;
        }
    }
    
    private void vaiParaEnderecoActivity() {
        Intent intent = new Intent(this, EnderecoActivity.class);
        startActivity(intent);
        finish();
    }

}
