package br.com.servicoemcasa.comum.activities;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import br.com.servicoemcasa.ServicoEmCasaBaseListaActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.GrupoTipoPrestador;
import br.com.servicoemcasa.model.LocalAtendimento;
import br.com.servicoemcasa.services.GrupoTipoPrestadorService;

import com.google.android.gms.maps.model.LatLng;

public class GrupoTipoPrestadorActivity extends ServicoEmCasaBaseListaActivity<GrupoTipoPrestador> {

    private static final int CADASTRO_TIPO_PRESTADOR = 1;
    private Bundle savedInstance;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        this.savedInstance = getIntent().getExtras();
        loadGrupoTipoPrestador();
        setResult(RESULT_CANCELED);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.savedInstance = savedInstanceState;
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putAll(savedInstance);
        super.onSaveInstanceState(outState);
    }

    private void loadGrupoTipoPrestador() {
        LocalAtendimento localAtendimento = (LocalAtendimento) getIntent().getExtras().getSerializable("localServico");
        double latitude = getIntent().getExtras().getDouble("latitude", 0);
        double longitude = getIntent().getExtras().getDouble("longitude", 0);
        GrupoTipoPrestadorService service = new GrupoTipoPrestadorService(this);
        service.listaGrupoTipoPrestador(new LatLng(latitude, longitude), localAtendimento,
                new Callback<SimpleCallback<List<GrupoTipoPrestador>>>() {
                    @Override
                    public void callback(SimpleCallback<List<GrupoTipoPrestador>> param) {
                        if (param.errorCode == 0) {
                            loadGrupo(param.object);
                        } else {
                            showError(param.errorCode);
                        }
                    }

                });
    }

    @SuppressLint("InflateParams")
    private void loadGrupo(List<GrupoTipoPrestador> object) {
        setListaOpcoes(new OpcaoSimplesConfig<GrupoTipoPrestador>() {
            @Override
            public void onClick(GrupoTipoPrestador obj) {
                if (obj != null) {
                    vaiParaTipoPrestador(obj);
                } else {
                    vaiParaConsultaRegiao();
                }
            }

            @Override
            public String getText(GrupoTipoPrestador obj) {
                return obj.getDescricaoGrupo();
            }

            @Override
            public boolean isValid(GrupoTipoPrestador obj) {
                return true;
            }

            @Override
            public String getEmptyMessage() {
                return getResources().getString(R.string.nenhum_prestador_cadastrado_na_regiao);
            }
        }, object, null);
    }

    protected void vaiParaConsultaRegiao() {
        openBrowser("consulta_regiao_atendimento.html");
    }

    protected void vaiParaTipoPrestador(GrupoTipoPrestador grupo) {
        @SuppressWarnings("unchecked")
        Class<Activity> tipoPrestadorClass = (Class<Activity>) savedInstance.get("tipoPrestadorActivity");
        Intent intent = new Intent(this, tipoPrestadorClass);
        savedInstance.putSerializable("grupoTipoPrestador", grupo);
        intent.putExtras(savedInstance);
        super.startActivityForResult(intent, CADASTRO_TIPO_PRESTADOR);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
        case CADASTRO_TIPO_PRESTADOR:
            if (resultCode == RESULT_OK) {
                Intent data = new Intent();
                data.putExtras(savedInstance);
                if (intent != null && intent.getExtras() != null) {
                    data.putExtras(intent.getExtras());
                }
                setResult(RESULT_OK, data);
                finish();
                break;
            }
        }
    }
}
