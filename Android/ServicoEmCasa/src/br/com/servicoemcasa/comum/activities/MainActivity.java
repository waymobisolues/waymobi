package br.com.servicoemcasa.comum.activities;

import android.content.Intent;
import android.os.Bundle;
import br.com.servicoemcasa.BaseApplication;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.Cliente;

public class MainActivity extends ServicoEmCasaBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        inicia();
    }

    private void inicia() {
        ((BaseApplication) getApplication()).checkVersion(this, new Callback<SimpleCallback<Void>>() {
            @Override
            public void callback(SimpleCallback<Void> param) {
                if (param.errorCode != 0) {
                    showLongToast(R.string.erro_ao_verificar_versao);
                }
                checkCliente();
            }
        });
    }

    private void checkCliente() {
        getCliente(new Callback<Cliente>() {
            @Override
            public void callback(Cliente param) {
                if (param != null) {
                    vaiParaEnderecoActivity();
                } else {
                    vaiParaLogin();
                }
            }
        });
    }

    private void vaiParaEnderecoActivity() {
        Intent intent = new Intent(this, EnderecoActivity.class);
        startActivity(intent);
        finish();
    }

    private void vaiParaLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}
