package br.com.servicoemcasa.comum.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.activities.CadastroClienteActivity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.TipoPrestador;
import br.com.servicoemcasa.prestador.activities.CadastroPrestadorActivity;

public class ConfiguracaoActivity extends ServicoEmCasaBaseActivity {

    private Prestador prestador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configuracao_activity_layout);
    }

    @Override
    protected void onResume() {
        loadCliente();
        loadPrestador();
        super.onResume();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        prestador = (Prestador) savedInstanceState.getSerializable(Prestador.class.getSimpleName());
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Prestador.class.getSimpleName(), prestador);
        super.onSaveInstanceState(outState);
    }

    private void loadPrestador() {
        getPrestador(new Callback<Prestador>() {
            @Override
            public void callback(Prestador param) {
                if (param != null) {
                    FrameLayout frame = findFrameLayout(R.id.prestador);
                    frame.removeAllViews();
                    ConfiguracaoActivity.this.prestador = param;
                    View prestador = LayoutInflater.from(ConfiguracaoActivity.this).inflate(
                            R.layout.configuracao_prestador_layout, frame);
                    loadPrestadorData(param, prestador);
                }
            }
        });
    }

    protected void loadPrestadorData(Prestador prestador, View view) {
        findTextView(view, R.id.nome_fantasia).setText(prestador.getNomeFantasia());
        findTextView(view, R.id.telefone)
                .setText(
                        String.format("(%s) %s", prestador.getTelefone().getDDD(), prestador.getTelefone()
                                .getNumeroTelefone()));
        findTextView(view, R.id.email).setText(prestador.getEmail().getEmail());
        findTextView(view, R.id.distancia).setText(
                String.format("%d Km", prestador.getRegiaoAtendimento().getMaximaDistanciaAtendimento()));

        TextView tituloGrupoServico = findTextView(view, R.id.grupo_servico);
        tituloGrupoServico.setText(tituloGrupoServico.getText()
                + String.format(" (%s)", prestador.getTipo().get(0).getGrupoTipoPrestador().getDescricaoGrupo()));
        LinearLayout listaServico = (LinearLayout) view.findViewById(R.id.lista_servico);
        for (TipoPrestador tipo : prestador.getTipo()) {
            TextView servico = new TextView(this);
            servico.setText(tipo.getDescricao());
            servico.setTextAppearance(this, android.R.style.TextAppearance_Medium);
            listaServico.addView(servico);
        }
    }

    private void loadCliente() {
        getCliente(new Callback<Cliente>() {
            @Override
            public void callback(Cliente param) {
                FrameLayout frame = findFrameLayout(R.id.cliente);
                frame.removeAllViews();
                View cliente = LayoutInflater.from(ConfiguracaoActivity.this).inflate(
                        R.layout.configuracao_cliente_layout, frame);
                loadClienteData(param, cliente);
            }
        });
    }

    protected void loadClienteData(Cliente cliente, View view) {
        findTextView(view, R.id.nome).setText(cliente.getNome());
        findTextView(view, R.id.telefone).setText(
                String.format("(%s) %s", cliente.getTelefone().getDDD(), cliente.getTelefone().getNumeroTelefone()));
        findTextView(view, R.id.email).setText(cliente.getEmail().getEmail());
    }

    public void alterarCliente(View view) {
        Intent intent = new Intent(this, CadastroClienteActivity.class);
        startActivity(intent);
    }

    public void alterarPrestador(View view) {
        Intent intent = new Intent(this, CadastroPrestadorActivity.class);
        intent.putExtra(Prestador.class.getSimpleName(), prestador);
        startActivity(intent);
    }

}
