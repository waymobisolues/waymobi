package br.com.servicoemcasa.comum.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.fragments.MarkerMapFragment;
import br.com.servicoemcasa.infra.AddressDescription;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.util.ActivityUtils;
import br.com.servicoemcasa.util.MapUtils;

import com.google.android.gms.maps.model.LatLng;

public class EnderecoManualActivity extends ServicoEmCasaBaseActivity {

    private double latitude, longitude;
    private String endereco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.endereco_manual_activity_layout);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_mapa, new MarkerMapFragment()).commit();
        setResult(RESULT_CANCELED);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putDouble("latitude", latitude);
        outState.putDouble("longitude", longitude);
        outState.putString("endereco", endereco);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        latitude = savedInstanceState.getDouble("latitude");
        longitude = savedInstanceState.getDouble("longitude");
        endereco = savedInstanceState.getString(endereco);
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void buscar(View view) {
        EditText txtEndereco = (EditText) findViewById(R.id.endereco);
        String endereco = txtEndereco.getText().toString();
        if (endereco.length() == 0) {
            Toast.makeText(this, R.string.selecione_o_endereco, Toast.LENGTH_SHORT).show();
        } else {
            ActivityUtils.hideKeyboard(this);
            carregaEndereco(endereco);
        }
    }

    public void confirmar(View view) {
        vaiParaEndereco();
    }

    private void vaiParaEndereco() {
        Intent data = new Intent();
        data.putExtra("latitude", latitude);
        data.putExtra("longitude", longitude);
        data.putExtra("addressDescription", endereco);
        setResult(RESULT_OK, data);
        finish();
    }

    private void carregaEndereco(String endereco) {
        MapUtils.getAddressDescription(this, endereco, new Callback<AddressDescription>() {
            @Override
            public void callback(AddressDescription param) {
                findViewById(R.id.frame_mapa).setVisibility(View.VISIBLE);
                findViewById(R.id.btn_continuar).setVisibility(View.VISIBLE);
                EnderecoManualActivity.this.latitude = param.latlng.latitude;
                EnderecoManualActivity.this.longitude = param.latlng.longitude;
                EnderecoManualActivity.this.endereco = param.addressDescription;
                loadMap();
            }
        });
    }

    private void loadMap() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        MarkerMapFragment markerMapFragment = (MarkerMapFragment) supportFragmentManager
                .findFragmentById(R.id.frame_mapa);
        markerMapFragment.selectLocation(new LatLng(latitude, longitude));
    }

}
