package br.com.servicoemcasa.comum.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import br.com.servicoemcasa.ServicoEmCasaBaseListaActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.activities.DetalhePrestadorActivity;
import br.com.servicoemcasa.model.SolicitacaoAtendimento;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.prestador.activities.DetalheClienteActivity;

public class ListaPrestadoresActivity extends ServicoEmCasaBaseListaActivity<SolicitacaoAtendimentoPrestador> {

    private static final int DETALHE_PRESTADOR_ACTIVITY = 1;
    private SolicitacaoAtendimento solicitacaoAtendimento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carregaLista();
        setResult(RESULT_CANCELED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case DETALHE_PRESTADOR_ACTIVITY:
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
            break;
        }
    }

    private void carregaLista() {
        solicitacaoAtendimento = (SolicitacaoAtendimento) getIntent().getExtras().getSerializable(
                SolicitacaoAtendimento.class.getSimpleName());
        if (solicitacaoAtendimento.getPrestadores().size() == 0) {
            super.setResult(RESULT_FIRST_USER);
            finish();
        } else {
            setListaOpcoes(new OpcaoLayoutConfig<SolicitacaoAtendimentoPrestador>() {
                @Override
                public void onClick(SolicitacaoAtendimentoPrestador prestador) {
                    if (prestador != null) {
                        vaiParaDetalhePrestador(prestador);
                    }
                }

                @SuppressLint("InflateParams")
                @Override
                public View getView(LayoutInflater inflater, SolicitacaoAtendimentoPrestador obj) {
                    View view = inflater.inflate(R.layout.lista_prestador_adapter_layout, null);
                    TextView pessoa = findTextView(view, R.id.nome);
                    String resposta = getResources().getString(
                            obj.isAceito() ? R.string.com_resposta : R.string.sem_resposta);
                    pessoa.setText(Html.fromHtml(String.format("<b>%s</b><br/>%s",
                            obj.getPrestador().getNomeFantasia(), resposta)));
                    findRatingBar(view, R.id.pontuacao).setRating(obj.getPrestador().getAvaliacao().getPontuacao());
                    return view;
                }

                @Override
                public boolean isValid(SolicitacaoAtendimentoPrestador obj) {
                    return true;
                }

                @Override
                public String getEmptyMessage() {
                    return getResources().getString(R.string.nenhum_prestador_encontrado);
                }
            }, solicitacaoAtendimento.getPrestadores(), null);
        }
    }

    private void vaiParaDetalhePrestador(SolicitacaoAtendimentoPrestador prestador) {
        Intent intent;
        if (getIntent().getExtras().getBoolean("ofertados")) {
            intent = new Intent(this, DetalheClienteActivity.class);
        } else {
            intent = new Intent(this, DetalhePrestadorActivity.class);
        }
        intent.putExtra(SolicitacaoAtendimentoPrestador.class.getSimpleName(), prestador);
        startActivityForResult(intent, DETALHE_PRESTADOR_ACTIVITY);
    }

}
