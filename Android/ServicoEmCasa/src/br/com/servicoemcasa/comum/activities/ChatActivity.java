package br.com.servicoemcasa.comum.activities;

import java.util.Date;
import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ScrollView;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.activities.DetalhePrestadorActivity;
import br.com.servicoemcasa.comum.helper.SolicitacaoAtendimentoPrestadorHelper;
import br.com.servicoemcasa.comum.helper.SolicitacaoAtendimentoPrestadorHelper.SolicitacaoAtendimentoPrestadorCallback;
import br.com.servicoemcasa.comum.services.MensagemService;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Conversa;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.message.ConversaMessage;
import br.com.servicoemcasa.prestador.activities.DetalheClienteActivity;
import br.com.servicoemcasa.prestador.services.PrestadorService;
import br.com.servicoemcasa.services.GcmService;
import br.com.servicoemcasa.util.StringUtils;
import br.com.servicoemcasa.widget.Cartao;

public class ChatActivity extends ServicoEmCasaBaseActivity implements SolicitacaoAtendimentoPrestadorCallback {

    private SolicitacaoAtendimentoPrestador prestador;
    private LocalBroadcastManager localBroadcastManager;
    private ChatBroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.chat_activity_layout);
        loadPrestador();
    }

    class ChatBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConversaMessage conversa = (ConversaMessage) intent.getExtras().get(ConversaMessage.class.getSimpleName());
            if (prestador.getCodigo() == conversa.getCodigoSolicitacaoPrestador()) {
                NotificationManager notificationManager = (NotificationManager) ChatActivity.this
                        .getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(intent.getExtras().getInt("notificationId"));
                loadMensagens();
            }
        }

    }

    private void startLocalBroadcast() {
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastReceiver = new ChatBroadcastReceiver();
        localBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(ChatActivity.class.getName()));
    }

    private void loadPrestador() {
        Bundle extras = getIntent().getExtras();
        if (extras.containsKey(ConversaMessage.class.getSimpleName())) {
            ConversaMessage conversa = (ConversaMessage) extras.get(ConversaMessage.class.getSimpleName());
            new SolicitacaoAtendimentoPrestadorHelper().carregaDadosSolicitacaoPrestador(this,
                    conversa.getCodigoSolicitacaoPrestador(), this);
        } else {
            carregaDetalhePrestador((SolicitacaoAtendimentoPrestador) getIntent().getExtras().getSerializable(
                    SolicitacaoAtendimentoPrestador.class.getSimpleName()));
        }
    }

    @Override
    public void carregaDetalhePrestador(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        this.prestador = solicitacaoAtendimentoPrestador;
        loadParticipante();
        loadMensagens();
    }

    private void loadParticipante() {
        isCliente(new Callback<Boolean>() {
            @Override
            public void callback(Boolean cliente) {
                if (cliente) {
                    loadParticipantePrestador();
                } else {
                    loadParticipanteCliente();
                }
            }
        });

    }

    private void isCliente(final Callback<Boolean> callback) {
        getCliente(new Callback<Cliente>() {
            @Override
            public void callback(Cliente param) {
                callback.callback(prestador.getSolicitacaoAtendimento().getCliente().getGcm().equals(param.getGcm()));
            }
        });
    }

    private void loadParticipanteCliente() {
        findTextView(R.id.nome_participante).setText(prestador.getSolicitacaoAtendimento().getCliente().getNome());
        findImageView(R.id.foto_prestador).setVisibility(View.GONE);
    }

    private void loadParticipantePrestador() {
        findTextView(R.id.nome_participante).setText(prestador.getPrestador().getNomeFantasia());
        PrestadorService prestadorService = new PrestadorService(this);
        prestadorService.recuperaFotoPretador(prestador.getPrestador(), new Callback<Bitmap>() {
            @Override
            public void callback(Bitmap param) {
                findImageView(R.id.foto_prestador).setImageBitmap(param);
            }
        });
    }

    @Override
    protected void onResume() {
        startLocalBroadcast();
        startListeners();
        super.onResume();
    }

    @Override
    protected void onPause() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onRestart() {
        findEditText(R.id.dummy).requestFocus();
        mostraCartaoPrestador();
        super.onRestart();
    }

    private void startListeners() {
        findEditText(R.id.mensagem).setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    escondeCartaoPrestador();
                } else {
                    mostraCartaoPrestador();
                }
                mostraUltimaMensagem();
            }
        });
    }

    private void loadMensagens() {
        new MensagemService(this).listaMensagens(prestador, new Callback<SimpleCallback<List<Conversa>>>() {
            @Override
            public void callback(SimpleCallback<List<Conversa>> param) {
                if (param.errorCode == 0) {
                    preencheConversa(param.object);
                } else {
                    showError(param.errorCode);
                }
            }

        });
    }

    private void preencheConversa(List<Conversa> conversas) {
        if (conversas.size() == 0) {
            preencheConversaVazia();
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (Conversa conversa : conversas) {
            String s = getMensagem(conversa);
            sb.append(s);
        }
        findTextView(R.id.texto_mensagem).setText(Html.fromHtml(sb.toString()));
        mostraUltimaMensagem();
    }

    private String getMensagem(Conversa conversa) {
        String remetente = conversa.getRemetente().equals(prestador.getPrestador().getGcm()) ? prestador.getPrestador()
                .getNomeFantasia() : prestador.getSolicitacaoAtendimento().getCliente().getNome();
        String s = String.format("<p><b><font color=\"blue\">%s - %s</font></b><br/>%s</p>",
                StringUtils.toDateTimeString(conversa.getData()), remetente, conversa.getMensagem());
        return s;
    }

    private void mostraUltimaMensagem() {
        final ScrollView mensagens = findScrollView(R.id.mensagens);
        mensagens.postDelayed(new Runnable() {
            @Override
            public void run() {
                mensagens.fullScroll(View.FOCUS_DOWN);
            }
        }, 200);
    }

    private void preencheConversaVazia() {
    }

    public void vaiParaSolicitacao(View view) {
        isCliente(new Callback<Boolean>() {
            @Override
            public void callback(Boolean cliente) {
                vaiParaDetalheSolicitacao(cliente ? DetalhePrestadorActivity.class : DetalheClienteActivity.class);
            }
        });
    }

    protected void vaiParaDetalheSolicitacao(Class<?> activityClass) {
        Intent intent = new Intent(this, activityClass);
        intent.putExtra(SolicitacaoAtendimentoPrestador.class.getSimpleName(), this.prestador);
        startActivity(intent);

    }

    public void enviar(View view) {
        String mensagem = getEditTextText(R.id.mensagem);
        if (mensagem.trim().length() == 0) {
            showToast("Digite uma mensagem para enviar");
        } else {
            criaConversa(mensagem, new Callback<Conversa>() {
                @Override
                public void callback(final Conversa conversa) {
                    new MensagemService(ChatActivity.this).enviaMensagem(conversa,
                            new Callback<SimpleCallback<List<Conversa>>>() {
                                @Override
                                public void callback(SimpleCallback<List<Conversa>> param) {
                                    if (param.errorCode != 0) {
                                        showError(param.errorCode);
                                        return;
                                    }
                                    findEditText(R.id.mensagem).setText("");
                                    preencheConversa(param.object);
                                }
                            });
                }
            });
        }
    }

    private void criaConversa(final String mensagem, final Callback<Conversa> callback) {
        new GcmService(this).getGcm(new Callback<SimpleCallback<Gcm>>() {
            @Override
            public void callback(SimpleCallback<Gcm> param) {
                if (param.errorCode != 0) {
                    showError(param.errorCode);
                    return;
                }
                Conversa conversa = new Conversa().setRemetente(param.object).setMensagem(mensagem)
                        .setSolicitacaoAtendimentoPrestador(prestador).setData(new Date());
                callback.callback(conversa);
            }
        });
    }

    private void mostraCartaoPrestador() {
        final Cartao prestador = findCartao(R.id.prestador);
        prestador.setVisibility(View.VISIBLE);
        prestador.setAlpha(0);
        prestador.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                prestador.setAlpha(1);
                prestador.setVisibility(View.VISIBLE);
            }
        }).setDuration(500).alpha(1).y(-prestador.getHeight()).translationY(0);
    }

    private void escondeCartaoPrestador() {
        final Cartao prestador = findCartao(R.id.prestador);
        prestador.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                prestador.setVisibility(View.GONE);
            }
        }).setDuration(500).alpha(0).y(0).translationY(-prestador.getHeight());
    }

}
