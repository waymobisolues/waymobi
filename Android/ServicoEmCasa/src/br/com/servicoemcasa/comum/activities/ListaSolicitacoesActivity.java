package br.com.servicoemcasa.comum.activities;

import java.util.Comparator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.Switch;
import br.com.servicoemcasa.ServicoEmCasaBaseListaActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.activities.DetalhePrestadorActivity;
import br.com.servicoemcasa.comum.services.SolicitacaoAtendimentoService;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimento;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.prestador.activities.DetalheClienteActivity;

public class ListaSolicitacoesActivity extends ServicoEmCasaBaseListaActivity<SolicitacaoAtendimento> {

    private static final int LISTA_DE_PRESTADORES_ACTIVITY = 1;

    private Cliente cliente;
    private Prestador prestador;
    private boolean ofertados;
    private boolean agendados = true;

    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        getPrestador(new Callback<Prestador>() {
            @Override
            public void callback(final Prestador param) {
                prestador = param;
                ofertados = param != null;
                getCliente(new Callback<Cliente>() {
                    @Override
                    public void callback(final Cliente param) {
                        cliente = param;
                        listaSolicitacoes();
                    }
                });
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("cliente", cliente);
        outState.putSerializable("prestador", prestador);
        outState.putBoolean("ofertados", ofertados);
        outState.putBoolean("agendados", agendados);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        cliente = (Cliente) savedInstanceState.getSerializable("cliente");
        prestador = (Prestador) savedInstanceState.getSerializable("prestador");
        ofertados = savedInstanceState.getBoolean("ofertados");
        agendados = savedInstanceState.getBoolean("agendados");
    }

    public void somenteAgendados(View view) {
        agendados = ((Switch) view).isChecked();
        listaSolicitacoes();
    }

    private void listaSolicitacoes() {
        SolicitacaoAtendimentoService service = new SolicitacaoAtendimentoService(ListaSolicitacoesActivity.this);
        if (ofertados) {
            listaSolicitacoesOfertadas(service);
        } else {
            listaSolicitacoesSolicitadas(service);
        }
    }

    private void listaSolicitacoesSolicitadas(SolicitacaoAtendimentoService service) {
        service.listaSolicitacoes(cliente, new Callback<SimpleCallback<List<SolicitacaoAtendimento>>>() {
            @Override
            public void callback(SimpleCallback<List<SolicitacaoAtendimento>> param) {
                if (param.errorCode == 0) {
                    preparaLista(param.object);
                } else {
                    showError(param.errorCode);
                }
            }

        });
    }

    private void listaSolicitacoesOfertadas(SolicitacaoAtendimentoService service) {
        service.listaSolicitacoes(prestador, new Callback<SimpleCallback<List<SolicitacaoAtendimento>>>() {
            @Override
            public void callback(SimpleCallback<List<SolicitacaoAtendimento>> param) {
                if (param.errorCode == 0) {
                    preparaLista(param.object);
                } else {
                    showError(param.errorCode);
                }
            }

        });
    }

    private void preparaLista(List<SolicitacaoAtendimento> param) {
        limpaCartoes();
        adicionaCartaoAgendamento();
        preparaListaSeSolicitacoes(param);
    }

    private void adicionaCartaoAgendamento() {
        super.adicionaCartao(new OpcaoLayoutConfig<SolicitacaoAtendimento>() {
            @Override
            public void onClick(SolicitacaoAtendimento obj) {
                return;
            }

            @Override
            @SuppressLint("InflateParams")
            public View getView(LayoutInflater inflater, SolicitacaoAtendimento obj) {
                View view = inflater.inflate(R.layout.lista_agendamentos_layout, null);
                findSwitch(view, R.id.btn_confirmados).setChecked(agendados);
                Spinner tipoSolicitacao = findSpinner(view, R.id.tipo_solicitacao);
                if (prestador == null) {
                    tipoSolicitacao.setVisibility(View.GONE);
                } else {
                    tipoSolicitacao.setSelection(ofertados ? 0 : 1);
                    tipoSolicitacao.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            boolean ofertados = position == 0;
                            if (ofertados != ListaSolicitacoesActivity.this.ofertados) {
                                ListaSolicitacoesActivity.this.ofertados = ofertados;
                                listaSolicitacoes();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }
                return view;
            }

            @Override
            public boolean isValid(SolicitacaoAtendimento obj) {
                return true;
            }

            @Override
            public String getEmptyMessage() {
                return getResources().getString(R.string.nenhum_agendamento_encontrado);
            }
        }, null);
    }

    private void preparaListaSeSolicitacoes(List<SolicitacaoAtendimento> param) {
        super.setListaOpcoes(new OpcaoSimplesConfig<SolicitacaoAtendimento>() {
            @Override
            public void onClick(SolicitacaoAtendimento solicitacao) {
                if (solicitacao != null) {
                    vaiParaDetalheSolicitacao(solicitacao);
                }
            }

            @Override
            public String getText(SolicitacaoAtendimento solicitacao) {
                return String.format(getResources().getString(R.string.data_e_descricao_solicitacao),
                        solicitacao.getDataInclusao(), solicitacao.getDataInclusao(), solicitacao.getDataInclusao(),
                        solicitacao.getDataInclusao(), solicitacao.getTipoPrestador().getDescricao())
                        + (solicitacao.getAgendamento() != null ? String.format(
                                getResources().getString(R.string.agendado_para), solicitacao.getData(),
                                solicitacao.getData(), solicitacao.getData(), solicitacao.getData())
                                : (existeAceito(solicitacao.getPrestadores()) ? getResources().getString(
                                        R.string.com_resposta) : getResources().getString(R.string.sem_resposta)));
            }

            @Override
            public boolean isValid(SolicitacaoAtendimento obj) {
                boolean valido = (agendados && obj.getAgendamento() != null) || !agendados;
                return valido;
            }

            @Override
            public String getEmptyMessage() {
                return getResources().getString(R.string.nenhuma_solicitacao_de_atendimento);
            }
        }, param, new Comparator<SolicitacaoAtendimento>() {
            @Override
            public int compare(SolicitacaoAtendimento lhs, SolicitacaoAtendimento rhs) {
                return rhs.getData().compareTo(lhs.getData());
            }
        }, false);
    }

    protected boolean existeAceito(List<SolicitacaoAtendimentoPrestador> prestadores) {
        for (SolicitacaoAtendimentoPrestador prestador : prestadores) {
            if (prestador.isAceito()) {
                return true;
            }
        }
        return false;
    }

    private void vaiParaDetalheSolicitacao(SolicitacaoAtendimento solicitacao) {
        if (ofertados) {
            vaiParaDetalheCliente(solicitacao);
        } else {
            if (solicitacao.getAgendamento() != null) {
                vaiParaDetalhePrestador(solicitacao);
            } else {
                vaiParaListaPrestador(solicitacao);
            }
        }
    }

    private void vaiParaDetalhePrestador(SolicitacaoAtendimento solicitacao) {
        Intent intent = new Intent(this, DetalhePrestadorActivity.class);
        intent.putExtra(SolicitacaoAtendimentoPrestador.class.getSimpleName(), solicitacao.getAgendamento()
                .getSolicitacaoAtendimentoPrestador());
        startActivity(intent);
    }

    private void vaiParaListaPrestador(SolicitacaoAtendimento solicitacao) {
        Intent intent = new Intent(this, ListaPrestadoresActivity.class);
        intent.putExtra(SolicitacaoAtendimento.class.getSimpleName(), solicitacao);
        intent.putExtra("ofertados", ofertados);
        startActivityForResult(intent, LISTA_DE_PRESTADORES_ACTIVITY);
    }

    private void vaiParaDetalheCliente(SolicitacaoAtendimento solicitacao) {
        Intent intent = new Intent(this, DetalheClienteActivity.class);
        SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador = null;
        for (SolicitacaoAtendimentoPrestador p : solicitacao.getPrestadores()) {
            if (p.getPrestador().equals(prestador)) {
                solicitacaoAtendimentoPrestador = p;
            }
        }
        intent.putExtra(SolicitacaoAtendimentoPrestador.class.getSimpleName(), solicitacaoAtendimentoPrestador);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case LISTA_DE_PRESTADORES_ACTIVITY:
            switch (resultCode) {
            case RESULT_FIRST_USER:
                showAlert(R.string.nenhum_prestador_encontrado);
                break;
            }
            break;
        }
    }
}
