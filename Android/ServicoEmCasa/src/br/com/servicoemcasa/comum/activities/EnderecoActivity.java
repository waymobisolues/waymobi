package br.com.servicoemcasa.comum.activities;

import static br.com.servicoemcasa.model.LocalAtendimento.Casa;
import static br.com.servicoemcasa.model.LocalAtendimento.Estabelecimento;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.activities.TipoPrestadorActivity;
import br.com.servicoemcasa.cliente.dialogs.EscolhaLocalServico;
import br.com.servicoemcasa.fragments.LocationAwareMapFragment;
import br.com.servicoemcasa.infra.AddressDescription;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.LocationAware;
import br.com.servicoemcasa.infra.MapService;
import br.com.servicoemcasa.model.Endereco;
import br.com.servicoemcasa.model.LocalAtendimento;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.prestador.activities.PrestadorActivity;

import com.google.android.gms.maps.model.LatLng;

public class EnderecoActivity extends ServicoEmCasaBaseActivity implements LocationAware {

    private static final int ENDERECO_MANUAL_ACTIVITY = 1;
    private LatLng latlng;
    private MapService mapService;
    private LocationAwareMapFragment locationAwareMapFragment;
    private Endereco endereco;
    private EscolhaLocalServico escolhaLocalServico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.disableDisplayHome();
        setContentView(R.layout.endereco_activity_layout);
        loadMap();
    }

    @Override
    protected void onResume() {
        invalidateOptionsMenu();
        super.onResume();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.endereco = (Endereco) savedInstanceState.getSerializable("endereco");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("endereco", endereco);
        super.onSaveInstanceState(outState);
    }

    public void confirmar(View view) {
        if (latlng == null) {
            Toast.makeText(this, R.string.endereco_nao_encontrado, Toast.LENGTH_SHORT).show();
            return;
        }
        this.endereco = new Endereco().setDescricao(((EditText) findViewById(R.id.endereco)).getText().toString())
                .setLatitude(latlng.latitude).setLongitude(latlng.longitude);
        escolhaLocalServico(endereco);
    }

    private void escolhaLocalServico(final Endereco endereco) {
        this.escolhaLocalServico = new EscolhaLocalServico();
        this.escolhaLocalServico.show(getFragmentManager(), "");
    }

    public void estabelecimento(View view) {
        vaiParaTipoServico(Estabelecimento);
    }

    public void casa(View view) {
        vaiParaTipoServico(Casa);
    }

    private void vaiParaTipoServico(LocalAtendimento localServico) {
        this.escolhaLocalServico.dismiss();
        Intent intent = new Intent(this, GrupoTipoPrestadorActivity.class);
        intent.putExtra("tipoPrestadorActivity", TipoPrestadorActivity.class);
        intent.putExtra("latitude", endereco.getLatitude());
        intent.putExtra("longitude", endereco.getLongitude());
        intent.putExtra("endereco", endereco);
        intent.putExtra("localServico", localServico);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case ENDERECO_MANUAL_ACTIVITY:
            if (resultCode == RESULT_OK) {
                double latitude = data.getExtras().getDouble("latitude");
                double longitude = data.getExtras().getDouble("longitude");
                String addressDescription = data.getExtras().getString("addressDescription");
                setAddressDescription(new AddressDescription(new LatLng(latitude, longitude), addressDescription));
            }
            break;
        }
    }

    private void vaiParaListaDeSolicitacoes() {
        Intent intent = new Intent(this, ListaSolicitacoesActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menu_solicitacoes:
            vaiParaListaDeSolicitacoes();
            break;
        case R.id.menu_prestador:
            vaiParaPrestador();
            break;
        case R.id.menu_configuracoes:
            vaiParaConfiguracoes();
            break;
        default:
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void vaiParaConfiguracoes() {
        Intent intent = new Intent(this, ConfiguracaoActivity.class);
        startActivity(intent);
    }

    private void vaiParaPrestador() {
        Intent intent = new Intent(this, PrestadorActivity.class);
        startActivity(intent);
    }

    public void selecionarEndereco(View view) {
        vaiParaEnderecoManual();
    }

    private void vaiParaEnderecoManual() {
        Intent intent = new Intent(this, EnderecoManualActivity.class);
        startActivityForResult(intent, ENDERECO_MANUAL_ACTIVITY);
    }

    public void setAddressDescription(AddressDescription addressDescription) {
        EditText endereco = (EditText) findViewById(R.id.endereco);
        endereco.setText(addressDescription.addressDescription);
        this.latlng = addressDescription.latlng;
        locationAwareMapFragment.setManualSelection(addressDescription);
    }

    private void loadMap() {
        locationAwareMapFragment = new LocationAwareMapFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.frame_mapa, locationAwareMapFragment).commit();
    }

    private MapService getMapService() {
        return (mapService = mapService == null ? new MapService(this) : mapService);
    }

    @Override
    public void setLatLng(final LatLng latlng) {
        getMapService().getAddressFromLatLng(latlng, new Callback<AddressDescription>() {
            @Override
            public void callback(AddressDescription address) {
                EditText editText = (EditText) findViewById(R.id.endereco);
                if (address == null) {
                    editText.setText("");
                    Toast.makeText(EnderecoActivity.this, getResources().getString(R.string.endereco_nao_encontrado),
                            Toast.LENGTH_SHORT).show();
                } else {
                    editText.setText(address.addressDescription);
                }
                EnderecoActivity.this.latlng = latlng;
            }
        });
    }

    @Override
    public void setLatLng(MapService mapService, LatLng latlng) {
        this.mapService = mapService;
        setLatLng(latlng);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.principal, menu);
        getPrestador(new Callback<Prestador>() {
            @Override
            public void callback(Prestador param) {
                MenuItem item = menu.findItem(R.id.menu_prestador);
                item.setVisible(param == null);
            }
        });
        return true;
    }

}
