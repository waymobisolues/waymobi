package br.com.servicoemcasa.comum.helper;

import br.com.servicoemcasa.activities.BaseActivity;
import br.com.servicoemcasa.comum.services.SolicitacaoAtendimentoPrestadorService;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;

public class SolicitacaoAtendimentoPrestadorHelper {

    public static interface SolicitacaoAtendimentoPrestadorCallback {
        void carregaDetalhePrestador(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador);
    }

    public void carregaDadosSolicitacaoPrestador(final BaseActivity activity, long codigoSolicitacaoPrestador,
            final SolicitacaoAtendimentoPrestadorCallback callback) {

        SolicitacaoAtendimentoPrestadorService service = new SolicitacaoAtendimentoPrestadorService(activity);
        service.recuperaSolicitacao(codigoSolicitacaoPrestador,
                new Callback<SimpleCallback<SolicitacaoAtendimentoPrestador>>() {
                    @Override
                    public void callback(SimpleCallback<SolicitacaoAtendimentoPrestador> param) {
                        if (param.errorCode == 0) {
                            callback.carregaDetalhePrestador(param.object);
                        } else {
                            activity.showError(param.errorCode);
                        }
                    }
                });
    }

}
