package br.com.servicoemcasa;

import android.app.Activity;
import br.com.servicoemcasa.cliente.services.ClienteService;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.prestador.services.PrestadorService;

public class ServicoEmCasaApplication extends BaseApplication {

    public void getCliente(final Activity activity, final Callback<Cliente> callback) {
        ClienteService clienteService = new ClienteService(activity);
        clienteService.get(new Callback<SimpleCallback<Cliente>>() {
            @Override
            public void callback(SimpleCallback<Cliente> param) {
                if (param.errorCode == 0) {
                    callback.callback(param.object);
                } else {
                    callback.callback(null);
                }
            }
        });
    }

    public void getPrestador(final Activity activity, final Callback<Prestador> callback) {
        PrestadorService prestadorService = new PrestadorService(activity);
        prestadorService.recuperaPrestador(new Callback<SimpleCallback<Prestador>>() {
            @Override
            public void callback(SimpleCallback<Prestador> param) {
                if (param.errorCode == 0) {
                    callback.callback(param.object);
                } else {
                    callback.callback(null);
                }
            }
        });
    }

}
