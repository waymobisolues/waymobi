package br.com.servicoemcasa;

import br.com.servicoemcasa.activities.BaseActivity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Prestador;

public abstract class ServicoEmCasaBaseActivity extends BaseActivity {

    public void getCliente(final Callback<Cliente> callback) {
        ((ServicoEmCasaApplication) getApplication()).getCliente(this, callback);
    }

    public void getPrestador(final Callback<Prestador> callback) {
        ((ServicoEmCasaApplication) getApplication()).getPrestador(this, callback);
    }

}
