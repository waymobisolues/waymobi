package br.com.servicoemcasa.cliente.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.Store;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.services.GcmService;
import br.com.servicoemcasa.util.JsonParser;
import br.com.servicoemcasa.util.StringUtils;

public class ClienteService {

    private static final String PATH = "/Cliente/";
    private final WebClient webClient;
    private final GcmService gcmService;
    private final Store store;

    public ClienteService(Activity activity) {
        webClient = WebClient.newInstance(activity);
        gcmService = new GcmService(activity);
        store = new Store(activity, ClienteService.class);
    }

    public void get(final Callback<SimpleCallback<Cliente>> callback) {
        String json = store.getString("cliente");
        if (!StringUtils.isNullOrEmpty(json)) {
            callback.callback(new SimpleCallback<Cliente>(JsonParser.fromJson(json, Cliente.class)));
        } else {
            gcmService.getGcm(new Callback<SimpleCallback<Gcm>>() {
                @Override
                public void callback(SimpleCallback<Gcm> param) {
                    if (param.errorCode == 0) {
                        get(param.object, callback);
                    } else {
                        callback.callback(new SimpleCallback<Cliente>(param.errorCode, param.errorDescription));
                    }
                }
            });
        }
    }

    private void get(Gcm gcm, final Callback<SimpleCallback<Cliente>> callback) {
        webClient.get(PATH + webClient.encode(gcm.getHashedGcmKey()), new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode != HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<Cliente>(param.responseCode, param.getErrorDescription()));
                } else {
                    Cliente cliente = JsonParser.fromJson(param.responseBody, Cliente.class);
                    callback.callback(new SimpleCallback<Cliente>(cliente));
                }
            }
        });
    }

    private interface ClienteGcmCommand {
        void execute(Cliente cliente);
    }

    private void getGcm(final Cliente cliente, final Callback<SimpleCallback<Cliente>> callback,
            final ClienteGcmCommand command) {

        gcmService.getGcm(new Callback<SimpleCallback<Gcm>>() {
            @Override
            public void callback(SimpleCallback<Gcm> param) {
                if (param.errorCode != 0) {
                    callback.callback(new SimpleCallback<Cliente>(param.errorCode, param.errorDescription));
                    return;
                }
                cliente.setGcm(param.object);
                command.execute(cliente);
            }
        });
    }

    public void post(final Cliente cliente, final Callback<SimpleCallback<Cliente>> callback) {
        store.getEditor().remove("cliente").commit();
        getGcm(cliente, callback, new ClienteGcmCommand() {
            @Override
            public void execute(Cliente cliente) {
                webClient.post(PATH, cliente, new ClienteCallback(HttpStatus.SC_CREATED, callback));
            }
        });
    }

    private class ClienteCallback extends Callback<WebClient.WebResponse> {

        private Callback<SimpleCallback<Cliente>> callback;
        private int httpStatus;

        ClienteCallback(final int httpStatus, final Callback<SimpleCallback<Cliente>> callback) {
            this.httpStatus = httpStatus;
            this.callback = callback;
        }

        @Override
        public void callback(WebResponse param) {
            if (param.responseCode == httpStatus) {
                Cliente cliente = JsonParser.fromJson(param.responseBody, Cliente.class);
                if (cliente != null) {
                    store.getEditor().putString("cliente", param.responseBody).commit();
                }
                callback.callback(new SimpleCallback<Cliente>(cliente));
            } else {
                callback.callback(new SimpleCallback<Cliente>(param.responseCode, param.getErrorDescription()));
            }
        }
    }

    public void put(final Cliente cliente, final Callback<SimpleCallback<Cliente>> callback) {
        getGcm(cliente, callback, new ClienteGcmCommand() {
            @Override
            public void execute(Cliente cliente) {
                webClient.put(PATH + cliente.getCodigo(), cliente, new ClienteCallback(HttpStatus.SC_OK, callback));
            }
        });
    }

}
