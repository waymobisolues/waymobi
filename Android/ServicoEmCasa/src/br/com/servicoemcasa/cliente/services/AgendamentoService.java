package br.com.servicoemcasa.cliente.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;

public class AgendamentoService {

    private static final String PATH = "/Agendamento";
    private WebClient webClient;

    public AgendamentoService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void confirmaAtendimento(final SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador,
            final Callback<SimpleCallback<Boolean>> callback) {

        webClient.post(PATH, solicitacaoAtendimentoPrestador, new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_CREATED) {
                    callback.callback(new SimpleCallback<Boolean>(Boolean.TRUE));
                } else {
                    callback.callback(new SimpleCallback<Boolean>(param.responseCode, param.getErrorDescription()));
                }
            }
        });
    }
}
