package br.com.servicoemcasa.cliente.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;

public class AvaliacaoAtendimentoService {

    private static final String PATH = "/AvaliarAtendimento/";
    private WebClient webClient;

    public AvaliacaoAtendimentoService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void avaliarAtendimento(final long codigoAgendamento, final boolean positivo,
            final Callback<SimpleCallback<Boolean>> callback) {

        webClient.post(PATH + codigoAgendamento, positivo, new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<Boolean>(Boolean.TRUE));
                } else {
                    callback.callback(new SimpleCallback<Boolean>(param.responseCode, param.getErrorDescription()));
                }
            }
        });
    }
}
