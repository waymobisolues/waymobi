package br.com.servicoemcasa.cliente.activities;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import br.com.servicoemcasa.ServicoEmCasaBaseListaActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.GrupoTipoPrestador;
import br.com.servicoemcasa.model.LocalAtendimento;
import br.com.servicoemcasa.model.TipoPrestador;
import br.com.servicoemcasa.services.TipoPrestadorService;

import com.google.android.gms.maps.model.LatLng;

public class TipoPrestadorActivity extends ServicoEmCasaBaseListaActivity<TipoPrestador> {
    private static final int SOLICITACAO = 1;
    private Bundle savedInstance;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        this.savedInstance = getIntent().getExtras();
        loadTipoPrestador();
        setResult(RESULT_CANCELED);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.savedInstance = savedInstanceState;
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putAll(savedInstance);
        super.onSaveInstanceState(outState);
    }

    private void loadTipoPrestador() {
        GrupoTipoPrestador grupo = (GrupoTipoPrestador) getIntent().getExtras().getSerializable("grupoTipoPrestador");
        LocalAtendimento localAtendimento = (LocalAtendimento) getIntent().getExtras().getSerializable("localServico");
        double latitude = getIntent().getExtras().getDouble("latitude", 0);
        double longitude = getIntent().getExtras().getDouble("longitude", 0);
        TipoPrestadorService service = new TipoPrestadorService(this);
        service.listaTipoPrestador(new LatLng(latitude, longitude), localAtendimento, grupo,
                new Callback<SimpleCallback<List<TipoPrestador>>>() {
                    @Override
                    public void callback(SimpleCallback<List<TipoPrestador>> param) {
                        if (param.errorCode == 0) {
                            loadTipo(param.object);
                        } else {
                            showError(param.errorCode);
                        }
                    }

                });
    }

    private void loadTipo(List<TipoPrestador> object) {
        super.setListaOpcoes(new OpcaoSimplesConfig<TipoPrestador>() {
            @Override
            public void onClick(TipoPrestador obj) {
                if (obj != null) {
                    vaiParaSolicitacao(obj);
                } else {
                    vaiParaConsultaRegiao();
                }
            }

            @Override
            public String getText(TipoPrestador obj) {
                return obj.getDescricao();
            }

            @Override
            public boolean isValid(TipoPrestador obj) {
                return true;
            }

            @Override
            public String getEmptyMessage() {
                return getResources().getString(R.string.nenhum_prestador_cadastrado_na_regiao);
            }
        }, object, null);
    }

    protected void vaiParaConsultaRegiao() {
        openBrowser("consulta_regiao_atendimento.html");
    }

    protected void vaiParaSolicitacao(TipoPrestador tipo) {
        Intent intent = new Intent(this, SolicitacaoActivity.class);
        intent.putExtras(savedInstance);
        intent.putExtra(TipoPrestador.class.getSimpleName(), tipo);
        startActivityForResult(intent, SOLICITACAO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
        case SOLICITACAO:
            setResult(resultCode);
            finish();
            break;
        }
    }
}
