package br.com.servicoemcasa.cliente.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import br.com.servicoemcasa.activities.AdvertisingActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.model.SolicitacaoAtendimento;

public class SolicitacaoAdvertisingActivity extends AdvertisingActivity {

    private SolicitacaoAtendimento solicitacaoAtendimento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.addAdUnitId("ca-app-pub-9457021085627597/6534289867");
        super.addAdUnitId("ca-app-pub-9457021085627597/8011023064");
        super.onCreate(savedInstanceState);
        load(savedInstanceState);
        iniciaContador();
    }

    private void load(Bundle savedInstanceState) {
        Bundle bundle = savedInstanceState == null ? getIntent().getExtras() : savedInstanceState;
        solicitacaoAtendimento = (SolicitacaoAtendimento) bundle.getSerializable("solicitacaoAtendimento");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("solicitacaoAtendimento", solicitacaoAtendimento);
    }

    private void iniciaContador() {
        new AsyncTask<SolicitacaoAtendimento, Integer, SolicitacaoAtendimento>() {

            @Override
            protected SolicitacaoAtendimento doInBackground(SolicitacaoAtendimento... solicitacao) {
                int minuto = 0, segundo = 20;
                do {
                    publishProgress(minuto, segundo);

                    segundo--;
                    if (segundo < 0) {
                        minuto--;
                        segundo = 59;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                } while (minuto > -1);
                return solicitacao[0];
            }

            @Override
            protected void onPostExecute(SolicitacaoAtendimento result) {
                super.onPostExecute(result);
                Intent intent = new Intent();
                intent.putExtra("solicitacaoAtendimento", result);
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                setMessage(R.string.aguardando_resposta_dos_prestadores, values[0], values[1]);
            }

        }.execute(solicitacaoAtendimento);
    }

}
