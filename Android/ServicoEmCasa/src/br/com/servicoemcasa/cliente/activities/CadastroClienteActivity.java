package br.com.servicoemcasa.cliente.activities;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.services.ClienteService;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Email;
import br.com.servicoemcasa.model.EstadoCivil;
import br.com.servicoemcasa.model.Sexo;
import br.com.servicoemcasa.model.Telefone;
import br.com.servicoemcasa.util.StringUtils;
import br.com.servicoemcasa.widget.InvalidValueException;

public class CadastroClienteActivity extends ServicoEmCasaBaseActivity {

    private static final String CODIGO_CLIENTE = "codigoCliente";
    private static final String SEXO = "sexo";
    private static final String ESTADO_CIVIL = "estadoCivil";
    private DatePickerClickListener dataNascimentoListener;
    private int codigoCliente;
    private EstadoCivil estadoCivil;
    private Sexo sexo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastro_cliente_activity_layout);
        dataNascimentoListener = new DatePickerClickListener();
        loadState(savedInstanceState);
    }

    private void loadState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            dataNascimentoListener = (DatePickerClickListener) savedInstanceState.getSerializable("dataNascimentoListener");
            estadoCivil = (EstadoCivil) savedInstanceState.getSerializable(ESTADO_CIVIL);
            sexo = (Sexo) savedInstanceState.getSerializable(SEXO);
            codigoCliente = savedInstanceState.getInt(CODIGO_CLIENTE);
        } else {
            carregaClienteCadastrado();
        }
    }

    private void carregaClienteCadastrado() {
        getCliente(new Callback<Cliente>() {
            @Override
            public void callback(Cliente param) {
                if (param != null) {
                    codigoCliente = param.getCodigo();
                    estadoCivil = param.getEstadoCivil();
                    sexo = param.getSexo();
                    findEditText(R.id.nome).setText(param.getNome());
                    findEditText(R.id.email).setText(param.getEmail().getEmail());
                    findEditText(R.id.ddd).setText(param.getTelefone().getDDD());
                    findEditText(R.id.telefone).setText(param.getTelefone().getNumeroTelefone());
                    findEditText(R.id.datanascimento).setText(StringUtils.toString(param.getDataNascimento()));
                    findSpinner(R.id.estadocivil).setSelection(estadoCivil != null ? estadoCivil.ordinal() : 0);
                    findSpinner(R.id.sexo).setSelection(sexo != null ? sexo.ordinal() : 0);
                    dataNascimentoListener = new DatePickerClickListener(param.getDataNascimento());
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("dataNascimentoListener", dataNascimentoListener);
        outState.putSerializable(ESTADO_CIVIL, (EstadoCivil) findSpinner(R.id.estadocivil).getSelectedItem());
        outState.putSerializable(SEXO, (Sexo) findSpinner(R.id.sexo).getSelectedItem());
        outState.putInt(CODIGO_CLIENTE, codigoCliente);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        findTextView(R.id.termos_de_uso).setMovementMethod(LinkMovementMethod.getInstance());
        startListeners();
        setSexoAdapter();
        setEstadoCivilAdapter();
    }

    private void startListeners() {
        findTextView(R.id.datanascimento).setOnClickListener(dataNascimentoListener);
    }

    private void setEstadoCivilAdapter() {
        Spinner estadoCivil = (Spinner) findViewById(R.id.estadocivil);
        ArrayAdapter<EstadoCivil> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, EstadoCivil.values());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estadoCivil.setAdapter(adapter);
        estadoCivil.setSelection(this.estadoCivil != null ? this.estadoCivil.ordinal() : 0);
    }

    private void setSexoAdapter() {
        Spinner sexo = (Spinner) findViewById(R.id.sexo);
        ArrayAdapter<Sexo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Sexo.values());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sexo.setAdapter(adapter);
        sexo.setSelection(this.sexo != null ? this.sexo.ordinal() : 0);
    }

    public void confirmar(View view) {
        final Cliente cliente = getCliente();
        if (cliente != null) {
            salvaCliente(cliente);
        }
    }

    private void salvaCliente(final Cliente cliente) {
        ClienteService clienteService = new ClienteService(CadastroClienteActivity.this);
        if (codigoCliente != 0) {
            cliente.setCodigo(codigoCliente);
            clienteService.put(cliente, new ClienteCallback());
        } else {
            clienteService.post(cliente, new ClienteCallback());
        }
    }

    class ClienteCallback extends Callback<SimpleCallback<Cliente>> {
        @Override
        public void callback(SimpleCallback<Cliente> cliente) {
            if (cliente.errorCode == 0) {
                Toast.makeText(CadastroClienteActivity.this, R.string.cadastro_efetuado_com_sucesso, Toast.LENGTH_SHORT).show();
                setResult(RESULT_FIRST_USER);
                finish();
            } else {
                showError(cliente.errorCode);
            }
        }
    }

    private Cliente getCliente() {
        try {
            return new Cliente().setNome(getEditTextText(R.id.nome)).setEmail(new Email().setEmail(getEditTextText(R.id.email)))
                    .setTelefone(new Telefone().setDDD(getEditTextText(R.id.ddd)).setNumeroTelefone(getEditTextText(R.id.telefone)))
                    .setEstadoCivil((EstadoCivil) findSpinner(R.id.estadocivil).getSelectedItem())
                    .setSexo((Sexo) findSpinner(R.id.sexo).getSelectedItem())
                    .setDataNascimento(dataNascimentoListener.getValidDate(this, R.string.a_data_de_nascimento_deve_ser_informada));
        } catch (InvalidValueException e) {
            showToast(e.getMessage());
            return null;
        }
    }
}
