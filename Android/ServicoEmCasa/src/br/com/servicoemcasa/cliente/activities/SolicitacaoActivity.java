package br.com.servicoemcasa.cliente.activities;

import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.comum.activities.ListaPrestadoresActivity;
import br.com.servicoemcasa.comum.services.SolicitacaoAtendimentoService;
import br.com.servicoemcasa.fragments.MarkerMapFragment;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Endereco;
import br.com.servicoemcasa.model.LocalAtendimento;
import br.com.servicoemcasa.model.SolicitacaoAtendimento;
import br.com.servicoemcasa.model.TipoPrestador;

public class SolicitacaoActivity extends ServicoEmCasaBaseActivity {

    private static final int TIPO_SERVICO_ADVERTISING = 2;
    private static final int LISTA_DE_PRESTADORES_ACTIVITY = 1;

    // private InterstitialAd interstitialAd;
    private SolicitacaoAtendimentoService solicitacaoAtendimentoService;
    private Endereco endereco;
    private TipoPrestador tipoPrestador;
    private LocalAtendimento localAtendimento;
    private DateTimePickerClickListener dataAgendamento;

    // private SolicitacaoAtendimento solicitacaoAtendimento;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.solicitacao_activity_layout);
        this.solicitacaoAtendimentoService = new SolicitacaoAtendimentoService(this);
        findTextView(R.id.botao_confirmar).setText(getString(R.string.pesquisar));
        dataAgendamento = new DateTimePickerClickListener(this);
        // loadAd();
        loadData();
        loadMap();
        loadPeriodList();
        setResult(RESULT_CANCELED);
    }
    
    @Override
    protected void onResume() {
        startListeners();
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("dataAgendamento", dataAgendamento.getDate());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Date date = (Date) savedInstanceState.getSerializable("dataAgendamento");
        dataAgendamento = new DateTimePickerClickListener(this, date);
        super.onRestoreInstanceState(savedInstanceState);
    }

    // private void loadAd() {
    // interstitialAd = new InterstitialAd(this);
    // interstitialAd.setAdUnitId("ca-app-pub-9457021085627597/7371894665");
    // requestNewInterstitial();
    // interstitialAd.setAdListener(new AdListener() {
    // @Override
    // public void onAdClosed() {
    // requestNewInterstitial();
    // recuperaRespostas(solicitacaoAtendimento);
    // }
    // });
    // }
    //
    // private void requestNewInterstitial() {
    // AdRequest adRequest = new
    // AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
    // interstitialAd.loadAd(adRequest);
    // }

    private void loadData() {
        Intent intent = getIntent();
        endereco = (Endereco) intent.getExtras().getSerializable("endereco");
        tipoPrestador = (TipoPrestador) intent.getExtras().getSerializable(TipoPrestador.class.getSimpleName());
        localAtendimento = (LocalAtendimento) intent.getExtras().getSerializable("localServico");
        findTextView(R.id.endereco).setText(endereco.getDescricao());
        findEditText(R.id.complemento).setText(endereco.getComplemento());
        findTextView(R.id.tipo_servico).setText(tipoPrestador.getDescricao());
        if (localAtendimento.equals(LocalAtendimento.Estabelecimento)) {
            findTextView(R.id.label_endereco).setText(R.string.proximos_ao_endereco);
            findTextView(R.id.label_complemento).setVisibility(View.GONE);
            findEditText(R.id.complemento).setVisibility(View.GONE);
        }
    }

    private void loadMap() {
        getSupportFragmentManager().beginTransaction().add(R.id.frame_mapa, new MarkerMapFragment()).commit();
    }

    private void loadPeriodList() {
        // PeriodoAtendimentoService periodoAtendimentoService = new
        // PeriodoAtendimentoService(this);
        // periodoAtendimentoService.listaPeriodosAtendimento(new
        // Callback<SimpleCallback<PeriodoAtendimento[]>>() {
        // @Override
        // public void callback(SimpleCallback<PeriodoAtendimento[]> param) {
        // if (param.errorCode != 0) {
        // showError(param.errorCode, param.errorDescription);
        // finish();
        // } else {
        // Spinner periodo = (Spinner) findViewById(R.id.periodo);
        // ArrayAdapter<PeriodoAtendimento> adapter = new
        // ArrayAdapter<PeriodoAtendimento>(
        // SolicitacaoActivity.this, android.R.layout.simple_spinner_item,
        // param.object);
        // adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // periodo.setAdapter(adapter);
        // }
        // }
        // });
    }

    private void startListeners() {
        findTextView(R.id.data).setOnClickListener(dataAgendamento);
    }

    public void confirmar(View v) {
        getSolicitacaoAtendimento(new Callback<SolicitacaoAtendimento>() {
            @Override
            public void callback(SolicitacaoAtendimento solicitacaoAtendimento) {
                solicitacaoAtendimentoService.solicitaAtendimento(solicitacaoAtendimento,
                        new Callback<SimpleCallback<SolicitacaoAtendimento>>() {
                            @Override
                            public void callback(SimpleCallback<SolicitacaoAtendimento> param) {
                                if (param.errorCode == 0) {
                                    aguardaRespostas(param.object);
                                } else {
                                    showError(param.errorCode);
                                }
                            }
                        });
            }
        });
    }

    private void aguardaRespostas(final SolicitacaoAtendimento solicitacaoAtendimento) {
        // if (interstitialAd.isLoaded()) {
        // this.solicitacaoAtendimento = solicitacaoAtendimento;
        // interstitialAd.show();
        // } else {
        // recuperaRespostas(solicitacaoAtendimento);
        // }
        Intent intent = new Intent(this, SolicitacaoAdvertisingActivity.class);
        intent.putExtra("solicitacaoAtendimento", solicitacaoAtendimento);
        startActivityForResult(intent, TIPO_SERVICO_ADVERTISING);
    }

    private void recuperaRespostas(SolicitacaoAtendimento solicitacaoAtendimento) {
        solicitacaoAtendimentoService.recuperaRespostas(solicitacaoAtendimento,
                new Callback<SimpleCallback<SolicitacaoAtendimento>>() {
                    @Override
                    public void callback(SimpleCallback<SolicitacaoAtendimento> param) {
                        if (param.errorCode != 0) {
                            showError(param.errorCode);
                        } else {
                            vaiParaListaDePrestadores(param.object);
                        }
                    }
                });
    }

    private void vaiParaListaDePrestadores(SolicitacaoAtendimento solicitacaoAtendimento) {
        Intent intent = new Intent(this, ListaPrestadoresActivity.class);
        intent.putExtra(solicitacaoAtendimento.getClass().getSimpleName(), solicitacaoAtendimento);
        startActivityForResult(intent, LISTA_DE_PRESTADORES_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case LISTA_DE_PRESTADORES_ACTIVITY:
            switch (resultCode) {
            case RESULT_FIRST_USER:
                showAlert(R.string.nenhum_prestador_encontrado_aguarde);
                break;
            default:
                finish();
            }
            break;
        case TIPO_SERVICO_ADVERTISING:
            switch (resultCode) {
            case RESULT_OK:
                SolicitacaoAtendimento solicitacaoAtendimento = (SolicitacaoAtendimento) data.getExtras()
                        .getSerializable("solicitacaoAtendimento");
                recuperaRespostas(solicitacaoAtendimento);
                break;
            }
            break;
        }
    }

    public void getSolicitacaoAtendimento(final Callback<SolicitacaoAtendimento> callback) {
        if (dataAgendamento.getDate() == null) {
            showToast(R.string.a_data_de_agendamento_deve_ser_informada);
            return;
        }
        endereco.setComplemento(((EditText) findViewById(R.id.complemento)).getText().toString());
        getCliente(new Callback<Cliente>() {
            @Override
            public void callback(Cliente param) {
                callback.callback(new SolicitacaoAtendimento().setCliente(param).setData(dataAgendamento.getDate())
                        .setEndereco(endereco).setTipoPrestador(tipoPrestador).setLocalAtendimento(localAtendimento));
            }
        });

    }

    public Endereco getEndereco() {
        return endereco;
    }
}
