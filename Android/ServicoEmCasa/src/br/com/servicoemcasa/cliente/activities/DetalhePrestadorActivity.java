package br.com.servicoemcasa.cliente.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.services.AgendamentoService;
import br.com.servicoemcasa.cliente.services.PrestadorService;
import br.com.servicoemcasa.comum.activities.ChatActivity;
import br.com.servicoemcasa.comum.helper.SolicitacaoAtendimentoPrestadorHelper;
import br.com.servicoemcasa.comum.helper.SolicitacaoAtendimentoPrestadorHelper.SolicitacaoAtendimentoPrestadorCallback;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.LocalAtendimento;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.message.SolicitacaoAtendimentoMessage;

public class DetalhePrestadorActivity extends ServicoEmCasaBaseActivity implements
        SolicitacaoAtendimentoPrestadorCallback {

    private SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador;
    private Prestador prestador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalhe_prestador_activity_layout);
        setResult(RESULT_CANCELED);
    }

    @Override
    protected void onResume() {
        carregaDetalhePrestador();
        super.onResume();
    }

    private void carregaDetalhePrestador() {
        Bundle extras = getIntent().getExtras();
        if (extras.containsKey(SolicitacaoAtendimentoMessage.class.getSimpleName())) {
            SolicitacaoAtendimentoMessage message = (SolicitacaoAtendimentoMessage) extras
                    .getSerializable(SolicitacaoAtendimentoMessage.class.getSimpleName());
            new SolicitacaoAtendimentoPrestadorHelper().carregaDadosSolicitacaoPrestador(this, message.getCodigo(),
                    this);
        } else {
            carregaDetalhePrestador((SolicitacaoAtendimentoPrestador) getIntent().getExtras().get(
                    SolicitacaoAtendimentoPrestador.class.getSimpleName()));
        }
    }

    public void carregaDetalhePrestador(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        this.solicitacaoAtendimentoPrestador = solicitacaoAtendimentoPrestador;
        prestador = solicitacaoAtendimentoPrestador.getPrestador();
        preencheDadosSolicitacao();
        preencheDadosPrestador();
        if (!solicitacaoAtendimentoPrestador.isAceito()
                || solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getAgendamento() != null) {
            findViewById(R.id.botao_confirmar).setVisibility(View.GONE);
        }
    }

    private void preencheDadosSolicitacao() {
        findTextView(R.id.tipo_servico).setText(
                solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getTipoPrestador().getDescricao());
        findTextView(R.id.local_atendimento).setText(
                solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getLocalAtendimento().toString());
        if (LocalAtendimento.Estabelecimento.equals(solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento()
                .getLocalAtendimento())) {

            findTextView(R.id.label_endereco).setText(R.string.proximos_ao_endereco);
        }
        findTextView(R.id.endereco).setText(
                solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getEndereco().getDescricao());
        findTextView(R.id.data).setText(
                DateFormat.getDateFormat(this).format(
                        solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getData())
                        + " "
                        + DateFormat.getTimeFormat(this).format(
                                solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getData()));
    }

    private void preencheDadosPrestador() {
        carregaImagem();
        findTextView(R.id.tipo_prestador).setText(
                solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getTipoPrestador().getDescricao());
        findTextView(R.id.nome).setText(prestador.getNomeFantasia());
        findTextView(R.id.telefone)
                .setText(
                        String.format("(%s) %s", prestador.getTelefone().getDDD(), prestador.getTelefone()
                                .getNumeroTelefone()));
        findRatingBar(R.id.pontuacao).setRating(prestador.getAvaliacao().getPontuacao());
        findTextView(R.id.qtd_pontuacao).setText(String.valueOf(prestador.getAvaliacao().getQuantidadeAvaliacoes()));
        if (prestador.getDocumentoConselho() != null
                && prestador.getDocumentoConselho().getTipoDocumentoConselho().getTipoDocumentoConselho() != null) {
            findTextView(R.id.label_conselho).setText(
                    prestador.getDocumentoConselho().getTipoDocumentoConselho().getTipoDocumentoConselho());
            findTextView(R.id.conselho).setText(prestador.getDocumentoConselho().getNumeroDocumentoConselho());
        } else {
            findTextView(R.id.label_conselho).setVisibility(View.GONE);
            findTextView(R.id.conselho).setVisibility(View.GONE);
        }
        findTextView(R.id.resumo_profissional).setText(prestador.getResumoProfissional());
        if (LocalAtendimento.Estabelecimento.equals(solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento()
                .getLocalAtendimento())) {
            findTextView(R.id.endereco_prestador).setText(prestador.getEndereco().getDescricao());
            findTextView(R.id.endereco_complemento_prestador).setText(prestador.getEndereco().getComplemento());
        } else {
            findTextView(R.id.label_endereco_prestador).setVisibility(View.GONE);
            findTextView(R.id.label_endereco_complemento_prestador).setVisibility(View.GONE);
            findTextView(R.id.endereco_prestador).setVisibility(View.GONE);
            findTextView(R.id.endereco_complemento_prestador).setVisibility(View.GONE);
        }
    }

    private void carregaImagem() {
        PrestadorService prestadorService = new PrestadorService(this);
        prestadorService.recuperaFotoPretador(prestador, new Callback<Bitmap>() {
            @Override
            public void callback(Bitmap param) {
                ImageView imageView = (ImageView) findViewById(R.id.foto);
                if (param == null) {
                    param = BitmapFactory.decodeResource(getResources(), R.drawable.ic_no_image);
                }
                imageView.setImageBitmap(param);
            }
        });
    }

    public void ligar(View view) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(String.format("tel:(%s)%s", prestador.getTelefone().getDDD(), prestador
                .getTelefone().getNumeroTelefone())));
        startActivity(callIntent);
    }

    public void vaiParaMensagens(View view) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(SolicitacaoAtendimentoPrestador.class.getSimpleName(), solicitacaoAtendimentoPrestador);
        startActivity(intent);
    }

    public void confirmar(View view) {
        AgendamentoService service = new AgendamentoService(this);
        service.confirmaAtendimento(solicitacaoAtendimentoPrestador, new Callback<SimpleCallback<Boolean>>() {
            @Override
            public void callback(SimpleCallback<Boolean> param) {
                if (param.errorCode != 0) {
                    showError(param.errorCode);
                } else {
                    Toast.makeText(DetalhePrestadorActivity.this, R.string.agendamento_realizado_com_sucesso,
                            Toast.LENGTH_SHORT).show();
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });
    }

}
