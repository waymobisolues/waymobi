package br.com.servicoemcasa.cliente.activities;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.cliente.services.AvaliacaoAtendimentoService;
import br.com.servicoemcasa.cliente.services.PrestadorService;
import br.com.servicoemcasa.comum.helper.SolicitacaoAtendimentoPrestadorHelper;
import br.com.servicoemcasa.comum.helper.SolicitacaoAtendimentoPrestadorHelper.SolicitacaoAtendimentoPrestadorCallback;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.message.AgendamentoMessage;

public class AvaliacaoAtendimentoActivity extends ServicoEmCasaBaseActivity implements
        SolicitacaoAtendimentoPrestadorCallback {

    private AgendamentoMessage message;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.avaliacao_atendimento_activity_layout);

    }

    @Override
    protected void onResume() {
        carregaDetalhePrestador();
        super.onResume();
    }

    private void carregaDetalhePrestador() {
        this.message = (AgendamentoMessage) getIntent().getExtras().getSerializable(
                AgendamentoMessage.class.getSimpleName());
        new SolicitacaoAtendimentoPrestadorHelper().carregaDadosSolicitacaoPrestador(this,
                message.getCodigoSolicitacaoPrestador(), this);
    }

    public void carregaDetalhePrestador(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        findTextView(R.id.tipo_prestador).setText(
                solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getTipoPrestador().getDescricao());
        findTextView(R.id.nome).setText(solicitacaoAtendimentoPrestador.getPrestador().getNomeFantasia());
        carregaImagem(solicitacaoAtendimentoPrestador.getPrestador());
    }

    private void carregaImagem(Prestador prestador) {
        PrestadorService prestadorService = new PrestadorService(this);
        prestadorService.recuperaFotoPretador(prestador, new Callback<Bitmap>() {
            @Override
            public void callback(Bitmap param) {
                ImageView imageView = (ImageView) findViewById(R.id.foto);
                if (param == null) {
                    param = BitmapFactory.decodeResource(getResources(), R.drawable.ic_no_image);
                }
                imageView.setImageBitmap(param);
            }
        });
    }

    public void positivo(View view) {
        avalia(true);
    }

    private void avalia(boolean positivo) {
        AvaliacaoAtendimentoService service = new AvaliacaoAtendimentoService(this);
        service.avaliarAtendimento(message.getCodigo(), positivo, new Callback<SimpleCallback<Boolean>>() {
            @Override
            public void callback(SimpleCallback<Boolean> param) {
                if (param.errorCode != 0) {
                    showError(param.errorCode, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                } else {
                    finish();
                }
            }
        });
    }

    public void negativo(View view) {
        avalia(false);
    }

}
