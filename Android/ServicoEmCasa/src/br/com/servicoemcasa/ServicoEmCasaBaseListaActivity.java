package br.com.servicoemcasa;

import android.content.Intent;
import android.net.Uri;
import br.com.servicoemcasa.activities.BaseListaActivity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.util.ApplicationUtils;

public abstract class ServicoEmCasaBaseListaActivity<T> extends BaseListaActivity<T> {

    public void getCliente(final Callback<Cliente> callback) {
        ((ServicoEmCasaApplication) getApplication()).getCliente(this, callback);
    }

    public void getPrestador(final Callback<Prestador> callback) {
        ((ServicoEmCasaApplication) getApplication()).getPrestador(this, callback);
    }

    public void openBrowser(String pagina) {
        String s = ApplicationUtils.getMetaDataAsString(getApplicationContext(), "br.com.servicoemcasa.infra.WebClient.site");
        String url = String.format("%s/%s", s, pagina);
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
