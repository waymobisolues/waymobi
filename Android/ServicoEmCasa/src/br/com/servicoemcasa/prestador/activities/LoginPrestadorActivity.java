package br.com.servicoemcasa.prestador.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.model.Documento;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.TipoDocumento;

public class LoginPrestadorActivity extends ServicoEmCasaBaseActivity {

    private static final int CADASTRO_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.login_prestador_activity_layout);
    }

    public void cpfCnpjClick(View view) {
        ToggleButton cpf = findToggleButton(R.id.btn_cpf);
        ToggleButton cnpj = findToggleButton(R.id.btn_cnpj);
        ToggleButton clicked = (ToggleButton) view;
        if (clicked.isChecked()) {
            check(false, clicked, cpf, cnpj);
        } else {
            check(true, clicked, cpf, cnpj);
        }
    }

    private void check(boolean checked, ToggleButton clicked, ToggleButton... togl) {
        for (ToggleButton tg : togl) {
            if (tg != clicked) {
                tg.setChecked(checked);
            }
        }
    }

    public void confirmar(View view) {
        String numeroDocumento = getEditTextText(R.id.documento);
        if (numeroDocumento.length() == 0) {
            showToast(R.string.numero_do_documento_deve_ser_informado);
        } else {
            Documento documento = new Documento().setTipoDocumento(getTipoDocumento()).setNumeroDocumento(
                    Long.parseLong(numeroDocumento));
            if (!documento.validaDigito()) {
                showAlert(R.string.numero_de_documento_invalido);
                return;
            }
            vaiParaCadastro(documento);
        }
    }

    private TipoDocumento getTipoDocumento() {
        if (findToggleButton(R.id.btn_cpf).isChecked()) {
            return TipoDocumento.CPF;
        } else {
            return TipoDocumento.CNPJ;
        }
    }

    private void vaiParaCadastro(Documento documento) {
        Intent intent = new Intent(this, CadastroPrestadorActivity.class);
        Prestador prestador = new Prestador();
        prestador.setDocumento(documento);
        intent.putExtra(Prestador.class.getSimpleName(), prestador);
        startActivityForResult(intent, CADASTRO_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case CADASTRO_ACTIVITY:
            if (resultCode == RESULT_OK) {
                parabens();
            }
            break;
        }
    }

    private void parabens() {
        Intent intent = new Intent(this, ParabensActivity.class);
        startActivity(intent);
        finish();
    }

}
