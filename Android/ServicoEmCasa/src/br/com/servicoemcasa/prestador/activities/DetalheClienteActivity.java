package br.com.servicoemcasa.prestador.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.comum.activities.ChatActivity;
import br.com.servicoemcasa.comum.helper.SolicitacaoAtendimentoPrestadorHelper;
import br.com.servicoemcasa.comum.helper.SolicitacaoAtendimentoPrestadorHelper.SolicitacaoAtendimentoPrestadorCallback;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.LocalAtendimento;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.message.AgendamentoMessage;
import br.com.servicoemcasa.model.message.SolicitacaoAtendimentoMessage;
import br.com.servicoemcasa.prestador.services.SolicitacaoAtendimentoService;

public class DetalheClienteActivity extends ServicoEmCasaBaseActivity implements SolicitacaoAtendimentoPrestadorCallback {

    private SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalhe_cliente_activity_layout);
    }

    @Override
    protected void onResume() {
        carregaDetalhePrestador();
        super.onResume();
    }

    private void carregaDetalhePrestador() {
        Bundle extras = getIntent().getExtras();
        if (extras.containsKey(AgendamentoMessage.class.getSimpleName())) {
            AgendamentoMessage message = (AgendamentoMessage) extras.getSerializable(AgendamentoMessage.class.getSimpleName());
            new SolicitacaoAtendimentoPrestadorHelper().carregaDadosSolicitacaoPrestador(this, message.getCodigoSolicitacaoPrestador(),
                    this);
        } else if (extras.containsKey(SolicitacaoAtendimentoMessage.class.getSimpleName())) {
            SolicitacaoAtendimentoMessage message = (SolicitacaoAtendimentoMessage) extras
                    .getSerializable(SolicitacaoAtendimentoMessage.class.getSimpleName());
            new SolicitacaoAtendimentoPrestadorHelper().carregaDadosSolicitacaoPrestador(this, message.getCodigo(), this);
        } else {
            carregaDetalhePrestador((SolicitacaoAtendimentoPrestador) getIntent().getExtras().get(
                    SolicitacaoAtendimentoPrestador.class.getSimpleName()));
        }
    }

    public void carregaDetalhePrestador(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        this.solicitacaoAtendimentoPrestador = solicitacaoAtendimentoPrestador;
        preencheDadosSolicitacao();
        if (agendado() || solicitacaoAtendimentoPrestador.isAceito()) {
            findButton(R.id.botao_confirmar).setVisibility(View.GONE);
            findTextView(R.id.label_telefone).setVisibility(View.VISIBLE);
            findTextView(R.id.telefone).setVisibility(View.VISIBLE);
        }
    }

    private boolean agendado() {
        return solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getAgendamento() != null
                && solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getAgendamento().getSolicitacaoAtendimentoPrestador()
                        .equals(solicitacaoAtendimentoPrestador);
    }

    private void preencheDadosSolicitacao() {
        findTextView(R.id.tipo_servico).setText(
                solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getTipoPrestador().getDescricao());
        findTextView(R.id.local_atendimento).setText(
                solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getLocalAtendimento().toString());
        if (LocalAtendimento.Casa.equals(solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getLocalAtendimento())) {

            findTextView(R.id.endereco).setText(solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getEndereco().getDescricao());
            findTextView(R.id.complemento).setText(
                    solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getEndereco().getComplemento());
        } else {
            findTextView(R.id.label_endereco).setVisibility(View.GONE);
            findTextView(R.id.endereco).setVisibility(View.GONE);
            findTextView(R.id.label_complemento).setVisibility(View.GONE);
            findTextView(R.id.complemento).setVisibility(View.GONE);
        }
        findTextView(R.id.data).setText(
                DateFormat.getDateFormat(this).format(solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getData()) + " "
                        + DateFormat.getTimeFormat(this).format(solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getData()));
        findTextView(R.id.telefone).setText(
                String.format("(%s) %s", solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCliente().getTelefone().getDDD(),
                        solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCliente().getTelefone().getNumeroTelefone()));
    }

    public void ligar(View view) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(String.format("tel:(%s)%s", solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCliente()
                .getTelefone().getDDD(), solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCliente().getTelefone()
                .getNumeroTelefone())));
        startActivity(callIntent);
    }

    public void vaiParaMensagens(View view) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(SolicitacaoAtendimentoPrestador.class.getSimpleName(), solicitacaoAtendimentoPrestador);
        startActivity(intent);
    }

    public void confirmar(View view) {
        SolicitacaoAtendimentoService service = new SolicitacaoAtendimentoService(this);
        if (agendado()) {

        } else {
            service.aceitarSolicitacao(solicitacaoAtendimentoPrestador, new Callback<SimpleCallback<Boolean>>() {
                @Override
                public void callback(SimpleCallback<Boolean> param) {
                    if (param.errorCode != 0) {
                        showError(param.errorCode);
                        return;
                    }
                    if (param.object) {
                        showAlert(R.string.confirmacao_realizada_com_sucesso);
                    } else {
                        showAlert(R.string.solicitacao_ja_foi_encerrada);
                    }
                }
            });
        }
    }

}
