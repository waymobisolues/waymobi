package br.com.servicoemcasa.prestador.activities;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.adapter.CheckBoxAdapter;
import br.com.servicoemcasa.adapter.CheckBoxAdapter.Select;
import br.com.servicoemcasa.adapter.SimpleAdapter.ItemId;
import br.com.servicoemcasa.adapter.SimpleAdapter.Text;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.GrupoTipoPrestador;
import br.com.servicoemcasa.model.TipoPrestador;
import br.com.servicoemcasa.services.TipoPrestadorService;

public class CadastroTipoPrestadorActivity extends ServicoEmCasaBaseActivity {

    private Set<TipoPrestador> selecionados = new HashSet<TipoPrestador>();
    private Bundle savedInstance;
    private GrupoTipoPrestador grupo;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.simple_list_with_button_activity_layout);
        this.savedInstance = getIntent().getExtras();
        grupo = (GrupoTipoPrestador) this.savedInstance.getSerializable("grupoTipoPrestador");
        loadTipoPrestador();
        loadSelecionados();
        setResult(RESULT_CANCELED);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        TipoPrestador[] tipo = new TipoPrestador[selecionados.size()];
        selecionados.toArray(tipo);
        savedInstance.putSerializable("selecionados", tipo);
        outState.putAll(savedInstance);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.savedInstance = savedInstanceState;
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void loadSelecionados() {
        Object[] serializable = (Object[]) savedInstance.getSerializable("tipoPrestador");
        if (serializable != null) {
            for (Object o : serializable) {
                TipoPrestador tipo = (TipoPrestador) o;
                if (tipo.getGrupoTipoPrestador().equals(grupo)) {
                    selecionados.add((TipoPrestador) o);
                }
            }
        }
    }

    private void loadTipoPrestador() {
        TipoPrestadorService service = new TipoPrestadorService(this);
        service.listaTipoPrestador(grupo, new Callback<SimpleCallback<List<TipoPrestador>>>() {
            @Override
            public void callback(SimpleCallback<List<TipoPrestador>> param) {
                if (param.errorCode == 0) {
                    loadTipo(param.object);
                } else {
                    showError(param.errorCode);
                }
            }

        });
    }

    private void loadTipo(List<TipoPrestador> object) {
        ListView listView = (ListView) findViewById(R.id.lista);
        listView.setAdapter(new CheckBoxAdapter<TipoPrestador>(this, object, new ItemId<TipoPrestador>() {
            @Override
            public long getItemId(TipoPrestador object) {
                return object.getCodigo();
            }
        }, new Text<TipoPrestador>() {
            @Override
            public String getText(TipoPrestador object) {
                return object.getDescricao();
            }
        }, new Select<TipoPrestador>() {
            @Override
            public boolean isSelected(TipoPrestador object) {
                return selecionados.contains(object);
            }

            @Override
            public void select(TipoPrestador object) {
                selecionados.add(object);
            }

            @Override
            public void remove(TipoPrestador object) {
                selecionados.remove(object);
            }

        }));
    }

    public void confirmar(View view) {
        if (selecionados.size() == 0) {
            showAlert(R.string.selecione_um_tipo_de_servico_para_confirmar_o_cadastro);
            return;
        }
        Intent data = new Intent();
        savedInstance.putSerializable("tipoPrestador", selecionados.toArray());
        data.putExtras(savedInstance);
        setResult(RESULT_OK, data);
        finish();
    }
}
