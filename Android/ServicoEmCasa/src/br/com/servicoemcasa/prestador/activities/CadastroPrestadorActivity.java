package br.com.servicoemcasa.prestador.activities;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;
import br.com.servicoemcasa.comum.activities.GrupoTipoPrestadorActivity;
import br.com.servicoemcasa.infra.AddressDescription;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.DocumentoConselho;
import br.com.servicoemcasa.model.Email;
import br.com.servicoemcasa.model.Endereco;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.RG;
import br.com.servicoemcasa.model.RegiaoAtendimento;
import br.com.servicoemcasa.model.TipoDocumento;
import br.com.servicoemcasa.model.TipoDocumentoConselho;
import br.com.servicoemcasa.model.TipoPrestador;
import br.com.servicoemcasa.model.UF;
import br.com.servicoemcasa.prestador.services.PrestadorService;
import br.com.servicoemcasa.prestador.services.TipoDocumentoConselhoService;
import br.com.servicoemcasa.services.UFService;
import br.com.servicoemcasa.util.ImageUtils;
import br.com.servicoemcasa.util.MapUtils;
import br.com.servicoemcasa.util.StringUtils;
import br.com.servicoemcasa.widget.InvalidValueException;

public class CadastroPrestadorActivity extends ServicoEmCasaBaseActivity {

    private static final String TAG = CadastroPrestadorActivity.class.getSimpleName();
    private static final int FOTO_CROP_INTENT = 2;
    private static final int SELECT_PHOTO = 3;
    private static final int CADASTRO_GRUPO_TIPO_PRESTADOR = 4;
    private Cliente cliente;
    private PrestadorService prestadorService;
    private Prestador prestador = new Prestador();
    private DatePickerClickListener dataExpedicaoRG;
    private UF ufSelecionada;
    private TipoDocumentoConselho tipoDocumentoConselhoSelecionado;

    @Override
    protected void onCreate(Bundle savedInstance) {
        prestadorService = new PrestadorService(this);
        super.onCreate(savedInstance);
        carregaPrestador();
        infalteLayout();
        dataExpedicaoRG = new DatePickerClickListener();
        preenche(new Callback<Boolean>() {
            @Override
            public void callback(Boolean param) {
                setAdapters();
                startListeners();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        findTextView(R.id.termos_de_uso).setMovementMethod(LinkMovementMethod.getInstance());
    }

    private boolean isEmpresa() {
        return prestador.getDocumento().getTipoDocumento().equals(TipoDocumento.CNPJ);
    }

    private void carregaPrestador() {
        this.prestador = (Prestador) getIntent().getExtras().get(Prestador.class.getSimpleName());
    }

    private void infalteLayout() {
        if (isEmpresa()) {
            setContentView(R.layout.cadastro_prestador_empresa_activity_layout);
        } else {
            setContentView(R.layout.cadastro_prestador_activity_layout);
        }
    }

    private void startListeners() {
        if (!isEmpresa()) {
            findEditText(R.id.data_expedicao).setOnClickListener(dataExpedicaoRG);
        }
    }

    private void setAdapters() {
        if (!isEmpresa()) {
            setUFAdapter();
            setDocumentoConselhoAdapter();
        }
    }

    private void setUFAdapter() {
        UFService ufService = new UFService(this);
        ufService.listaUF(new Callback<SimpleCallback<UF[]>>() {
            @Override
            public void callback(SimpleCallback<UF[]> param) {
                if (param.errorCode == 0) {
                    setSpinnerArrayAdapter(findSpinner(R.id.uf), param.object, ufSelecionada);
                } else {
                    showError(param.errorCode);
                }
            }
        });
    }

    private void setDocumentoConselhoAdapter() {
        new TipoDocumentoConselhoService(this)
                .listarTipoDocumento(new Callback<SimpleCallback<TipoDocumentoConselho[]>>() {
                    @Override
                    public void callback(SimpleCallback<TipoDocumentoConselho[]> param) {
                        if (param.errorCode == 0) {
                            List<TipoDocumentoConselho> lista = new ArrayList<TipoDocumentoConselho>();
                            lista.addAll(Arrays.asList(param.object));
                            lista.add(
                                    0,
                                    new TipoDocumentoConselho().setDescricaoTipoDocumentoConselho(
                                            getString(R.string.nao_se_aplica)).setTipoDocumentoConselho(""));

                            setSpinnerArrayAdapter(findSpinner(R.id.conselho_regional), lista,
                                    tipoDocumentoConselhoSelecionado);
                        } else {
                            showError(param.errorCode);
                        }
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("prestador", prestador);
        if (!isEmpresa()) {
            outState.putSerializable("dataExpedicaoRG", dataExpedicaoRG.getDate());
            outState.putSerializable("ufSelecionada", (Serializable) findSpinner(R.id.uf).getSelectedItem());
            outState.putSerializable("tipoDocumentoConselhoSelecionado",
                    (Serializable) findSpinner(R.id.conselho_regional).getSelectedItem());
        }
        outState.putSerializable("cliente", cliente);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        File caminhoFoto = getCaminhoFoto();
        if (caminhoFoto.exists()) {
            findImageView(R.id.foto).setImageBitmap(ImageUtils.getImageFromFile(caminhoFoto));
        }
        dataExpedicaoRG = new DatePickerClickListener((Date) savedInstanceState.get("dataExpedicaoRG"));
        ufSelecionada = (UF) savedInstanceState.getSerializable("ufSelecionada");
        tipoDocumentoConselhoSelecionado = (TipoDocumentoConselho) savedInstanceState
                .getSerializable("tipoDocumentoConselhoSelecionado");
        cliente = (Cliente) savedInstanceState.getSerializable("cliente");
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void preenche(final Callback<Boolean> callback) {
        if (cliente == null) {
            getCliente(new Callback<Cliente>() {
                @Override
                public void callback(final Cliente cliente) {
                    CadastroPrestadorActivity.this.cliente = cliente;
                    preenchePF();
                    findEditText(R.id.nome_fantasia).setText(
                            StringUtils.isNullOrEmpty(prestador.getNomeFantasia()) ? cliente.getNome() : prestador
                                    .getNomeFantasia());
                    findEditText(R.id.email).setText(
                            prestador.getEmail() != null ? prestador.getEmail().getEmail() : cliente.getEmail()
                                    .getEmail());
                    findEditText(R.id.endereco).setText(
                            prestador.getEndereco() != null ? prestador.getEndereco().getDescricao() : "");
                    if (prestador.getRegiaoAtendimento() != null) {
                        if (prestador.getRegiaoAtendimento().getMaximaDistanciaAtendimento() > 0) {
                            EditText edit = findEditText(R.id.distancia);
                            edit.setText(String.valueOf(prestador.getRegiaoAtendimento()
                                    .getMaximaDistanciaAtendimento()));
                            edit.setEnabled(true);
                            findCheckBox(R.id.atende_remoto).setChecked(true);
                        } else {
                            findEditText(R.id.distancia).setText("");
                            findCheckBox(R.id.atende_remoto).setChecked(false);
                        }
                    }
                    findEditText(R.id.resumo).setText(prestador.getResumoProfissional());

                    carregaFotoDoServidor(prestador);
                    callback.callback(true);
                }

                private void preenchePF() {
                    if (!isEmpresa()) {
                        tipoDocumentoConselhoSelecionado = prestador.getDocumentoConselho() != null ? prestador
                                .getDocumentoConselho().getTipoDocumentoConselho() : null;
                        if (prestador.getRg() != null) {
                            dataExpedicaoRG = new DatePickerClickListener(prestador.getRg().getDataExpedicao());
                            findEditText(R.id.rg).setText(String.valueOf(prestador.getRg().getRg()));
                            findEditText(R.id.data_expedicao).setText(
                                    StringUtils.toString(prestador.getRg().getDataExpedicao()));
                            ufSelecionada = prestador.getRg().getUf();
                        }
                        findEditText(R.id.numero_conselho_regional).setText(
                                prestador.getDocumentoConselho() != null ? prestador.getDocumentoConselho()
                                        .getNumeroDocumentoConselho() : "");
                    }
                }
            });
        }
    }

    private void carregaFotoDoServidor(Prestador prestador) {
        if (prestador.getCodigo() > 0) {
            prestadorService.recuperaFotoPretador(prestador, new Callback<Bitmap>() {
                @Override
                public void callback(Bitmap param) {
                    findImageView(R.id.foto).setImageBitmap(param);
                    salvaFoto(param);
                }
            });
        }
    }

    public void foto(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, SELECT_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case SELECT_PHOTO:
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();
                salvaFoto(ImageUtils.getImageFromFile(new File(filePath)));
                carregaFoto();
            } else {
                carregaFotoSemFoto();
            }
            break;
        case FOTO_CROP_INTENT:
            if (resultCode == RESULT_OK) {
                carregaFotoEmEscala(data);
            }
            break;
        case CADASTRO_GRUPO_TIPO_PRESTADOR:
            if (resultCode == RESULT_OK) {
                salvaPrestador(data);
            }
            break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void carregaFotoEmEscala(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap bitmap = extras.getParcelable("data");
        salvaFoto(bitmap);
        findImageView(R.id.foto).setImageBitmap(bitmap);
    }

    private File getCaminhoFoto() {
        File dir = new File(Environment.getExternalStorageDirectory(), getPackageName());
        dir.mkdirs();
        return new File(dir, "wmp.png");
    }

    private void salvaFoto(Bitmap bitmap) {
        try {
            ImageUtils.saveImageToFile(bitmap, getCaminhoFoto());
        } catch (IOException e) {
            Toast.makeText(this, R.string.erro_ao_salvar_a_foto, Toast.LENGTH_SHORT).show();
            Log.e(TAG, e.toString(), e);
        }
    }

    private void carregaFoto() {
        File caminhoFoto = getCaminhoFoto();
        if (caminhoFoto.exists()) {
            try {
                Intent intent = ImageUtils.createImageCropIntent(caminhoFoto);
                startActivityForResult(intent, FOTO_CROP_INTENT);
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, e.toString(), e);
                Bitmap bitmap = ImageUtils.getScaledImageFromFile(caminhoFoto);
                salvaFoto(bitmap);
                findImageView(R.id.foto).setImageBitmap(bitmap);
            }
        }
    }

    private void carregaFotoSemFoto() {
        findImageView(R.id.foto).setImageResource(R.drawable.ic_no_image);
    }

    public void confirmar(View view) {
        try {
            MapUtils.getAddressDescription(this, getEditTextText(R.id.endereco), false,
                    new Callback<AddressDescription>() {
                        @Override
                        public void callback(AddressDescription param) {
                            findEditText(R.id.endereco).setText(param.addressDescription);
                            Endereco endereco = new Endereco().setDescricao(param.addressDescription)
                                    .setLatitude(param.latlng.latitude).setLongitude(param.latlng.longitude);
                            String distancia = getEditTextText(R.id.distancia);
                            int maximaDistancia = StringUtils.isNullOrEmpty(distancia) ? 0 : Integer
                                    .parseInt(distancia);
                            Prestador prestador = getPrestador(endereco, maximaDistancia);
                            if (prestador != null) {
                                confirmaPrestador(prestador);
                            }
                        }
                    });
        } catch (InvalidValueException e) {
            showToast(e.getMessage());
        }
    }

    private void confirmaPrestador(final Prestador prestador) {
        if (prestador != null) {
            File caminhoFoto = getCaminhoFoto();
            final Bitmap foto = caminhoFoto.exists() ? ImageUtils.getImageFromFile(caminhoFoto) : null;
            if (foto == null) {
                showToast(R.string.e_necessario_escolher_uma_foto_para_o_cadastro);
            } else {
                vaiParaSelecaoTipoPrestador(prestador, foto);
            }
        }

    }

    private void vaiParaSelecaoTipoPrestador(Prestador prestador, Bitmap foto) {
        Intent intent = new Intent(this, GrupoTipoPrestadorActivity.class);
        intent.putExtra("prestador", prestador);
        intent.putExtra("foto", foto);
        if (prestador.getTipo() != null) {
            intent.putExtra("tipoPrestador", prestador.getTipo().toArray());
        }
        intent.putExtra("tipoPrestadorActivity", CadastroTipoPrestadorActivity.class);
        startActivityForResult(intent, CADASTRO_GRUPO_TIPO_PRESTADOR);
    }

    private void salvaPrestador(final Intent intent) {
        getCliente(new Callback<Cliente>() {
            @Override
            public void callback(final Cliente cliente) {
                Prestador prestador = (Prestador) intent.getExtras().getSerializable("prestador");
                Bitmap foto = (Bitmap) intent.getExtras().getParcelable("foto");
                prestador
                        .setTipo(getListaTipoPrestador((Object[]) intent.getExtras().getSerializable("tipoPrestador")));
                prestadorService.salvaPrestador(cliente, prestador, foto, new Callback<SimpleCallback<Prestador>>() {
                    @Override
                    public void callback(SimpleCallback<Prestador> param) {
                        if (param.errorCode == 0) {
                            setResult(RESULT_OK);
                            finish();
                            return;
                        }
                        showError(param.errorCode);
                    }
                });
            }
        });
    }

    private List<TipoPrestador> getListaTipoPrestador(Object[] serializable) {
        List<TipoPrestador> lista = new ArrayList<>();
        for (Object o : serializable) {
            lista.add((TipoPrestador) o);
        }
        return lista;
    }

    private Prestador getPrestador(Endereco endereco, int maximaDistancia) {
        try {
            if (!isEmpresa()) {
                prestador.setRg(
                        new RG().setRg(
                                !StringUtils.isNullOrEmpty(getEditTextText(R.id.rg)) ? Long
                                        .parseLong(getEditTextText(R.id.rg)) : 0)
                                .setDataExpedicao(
                                        dataExpedicaoRG.getValidDate(this,
                                                R.string.a_data_de_expedicao_deve_ser_informada))
                                .setOrgaoEmissorRG(getEditTextText(R.id.orgao_expedicao))
                                .setUf((UF) findSpinner(R.id.uf).getSelectedItem())).setDocumentoConselho(
                        getDocumentoConselho());
            }
            prestador.setNomeFantasia(getEditTextText(R.id.nome_fantasia)).setEndereco(endereco)
                    .setEmail(new Email().setEmail(getEditTextText(R.id.email)))
                    .setRegiaoAtendimento(getRegiaoAtendimento(endereco, maximaDistancia))
                    .setResumoProfissional(getEditTextText(R.id.resumo));

            return prestador;
        } catch (InvalidValueException e) {
            showToast(e.getMessage());
            return null;
        }
    }

    private DocumentoConselho getDocumentoConselho() {
        TipoDocumentoConselho conselho = (TipoDocumentoConselho) findSpinner(R.id.conselho_regional).getSelectedItem();
        if (conselho == null || StringUtils.isNullOrEmpty(conselho.getTipoDocumentoConselho())) {
            return null;
        }
        String numeroConselho = getEditTextText(R.id.numero_conselho_regional);
        return new DocumentoConselho().setTipoDocumentoConselho(conselho).setNumeroDocumentoConselho(numeroConselho);
    }

    private RegiaoAtendimento getRegiaoAtendimento(Endereco endereco, int maximaDistancia) {
        return new RegiaoAtendimento().setLatitudeRegiao((float) endereco.getLatitude())
                .setLongitudeRegiao((float) endereco.getLongitude()).setMaximaDistanciaAtendimento(maximaDistancia);
    }

    public void atendeEnderecoCliente(View view) {
        CheckBox chk = (CheckBox) view;
        EditText edit = findEditText(R.id.distancia);
        if (chk.isChecked()) {
            edit.setEnabled(true);
        } else {
            edit.setText("");
            edit.setEnabled(false);
        }
    }

}
