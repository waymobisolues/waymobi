package br.com.servicoemcasa.prestador.activities;

import android.content.Intent;
import android.os.Bundle;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.model.Prestador;

public class PrestadorActivity extends ServicoEmCasaBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPrestador(new Callback<Prestador>() {
            @Override
            public void callback(Prestador param) {
                if (param != null) {
                    vaiParaCadastro(param);
                } else {
                    vaiParaLogin();
                }
            }
        });
    }

    private void vaiParaCadastro(Prestador prestador) {
        Intent intent = new Intent(this, CadastroPrestadorActivity.class);
        intent.putExtra(Prestador.class.getSimpleName(), prestador);
        startActivity(intent);
        finish();
    }

    private void vaiParaLogin() {
        Intent intent = new Intent(this, LoginPrestadorActivity.class);
        startActivity(intent);
        finish();
    }

}
