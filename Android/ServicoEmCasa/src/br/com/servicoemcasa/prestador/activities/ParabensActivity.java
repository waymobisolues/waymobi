package br.com.servicoemcasa.prestador.activities;

import android.os.Bundle;
import android.view.View;
import br.com.servicoemcasa.ServicoEmCasaBaseActivity;
import br.com.servicoemcasa.cliente.R;

public class ParabensActivity extends ServicoEmCasaBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.parabens_activity_layout);
    }
    
    public void fechar(View view) {
        finish();
    }
    
}
