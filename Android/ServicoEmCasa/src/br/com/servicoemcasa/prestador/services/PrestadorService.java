package br.com.servicoemcasa.prestador.services;

import java.io.ByteArrayOutputStream;

import org.apache.http.HttpStatus;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.Store;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.services.GcmService;
import br.com.servicoemcasa.util.JsonParser;
import br.com.servicoemcasa.util.StringUtils;

public class PrestadorService extends br.com.servicoemcasa.services.PrestadorService {

    private static final String PATH = "/Prestador/";

    private final WebClient webClient;
    private final GcmService gcmService;
    private final Store store;

    public PrestadorService(Activity activity) {
        super(activity);
        webClient = WebClient.newInstance(activity);
        gcmService = new GcmService(activity);
        store = new Store(activity, PrestadorService.class);
    }

    public class CadastroPrestador {
        Prestador prestador;
        byte[] foto;
    }

    public void salvaPrestador(final Cliente cliente, final Prestador prestador, final Bitmap foto,
            final Callback<SimpleCallback<Prestador>> callback) {

        store.getEditor().remove("naoExiste").remove("prestador").commit();
        gcmService.getGcm(new Callback<SimpleCallback<Gcm>>() {
            @Override
            public void callback(SimpleCallback<Gcm> param) {
                if (param.errorCode != 0) {
                    callback.callback(new SimpleCallback<Prestador>(param.errorCode, param.errorDescription));
                    return;
                }
                prestador.setCliente(cliente);
                CadastroPrestador cadastroPrestador = createCadastroPrestador(prestador, foto);
                webClient.put(PATH + param.object.getHashedGcmKey(), cadastroPrestador, new PrestadorCallback(callback));
            }
        });
    }

    private CadastroPrestador createCadastroPrestador(Prestador prestador, Bitmap foto) {
        CadastroPrestador cadastroPrestador = new CadastroPrestador();
        cadastroPrestador.prestador = prestador;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        foto.compress(CompressFormat.PNG, 100, bout);
        cadastroPrestador.foto = bout.toByteArray();
        return cadastroPrestador;
    }

    public void recuperaPrestador(final Callback<SimpleCallback<Prestador>> callback) {
        String json = store.getString("prestador");
        if (!StringUtils.isNullOrEmpty(json)) {
            callback.callback(new SimpleCallback<Prestador>(JsonParser.fromJson(json, Prestador.class)));
        } else if (store.getBoolean("naoExiste")) {
            callback.callback(new SimpleCallback<Prestador>(null));
        } else {
            gcmService.getGcm(new Callback<SimpleCallback<Gcm>>() {
                @Override
                public void callback(SimpleCallback<Gcm> param) {
                    if (param.errorCode != 0) {
                        callback.callback(new SimpleCallback<Prestador>(param.errorCode, param.errorDescription));
                        return;
                    }
                    webClient.get(PATH + param.object.getHashedGcmKey(), new PrestadorCallback(callback));
                }
            });
        }
    }

    private class PrestadorCallback extends Callback<WebClient.WebResponse> {

        private Callback<SimpleCallback<Prestador>> callback;

        public PrestadorCallback(Callback<SimpleCallback<Prestador>> callback) {
            this.callback = callback;
        }

        @Override
        public void callback(WebResponse param) {
            if (param.responseCode == HttpStatus.SC_OK) {
                Prestador prestador = JsonParser.fromJson(param.responseBody, Prestador.class);
                if (prestador != null) {
                    store.getEditor().putString("prestador", param.responseBody).commit();
                }
                callback.callback(new SimpleCallback<Prestador>(prestador));
            } else {
                if (param.responseCode == HttpStatus.SC_NOT_FOUND) {
                    store.getEditor().putBoolean("naoExiste", true).commit();
                }
                callback.callback(new SimpleCallback<Prestador>(param.responseCode, param.getErrorDescription()));
            }
        }

    }

}
