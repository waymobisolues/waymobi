package br.com.servicoemcasa.prestador.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.AutorizacaoCadastro;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.services.GcmService;
import br.com.servicoemcasa.util.JsonParser;

public class LoginService {

    private static final String PATH = "/Login";

    private final WebClient webClient;
    private final GcmService gcmService;

    public static class LoginInfo {
        String email;
        Gcm gcm;
    }

    public LoginService(Activity activity) {
        webClient = WebClient.newInstance(activity);
        gcmService = new GcmService(activity);
    }

    public void login(final String email, final Callback<SimpleCallback<AutorizacaoCadastro>> callback) {
        gcmService.getGcm(new Callback<SimpleCallback<Gcm>>() {
            @Override
            public void callback(SimpleCallback<Gcm> param) {
                if (param.errorCode != 0) {
                    callback.callback(new SimpleCallback<AutorizacaoCadastro>(param.errorCode, param.errorDescription));
                    return;
                }
                LoginInfo info = new LoginInfo();
                info.email = email;
                info.gcm = param.object;
                webClient.post(PATH, info, new Callback<WebClient.WebResponse>() {
                    @Override
                    public void callback(WebResponse param) {
                        switch (param.responseCode) {
                        case HttpStatus.SC_PARTIAL_CONTENT:
                            callback.callback(new SimpleCallback<AutorizacaoCadastro>(JsonParser.fromJson(
                                    param.responseBody, AutorizacaoCadastro.class)));
                            break;
                        case HttpStatus.SC_NOT_FOUND:
                            callback.callback(new SimpleCallback<AutorizacaoCadastro>(null));
                            break;
                        default:
                            callback.callback(new SimpleCallback<AutorizacaoCadastro>(param.responseCode, param
                                    .getErrorDescription()));
                        }
                    }
                });
            }
        });
    }
}
