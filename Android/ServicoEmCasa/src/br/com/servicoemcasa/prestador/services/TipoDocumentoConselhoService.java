package br.com.servicoemcasa.prestador.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.TipoDocumentoConselho;
import br.com.servicoemcasa.util.JsonParser;

public class TipoDocumentoConselhoService {

    private static final String PATH = "/TipoDocumentoConselho/";

    private final WebClient webClient;

    public TipoDocumentoConselhoService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void listarTipoDocumento(final Callback<SimpleCallback<TipoDocumentoConselho[]>> callback) {

        webClient.get(PATH, new Callback<WebClient.WebResponse>() {
            @Override
            public void callback(WebResponse param) {
                if (param.responseCode == HttpStatus.SC_OK) {
                    callback.callback(new SimpleCallback<TipoDocumentoConselho[]>(JsonParser.fromJson(
                            param.responseBody, TipoDocumentoConselho[].class)));
                } else {
                    callback.callback(new SimpleCallback<TipoDocumentoConselho[]>(param.responseCode, param
                            .getErrorDescription()));
                }
            }
        });
    }

}
