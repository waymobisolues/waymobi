package br.com.servicoemcasa.prestador.services;

import org.apache.http.HttpStatus;

import android.app.Activity;
import br.com.servicoemcasa.infra.Callback;
import br.com.servicoemcasa.infra.SimpleCallback;
import br.com.servicoemcasa.infra.WebClient;
import br.com.servicoemcasa.infra.WebClient.WebResponse;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;

public class SolicitacaoAtendimentoService {

    private static final String PATH = "/SolicitacaoAtendimento/";

    private final WebClient webClient;

    public SolicitacaoAtendimentoService(Activity activity) {
        webClient = WebClient.newInstance(activity);
    }

    public void aceitarSolicitacao(final SolicitacaoAtendimentoPrestador prestador,
            final Callback<SimpleCallback<Boolean>> callback) {

        webClient.put(PATH + prestador.getSolicitacaoAtendimento().getCodigo(), prestador,
                new Callback<WebClient.WebResponse>() {
                    @Override
                    public void callback(WebResponse param) {
                        if (param.responseCode == HttpStatus.SC_OK) {
                            callback.callback(new SimpleCallback<Boolean>(true));
                        } else if (param.responseCode == HttpStatus.SC_FORBIDDEN) {
                            callback.callback(new SimpleCallback<Boolean>(false));
                        } else {
                            callback.callback(new SimpleCallback<Boolean>(param.responseCode, param
                                    .getErrorDescription(), false));
                        }
                    }
                });
    }

}
