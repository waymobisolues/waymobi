#!/usr/bin/env python3
import requests
import logging
import json
import http.client as http_client

http_client.HTTPConnection.debuglevel = 1
logging.basicConfig() 
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


headers = {'content-type': 'application/json', 'Authorization': 'key=AIzaSyB3szhVxlmGsyd5gmerDZyK4vJ6oVOOz_I'}


message = {
    'collapse_key': 'SimpleMessage',
    'time_to_live': 30000,
    'delay_while_idle': False,
    'data': {
        'type':'SimpleMessage',
        'message': {
            'mensagem': 'Teste',
            'url': 'http://www.waymobi.com.br/teste.html'
        }
    },
    'registration_ids': ['APA91bE61jjrXMVGeg2GV8e03LURlJkkZ5cnmsx0ZMuGDZqLfttjKe_yTAxvC7ZfCQoZDdqTN8-IZDtgzbamBkcXfmDGMsIWEB2hEypV3ryGpT2evMHrSyTURri5yGj5XGbtv7AJYS0N6ehtQvL_Td28YOYC_RCJvKSo7HcCIbnfKx9g3DVlbHM']
}


response = requests.post("https://android.googleapis.com/gcm/send", data=json.dumps(message), headers=headers)
print(response.text)
