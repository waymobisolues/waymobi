delimiter $$

CREATE TABLE `endereco` (
  `cod_endereco` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`cod_endereco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `gcm` (
  `gcm_id` int(11) NOT NULL AUTO_INCREMENT,
  `gcm_key` text NOT NULL,
  `gcm_hash_key` varchar(32) NOT NULL,
  PRIMARY KEY (`gcm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `prestador` (
  `cod_prestador` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_prestador` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `endereco` int(11) NOT NULL,
  `foto` blob,
  `regiao_atendimento` int(11) NOT NULL,
  `gcm_id` int(11) NOT NULL,
  PRIMARY KEY (`cod_prestador`),
  KEY `fk_prestador_tipo_prestador_idx` (`tipo_prestador`),
  KEY `fk_prestador_endereco_idx` (`endereco`),
  KEY `fk_prestador_regiao_atendimento_idx` (`regiao_atendimento`),
  KEY `fk_prestador_gcm_idx` (`gcm_id`),
  CONSTRAINT `fk_prestador_gcm` FOREIGN KEY (`gcm_id`) REFERENCES `gcm` (`gcm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prestador_endereco` FOREIGN KEY (`endereco`) REFERENCES `endereco` (`cod_endereco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prestador_regiao_atendimento` FOREIGN KEY (`regiao_atendimento`) REFERENCES `regiao_atendimento` (`cod_regiao_atendimento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prestador_tipo_prestador` FOREIGN KEY (`tipo_prestador`) REFERENCES `tipo_prestador` (`cod_tipo_prestador`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `prestador_pontuacao` (
  `cod_pontuacao` int(11) NOT NULL AUTO_INCREMENT,
  `cod_prestador` int(11) NOT NULL,
  `pontuacao` int(11) NOT NULL,
  PRIMARY KEY (`cod_pontuacao`),
  KEY `fk_prestador_pontuacao_prestador_idx` (`cod_prestador`),
  CONSTRAINT `fk_prestador_pontuacao_prestador` FOREIGN KEY (`cod_prestador`) REFERENCES `prestador` (`cod_prestador`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `prestador_telefone` (
  `cod_telefone` int(11) NOT NULL AUTO_INCREMENT,
  `cod_prestador` int(11) NOT NULL,
  `tipo_telefone` int(11) NOT NULL,
  `numero` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_telefone`),
  KEY `fk_prestador_telefone_prestador_idx` (`cod_prestador`),
  KEY `fk_prestador_telefone_tipo_telefone_idx` (`tipo_telefone`),
  CONSTRAINT `fk_prestador_telefone_prestador` FOREIGN KEY (`cod_prestador`) REFERENCES `prestador` (`cod_prestador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prestador_telefone_tipo_telefone` FOREIGN KEY (`tipo_telefone`) REFERENCES `tipo_telefone` (`cod_tipo_telefone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `regiao_atendimento` (
  `cod_regiao_atendimento` int(11) NOT NULL AUTO_INCREMENT,
  `maxima_distancia` int(11) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`cod_regiao_atendimento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `tipo_prestador` (
  `cod_tipo_prestador` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_tipo_prestador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `tipo_telefone` (
  `cod_tipo_telefone` int(11) NOT NULL,
  `numero` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_tipo_telefone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$



