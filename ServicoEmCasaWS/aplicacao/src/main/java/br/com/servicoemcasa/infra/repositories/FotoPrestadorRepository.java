package br.com.servicoemcasa.infra.repositories;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import br.com.servicoemcasa.infra.PathFotoPrestador;
import br.com.servicoemcasa.model.Prestador;

public class FotoPrestadorRepository {

    private final String path;

    @Inject
    FotoPrestadorRepository(@PathFotoPrestador String path) {
        this.path = path;
    }

    public void salvaFoto(Prestador prestador, byte[] foto) {
        if (foto == null) {
            return;
        }
        File file = new File(path, prestador.getCodigo() + ".png");
        try (FileOutputStream fout = new FileOutputStream(file)) {
            fout.write(foto);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
