package br.com.servicoemcasa.infra.repositories;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.TipoPrestador;
import br.com.servicoemcasa.model.TipoPrestadorRepository;
import br.com.servicoemcasa.model.vo.LocalAtendimento;

public class TipoPrestadorRepositoryImpl extends DefaultRepository<Integer, TipoPrestador> implements TipoPrestadorRepository {

    @Inject
    TipoPrestadorRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<TipoPrestador> findByCodigoGrupo(Integer codigoGrupo) {
        return entityManager.createNamedQuery("TipoPrestador.FindByCodigoGrupo", TipoPrestador.class)
                .setParameter("codigoGrupo", codigoGrupo).getResultList();
    }

    @Override
    public List<TipoPrestador> findByCodigoGrupoAndDistancia(int codigoGrupo, float latitude, float longitude,
            LocalAtendimento localAtendimento) {

        return entityManager.createNamedQuery("TipoPrestador.FindByCodigoGrupoAndDistancia", TipoPrestador.class)
                .setParameter("codigoGrupo", codigoGrupo).setParameter("latitude", latitude).setParameter("longitude", longitude)
                .setParameter("distancia", localAtendimento.getDistanciaAtendimento()).getResultList();
    }

}
