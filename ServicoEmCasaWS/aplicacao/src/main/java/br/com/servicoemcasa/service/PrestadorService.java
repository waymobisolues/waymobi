package br.com.servicoemcasa.service;

import javax.inject.Inject;

import br.com.servicoemcasa.aop.Transactional;
import br.com.servicoemcasa.infra.repositories.FotoPrestadorRepository;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.PrestadorRepository;

public class PrestadorService {

    private final PrestadorRepository prestadorRepository;
    private final FotoPrestadorRepository fotoRepository;

    @Inject
    PrestadorService(final PrestadorRepository prestadorRepository, final FotoPrestadorRepository fotoRepository) {
        this.prestadorRepository = prestadorRepository;
        this.fotoRepository = fotoRepository;
    }

    @Transactional
    public void salvaPrestador(Prestador prestador, byte[] foto) {
        Prestador p = prestadorRepository.get(prestador.getCodigo());
        if (p != null) {
            p.atualiza(prestador);
            p.atualizaAvaliacao();
            prestador = p;
        } else {
            prestadorRepository.insert(prestador);
        }
        fotoRepository.salvaFoto(prestador, foto);
    }

}
