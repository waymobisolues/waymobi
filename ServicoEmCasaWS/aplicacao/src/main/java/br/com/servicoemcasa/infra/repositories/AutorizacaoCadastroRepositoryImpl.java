package br.com.servicoemcasa.infra.repositories;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.AutorizacaoCadastro;
import br.com.servicoemcasa.model.AutorizacaoCadastroRepository;
import br.com.servicoemcasa.model.Gcm;

public class AutorizacaoCadastroRepositoryImpl extends DefaultRepository<String, AutorizacaoCadastro> implements
        AutorizacaoCadastroRepository {

    @Inject
    AutorizacaoCadastroRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public AutorizacaoCadastro getByEmailAndGcm(String email, Gcm gcm) {
        return entityManager.createNamedQuery("AutorizacaoCadastro.GetByEmailAndGcm", AutorizacaoCadastro.class)
                .setParameter("email", email).setParameter("gcm", gcm).getSingleResult();
    }

}
