package br.com.servicoemcasa.infra.repositories;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestadorRepository;

public class SolicitacaoAtendimentoPrestadorRepositoryImpl extends
        DefaultRepository<Long, SolicitacaoAtendimentoPrestador> implements SolicitacaoAtendimentoPrestadorRepository {

    @Inject
    SolicitacaoAtendimentoPrestadorRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

}
