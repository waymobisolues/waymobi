package br.com.servicoemcasa.infra.repositories;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.SolicitacaoAtendimento;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoRepository;

public class SolicitacaoAtendimentoRepositoryImpl extends DefaultRepository<Long, SolicitacaoAtendimento> implements
        SolicitacaoAtendimentoRepository {

    @Inject
    SolicitacaoAtendimentoRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SolicitacaoAtendimento> findByCodigoCliente(int codigoCliente) {
        return (List<SolicitacaoAtendimento>) entityManager
                .createNamedQuery("SolicitacaoAtendimento.FindByCodigoCliente")
                .setParameter("codigoCliente", codigoCliente).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SolicitacaoAtendimento> findByCodigoPrestador(int codigoPrestador) {
        return (List<SolicitacaoAtendimento>) entityManager
                .createNamedQuery("SolicitacaoAtendimento.FindByCodigoPrestador")
                .setParameter("codigoPrestador", codigoPrestador).getResultList();
    }

}
