package br.com.servicoemcasa.service;

import java.util.List;

import javax.inject.Inject;

import br.com.servicoemcasa.aop.Transactional;
import br.com.servicoemcasa.model.Conversa;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.PrestadorRepository;
import br.com.servicoemcasa.model.SolicitacaoAtendimento;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoRepository;

public class SolicitarAtendimentoService {

    private SolicitacaoAtendimentoRepository solicitacaoAtendimentoRepository;
    private PrestadorRepository prestadorRepository;

    @Inject
    SolicitarAtendimentoService(PrestadorRepository prestadorRepository,
            SolicitacaoAtendimentoRepository solicitacaoAtendimentoRepository) {

        this.prestadorRepository = prestadorRepository;
        this.solicitacaoAtendimentoRepository = solicitacaoAtendimentoRepository;
    }

    @Transactional
    public void solicitaAtendimento(SolicitacaoAtendimento solicitacaoAtendimento) {
        List<Prestador> prestadores = prestadorRepository.findByTipo(solicitacaoAtendimento.getTipoPrestador());
        for (Prestador prestador : prestadores) {
            if (!prestador.getGcm().equals(solicitacaoAtendimento.getCliente().getGcm())) {
                solicitacaoAtendimento.solicitaAtendimento(prestador);
            }
        }
        solicitacaoAtendimentoRepository.insert(solicitacaoAtendimento);
    }

    @Transactional
    public boolean aceitaAtendimento(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        SolicitacaoAtendimento solicitacaoAtendimento = solicitacaoAtendimentoRepository
                .get(solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCodigo());
        if (solicitacaoAtendimento != null && !solicitacaoAtendimento.isEncerrado()) {
            solicitacaoAtendimento.aceitaAgendamento(solicitacaoAtendimentoPrestador);
            return true;
        }
        return false;
    }

    @Transactional
    public void enviaMensagem(Conversa conversa) {
        SolicitacaoAtendimento solicitacaoAtendimento = solicitacaoAtendimentoRepository.get(conversa
                .getSolicitacaoAtendimentoPrestador().getSolicitacaoAtendimento().getCodigo());
        SolicitacaoAtendimentoPrestador prestador = solicitacaoAtendimento.getPrestadores().get(
                solicitacaoAtendimento.getPrestadores().indexOf(conversa.getSolicitacaoAtendimentoPrestador()));
        prestador.adicionaConversa(conversa);
    }

}
