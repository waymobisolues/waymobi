package br.com.servicoemcasa.infra.repositories;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.GrupoTipoPrestador;
import br.com.servicoemcasa.model.GrupoTipoPrestadorRepository;
import br.com.servicoemcasa.model.vo.LocalAtendimento;

public class GrupoTipoPrestadorRepositoryImpl extends DefaultRepository<Integer, GrupoTipoPrestador> implements
        GrupoTipoPrestadorRepository {

    @Inject
    GrupoTipoPrestadorRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<GrupoTipoPrestador> findByDistancia(float latitude, float longitude, LocalAtendimento localAtendimento) {
        return entityManager.createNamedQuery("GrupoTipoPrestador.FindByDistancia", GrupoTipoPrestador.class)
                .setParameter("latitude", latitude).setParameter("longitude", longitude)
                .setParameter("distancia", localAtendimento.getDistanciaAtendimento()).getResultList();
    }

}
