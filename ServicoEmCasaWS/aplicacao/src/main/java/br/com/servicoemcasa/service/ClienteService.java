package br.com.servicoemcasa.service;

import javax.inject.Inject;
import javax.persistence.NoResultException;

import br.com.servicoemcasa.aop.Transactional;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.ClienteRepository;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.GcmRepository;

public class ClienteService {

    private final ClienteRepository clienteRepository;
    private final GcmRepository gcmRepository;

    @Inject
    ClienteService(final ClienteRepository clienteRepository, final GcmRepository gcmRepository) {
        this.clienteRepository = clienteRepository;
        this.gcmRepository = gcmRepository;
    }

    @Transactional
    public Cliente salvaCliente(Cliente cliente) {
        Cliente c = clienteRepository.get(cliente.getCodigo());
        if (c != null) {
            c.atualiza(cliente);
            cliente = c;
        } else {
            Gcm gcm = trataGcm(cliente.getGcm());
            cliente.setGcm(gcm);
            clienteRepository.insert(cliente);
        }
        return cliente;
    }

    private Gcm trataGcm(Gcm gcm) {
        try {
            Gcm gravado = gcmRepository.findByHash(gcm.getHashedKey());
            return gravado;
        } catch (NoResultException e) {
            gcmRepository.insert(gcm);
            return gcm;
        }
    }
}
