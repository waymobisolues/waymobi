package br.com.servicoemcasa.infra.repositories;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.Agendamento;
import br.com.servicoemcasa.model.AgendamentoRepository;

public class AgendamentoRepositoryImpl extends DefaultRepository<Long, Agendamento> implements AgendamentoRepository {

    @Inject
    AgendamentoRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Agendamento> listaAgendamentosParaEncerrar() {
        return entityManager.createNamedQuery("Agendamento.AgendamentosParaEncerrar", Agendamento.class)
                .getResultList();
    }

}
