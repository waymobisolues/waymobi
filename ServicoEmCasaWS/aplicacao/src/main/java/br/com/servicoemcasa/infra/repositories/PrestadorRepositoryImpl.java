package br.com.servicoemcasa.infra.repositories;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.PrestadorRepository;
import br.com.servicoemcasa.model.TipoPrestador;
import br.com.servicoemcasa.model.vo.AvaliacaoPrestador;
import br.com.servicoemcasa.model.vo.Documento;

public class PrestadorRepositoryImpl extends DefaultRepository<Integer, Prestador> implements PrestadorRepository {

    @Inject
    PrestadorRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Prestador> findByTipo(TipoPrestador tipoPrestador) {
        return (List<Prestador>) entityManager.createNamedQuery("Prestador.FindByTipo")
                .setParameter("tipoPrestador", tipoPrestador).getResultList();
    }

    @Override
    public Prestador getByGcm(Gcm gcm) {
        return getByGcm(gcm.getHashedKey());
    }

    @Override
    public Prestador getByGcm(String hashedKey) {
        return entityManager.createNamedQuery("Prestador.GetByGcm", Prestador.class).setParameter("gcmKey", hashedKey)
                .getSingleResult();
    }

    @Override
    public Prestador findByDocumento(Documento documento) {
        List<Prestador> resultList = entityManager.createNamedQuery("Prestador.FindByDocumento", Prestador.class)
                .setParameter("numeroDocumento", documento.getNumeroDocumento()).getResultList();
        return (resultList.size() > 0) ? resultList.get(0) : null;
    }

    @Override
    public Prestador findByEmail(String email) {
        return entityManager.createNamedQuery("Prestador.FindByEmail", Prestador.class).setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public AvaliacaoPrestador getAvaliacao(Prestador prestador) {
        return entityManager
                .createQuery(
                        "select new br.com.servicoemcasa.model.vo.AvaliacaoPrestador(count(distinct a.codigo), count(distinct b.codigo))"
                                + " from Agendamento a, Agendamento b"
                                + " where a.solicitacaoAtendimentoPrestador.prestador.codigo = :prestador"
                                + " and a.avaliacaoPositiva is not null"
                                + " and b.solicitacaoAtendimentoPrestador.prestador.codigo = :prestador"
                                + " and b.avaliacaoPositiva = true", AvaliacaoPrestador.class)
                .setParameter("prestador", prestador.getCodigo()).getSingleResult();
    }

}
