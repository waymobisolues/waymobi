package br.com.servicoemcasa.service;

import javax.inject.Inject;
import javax.persistence.NoResultException;

import br.com.servicoemcasa.aop.Transactional;
import br.com.servicoemcasa.model.AutorizacaoCadastro;
import br.com.servicoemcasa.model.AutorizacaoCadastroRepository;
import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.ClienteRepository;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.GcmRepository;
import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.PrestadorRepository;
import br.com.servicoemcasa.model.vo.Email;

public class LoginService {

    private final ClienteRepository clienteRepository;
    private final AutorizacaoCadastroRepository autorizacaoCadastroRepository;
    private final PrestadorRepository prestadorRepository;
    private final GcmRepository gcmRepository;

    @Inject
    LoginService(final ClienteRepository clienteRepository, final PrestadorRepository prestadorRepository,
            final AutorizacaoCadastroRepository autorizacaoCadastroRepository, final GcmRepository gcmRepository) {

        this.clienteRepository = clienteRepository;
        this.prestadorRepository = prestadorRepository;
        this.autorizacaoCadastroRepository = autorizacaoCadastroRepository;
        this.gcmRepository = gcmRepository;
    }

    public AutorizacaoCadastro processaAutorizacao(String email, Gcm gcm) {
        Gcm gcmAtual = encontraGcmAtual(email);
        if (gcmAtual == null) {
            return null;
        }
        if (gcmAtual.equals(gcm)) {
            return null;
        }
        return verificaAutorizacao(email, gcmAtual, gcm);
    }

    private AutorizacaoCadastro verificaAutorizacao(String emailCliente, Gcm gcmAtual, Gcm gcmAutorizado) {
        AutorizacaoCadastro autorizacaoCadastro;
        try {
            autorizacaoCadastro = autorizacaoCadastroRepository.getByEmailAndGcm(emailCliente, gcmAutorizado);
        } catch (NoResultException e) {
            autorizacaoCadastro = criaAutorizacao(emailCliente, gcmAtual, gcmAutorizado);
        }
        autorizacaoCadastro.enviaNotificacao();
        return autorizacaoCadastro;

    }

    private Gcm encontraGcmAtual(String email) {
        try {
            Cliente cliente = clienteRepository.findByEmail(email);
            return cliente.getGcm();
        } catch (NoResultException e) {
            try {
                Prestador prestador = prestadorRepository.findByEmail(email);
                return prestador.getGcm();
            } catch (NoResultException e1) {
            }
        }
        return null;
    }

    @Transactional
    private AutorizacaoCadastro criaAutorizacao(String emailCliente, Gcm gcm, Gcm gcmAutorizado) {
        gcm = gcmRepository.findByHash(gcm.getHashedKey());
        gcmAutorizado = gcmRepository.findByHash(gcmAutorizado.getHashedKey());
        AutorizacaoCadastro autorizacaoCadastro = new AutorizacaoCadastro(new Email(emailCliente), gcm, gcmAutorizado);
        autorizacaoCadastroRepository.insert(autorizacaoCadastro);
        return autorizacaoCadastro;
    }

}
