package br.com.servicoemcasa.infra.repositories;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.GcmRepository;

public class GcmRepositoryImpl extends DefaultRepository<Long, Gcm> implements GcmRepository {

    @Inject
    GcmRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Gcm findByHash(String hash) {
        return entityManager.createNamedQuery("Gcm.FindByHash", Gcm.class).setParameter("hashedGcmKey", hash).getSingleResult();
    }

}
