package br.com.servicoemcasa.infra.repositories;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.servicoemcasa.aop.Transactional;

abstract class DefaultRepository<PK, T> {

    protected final EntityManager entityManager;

    public DefaultRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @SuppressWarnings("unchecked")
    public T get(PK pk) {
        return (T) entityManager.find(getTypeClass(), pk);
    }

    @Transactional
    public void insert(T entity) {
        entityManager.persist(entity);
    }

    @Transactional
    public void delete(T entity) {
        entityManager.remove(entity);
    }

    @SuppressWarnings("unchecked")
    public List<T> list() {
        return entityManager.createQuery(String.format("FROM %s c", getTypeClass().getName())).getResultList();
    }

    private Class<?> getTypeClass() {
        Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        return clazz;
    }

}
