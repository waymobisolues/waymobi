package br.com.servicoemcasa.infra.repositories;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.ClienteRepository;
import br.com.servicoemcasa.model.Gcm;

public class ClienteRepositoryImpl extends DefaultRepository<Integer, Cliente> implements ClienteRepository {

    @Inject
    ClienteRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Cliente findByEmail(String email) {
        return (Cliente) entityManager.createNamedQuery("Cliente.FindByEmail").setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public Cliente getByGcm(Gcm gcm) {
        return getByGcm(gcm.getHashedKey());
    }

    @Override
    public Cliente getByGcm(String hashedKey) {
        return (Cliente) entityManager.createNamedQuery("Cliente.GetByGcm").setParameter("gcmKey", hashedKey)
                .getSingleResult();
    }

}
