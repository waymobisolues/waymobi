package br.com.servicoemcasa.infra.repositories;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.servicoemcasa.model.Conversa;
import br.com.servicoemcasa.model.ConversaRepository;
import br.com.servicoemcasa.model.Gcm;

public class ConversaRepositoryImpl extends DefaultRepository<Integer, Conversa> implements ConversaRepository {

    @Inject
    ConversaRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Conversa> listByCodigoAtendimentoPrestador(long codigoSolicitacaoAtendimentoPrestador) {
        return (List<Conversa>) entityManager.createNamedQuery("Conversa.FindByCodigoAtendimentoPrestador")
                .setParameter("codigoSolicitacaoAtendimentoPrestador", codigoSolicitacaoAtendimentoPrestador)
                .getResultList();
    }

    @Override
    public void updateGcm(Gcm gcmAntigo, Gcm gcmNovo) {
        String update = "UPDATE Conversa SET %1$s = :gcmNovo WHERE %1$s = :gcmAntigo";
        Query query = entityManager.createQuery(String.format(update, "remetente"));
        query.setParameter("gcmNovo", gcmNovo).setParameter("gcmAntigo", gcmAntigo).executeUpdate();
    }

}
