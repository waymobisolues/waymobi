package br.com.servicoemcasa.service;

import javax.inject.Inject;
import javax.persistence.NoResultException;

import br.com.servicoemcasa.aop.Transactional;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.GcmRepository;

public class GcmService {

    private final GcmRepository gcmRepository;

    @Inject
    GcmService(final GcmRepository gcmRepository) {
        this.gcmRepository = gcmRepository;
    }

    private Gcm getSavedGcm(String hashIdExistente) {
        return gcmRepository.findByHash(hashIdExistente);
    }

    @Transactional
    public Gcm registraGcm(String hashIdExistente, String gcmKey) {
        Gcm novoGcm = new Gcm(gcmKey);
        if (hashIdExistente != null) {
            Gcm gcm = getSavedGcm(hashIdExistente);
            gcm.mudaGcm(novoGcm);
            novoGcm = gcm;
        } else {
            try {
                getSavedGcm(novoGcm.getHashedKey());
            } catch (NoResultException e) {
                incluiNovoGcm(novoGcm);
            }
        }
        return novoGcm;
    }

    private void incluiNovoGcm(Gcm gcm) {
        gcmRepository.insert(gcm);
    }

}
