package br.com.servicoemcasa;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;
import javax.mail.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.servicoemcasa.aop.TransactionScope;
import br.com.servicoemcasa.aop.TransactionScoped;
import br.com.servicoemcasa.infra.GcmServiceKey;
import br.com.servicoemcasa.infra.GcmServiceUrl;
import br.com.servicoemcasa.infra.PathFotoPrestador;
import br.com.servicoemcasa.infra.WebPath;
import br.com.servicoemcasa.infra.repositories.AgendamentoRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.AutorizacaoCadastroRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.ClienteRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.ConversaRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.GcmRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.GrupoTipoPrestadorRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.PrestadorRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.SolicitacaoAtendimentoPrestadorRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.SolicitacaoAtendimentoRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.TIpoDocumentoConselhoRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.TipoPrestadorRepositoryImpl;
import br.com.servicoemcasa.infra.repositories.UFRepositoryImpl;
import br.com.servicoemcasa.infra.service.DistanceCalculatorServiceImpl;
import br.com.servicoemcasa.infra.service.EmailBroadcastServiceImpl;
import br.com.servicoemcasa.infra.service.GcmBroadcastServiceImpl;
import br.com.servicoemcasa.model.AgendamentoRepository;
import br.com.servicoemcasa.model.AutorizacaoCadastroRepository;
import br.com.servicoemcasa.model.ClienteRepository;
import br.com.servicoemcasa.model.ConversaRepository;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.GcmRepository;
import br.com.servicoemcasa.model.GrupoTipoPrestadorRepository;
import br.com.servicoemcasa.model.PrestadorRepository;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestadorRepository;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoRepository;
import br.com.servicoemcasa.model.TipoDocumentoConselhoRepository;
import br.com.servicoemcasa.model.TipoPrestadorRepository;
import br.com.servicoemcasa.model.UFRepository;
import br.com.servicoemcasa.model.service.BroadcastService;
import br.com.servicoemcasa.model.service.DependencyResolver;
import br.com.servicoemcasa.model.service.DistanceCalculatorService;
import br.com.servicoemcasa.model.vo.Email;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;

public class DependencyFactory extends AbstractModule implements DependencyResolver {

    private static final Injector injector = Guice.createInjector(new DependencyFactory());
    private final TransactionScope requestScope = new TransactionScope();

    protected DependencyFactory() {
    }

    protected void configure() {
        InitialContext initialContext;
        try {
            initialContext = new InitialContext();
            bind(DependencyResolver.class).toInstance(this);
            bindConstants(initialContext);
            bindServices();
            bindRepositories();
            bindTransactionService();
            bindMailSession(initialContext);
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    private void bindMailSession(final InitialContext initialContext) throws NamingException {
        Session session = (Session) initialContext.lookup("java:comp/env/mail/Session");
        bind(Session.class).toInstance(session);
    }

    private void bindConstants(final InitialContext initialContext) throws NamingException {
        bindConstant().annotatedWith(PathFotoPrestador.class).to(
                (String) initialContext.lookup("java:comp/env/path/fotoPrestador"));
        bindConstant().annotatedWith(WebPath.class).to((String) initialContext.lookup("java:comp/env/url/webPath"));
        bindConstant().annotatedWith(GcmServiceUrl.class).to(
                (String) initialContext.lookup("java:comp/env/url/gcmService"));
        bindGcmKeyConstant(initialContext);
    }

    private void bindGcmKeyConstant(final InitialContext initialContext) throws NamingException {
        String gcmServiceKey = (String) initialContext.lookup("java:comp/env/key/gcmService");
        Map<String, String> map = new HashMap<String, String>();
        map.put("Authorization", "key=" + gcmServiceKey);
        bind(new TypeLiteral<Map<String, String>>() {
        }).annotatedWith(GcmServiceKey.class).toInstance(map);
    }

    private void bindTransactionService() {
        bindScope(TransactionScoped.class, requestScope);
        bind(TransactionScope.class).toInstance(requestScope);
        bind(EntityManagerFactory.class).toProvider(new Provider<EntityManagerFactory>() {
            @Override
            public EntityManagerFactory get() {
                return Persistence.createEntityManagerFactory("default");
            }
        }).in(Singleton.class);
        bind(EntityManager.class).toProvider(TransactionScope.<EntityManager> seededKeyProvider()).in(requestScope);
    }

    private void bindServices() {
        bind(DistanceCalculatorService.class).to(DistanceCalculatorServiceImpl.class);

        bind(new TypeLiteral<BroadcastService<Gcm>>() {
        }).to(GcmBroadcastServiceImpl.class);

        bind(new TypeLiteral<BroadcastService<Email>>() {
        }).to(EmailBroadcastServiceImpl.class);
    }

    private void bindRepositories() {
        bind(ClienteRepository.class).to(ClienteRepositoryImpl.class);
        bind(PrestadorRepository.class).to(PrestadorRepositoryImpl.class);
        bind(GcmRepository.class).to(GcmRepositoryImpl.class);
        bind(GrupoTipoPrestadorRepository.class).to(GrupoTipoPrestadorRepositoryImpl.class);
        bind(TipoPrestadorRepository.class).to(TipoPrestadorRepositoryImpl.class);
        bind(SolicitacaoAtendimentoRepository.class).to(SolicitacaoAtendimentoRepositoryImpl.class);
        bind(AgendamentoRepository.class).to(AgendamentoRepositoryImpl.class);
        bind(AutorizacaoCadastroRepository.class).to(AutorizacaoCadastroRepositoryImpl.class);
        bind(TipoDocumentoConselhoRepository.class).to(TIpoDocumentoConselhoRepositoryImpl.class);
        bind(UFRepository.class).to(UFRepositoryImpl.class);
        bind(ConversaRepository.class).to(ConversaRepositoryImpl.class);
        bind(SolicitacaoAtendimentoPrestadorRepository.class).to(SolicitacaoAtendimentoPrestadorRepositoryImpl.class);
    }

    public static Injector getInjector() {
        return injector;
    }

    public static void injectMembers(Object o) {
        injector.injectMembers(o);
    }

    public static <T> T getInstance(Class<T> clazz) {
        return injector.getInstance(clazz);
    }

    public <T> T resolve(Class<T> classOf) {
        return injector.getInstance(classOf);
    }

    public static Object getInstanceAsObject(Class<?> clazz) {
        return injector.getInstance(clazz);
    }

}
