package br.com.servicoemcasa.infra.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import br.com.servicoemcasa.infra.GcmServiceKey;
import br.com.servicoemcasa.infra.GcmServiceUrl;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.model.service.BroadcastMessage;
import br.com.servicoemcasa.model.service.BroadcastService;
import br.com.servicoemcasa.model.service.Broadcastable;
import br.com.servicoemcasa.util.JsonParser;
import br.com.servicoemcasa.util.WebClient;
import br.com.servicoemcasa.util.WebClient.WebResponse;

public class GcmBroadcastServiceImpl implements BroadcastService<Gcm> {
    
    private final String gcmUrl;
    private final Map<String, String> gcmKey;

    @Inject
    GcmBroadcastServiceImpl(@GcmServiceKey Map<String, String> gcmKey, @GcmServiceUrl String gcmUrl) {
        this.gcmKey = gcmKey;
        this.gcmUrl = gcmUrl;
    }

    public class GcmData {
        String type;
        Object message;

        GcmData(BroadcastMessage messageInfo) {
            this.type = messageInfo.getMessageType();
            this.message = messageInfo.getMessage();
        }
    }
    
    public class GcmEntity {

        String collapse_key;
        int time_to_live = 259200; // 3 dias
        boolean delay_while_idle = false;
        GcmData data;
        List<String> registration_ids;

        GcmEntity(GcmData data, List<String> registration_ids) {
            collapse_key = data.type;
            this.data = data;
            this.registration_ids = registration_ids;
        }

    }

    @Override
    public void broadcast(Broadcastable message, Gcm... broadcastDestination) {
        List<String> keys = new ArrayList<String>();
        for (Gcm gcm : broadcastDestination) {
            keys.add(gcm.getKey());
        }
        GcmEntity gcmEntity = new GcmEntity(new GcmData(message.getMessage()), keys);
        WebClient webClient = new WebClient(gcmUrl);
        WebResponse webResponse = webClient.post("/send", JsonParser.toJson(gcmEntity), gcmKey);
        if (webResponse.responseCode != 200) {
            throw new GcmBroadcastException(webResponse.responseBody);
        }
    }
}
