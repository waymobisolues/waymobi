package br.com.servicoemcasa.infra.repositories;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.UF;
import br.com.servicoemcasa.model.UFRepository;

public class UFRepositoryImpl extends DefaultRepository<String, UF> implements UFRepository {

    @Inject
    UFRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

}
