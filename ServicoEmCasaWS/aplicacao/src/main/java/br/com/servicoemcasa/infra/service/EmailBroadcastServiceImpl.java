package br.com.servicoemcasa.infra.service;

import java.util.Map;

import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.text.StrSubstitutor;

import br.com.servicoemcasa.infra.WebPath;
import br.com.servicoemcasa.model.service.BroadcastService;
import br.com.servicoemcasa.model.service.Broadcastable;
import br.com.servicoemcasa.model.vo.Email;

public class EmailBroadcastServiceImpl implements BroadcastService<Email> {

    private Session session;
    private String webPath;

    @Inject
    EmailBroadcastServiceImpl(final Session session, final @WebPath String webPath) {
        this.session = session;
        this.webPath = webPath;
    }

    public void send(String destino, String subject, String body) {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("waymobi@waymobi.com.br <WayMobi>"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destino));
            message.setSubject(subject);
            message.setText(body);
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getResource(Broadcastable message) {
        return webPath + message.getClass().getSimpleName() + "/";
    }

    @Override
    public void broadcast(Broadcastable message, Email... broadcastDestination) {
        Map<String, String> valuesMap = message.getMessage().getParams();
        valuesMap.put("resource", getResource(message));
        String body = message.getMessage().getMessage() + "\n\nWayMobi\nhttp://www.waymobi.com.br";
        StrSubstitutor sub = new StrSubstitutor(valuesMap);
        send(broadcastDestination[0].getEmail(), "WayMobi - " + message.getMessage().getSubject(), sub.replace(body));
    }

}
