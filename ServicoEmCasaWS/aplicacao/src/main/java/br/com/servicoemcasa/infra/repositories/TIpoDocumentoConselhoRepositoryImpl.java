package br.com.servicoemcasa.infra.repositories;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.servicoemcasa.model.TipoDocumentoConselho;
import br.com.servicoemcasa.model.TipoDocumentoConselhoRepository;

public class TIpoDocumentoConselhoRepositoryImpl extends DefaultRepository<String, TipoDocumentoConselho> implements
        TipoDocumentoConselhoRepository {

    @Inject
    TIpoDocumentoConselhoRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

}
