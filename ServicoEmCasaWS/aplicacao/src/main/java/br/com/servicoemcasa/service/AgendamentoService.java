package br.com.servicoemcasa.service;

import java.util.List;

import javax.inject.Inject;

import br.com.servicoemcasa.aop.Transactional;
import br.com.servicoemcasa.model.Agendamento;
import br.com.servicoemcasa.model.AgendamentoRepository;
import br.com.servicoemcasa.model.SolicitacaoAtendimento;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoRepository;

public class AgendamentoService {

    private SolicitacaoAtendimentoRepository solicitacaoAtendimentoRepository;
    private AgendamentoRepository agendamentoRepository;

    @Inject
    AgendamentoService(SolicitacaoAtendimentoRepository solicitacaoAtendimentoRepository,
            AgendamentoRepository agendamentoRepository) {

        this.solicitacaoAtendimentoRepository = solicitacaoAtendimentoRepository;
        this.agendamentoRepository = agendamentoRepository;
    }

    @Transactional
    public Agendamento incluiAgendamento(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        SolicitacaoAtendimento solicitacaoAtendimento = solicitacaoAtendimentoRepository
                .get(solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCodigo());
        return solicitacaoAtendimento.agenda(solicitacaoAtendimentoPrestador);
    }

    @Transactional
    public void encerraAtendimentos() {
        List<Agendamento> agendamentos = agendamentoRepository.listaAgendamentosParaEncerrar();
        for (Agendamento agendamento : agendamentos) {
            agendamento.encerra();
        }
    }

    @Transactional
    public void avaliaAtendimento(long codigo, boolean positivo) {
        Agendamento agendamento = agendamentoRepository.get(codigo);
        agendamento.avalia(positivo);
    }

}
