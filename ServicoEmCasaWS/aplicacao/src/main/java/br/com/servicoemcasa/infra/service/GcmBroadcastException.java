package br.com.servicoemcasa.infra.service;

public class GcmBroadcastException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public GcmBroadcastException(String message) {
        super(message);
    }

}
