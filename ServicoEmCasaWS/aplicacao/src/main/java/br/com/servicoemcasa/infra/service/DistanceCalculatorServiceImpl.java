package br.com.servicoemcasa.infra.service;

import br.com.servicoemcasa.model.service.DistanceCalculatorService;


public class DistanceCalculatorServiceImpl implements DistanceCalculatorService {

    public final static double AVERAGE_RADIUS_OF_EARTH = 6371;
    
    DistanceCalculatorServiceImpl() {
    }

    public int calculateDistance(double latitudeOrigin, double longitudeOrigin, double latitudeDest,
            double longitudeDest) {
        
        /***
         * IMPLEMENTADO TAMBÈM NA FUNÇÃO DO BANCO CALCULATE_DISTANCE
         */

        double latDistance = Math.toRadians(latitudeOrigin - latitudeDest);
        double lngDistance = Math.toRadians(longitudeOrigin - longitudeDest);

        double a = (Math.sin(latDistance / 2) * Math.sin(latDistance / 2)) + (Math.cos(Math.toRadians(latitudeOrigin)))
                * (Math.cos(Math.toRadians(latitudeDest))) * (Math.sin(lngDistance / 2)) * (Math.sin(lngDistance / 2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH * c));
    }

}
