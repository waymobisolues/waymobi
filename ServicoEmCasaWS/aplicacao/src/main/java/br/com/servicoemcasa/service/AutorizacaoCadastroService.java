package br.com.servicoemcasa.service;

import javax.inject.Inject;
import javax.persistence.NoResultException;

import br.com.servicoemcasa.aop.Transactional;
import br.com.servicoemcasa.model.AutorizacaoCadastro;
import br.com.servicoemcasa.model.AutorizacaoCadastroRepository;

public class AutorizacaoCadastroService {

    private final AutorizacaoCadastroRepository autorizacaoCadastroRepository;

    @Inject
    AutorizacaoCadastroService(final AutorizacaoCadastroRepository autorizacaoCadastroRepository) {
        this.autorizacaoCadastroRepository = autorizacaoCadastroRepository;
    }

    @Transactional
    public void liberaAcesso(String id) {
        AutorizacaoCadastro autorizacao = autorizacaoCadastroRepository.get(id);
        if (autorizacao != null) {
            autorizacao.liberaCadastro();
        } else {
            throw new NoResultException();
        }
    }

}
