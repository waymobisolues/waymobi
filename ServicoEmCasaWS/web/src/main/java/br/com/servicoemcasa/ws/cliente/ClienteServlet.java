package br.com.servicoemcasa.ws.cliente;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.Cliente;
import br.com.servicoemcasa.model.ClienteRepository;
import br.com.servicoemcasa.service.ClienteService;
import br.com.servicoemcasa.util.Types;
import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.ConflictException;
import br.com.servicoemcasa.ws.NotFoundException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/Cliente/*")
public class ClienteServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String gcmKey = getIdFromURLAsString(req);
        if (Types.isInteger(gcmKey)) {
            buscaClientePorId(req, resp);
        } else {
            buscaClientePorGcm(gcmKey, req, resp);
        }
    }

    private void buscaClientePorId(HttpServletRequest req, HttpServletResponse resp) throws NotFoundException,
            IOException, ServletException {

        Long id = getIdFromURL(req);
        if (id == null) {
            throw new NotFoundException();
        }
        ClienteRepository clienteRepository = getInstance(ClienteRepository.class);
        Cliente cliente = clienteRepository.get(id.intValue());
        if (cliente == null) {
            throw new NotFoundException();
        } else {
            sendResponse(req, resp, Cliente.class, cliente);
        }
    }

    private void buscaClientePorGcm(String gcmKey, HttpServletRequest req, HttpServletResponse resp)
            throws NotFoundException, IOException, ServletException {

        ClienteRepository clienteRepository = getInstance(ClienteRepository.class);
        Cliente cliente = clienteRepository.getByGcm(gcmKey);
        if (cliente == null) {
            throw new NotFoundException();
        } else {
            sendResponse(req, resp, Cliente.class, cliente);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id != null) {
            throw new BadRequestException();
        }
        Cliente cliente = getCliente(req, resp);
        if (cliente.getCodigo() != 0) {
            throw new ConflictException();
        }
        cliente = salvaCliente(cliente);
        sendCreatedResponse(req, resp, "Cliente/" + cliente.getCodigo(), Cliente.class, cliente);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            throw new BadRequestException();
        }
        Cliente cliente = getCliente(req, resp);
        if (cliente.getCodigo() == 0) {
            throw new ConflictException();
        }
        cliente = salvaCliente(cliente);
        sendResponse(req, resp, Cliente.class, cliente);
    }

    private Cliente getCliente(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException,
            BadRequestException {
        Cliente cliente = parseParameters(req, resp, Cliente.class);
        if (cliente == null) {
            throw new BadRequestException();
        }
        return cliente;
    }

    private Cliente salvaCliente(Cliente cliente) {
        ClienteService clienteService = getInstance(ClienteService.class);
        return clienteService.salvaCliente(cliente);
    }

}
