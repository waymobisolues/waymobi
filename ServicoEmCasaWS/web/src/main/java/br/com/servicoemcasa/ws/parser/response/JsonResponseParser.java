package br.com.servicoemcasa.ws.parser.response;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.util.JsonParser;

public class JsonResponseParser implements ResponseParser {

    @Override
    public void sendResponse(HttpServletRequest request, HttpServletResponse response, Class<?> classOf, Object obj) throws IOException {
        JsonParser.toJson(obj, response.getWriter());
    }

    @Override
    public boolean accept(HttpServletRequest request) {
        String accept = request.getHeader("Accept");
        return accept.indexOf("application/json") > -1;
    }

}
