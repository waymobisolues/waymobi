package br.com.servicoemcasa.ws.prestador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.Prestador;
import br.com.servicoemcasa.model.PrestadorRepository;
import br.com.servicoemcasa.service.PrestadorService;
import br.com.servicoemcasa.util.Types;
import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.ConflictException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/Prestador/*")
public class PrestadorServlet extends WsServlet {

    public static final String PATH = "/Prestador/";

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String gcmKey = getIdFromURLAsString(req);
        if (Types.isNullOrEmpty(gcmKey)) {
            throw new BadRequestException();
        } else {
            recuperaPrestador(req, resp, gcmKey);
        }
    }

    private void recuperaPrestador(HttpServletRequest req, HttpServletResponse resp, String gcmKey) throws IOException,
            ServletException {

        PrestadorRepository prestadorRepository = getInstance(PrestadorRepository.class);
        Prestador prestador = prestadorRepository.getByGcm(gcmKey);
        sendResponse(req, resp, Prestador.class, prestador);
    }

    public static class CadastroPrestador {
        Prestador prestador;
        byte[] foto;
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String gcmKey = getIdFromURLAsString(req);
        if (Types.isNullOrEmpty(gcmKey)) {
            throw new BadRequestException();
        } else {
            CadastroPrestador cadastroPrestador = parseParameters(req, resp, CadastroPrestador.class);
            if (!cadastroPrestador.prestador.getGcm().getHashedKey().equals(gcmKey)) {
                throw new ConflictException();
            } else {
                PrestadorService prestadorService = getInstance(PrestadorService.class);
                prestadorService.salvaPrestador(cadastroPrestador.prestador, cadastroPrestador.foto);
            }
        }
    }

}
