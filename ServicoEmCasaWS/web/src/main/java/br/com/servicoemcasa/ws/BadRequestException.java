package br.com.servicoemcasa.ws;

import javax.servlet.http.HttpServletResponse;

public class BadRequestException extends WsServletException {

    private static final long serialVersionUID = 1L;

    public BadRequestException() {
        super();
    }

    public BadRequestException(String message) {
        super(message);
    }

    @Override
    int getStatusCode() {
        return HttpServletResponse.SC_BAD_REQUEST;
    }

}
