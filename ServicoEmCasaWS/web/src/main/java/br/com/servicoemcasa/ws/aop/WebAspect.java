package br.com.servicoemcasa.ws.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareError;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class WebAspect {

    @Pointcut("call(* javax.servlet.http.HttpServletResponse.sendRedirect(..)) && !within(br.com.servicoemcasa.ws.WsServlet)")
    void sendRedirect() {
    }

    @Pointcut("call(* javax.servlet.http.HttpServletResponse.sendError(..)) && !within(br.com.servicoemcasa.ws.WsServlet) "
            + "&& !within(br.com.servicoemcasa.ws.parser.ResponseStrategy) && !within(br.com.servicoemcasa.ws.parser.RequestStrategy)")
    void sendError() {
    }

    @Pointcut("call(* javax.servlet.http.HttpServletRequest.getRequestDispatcher(..)) && !within(br.com.servicoemcasa.ws.parser.response.ResponseParser+)")
    void getRequestDispatcher() {
    }

    @Pointcut("call(* javax.servlet.http.HttpServletResponse.getWriter()) && !within(br.com.servicoemcasa.ws.WsServlet) "
            + "&& !within(br.com.servicoemcasa.ws.parser.response.ResponseParser+)")
    void getWriter() {
    }

    @Pointcut("call(* javax.servlet.http.HttpServletResponse.getOutputStream()) && !within(br.com.servicoemcasa.ws.WsServlet) "
            + "&& !within(br.com.servicoemcasa.ws.parser.response.ResponseParser+)")
    void getOutputStream() {
    }
    
    @Pointcut("execution(@br.com.servicoemcasa.aop.Transactional * *(..))")
    void transactional() {
    }

    @DeclareError("sendRedirect()")
    static final String sendRedirect = "Chamada ao método sendRedirect inválido. Use WsServlet.sendRedirect.";

    @DeclareError("sendError()")
    static final String sendError = "Chamada ao método sendError inválido. Use WsServlet.sendError.";

    @DeclareError("getRequestDispatcher()")
    static final String requestDispatcher = "Chamada ao método getRequestDispatcher() inválida. Use WsServlet.sendResponse.";

    @DeclareError("getWriter()")
    static final String writer = "Chamada ao método getWriter() inválida. Use WsServlet.sendResponse.";

    @DeclareError("getOutputStream()")
    static final String outputStream = "Chamada ao método getOutputStream() inválida. Use WsServlet.sendResponse.";
    
    @DeclareError("transactional()")
    static final String transactional = "Métodos da servlet não devem ser transacionais. Implemente um serviço.";

}
