package br.com.servicoemcasa.ws.comum;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.infra.WebPath;
import br.com.servicoemcasa.ws.WsRegistry;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/Map")
public class MapServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Inject
    @WebPath
    private String webPath;

    static class Map {
        final String nome;
        final String url;

        Map(String nome, String url) {
            this.nome = nome;
            this.url = url;
        }
    }

    @Inject
    public MapServlet() {
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Map> map = new ArrayList<>();
        for (WsRegistry mapping : WsRegistry.values()) {
            map.add(new Map(mapping.name(), new URL(new URL(webPath), req.getContextPath() + "/" + mapping.name())
                    .toExternalForm()));
        }
        sendResponse(req, resp, Map.class, map);
    }
}
