package br.com.servicoemcasa.ws.comum;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.UF;
import br.com.servicoemcasa.model.UFRepository;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/UF")
public class UFServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UFRepository repository = getInstance(UFRepository.class);
        List<UF> lista = repository.list();
        super.sendResponse(req, resp, UF.class, lista);
    }

}
