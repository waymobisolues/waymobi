package br.com.servicoemcasa.ws.comum;

import static br.com.servicoemcasa.util.StringUtils.isNullOrEmpty;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.SolicitacaoAtendimento;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoRepository;
import br.com.servicoemcasa.service.SolicitarAtendimentoService;
import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.ConflictException;
import br.com.servicoemcasa.ws.NotFoundException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/SolicitacaoAtendimento/*")
public class SolicitacaoAtendimentoServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    public SolicitacaoAtendimentoServlet() {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            listaSolicitacoes(req, resp);
        } else {
            recuperaSolicitacao(req, resp, id);
        }
    }

    private void listaSolicitacoes(HttpServletRequest req, HttpServletResponse resp) throws IOException,
            ServletException {

        List<SolicitacaoAtendimento> solicitacoes = null;
        String cod = req.getParameter("codigoCliente");
        if (cod == null) {
            cod = req.getParameter("codigoPrestador");
            if (isNullOrEmpty(cod)) {
                throw new BadRequestException();
            } else {
                int codigoPrestador = Integer.parseInt(cod);
                SolicitacaoAtendimentoRepository repo = getInstance(SolicitacaoAtendimentoRepository.class);
                solicitacoes = repo.findByCodigoPrestador(codigoPrestador);
            }
        } else {
            int codigoCliente = Integer.parseInt(cod);
            SolicitacaoAtendimentoRepository repo = getInstance(SolicitacaoAtendimentoRepository.class);
            solicitacoes = repo.findByCodigoCliente(codigoCliente);
        }
        sendResponse(req, resp, SolicitacaoAtendimento.class, solicitacoes);
    }

    private void recuperaSolicitacao(HttpServletRequest req, HttpServletResponse resp, Long id)
            throws NotFoundException, IOException, ServletException {

        SolicitacaoAtendimentoRepository repo = getInstance(SolicitacaoAtendimentoRepository.class);
        SolicitacaoAtendimento solicitacaoAtendimento = repo.get(id);
        if (solicitacaoAtendimento == null) {
            throw new NotFoundException();
        } else {
            sendResponse(req, resp, SolicitacaoAtendimento.class, solicitacaoAtendimento);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SolicitacaoAtendimento solicitacao = parseParameters(req, resp, SolicitacaoAtendimento.class);
        if (solicitacao == null) {
            throw new BadRequestException();
        }
        solicitaAtendimento(solicitacao);
        sendPartialResponse(req, resp, SolicitacaoAtendimento.class, solicitacao);
    }

    private void solicitaAtendimento(SolicitacaoAtendimento solicitacao) {
        SolicitarAtendimentoService localizador = getInstance(SolicitarAtendimentoService.class);
        localizador.solicitaAtendimento(solicitacao);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            throw new BadRequestException();
        }
        SolicitacaoAtendimentoPrestador prestador = parseParameters(req, resp, SolicitacaoAtendimentoPrestador.class);
        if (prestador.getSolicitacaoAtendimento().getCodigo() != id) {
            throw new ConflictException();
        }
        aceitaAtendimento(id, prestador);
    }

    private void aceitaAtendimento(Long id, SolicitacaoAtendimentoPrestador prestador) {
        SolicitarAtendimentoService service = getInstance(SolicitarAtendimentoService.class);
        service.aceitaAtendimento(prestador);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            throw new BadRequestException();
        }
        SolicitacaoAtendimento solicitacao = encerrarSolicitacao(id);
        sendResponse(req, resp, SolicitacaoAtendimento.class, solicitacao);
    }

    private SolicitacaoAtendimento encerrarSolicitacao(Long id) {
        SolicitacaoAtendimentoRepository repo = getInstance(SolicitacaoAtendimentoRepository.class);
        SolicitacaoAtendimento solicitacaoAtendimento = repo.get(id);
        solicitacaoAtendimento.encerrarSolicitacao();
        return solicitacaoAtendimento;
    }

}
