package br.com.servicoemcasa.ws.comum;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.service.AutorizacaoCadastroService;
import br.com.servicoemcasa.util.Types;
import br.com.servicoemcasa.ws.NotFoundException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/AutorizacaoCadastro/*")
public class AutorizacaoCadastroServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = getIdFromURLAsString(req);
        if (Types.isNullOrEmpty(id)) {
            throw new NotFoundException();
        }
        AutorizacaoCadastroService service = getInstance(AutorizacaoCadastroService.class);
        service.liberaAcesso(id);
        sendSeeOther(resp, "/html/autorizacao/sucesso.html");
    }

}
