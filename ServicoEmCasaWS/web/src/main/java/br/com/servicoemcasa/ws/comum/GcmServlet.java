package br.com.servicoemcasa.ws.comum;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.aop.Level;
import br.com.servicoemcasa.aop.Loggable;
import br.com.servicoemcasa.model.Gcm;
import br.com.servicoemcasa.service.GcmService;
import br.com.servicoemcasa.util.Types;
import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/Gcm/*")
public class GcmServlet extends WsServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("regId");
        if (Types.isNullOrEmpty(id)) {
            throw new BadRequestException();
        }
        Gcm gcm = save(null, id);
        sendCreatedResponse(req, resp, null, Gcm.class, gcm);
    }

    @Loggable(level = Level.INFO)
    private Gcm save(String hashIdExistente, String id) {
        GcmService gcmService = getInstance(GcmService.class);
        return gcmService.registraGcm(hashIdExistente, id);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String hashIdExistente = getIdFromURLAsString(req);
        String novoId = req.getParameter("regId");
        if (Types.isNullOrEmpty(hashIdExistente) || Types.isNullOrEmpty(novoId)) {
            throw new BadRequestException();
        }
        Gcm gcm = save(hashIdExistente, novoId);
        sendResponse(req, resp, Gcm.class, gcm);
    }

}
