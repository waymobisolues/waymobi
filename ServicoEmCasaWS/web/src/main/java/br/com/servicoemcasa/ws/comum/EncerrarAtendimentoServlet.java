package br.com.servicoemcasa.ws.comum;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.service.AgendamentoService;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/EncerrarAtendimento")
public class EncerrarAtendimentoServlet extends WsServlet {

    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AgendamentoService agendamentoService = getInstance(AgendamentoService.class);
        agendamentoService.encerraAtendimentos();
    }

}
