package br.com.servicoemcasa.ws.parser.response;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HtmlResponseParser implements ResponseParser {

    @Override
    public void sendResponse(HttpServletRequest request, HttpServletResponse response, Class<?> classOf, Object obj) throws IOException {
        try {
            String className = obj.getClass().getSimpleName();
            request.setAttribute(className, obj);
            request.getRequestDispatcher(String.format("/WEB-INF/jsp/%s.jsp", className)).forward(request, response);
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean accept(HttpServletRequest request) {
        String accept = request.getHeader("Accept");
        return accept.indexOf("text/html") > -1 || accept.indexOf("text/plain") > -1;
    }

}
