package br.com.servicoemcasa.ws;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import br.com.servicoemcasa.DependencyFactory;
import br.com.servicoemcasa.aop.Level;
import br.com.servicoemcasa.aop.Loggable;
import br.com.servicoemcasa.util.SimpleValidator;
import br.com.servicoemcasa.ws.parser.RequestStrategy;
import br.com.servicoemcasa.ws.parser.ResponseStrategy;

public abstract class WsServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected <T> T getInstance(Class<T> classOf) {
        return DependencyFactory.getInstance(classOf);
    }
    
    @Loggable(level = Level.DEBUG)
    protected Long getIdFromURL(HttpServletRequest req) {
        String[] urlInfo = getURLInfo(req);
        if (urlInfo.length > 1) {
            return Long.parseLong(urlInfo[1]);
        }
        return null;
    }

    @Loggable(level = Level.DEBUG)
    protected String getIdFromURLAsString(HttpServletRequest req) {
        String[] urlInfo = getURLInfo(req);
        if (urlInfo.length > 1) {
            return urlInfo[1];
        }
        return null;
    }

    @Loggable(level = Level.DEBUG)
    protected String[] getURLInfo(HttpServletRequest req) {
        try {
            URL url = new URL(req.getRequestURL().toString());
            String path = url.getPath();
            return path.substring(req.getContextPath().length() + 1).split("\\/");
        } catch (MalformedURLException e) {
            return new String[0];
        }
    }

    @Loggable(level = Level.DEBUG)
    protected void sendSeeOther(HttpServletResponse resp, String location) throws IOException {
        resp.setStatus(HttpServletResponse.SC_SEE_OTHER);
        setLocationHeader(resp, location);
    }

    protected void sendResponse(HttpServletRequest req, HttpServletResponse resp, Class<?> classOf, Object obj)
            throws IOException, ServletException {

        sendResponse(req, resp, HttpServletResponse.SC_OK, classOf, obj);
    }

    protected void sendPartialResponse(HttpServletRequest req, HttpServletResponse resp, Class<?> classOf, Object obj)
            throws IOException, ServletException {

        sendResponse(req, resp, HttpServletResponse.SC_PARTIAL_CONTENT, classOf, obj);
    }

    protected void sendCreatedResponse(HttpServletRequest req, HttpServletResponse resp, String location,
            Class<?> classOf, Object obj) throws IOException, ServletException {

        if (location != null) {
            setLocationHeader(resp, location);
        }
        sendResponse(req, resp, HttpServletResponse.SC_CREATED, classOf, obj);
    }

    private void setLocationHeader(HttpServletResponse resp, String location) {
        resp.setHeader("Location", getServletContext().getContextPath() + location);
    }

    @Loggable(level = Level.DEBUG)
    private void sendResponse(HttpServletRequest req, HttpServletResponse resp, int code, Class<?> classOf, Object obj)
            throws IOException, ServletException {

        resp.setStatus(code);
        ResponseStrategy.getInstance().sendResponse(req, resp, classOf, obj);
    }

    @Loggable(level = Level.DEBUG)
    protected <T> T parseParameters(HttpServletRequest req, HttpServletResponse resp, Class<T> classOf)
            throws IOException, ServletException {

        return RequestStrategy.getInstance().parse(req, resp, classOf);
    }

    private void sendError(HttpServletRequest req, HttpServletResponse resp, int code, String desc) throws IOException,
            ServletException {

        resp.setStatus(code);
        ResponseStrategy.getInstance().sendResponse(req, resp, String.class, desc);
    }

    @Loggable(level = Level.DEBUG)
    private void sendError(HttpServletResponse resp, int code) throws IOException {
        resp.sendError(code);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            super.service(req, resp);
        } catch (ConstraintViolationException e) {
            ConstraintViolationException val = (ConstraintViolationException) e;
            sendError(req, resp, HttpServletResponse.SC_BAD_REQUEST,
                    SimpleValidator.getFirstException(val.getConstraintViolations()));
        } catch (WsServletException e) {
            sendError(resp, e.getStatusCode());
        } catch (NoResultException e) {
            sendError(resp, HttpServletResponse.SC_NOT_FOUND);
        } catch (Exception e) {
            sendError(resp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

}
