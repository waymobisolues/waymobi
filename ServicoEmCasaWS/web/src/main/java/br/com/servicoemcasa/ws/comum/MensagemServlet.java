package br.com.servicoemcasa.ws.comum;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.Conversa;
import br.com.servicoemcasa.model.ConversaRepository;
import br.com.servicoemcasa.service.SolicitarAtendimentoService;
import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.ConflictException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/Mensagem/*")
public class MensagemServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            throw new BadRequestException();
        }
        sendLista(id, req, resp);
    }

    private void sendLista(long codigoSolicitacaoAtendimentoPrestador, HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException {
        ConversaRepository conversaRepository = getInstance(ConversaRepository.class);
        sendResponse(req, resp, Conversa.class,
                conversaRepository.listByCodigoAtendimentoPrestador(codigoSolicitacaoAtendimentoPrestador));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            throw new BadRequestException();
        }
        SolicitarAtendimentoService solicitarAtendimentoService = getInstance(SolicitarAtendimentoService.class);
        Conversa conversa = parseParameters(req, resp, Conversa.class);
        if (conversa == null || conversa.getSolicitacaoAtendimentoPrestador().getCodigo() != id) {
            throw new ConflictException();
        }
        solicitarAtendimentoService.enviaMensagem(conversa);
        sendLista(id, req, resp);
    }
}
