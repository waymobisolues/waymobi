package br.com.servicoemcasa.ws;

import javax.servlet.annotation.WebServlet;

import br.com.servicoemcasa.ws.cliente.ClienteServlet;
import br.com.servicoemcasa.ws.comum.AgendamentoServlet;
import br.com.servicoemcasa.ws.comum.AutorizacaoCadastroServlet;
import br.com.servicoemcasa.ws.comum.GcmServlet;
import br.com.servicoemcasa.ws.comum.GrupoTipoPrestadorServlet;
import br.com.servicoemcasa.ws.comum.LoginServlet;
import br.com.servicoemcasa.ws.comum.MapServlet;
import br.com.servicoemcasa.ws.comum.MensagemServlet;
import br.com.servicoemcasa.ws.comum.SolicitacaoAtendimentoPrestadorServlet;
import br.com.servicoemcasa.ws.comum.SolicitacaoAtendimentoServlet;
import br.com.servicoemcasa.ws.comum.TipoPrestadorServlet;
import br.com.servicoemcasa.ws.comum.UFServlet;
import br.com.servicoemcasa.ws.prestador.PrestadorServlet;
import br.com.servicoemcasa.ws.prestador.TipoDocumentoConselhoServlet;

public enum WsRegistry {

    Cliente(ClienteServlet.class), SolicitacaoAtendimento(SolicitacaoAtendimentoServlet.class), Agendamento(
            AgendamentoServlet.class), AutorizacaoCadastro(AutorizacaoCadastroServlet.class), Gcm(GcmServlet.class), GrupoTipoPrestador(
            GrupoTipoPrestadorServlet.class), Login(LoginServlet.class), Map(MapServlet.class), TipoPrestador(
            TipoPrestadorServlet.class), UF(UFServlet.class), Prestador(PrestadorServlet.class), TipoDocumentoConselho(
            TipoDocumentoConselhoServlet.class), Mensagem(MensagemServlet.class), SolicitacaoAtendimentoPrestador(
            SolicitacaoAtendimentoPrestadorServlet.class);

    private final String uri;

    WsRegistry(Class<? extends WsServlet> type) {
        getClass().getClassLoader().getResource("/");
        WebServlet webServlet = type.getAnnotation(WebServlet.class);
        String[] uris = webServlet.value();
        if (uris.length != 1) {
            uris = webServlet.urlPatterns();
            if (uris.length != 1) {
                throw new RuntimeException("Servlet com configuração inválida " + type);
            }
        }
        if (uris[0].endsWith("*")) {
            uri = uris[0].substring(0, uris[0].indexOf("/", 2));
        } else {
            uri = uris[0];
        }
    }

    public String map(int param) {
        return map(String.valueOf(param));
    }

    public String map(long param) {
        return map(String.valueOf(param));
    }

    public String map(String param) {
        return String.format("/%s/%s", uri, param);
    }

}
