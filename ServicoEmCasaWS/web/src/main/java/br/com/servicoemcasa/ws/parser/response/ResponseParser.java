package br.com.servicoemcasa.ws.parser.response;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ResponseParser {

    void sendResponse(HttpServletRequest request, HttpServletResponse response, Class<?> classOf, Object obj) throws IOException;

    boolean accept(HttpServletRequest request);

}
