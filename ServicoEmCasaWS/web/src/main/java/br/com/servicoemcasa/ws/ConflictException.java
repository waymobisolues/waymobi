package br.com.servicoemcasa.ws;

import javax.servlet.http.HttpServletResponse;

public class ConflictException extends WsServletException {

    private static final long serialVersionUID = 1L;

    @Override
    int getStatusCode() {
        return HttpServletResponse.SC_CONFLICT;
    }

}
