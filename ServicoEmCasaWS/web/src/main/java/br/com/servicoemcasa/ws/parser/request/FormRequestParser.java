package br.com.servicoemcasa.ws.parser.request;

import javax.servlet.http.HttpServletRequest;

import br.com.servicoemcasa.util.MapParser;

public class FormRequestParser implements RequestParser {

    @Override
    public <T> T parse(HttpServletRequest request, Class<T> classOf) {
        return MapParser.fromMap(request.getParameterMap(), classOf);
    }

    @Override
    public boolean accept(HttpServletRequest request) {
        String contentType = request.getContentType();
        return "GET".equals(request.getMethod()) || "application/x-www-form-urlencoded".equals(contentType);
    }

}
