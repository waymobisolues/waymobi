package br.com.servicoemcasa.ws.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.parser.request.FormRequestParser;
import br.com.servicoemcasa.ws.parser.request.JsonRequestParser;
import br.com.servicoemcasa.ws.parser.request.RequestParser;
import br.com.servicoemcasa.ws.parser.request.XmlRequestParser;

public class RequestStrategy {

    private static RequestStrategy instance;

    private List<RequestParser> parsers = new ArrayList<RequestParser>();

    private RequestStrategy() {
        parsers.add(new JsonRequestParser());
        parsers.add(new XmlRequestParser());
        parsers.add(new FormRequestParser());
    }

    public static RequestStrategy getInstance() {
        return instance = instance == null ? new RequestStrategy() : instance;
    }

    public <T> T parse(HttpServletRequest request, HttpServletResponse response, Class<T> classOf) throws IOException,
            ServletException {

        for (RequestParser parser : parsers) {
            if (parser.accept(request)) {
                try {
                    T result = parser.parse(request, classOf);
                    return result;
                } catch (Exception e) {
                    throw new BadRequestException();
                }
            }
        }
        throw new BadRequestException("Invalid Content-Type Header");
    }

}
