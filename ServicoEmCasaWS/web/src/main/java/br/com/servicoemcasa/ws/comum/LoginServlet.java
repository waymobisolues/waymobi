package br.com.servicoemcasa.ws.comum;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.AutorizacaoCadastro;
import br.com.servicoemcasa.service.LoginService;
import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.NotFoundException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/Login/*")
public class LoginServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LoginInfo loginInfo = parseParameters(req, resp, LoginInfo.class);
        if (loginInfo == null || loginInfo.getGcm() == null || loginInfo.getEmail() == null) {
            throw new BadRequestException();
        } else {
            processaLogin(req, resp, loginInfo);
        }
    }

    private void processaLogin(HttpServletRequest req, HttpServletResponse resp, LoginInfo loginInfo)
            throws IOException, ServletException {

        LoginService loginService = getInstance(LoginService.class);
        AutorizacaoCadastro autorizacaoCadastro = loginService.processaAutorizacao(loginInfo.getEmail(),
                loginInfo.getGcm());
        if (autorizacaoCadastro == null) {
            throw new NotFoundException();
        }
        sendPartialResponse(req, resp, AutorizacaoCadastro.class, autorizacaoCadastro);
    }
}
