package br.com.servicoemcasa.ws.cliente;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.service.AgendamentoService;
import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/AvaliarAtendimento/*")
public class AvaliacaoAtendimentoServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            throw new BadRequestException();
        }
        AgendamentoService service = getInstance(AgendamentoService.class);
        boolean positivo = parseParameters(req, resp, Boolean.class);
        service.avaliaAtendimento(id, positivo);
    }

}
