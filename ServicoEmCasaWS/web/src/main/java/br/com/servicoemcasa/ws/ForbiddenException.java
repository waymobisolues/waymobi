package br.com.servicoemcasa.ws;

import javax.servlet.http.HttpServletResponse;

public class ForbiddenException extends WsServletException {

    private static final long serialVersionUID = 1L;

    @Override
    int getStatusCode() {
        return HttpServletResponse.SC_FORBIDDEN;
    }

}
