package br.com.servicoemcasa.ws.comum;

import br.com.servicoemcasa.model.Gcm;

public class LoginInfo {

    private String email;
    private Gcm gcm;

    public String getEmail() {
        return email;
    }

    public Gcm getGcm() {
        return gcm;
    }

}
