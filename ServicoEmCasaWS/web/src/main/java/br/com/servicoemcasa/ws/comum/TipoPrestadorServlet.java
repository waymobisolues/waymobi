package br.com.servicoemcasa.ws.comum;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.TipoPrestador;
import br.com.servicoemcasa.model.TipoPrestadorRepository;
import br.com.servicoemcasa.model.vo.LocalAtendimento;
import br.com.servicoemcasa.util.Types;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/TipoPrestador")
public class TipoPrestadorServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TipoPrestadorRepository tipoPrestadorRepository = getInstance(TipoPrestadorRepository.class);
        int grupo = Types.parseInt(req.getParameter("grupo"));
        Float latitude = Types.parseFloat(req.getParameter("latitude"));
        Float longitude = Types.parseFloat(req.getParameter("longitude"));
        String localAtendimento = req.getParameter("localAtendimento");
        List<TipoPrestador> list = null;
        if (latitude != null && longitude != null && localAtendimento != null) {
            list = tipoPrestadorRepository.findByCodigoGrupoAndDistancia(grupo, latitude, longitude,
                    LocalAtendimento.valueOf(localAtendimento));
        } else {
            list = tipoPrestadorRepository.findByCodigoGrupo(grupo);
        }
        sendResponse(req, resp, TipoPrestador.class, list);
    }

}
