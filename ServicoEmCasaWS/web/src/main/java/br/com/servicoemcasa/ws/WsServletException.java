package br.com.servicoemcasa.ws;

import javax.servlet.ServletException;

public abstract class WsServletException extends ServletException {

    private static final long serialVersionUID = 1L;

    public WsServletException() {
        super();
    }

    public WsServletException(String message) {
        super(message);
    }

    abstract int getStatusCode();

}
