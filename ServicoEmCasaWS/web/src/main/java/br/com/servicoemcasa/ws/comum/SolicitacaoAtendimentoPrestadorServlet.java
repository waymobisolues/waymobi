package br.com.servicoemcasa.ws.comum;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestadorRepository;
import br.com.servicoemcasa.ws.NotFoundException;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/SolicitacaoAtendimentoPrestador/*")
public class SolicitacaoAtendimentoPrestadorServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    public SolicitacaoAtendimentoPrestadorServlet() {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            throw new NotFoundException();
        } else {
            recuperaSolicitacao(req, resp, id);
        }
    }

    private void recuperaSolicitacao(HttpServletRequest req, HttpServletResponse resp, Long id)
            throws NotFoundException, IOException, ServletException {

        SolicitacaoAtendimentoPrestadorRepository repo = getInstance(SolicitacaoAtendimentoPrestadorRepository.class);
        SolicitacaoAtendimentoPrestador solicitacaoAtendimento = repo.get(id);
        if (solicitacaoAtendimento == null) {
            throw new NotFoundException();
        } else {
            sendResponse(req, resp, SolicitacaoAtendimentoPrestador.class, solicitacaoAtendimento);
        }
    }

}
