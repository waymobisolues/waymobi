package br.com.servicoemcasa.ws.parser.request;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

public interface RequestParser {

    <T> T parse(HttpServletRequest request, Class<T> classOf) throws IOException;

    boolean accept(HttpServletRequest request);

}
