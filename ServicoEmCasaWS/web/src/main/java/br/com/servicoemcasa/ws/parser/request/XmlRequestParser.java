package br.com.servicoemcasa.ws.parser.request;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import br.com.servicoemcasa.util.XmlParser;

public class XmlRequestParser implements RequestParser {

    @Override
    public <T> T parse(HttpServletRequest request, Class<T> classOf) throws IOException {
        return XmlParser.fromXml(request.getReader(), classOf);
    }

    @Override
    public boolean accept(HttpServletRequest request) {
        String contentType = request.getContentType();
        return !"GET".equals(request.getMethod()) && "text/xml".equals(contentType)
                || "application/xml".equals(contentType);
    }

}
