package br.com.servicoemcasa.ws.comum;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.GrupoTipoPrestador;
import br.com.servicoemcasa.model.GrupoTipoPrestadorRepository;
import br.com.servicoemcasa.model.vo.LocalAtendimento;
import br.com.servicoemcasa.util.Types;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/GrupoTipoPrestador")
public class GrupoTipoPrestadorServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GrupoTipoPrestadorRepository grupoTipoPrestadorRepository = getInstance(GrupoTipoPrestadorRepository.class);
        Float latitude = Types.parseFloat(req.getParameter("latitude"));
        Float longitude = Types.parseFloat(req.getParameter("longitude"));
        String localAtendimento = req.getParameter("localAtendimento");
        List<GrupoTipoPrestador> list = null;
        if (latitude != null && longitude != null && localAtendimento != null) {
            list = grupoTipoPrestadorRepository.findByDistancia(latitude, longitude, LocalAtendimento.valueOf(localAtendimento));
        } else {
            list = grupoTipoPrestadorRepository.list();
        }
        sendResponse(req, resp, GrupoTipoPrestador.class, list);
    }

}
