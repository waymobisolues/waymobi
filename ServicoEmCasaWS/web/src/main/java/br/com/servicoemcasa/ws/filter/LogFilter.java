package br.com.servicoemcasa.ws.filter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;

import br.com.servicoemcasa.util.IOUtils;

@WebFilter(urlPatterns = { "/*" })
public class LogFilter implements Filter {

    private static final Logger logger = Logger.getLogger(LogFilter.class);
    
    private static final String TAG = LogFilter.class.getSimpleName();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        if (logger.isDebugEnabled()) {
            HttpServletRequest req = (HttpServletRequest) request;
            LoggedServletResponse resp = createResponseWrapper(response);
            logger.debug(String.format(
                    "[%s] Requisição para URI %s, método %s, ContentType %s e gerando retorno do tipo %s", TAG,
                    req.getRequestURI(), req.getMethod(), req.getContentType(), req.getHeader("Accept")));
            if (req.getParameterMap().size() == 0) {
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                IOUtils.echo(request.getInputStream(), bout);
                String charset = request.getCharacterEncoding() == null ? "ISO-8859-1" : request.getCharacterEncoding();
                logger.debug(String.format("[%s] Dados recebidos: %s", TAG, new String(bout.toByteArray(), charset)));
                chain.doFilter(createRequestWrapper(request, bout.toByteArray()), resp);
            } else {
                logger.debug(String.format("[%s] Dados recebidos: %s", TAG, request.getParameterMap().toString()));
                chain.doFilter(request, resp);
            }
            logger.debug(String.format("[%s] Resposta %s, conteúdo %s", TAG, resp.getStatus(), resp.getOutputContent()));
        } else {
            chain.doFilter(request, response);
        }
    }

    private class ServletInputStreamWrapper extends ServletInputStream {

        private byte[] data;
        private int i = 0;

        ServletInputStreamWrapper(byte[] data) {
            if (data == null) {
                data = new byte[0];
            }
            this.data = data;
        }

        @Override
        public int read() throws IOException {
            if (i == data.length)
                return -1;
            return data[i++];
        }

        @Override
        public boolean isFinished() {
            return i == data.length;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener readListener) {
            throw new NotImplementedException();
        }

    }

    private ServletRequest createRequestWrapper(final ServletRequest request, final byte[] data) {
        return new HttpServletRequestWrapper((HttpServletRequest) request) {
            @Override
            public ServletInputStream getInputStream() throws IOException {
                return new ServletInputStreamWrapper(data);
            }

            @Override
            public BufferedReader getReader() throws IOException {
                String enc = request.getCharacterEncoding();
                if (enc == null) {
                    enc = "ISO-8859-1";
                }
                return new BufferedReader(new InputStreamReader(new ByteArrayInputStream(data), enc));
            }
        };
    }

    private class ServletOutputStreamWrapper extends ServletOutputStream {

        private ByteArrayOutputStream streamEcho = new ByteArrayOutputStream();
        private ServletOutputStream servletOutputStream;

        public ServletOutputStreamWrapper(ServletOutputStream servletOutputStream) {
            this.servletOutputStream = servletOutputStream;
        }

        @Override
        public boolean isReady() {
            return servletOutputStream.isReady();
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {
            servletOutputStream.setWriteListener(writeListener);
        }

        @Override
        public void write(int b) throws IOException {
            servletOutputStream.write(b);
            streamEcho.write(b);
        }

        @Override
        public String toString() {
            return streamEcho.toString();
        }

    }

    private static class PrintWriterWrapper extends PrintWriter {

        private final ByteArrayOutputStream streamEcho;

        public static PrintWriterWrapper getInstance(final PrintWriter servletWriter) {
            final ByteArrayOutputStream streamEcho = new ByteArrayOutputStream();
            final OutputStreamWriter writerEchoToStream = new OutputStreamWriter(streamEcho);

            PrintWriter echoWriter = new PrintWriter(new Writer() {
                @Override
                public void write(char[] cbuf, int off, int len) throws IOException {
                    writerEchoToStream.write(cbuf, off, len);
                    servletWriter.write(cbuf, off, len);
                }

                @Override
                public void flush() throws IOException {
                    writerEchoToStream.flush();
                    servletWriter.flush();
                }

                @Override
                public void close() throws IOException {
                    writerEchoToStream.close();
                    servletWriter.close();
                }
            });
            return new PrintWriterWrapper(streamEcho, echoWriter);
        }

        private PrintWriterWrapper(ByteArrayOutputStream streamEcho, PrintWriter echoWriter) {
            super(echoWriter);
            this.streamEcho = streamEcho;
        }

        @Override
        public String toString() {
            super.flush();
            return streamEcho.toString();
        }

    }

    private class LoggedServletResponse extends HttpServletResponseWrapper {

        private ServletOutputStream outputStreamWrapper;
        private PrintWriterWrapper printWriterWrapper;

        public LoggedServletResponse(HttpServletResponse resp) {
            super(resp);
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            outputStreamWrapper = new ServletOutputStreamWrapper(super.getOutputStream());
            return outputStreamWrapper;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            printWriterWrapper = PrintWriterWrapper.getInstance(super.getWriter());
            return printWriterWrapper;
        }

        public String getOutputContent() {
            return outputStreamWrapper != null ? outputStreamWrapper.toString()
                    : printWriterWrapper != null ? printWriterWrapper.toString() : "null";
        }

    }

    private LoggedServletResponse createResponseWrapper(final ServletResponse response) {
        return new LoggedServletResponse((HttpServletResponse) response);
    }

    @Override
    public void destroy() {
    }

}
