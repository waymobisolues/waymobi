package br.com.servicoemcasa.ws.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import br.com.servicoemcasa.DependencyFactory;
import br.com.servicoemcasa.aop.Guice;

@WebListener
public class AppContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Guice.setGuiceFactory(DependencyFactory.getInjector());
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }

}