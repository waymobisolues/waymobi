package br.com.servicoemcasa.ws.prestador;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.TipoDocumentoConselho;
import br.com.servicoemcasa.model.TipoDocumentoConselhoRepository;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/TipoDocumentoConselho/*")
public class TipoDocumentoConselhoServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TipoDocumentoConselhoRepository repo = getInstance(TipoDocumentoConselhoRepository.class);
        List<TipoDocumentoConselho> lista = repo.list();
        sendResponse(req, resp, TipoDocumentoConselho.class, lista);
    }

}
