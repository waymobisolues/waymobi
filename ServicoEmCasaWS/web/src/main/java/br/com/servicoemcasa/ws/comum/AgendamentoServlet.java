package br.com.servicoemcasa.ws.comum;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.model.Agendamento;
import br.com.servicoemcasa.model.AgendamentoRepository;
import br.com.servicoemcasa.model.SolicitacaoAtendimentoPrestador;
import br.com.servicoemcasa.service.AgendamentoService;
import br.com.servicoemcasa.ws.BadRequestException;
import br.com.servicoemcasa.ws.NotFoundException;
import br.com.servicoemcasa.ws.WsRegistry;
import br.com.servicoemcasa.ws.WsServlet;

@WebServlet("/Agendamento/*")
public class AgendamentoServlet extends WsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = getIdFromURL(req);
        if (id == null) {
            throw new NotFoundException();
        }
        AgendamentoRepository agendamentoRepository = getInstance(AgendamentoRepository.class);
        sendResponse(req, resp, Agendamento.class, agendamentoRepository.get(id));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador = parseParameters(req, resp,
                SolicitacaoAtendimentoPrestador.class);
        if (solicitacaoAtendimentoPrestador == null) {
            throw new BadRequestException();
        }
        Agendamento agendamento = criaAgendamento(solicitacaoAtendimentoPrestador);
        sendCreatedResponse(req, resp, WsRegistry.Agendamento.map(agendamento.getCodigo()), Agendamento.class,
                agendamento);
    }

    private Agendamento criaAgendamento(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        AgendamentoService agendamentoService = getInstance(AgendamentoService.class);
        return agendamentoService.incluiAgendamento(solicitacaoAtendimentoPrestador);
    }
    
}
