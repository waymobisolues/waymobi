package br.com.servicoemcasa.ws.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.servicoemcasa.ws.NotAcceptableException;
import br.com.servicoemcasa.ws.parser.response.JsonResponseParser;
import br.com.servicoemcasa.ws.parser.response.ResponseParser;
import br.com.servicoemcasa.ws.parser.response.XmlResponseParser;

public class ResponseStrategy {

    private static ResponseStrategy instance;

    private List<ResponseParser> parsers = new ArrayList<ResponseParser>();

    private ResponseStrategy() {
        parsers.add(new JsonResponseParser());
        parsers.add(new XmlResponseParser());
    }

    public static ResponseStrategy getInstance() {
        return instance = instance == null ? new ResponseStrategy() : instance;
    }

    public void sendResponse(HttpServletRequest request, HttpServletResponse response, Class<?> classOf, Object obj)
            throws IOException, ServletException {

        for (ResponseParser parser : parsers) {
            if (parser.accept(request)) {
                response.setContentType(request.getHeader("Accept"));
                parser.sendResponse(request, response, classOf, obj);
                return;
            }
        }
        throw new NotAcceptableException();
    }

}
