![WayMobi](https://www.waymobi.com.br/logo.png "WayMobi")

#Termos e Condições de Uso

1 - A WayMobi é um provedor de serviço hospedado sob o domínio waymobi.com.br, e disponibilizado através do site http://www.waymobi.com.br e Apps para smartphones;

2 - A sede da WayMobi se localiza na [PREENCHER], o telefone de contato é [PREENCHER] e e-mail de contato é contato@waymobi.com.br

3 - O serviço oferecido pela WayMobi consiste na oferta de espaços virtuais aos usuários de aparelhos smartphone para compra e/ou venda de produtos e/ou serviços em sua plataforma de classificados. Desta forma, a WayMobi apenas oferece um canal de comunicação e aproximação via aplicativo de celular para que o usuário por sua conta e risco venda e/ou compre produtos e/ou serviços, contratem entre si e troquem informações, sem a sua intervenção na conclusão dos negócios. Cabendo ao usuário, proceder levantamentos sobre a fornecedora de produtos e serviços e se certificar das veracidades e informações que lhe são prestadas. A WayMobi não é fornecedora dos produtos e/ou serviços anunciados exclusivamente pelos seus usuários (“Usuário(s)”), tampouco garantidora do cumprimento de qualquer obrigação assumida pelos usuários.

4 - O Usuário que utiliza o aplicativo da WayMobi para vender ou comprar produtos e/ou serviços, o faz por sua conta e risco devendo averiguar cada informação recebida e sua procedência, sem que a WayMobi seja responsável pela veracidade das informações prestadas pelos Usuários, nem pela qualidade e demais detalhes daquilo que é ofertado. A WayMobi não participa e/ou interfere, de qualquer forma, da negociação e/ou efetivação de quaisquer transações.

5 – Todos usuários, deverão com antecedência se certificar sobre a solicitação recebida e sobre a empresa solicitada, não sendo a WayMobi, responsável pelo comparecimento ou exatidão da solicitação.

#####1. OBJETO, ACEITE, CAPACIDADE PARA CADASTRAR-SE E ALTERAÇÕES

1.1. A WayMobi oferece aos Usuários espaços virtuais para compra e/ou venda de produtos e/ou serviços em sua plataforma de classificados. A WayMobi disponibiliza em sua plataforma ferramentas para que os Usuários negociem entre si diretamente e sem sua intervenção na finalização dos negócios. Estes serviços (“Serviços”) são regido por estes Termos e Condições de Uso e Acesso da WayMobi (“T&C”).

1.2. A utilização dos Serviços implica na mais alta compreensão, aceitação e vinculação automática do Usuário aos T&C. Ao fazer uso de quaisquer dos Serviços oferecidos, o Usuário concorda em respeitar e seguir todas e quaisquer diretrizes dispostas nestes T&C e demais documentos incorporados e mencionados nestes.

1.3. O Usuário, se pessoa física, declara ser plenamente capaz para contratar os Serviços. Assim, as pessoas que não gozem de capacidade legal, inclusive os menores de 18 anos, estão impedidos de contratar os Serviços. O usuário que não compreender o presente termo, ou qualquer outra informação constante desse documento, poderá obter suporte para melhor compreensão entrando em contato por e-mail. A WayMobi prestará todos os esclarecimentos necessários, para a perfeita compreensão do teor do referido documento.

1.4. O Usuário, sendo pessoa jurídica, o fará na figura do seu representante legal, razão pela qual, deverá previamente preencher um cadastro junto a WayMobi, fornecer documentos que atestem a existência da empresa e sua capacidade para oferecer produtos e serviços. Após o cadastro realizado, empresa poderá oferecer produtos e serviços livremente. 

1.5. Estes T&C poderão sofrer alterações periódicas, seja por questões legais ou estratégicas da WayMobi. O Usuário desde já concorda e reconhece que é de sua única e inteira responsabilidade a verificação periódica destes T&C. A WayMobi poderá, por mera liberalidade, informar ao Usuário sobre alterações significativas nos T&C, através de avisos na página principal do Site ou por e-mail.

1.5.1. As alterações entrarão em vigor no dia seguinte após a publicação no Site. No prazo de 03 (três) dias contados a partir da publicação das modificações, o Usuário deverá manifestar-se por e-mail caso não concorde com os termos alterados. Caso a não-concordância com a alteração de termos torne-se irreconciliável, o presente vínculo contratual deixará de existir, desde que não haja contas ou dívidas em aberto.

1.5.2. Não havendo manifestação no prazo estipulado, entender-se-á que o Usuário aceitou tacitamente os novos T&C e o contrato continuará vinculando as partes.

1.5.3. As alterações apenas serão válidas para as negociações e anúncios novos.

#####2. CADASTRO
2.1. Para vender ou adquirir produtos e/ou serviços no Site é necessária a realização de um cadastro prévio. Para a efetivação do cadastro, o usuário deve informar um e-mail válido que será confirmado na sequência com o envio de um e-mail automático de confirmação ao endereço informado. O Usuário é responsável por manter estes dados atualizados

2.1.1. O usuário que se cadastrar na condição de prestador de serviços, deverá preencher formulário diferenciado que será enviado pela WayMobi, fornecendo informações sobre a empresa prestadora, ou sua condição autonôma, se adequando aos planos que serão remunerados a WayMobi.

2.1.2. A WayMobi se reserva o direito de utilizar todos os meios possíveis para identificação do Usuário.

2.1.3. Caso a WayMobi tome conhecimento, por meios próprios ou denúncias, da inexatidão dos dados fornecidos no cadastro, o usuário poderá ter seu cadastro suspenso ou excluído imediatamente, sem prévio contato ou formalização,  sem prejuízo de responder civil e criminalmente por isso.

2.1.4 O Usuário ao se cadastrar informará apelido e senha que deverá ser utilizado para acessar sua conta gráfica e página de cadastro. Esta senha é personalíssima e de exclusiva responsabilidade do Usuário, que se compromete a informar imediatamente a WayMobi caso verifique utilização ou conhecimento destas informações por terceiros.

2.2.1. O Usuário também poderá se cadastrar utilizando serviço de autenticação de terceiros como, por exemplo, o Facebook Connect. Desta forma, sua senha e apelido/e-mail cadastrado continuam sendo personalíssimos e de sua exclusiva responsabilidade.

2.2.2 O Usuário só poderá manter um cadastro no Site e é completamente proibida a cessão, compartilhamento, venda e/ou aluguel de cadastro.

2.2.3   É terminantemente proibida a criação de cadastro com uma segunda identidade, ou fornecimento de produtos e serviços que não corresponda a sua capacidade técnica, sob pena de responder civil e criminalmente pela inveracidade das informações.

2.3.1. O cadastro de vídeo testemunho deve seguir os termos de uso da ferramenta utilizada e detalhados em seu respectivo Termo de uso.

2.4.1. Caso seja constatada qualquer das práticas acima citadas, a WayMobi se reserva ao direito de suspender, cancelar e/ou excluir todos os cadastros relacionados sumariamente sem necessidade de oitiva do usuário, mesmo que indiretamente.

2.4.2 As informações prestadas são de inteira responsabilidade dos usuários, cabendo a estes o fornecimento de informações verdadeiras e correspondente a realidade, portanto, a WayMobi, não se responsabilidade ou garante a veracidade dos dados.

2.4.5 A WayMobi se reserva o direito de recusar, cancelar, suspender e/ou excluir qualquer cadastro sem aviso prévio e sem qualquer tipo de justificativa, quando divergente com sua política e as boas práticas de mercado.

##POLÍTICA DE USO

#####Responsabilidade Quanto ao Anúncio

3.1. O Usuário está ciente de que todos os anúncios disponíveis no App e ou Site são produzidos e disponibilizados por terceiros, sem que haja qualquer tipo de controle, monitoramento ou verificação prévia da WayMobi. A WayMobi não controla, não edita, não monitora e não é, de qualquer forma, responsável pelos anúncios disponibilizados através da sua plataforma de classificados virtuais (“Anúncios”), mensagens, comentários, publicidade, arquivos, imagens, fotos, vídeos, sons ou qualquer outro material disponibilizado através dos Anúncios ou a eles relacionados (“Conteúdo”) e submetidos ao App e ou Site por seus Usuários. O Usuário também concorda que ao utilizar o App e ou Site poderá se expor a Conteúdo eventualmente ofensivo, inexato, enganoso ou, de qualquer outro modo, censurável.

3.1.1. Mesmo ciente de que a WayMobi não realiza nenhuma triagem prévia do Conteúdo e não o submete a um processo prévio de aprovação, o Usuário reconhece que a WayMobi terá o direito (mas não a obrigação) de, a seu exclusivo critério, recusar, excluir ou alterar qualquer Conteúdo e/ou Anúncio disponibilizado no App e ou Site, seja por violação aos T&C ou por quaisquer outras razões de ordem ética, legal ou moral.

3.1.2 O usuário que presenciar que qualquer usuário efetuou um cadastro malicioso, tendencioso a prática de qualquer ilícito seja cível ou criminal, ou prejuízo social, deverá imediatamente enviar e-mail para WayMobi exigindo a confirmação de recebimento do referido e-mail, para que a WayMobi possa tomar as medidas cabíveis.

3.2. O Usuário é o único responsável por todo o Anúncio e/ou Conteúdo de sua autoria ou por ele divulgado, transmitido por e-mail ou associado a link disponibilizado no/ou através do Site; assim como pelo seu uso, divulgação ou publicação, devendo o Usuário manter a WayMobi, seus funcionários, parceiros e controladores livres e indenes de toda e qualquer consequência advinda de seus atos/ omissões, inclusive em relação a terceiros.

3.2.1. Com relação ao Anúncio e/ou Conteúdo divulgado, transmitido ou associado a link no Site, o Usuário declara que: É o titular dos direitos e/ou possui as autorizações necessárias para divulgar o Conteúdo e permitir que a WayMobi o utilize e/ou eventualmente o formate para melhor disposição no App e ou Site;  Possui o consentimento expresso, licença e/ou permissão de toda e qualquer pessoa identificável no Conteúdo, seja ela para usar seu nome e/ou sua imagem; Detem todas as permissões, capacidade técnica e registros eventualmente necessários para a prestação dos serviços oferecidos e anunciados através do App e ou Site; eUtilizará das melhores práticas de mercado, sejam elas relacionadas à qualidade, segurança ou outras, na execução dos serviços oferecidos e anunciados através do App e ou Site.

#####Controle e Monitoramento dos Anúncios

3.3. A WayMobi não ratifica, edita, aprova ou monitora previamente qualquer Anúncio, Conteúdo, comentário, opinião ou recomendação expressos no App e ou Site e não assume quaisquer responsabilidades a eles relacionados.

3.3.1. A WayMobi não permite, e o Usuário desde já aceita e concorda, a submissão ao Site de Anúncios e/ou Conteúdos de qualquer forma relacionados a atividades que desrespeitem direitos de propriedade intelectual, ou qualquer outra violação a direitos de terceiros, e poderá remover todo o Anúncio e/ou Conteúdo inadequado, quando devidamente notificada de que o mesmo viola direitos de terceiros e/ou a legislação brasileira.

#####Comentários e Política de Reputação

3.4. A WayMobi oferece uma ferramenta de qualificação para ajudar os Usuários a identificar e analisar a idoneidade de outros Usuários.[PREENCHER] QUAL A FERRAMENTA QUE É OFERECIDA PARA IDENTIFICAR ESSA IDONEIDADE

3.4.1. A WayMobi não se obriga a verificar a veracidade e/ou exatidão dos comentários e/ou qualificação de reputação. Desta forma, a WayMobi não se responsabiliza pela natureza destes comentários e/ou qualificação de reputação.

3.4.2. A WayMobi poderá, a qualquer tempo, excluir qualquer comentário ou qualificação de reputação quando entender, a seu exclusivo critério, que o conteúdo é ofensivo ou inadequado.

3.4.3. A WayMobi poderá suspender, excluir ou cancelar cadastros de usuários que apresentem qualificações, informações inverídicas ou negativas de forma reiterada.

#####POLÍTICA DE CONTEÚDO E PENALIDADES

4.1. A WayMobi não permite, e desde já considera como inadequado, quaisquer Anúncios e/ou Conteúdos enquadrados na lista abaixo. Desta forma, o Usuário compromete-se a não divulgar, enviar por e-mail, ou disponibilizar de qualquer outro modo, Conteúdo e/ou Anúncio:
Ilícito;
Protegido por direitos autorais, segredo comercial, industrial ou de terceiros; a menos que o Usuário tenha permissão do titular de tais direitos para divulgar o Conteúdo e/ou Anúncio;
Nocivo, abusivo, difamatório, pornográfico, libidinoso ou que de qualquer forma represente assédio, invasão de privacidade ou risco a menores;
Que represente assédio, degradação, intimidação ou ódio em relação a um indivíduo ou grupo de indivíduos com base na religião, sexo, orientação sexual, raça, origem ética, idade ou deficiência;
Que inclua informações pessoais ou que permitam a identificação de terceiro sem seu expresso consentimento;
Falso, fraudulento, enganoso ou que represente venda casada ou promoção enganosa;
Que represente ou contenha marketing de associação ilícito, referência a link, spam, correntes ou esquemas de pirâmide;
Que divulgue, mencione ou faça apologia a quaisquer serviços ilegais ou venda de quaisquer itens cujo comércio e propaganda sejam proibidos ou restritos por lei; e
Que contenha vírus ou qualquer outro código malicioso, arquivos ou programas projetados para interromper, destruir ou limitar a funcionalidade de qualquer software ou hardware.

4.1.1. Além disso, o Usuário concorda em não:
Obter, guardar, divulgar, comercializar e/ou utilizar dados pessoais sobre outros Usuários para fins comerciais ou ilícitos;
Usar meios automáticos, incluindo spiders, robôs, crawlers, ferramentas de captação de dados ou similares para baixar dados do App e ou Site – exceto quando for o caso de ferramentas de busca na Internet (ex. Google Search) e arquivos públicos não comerciais (ex. archive.org);
 Divulgar o mesmo Anúncio, Conteúdo, item ou serviço em mais de uma categoria e/ou repetidamente; e
Burlar de qualquer forma que seja o sistema, mecanismo e/ou plataforma da WayMobi;
Incluir meios de contato como telefone, email, endereço e outras externas de comunicação nas ofertas.
Não trocar mensagens com demais usuários do site quando o assunto se tratar ofertas da WayMobi, pois, apenas trocando mensagens pela ferramenta de comunicação do site será garantida a satisfação ou a devolução dos valores pagos por serviços não prestados.

4.2. O Usuário declara conhecer todas as regras e formas de anunciar seus produtos e/ou serviços contempladas nestes T&C. A WayMobi se reserva o direito de excluir sumariamente qualquer anúncio em desacordo com estas regras e formas.

4.3. A listagem prevista neste item 4 possui caráter meramente exemplificativo e não exaustivo.

4.4. A WayMobi se reserva o direito de remover o Anúncio e/ou qualquer Conteúdo submetido ao Site sem aviso prévio. A WayMobi PODERÁ, A SEU EXCLUSIVO CRITÉRIO, SUSPENDER OU CANCELAR, DE QUALQUER FORMA QUE SEJA, O CADASTRO E/OU ACESSO DO USUÁRIO AO SITE EM CASO DE INFRAÇÃO LEGAL OU A QUALQUER DAS DISPOSIÇÕES DESTES T&C. 
A WayMobi também se reserva o direito de decidir, a seu único e exclusivo critério, quais Conteúdos e/ou Anúncios entende como inadequados. Apenas a título exemplificativo, são entendidos como inadequados Anúncios e/ou Conteúdos listados nesta Cláusula 4.

4.5. Qualquer Usuário que desrespeitar os compromissos previstos neste item estará sujeito às sanções previstas nestes T&C, inclusive aquelas do item 4.3 acima, sem prejuízo de responder civil e criminalmente pelas conseqüências de seus atos/ omissões.

#####5. NOTIFICAÇÃO SOBRE INFRAÇÕES

5.1. Caso qualquer pessoa, Usuário ou não, se sentir lesado em relação a qualquer Anúncio e/ou Conteúdo, poderá encaminhar à WayMobi notificação por escrito solicitando sua retirada.

5.2. No entanto, para não prejudicar Usuários de boa-fé, a retirada do Anúncio e/ou Conteúdo do Site pela WayMobi dependerá de efetiva comprovação ou forte evidência da ilegalidade ou infração à lei, direitos de terceiros e/ou aos T&C.

5.3. As notificações deverão ainda conter as seguintes informações:
Assinatura física ou eletrônica da pessoa supostamente lesada, ou, se for o caso, do titular do direito intelectual violado;
Identificação do objeto protegido por direitos intelectuais que tenha sido violado, se for o caso;
Identificação do material que supostamente representa a infração, código do(s) Anúncio(s) e/ou link do(s) Anúncio(s), ou, em caso de não se tratarem de Anúncios, informações necessárias para a devida identificação do Conteúdo; e

Declaração de que o notificante possui elementos suficientes para embasar a alegação de violação legal.
   O notificante declara que as informações contidas na notificação são precisas e verdadeiras, sob pena de incorrer nas consequentes responsabilidades cíveis e penais, e que o notificante está autorizado a agir em nome do titular do direito supostamente violado.

5.4. As notificações deverão ser encaminhadas à WayMobi para o seguinte e-mail: contato@waymobi.com.br. Para esclarecer, apenas as notificações relacionadas à efetiva violação de direitos devem ser encaminhadas para este endereço. Quaisquer outros feedbacks, comentários, sugestões, críticas, solicitações de suporte técnico e outras comunicações devem ser direcionadas ao atendimento ao cliente da WayMobi clicando no link “Contato” no Site. O notificante reconhece que caso não cumpra com todos os requisitos deste item 5, sua notificação poderá não ser considerada.

#####6. POLÍTICA DE PRIVACIDADE E DIVULGAÇÃO DE INFORMAÇÕES

6.1. O Usuário desde já autoriza a WayMobi, a seu critério, preservar, armazenar todos os Anúncios e/ou Conteúdos submetidos ao App e ou Site, bem como todos os seus dados pessoais, cadastrais e de acesso, a exemplo de endereços de e-mail, endereços de IP (Internet Protocol), informações de data e horário de acesso, entre outras informações. O Usuário também autoriza a WayMobi a informar e/ou divulgar estes dados em caso de exigência legal ou se razoavelmente necessárias para: cumprir com o devido processo legal; fazer cumprir estes T&C; responder a alegações de suposta violação de direitos de terceiros e para proteger os direitos, a propriedade ou a segurança de terceiros ou da própria WayMobi e de seus Usuários.

6.2. A WayMobi poderá utilizar empresas de processamento de cartões de crédito e/ou de gestão de meios de pagamentos online terceirizadas, assim como empresas para monitorar o tráfego do Site, que, em alguns casos, poderão armazenar e coletar informações e dados do Usuário.

6.3. A utilização do Site implica no consentimento do Usuário para coleta, armazenamento e uso das informações pessoais fornecidas, cadastrais, de acesso e suas atualizações.

6.4. A WayMobi poderá utilizar cookies para administrar as sessões, navegações, acessos e cadastros dos Usuários e armazenar preferências, rastrear informações, entre outros. Cookies poderão ser utilizados independentemente de cadastro do Usuário.

6.4.1 “Cookies” são pequenos arquivos de texto transferidos via servidor para o disco rígido e armazenados no computador do Usuário. Cookies podem coletar informações como data e horário de acesso, histórico de navegação, preferências e nome do usuário.

6.4.2. Em alguns casos, prestadores de serviços terceirizados poderão utilizar cookies no Site, que a WayMobi não controla nem acessa. Tais cookies não são regulados por estes Termos e Condições.

6.5. O Usuário tem a opção de aceitar ou recusar o uso de cookies em seu computador, independente de cadastro no Site, configurando seu navegador como desejar. 
A recusa do uso de cookies pode, contudo, resultar na limitação do acesso do Usuário a certas ferramentas disponíveis no Site, dificuldades no login e na ferramenta de comentários.

6.6. Considerando que a WayMobi poderá realizar parcerias com terceiros, eventualmente estes poderão coletar informações de usuários como endereço IP, especificação do navegador e sistema operacional. Estes T&C não se aplicam às informações pessoais fornecidas aos terceiros e por eles armazenadas e utilizadas.

6.7. O Site poderá, eventualmente, conter links para sites de terceiros. A  WayMobi não se responsabiliza pelo conteúdo ou pela segurança das informações do Usuário quando acessar sites de terceiros. Tais sites podem possuir suas próprias políticas de privacidade quanto ao armazenamento e conservação de informações pessoais, completamente alheias à WayMobi.

6.8. A WayMobi poderá trabalhar com empresas terceirizadas de propaganda para a divulgação de anúncios durante seu acesso ao Site. Tais empresas poderão coletar informações sobre as visitas de Usuários ao Site, no intuito de fornecer anúncios personalizados sobre bens e serviços do interesse do Usuário. Tais informações não incluem nem incluirão nome, endereço, e-mail ou número de telefone do Usuário.

6.9. Ao publicar um anúncio na WayMobi, o Usuário compreende que os anúncios e o Conteúdo são públicos e acessíveis a terceiros, podendo ser listados nos resultados de ferramentas de pesquisa como Yahoo!, MSN, Google e outros e no cache dessas ferramentas de pesquisa, em feeds e outros websites. O Usuário compreende que é de responsabilidade de cada uma dessas ferramentas de pesquisa, websites ou recursos RSS manter seus índices e cache atualizados e remover o Conteúdo de seus índices e cache, pois não é possível que a WayMobi exerça influência sobre eles.

6.10. A WayMobi se reserva o direito de reter informações pelo período que entender necessário para o bom cumprimento de seus negócios, mesmo após o encerramento da conta do Usuário.

6.11. As informações referentes ao Serviço são mantidas em uma base de dados escolhida pela WayMobi e que atende aos padrões de segurança atuais.

6.12. O Usuário desde já declara estar ciente de que a WayMobi não assume nenhuma responsabilidade em caso de roubo, perda, alteração ou uso indevido de suas informações pessoais e do Conteúdo como um todo, inclusive na hipótese de informações fornecidas a prestadores de serviços terceirizados ou a outros Usuários e na hipótese de descumprimento destes T&C por prestadores de serviços terceirizados.

6.13 O Usuário poderá solicitar a qualquer momento o cancelamento do recebimento de quaisquer e-mails transacionais ou promocionais da WayMobi enviando um e-mail para cancelar@waymobi.com.br

#####7. DIREITOS DE PROPRIEDADE INTELECTUAL

7.1. Os elementos e/ou ferramentas encontrados no App e ou Site, com exceção dos Conteúdos e/ou Anúncios submetidos por Usuários, são de titularidade ou licenciados para a WayMobi, sujeitos aos direitos intelectuais de acordo com as leis brasileiras e tratados e convenções internacionais dos quais o Brasil seja signatário. 
Apenas a título exemplificativo, entendem-se como tais: textos, softwares, scripts, imagens gráficas, fotos, sons, músicas, vídeos, recursos interativos e similares, marcas, marcas de serviços, logotipos e look and feel.

7.2. Os elementos e/ou ferramentas que são disponibilizados para o Usuário no Site os são tão e somente no estado em que se encontram e apenas para sua informação e uso pessoal na forma designada pela WayMobi. Tais elementos e/ou ferramentas não podem ser usados, copiados, reproduzidos, distribuídos, transmitidos, difundidos, exibidos, vendidos, licenciados ou, de outro modo, explorados para quaisquer fins, sem o consentimento prévio e por escrito da WayMobi.

7.3. A WayMobi reserva a si todos os direitos que não foram expressamente previstos em relação ao Site, aos seus elementos e/ou ferramentas. O usuário compromete-se a não usar, reproduzir ou distribuir quaisquer elementos e/ou ferramentas que não sejam expressamente permitidos pela WayMobi – inclusive o uso, reprodução ou distribuição para fins comerciais dos Anúncios e/ou Conteúdos extraídos do App e ou Site. Caso o Usuário faça qualquer cópia, seja ela via download ou impressão, dos elementos e/ou ferramentas da WayMobi para uso exclusivamente pessoal, deverá preservar todos os direitos de propriedade intelectual inerentes. O Usuário concorda em não burlar, desativar ou, de alguma forma, interferir em recursos e/ou ferramentas relacionados à segurança do Site, sob pena de incorrer nas medidas judiciais cabíveis.

#####8. GARANTIA DE ANÚNCIO E/OU CONTEÚDO

8.1. O Usuário está ciente de que, ao acessar o Site da WayMobi, (i) estará exposto a Anúncios e/ou Conteúdos de uma variedade de fontes não validadas ou garantidas previamente pela WayMobi, e (ii) que a WayMobi  não é responsável pela sua precisão, veracidade, legalidade, licitude, utilidade e/ou segurança destes Anúncios e/ou Conteúdos.

8.2. A WayMobi também não garante o sucesso nas transações ou informações buscadas no App e ou Site.

#####9. OBRIGAÇÃO DE INDENIZAR

O Usuário se compromete a defender, indenizar e isentar de qualquer responsabilidade a WayMobi, seus executivos, subsidiárias, afiliados, sucessores, prepostos, diretores, agentes, prestadores de serviços, fornecedores e funcionários com relação a toda e qualquer reclamação, perda, dano, obrigação, custos, dívidas ou despesas (incluindo, entre outros, honorários advocatícios e custas processuais) incorridas em razão de: (i) mau uso e acesso do Site; (ii) violação e/ou não observância de quaisquer dispositivos destes T&C; (iii) violação de qualquer direito de terceiros, incluindo, mas sem se limitar a, qualquer direito proprietário, intelectual ou de privacidade; ou (iv) qualquer reclamação de que algum Anúncio e/ou Conteúdo causou dano a terceiros. Esta defesa e obrigação de indenização subsistirão a estes T&C e ao uso e/ou acesso do Site.

#####10. TRANSAÇÕES ENTRE USUÁRIOS E ENTRE USUÁRIOS E TERCEIROS

10.1. A WayMobi não será responsável pelos negócios e/ou transações, efetivadas ou não, entre Usuários ou entre Usuários e terceiros. Apenas a título exemplificativo, entendem-se como tais negócios: o pagamento e entrega de bens e serviços e quaisquer outros termos, condições, garantias ou representações associadas a essas transações. Tais transações são realizadas entre Usuários ou entre Usuários e terceiros, e portanto de sua exclusiva responsabilidade, sem qualquer envolvimento ou participação da WayMobi.

10.2. O usuário está ciente de que a WayMobi não deve ser responsabilizada ou sujeitar-se a quaisquer perdas e/ou danos incorridos em face destas transações. Os usuários concordam que, em caso de litígio entre Usuários ou entre Usuários e terceiros, a WayMobi não será, de qualquer forma, envolvida ou responsabilizada.

10.3. O Usuário entende e reconhece que a WayMobi apenas mantem uma plataforma virtual disponibilizada aos Usuários para se aproximarem uns dos outros e negociarem entre si a contratação e/ou compra e venda de produtos e serviços sem a intervenção da WayMobi, desta forma:
Os Usuários são os únicos e exclusivos responsáveis por suas obrigações fiscais, trabalhistas, consumeristas e demais envolvidas nas transações realizadas através do Site; e

 A WayMobi não pode, ou mesmo possui meios hábeis para tal, obrigar os Usuários, seja ele vendedor ou comprador, a honrar seus compromissos.

10.3.1. A WayMobi não se responsabiliza pelas obrigações de natureza tributárias incidentes nas transações realizadas através do Site. Desta forma, todas as obrigações oriundas de suas atividades, especificamente com relação aos tributos, são de exclusiva responsabilidade do Usuário Vendedor que atue ou não de forma comercial.

#####11. LIMITAÇÃO E TÉRMINO DO SERVIÇO

11.1. O Usuário reconhece que a WayMobi pode estabelecer limites referentes ao uso e acesso do App e ou Site, inclusive quanto ao número máximo de dias que um anúncio será exibido no Site, o número e tamanho máximo de textos, mensagens de e-mail ou outros Anúncios e/ou Conteúdos que possam ser transmitidos ou armazenados no Site.

11.2. O Usuário reconhece que a WayMobi não é obrigada a manter ou armazenar qualquer Anúncio e/ou Conteúdo no Site, podendo, a qualquer momento e por qualquer motivo, modificar ou descontinuar o Serviço, sem aviso prévio, e sem que qualquer obrigação seja criada com os Usuários ou com terceiros por tais modificações, suspensões ou descontinuidade do Serviço.

11.3. O usuário concorda que a WayMobi, a seu critério, pode excluir ou desativar sua conta, bloquear seu e-mail ou endereço de IP, ou, de outro modo, encerrar seu acesso ou uso do Site (ou parte dele) imediatamente, sem aviso prévio, além de remover qualquer Anúncio e/ou Conteúdo do Site.

#####12. TRANSFERÊNCIA DE DIREITOS

12.1. Estes T&C, bem como quaisquer direitos, licenças e/ou autorizações implícitas ou expressas concedidas pela WayMobi aos seus Usuários, não podem ser cedidos, ou de qualquer forma transferidos, pelo Usuário.

#####13. REGRAS GERAIS, LEGISLAÇÃO APLICÁVEL E FORO

13.1. Estes T&C, seus Anexos e quaisquer outras políticas divulgadas pela WayMobi no Site estabelecem o pleno e completo acordo e entendimento entre o Usuário superando e revogando todos e quaisquer entendimentos, propostas, acordos, negociações e discussões havidos anteriormente entre as partes.

13.2. Os T&C e a relação entre as partes são regidos pelas leis da República Federativa do Brasil.

13.3. A incapacidade da WayMobi em exercer ou fazer cumprir qualquer direito ou cláusula dos T&C não representa uma renúncia desse direito ou cláusula.


13.4. Na hipótese de que qualquer item, termo ou disposição destes T&C vir a ser declarado nulo ou não aplicável, tal nulidade ou inexequibilidade não afetará  quaisquer outros itens, termos ou disposições aqui contidos, os quais permanecerão em pleno vigor e efeito.

13.5. Os T&C e a relação entre as partes são regidos pelas leis da República Federativa do Brasil.

13.6. As partes elegem o Foro da Comarca de São Paulo como sendo o único competente para dirimir quaisquer litígios e/ou demandas que venham a envolver as Partes em relação ao uso e acesso do Site.