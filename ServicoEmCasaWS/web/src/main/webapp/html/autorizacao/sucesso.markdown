![WayMobi](/imagens/logo.png "WayMobi")

# Sucesso

A solicitação de cadastro de um novo aparelho celular foi efetuada com sucesso.

Agora você pode acessar a aplicação no seu celular para habilitar o novo aparelho na sua conta.


#### Obrigado por usar nosso serviço


** WayMobi **
[http://www.waymobi.com.br](http://www.waymobi.com.br)