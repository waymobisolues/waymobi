package br.com.servicoemcasa.aop;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Key;
import com.google.inject.OutOfScopeException;
import com.google.inject.Provider;
import com.google.inject.Scope;

public class TransactionScope implements Scope {

    private static final Provider<Object> SEEDED_KEY_PROVIDER = new Provider<Object>() {
        public Object get() {
            // Geralmente só entra aqui se der algum problema na configuração do
            // escopo
            throw new IllegalStateException("Alguma dependência não foi configurada corretamente para o escopo.");
        }
    };

    private final ThreadLocal<Map<Key<?>, Object>> values = new ThreadLocal<Map<Key<?>, Object>>();

    public void enter() {
        if (values.get() != null) {
            throw new IllegalStateException("Escopo já iniciado");
        }
        values.set(new HashMap<Key<?>, Object>());
    }

    public void exit() {
        if (values.get() == null) {
            throw new IllegalStateException("Escopo não iniciado");
        }
        values.remove();
    }

    public boolean isStarted() {
        return values.get() != null;
    }

    public <T> void seed(Key<?> key, Object arguments) {
        Map<Key<?>, Object> scopedObjects = getScopedObjectMap(key);
        scopedObjects.put(key, arguments);
    }

    public <T> void seed(Class<T> clazz, T value) {
        seed(Key.get(clazz), value);
    }

    public <T> Provider<T> scope(final Key<T> key, final Provider<T> unscoped) {
        return new Provider<T>() {
            public T get() {
                Map<Key<?>, Object> scopedObjects = getScopedObjectMap(key);

                @SuppressWarnings("unchecked")
                T current = (T) scopedObjects.get(key);
                if (current == null && !scopedObjects.containsKey(key)) {
                    current = unscoped.get();
                    // scopedObjects.put(key, current);
                }
                return current;
            }
        };
    }

    private <T> Map<Key<?>, Object> getScopedObjectMap(Key<T> key) {
        Map<Key<?>, Object> scopedObjects = values.get();
        if (scopedObjects == null) {
            throw new OutOfScopeException("Escopo não iniciado");
        }
        return scopedObjects;
    }

    @SuppressWarnings({ "unchecked" })
    public static <T> Provider<T> seededKeyProvider() {
        return (Provider<T>) SEEDED_KEY_PROVIDER;
    }

}
