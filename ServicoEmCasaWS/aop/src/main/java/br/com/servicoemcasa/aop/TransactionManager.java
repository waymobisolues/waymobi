package br.com.servicoemcasa.aop;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class TransactionManager {

    @Pointcut("execution(@br.com.servicoemcasa.aop.Transactional * *(..)) && !within(br.com.servicoemcasa.aop.*)")
    void transactional() {
    }

    @Pointcut("execution(* javax.servlet.http.HttpServlet+.doGet(javax.servlet.ServletRequest+,javax.servlet.ServletResponse+))")
    void servletGet() {
    }

    @Pointcut("execution(* javax.servlet.http.HttpServlet+.doPost(javax.servlet.ServletRequest+,javax.servlet.ServletResponse+))")
    void servletPost() {
    }

    @Pointcut("execution(* javax.servlet.http.HttpServlet+.doPut(javax.servlet.ServletRequest+,javax.servlet.ServletResponse+))")
    void servletPut() {
    }

    @Pointcut("execution(* javax.servlet.http.HttpServlet+.doDelete(javax.servlet.ServletRequest+,javax.servlet.ServletResponse+))")
    void servletDelete() {
    }

    @Around("servletGet() || servletPost() || servletPut() || servletDelete()")
    public Object runNewEntityManager(final ProceedingJoinPoint thisJoinPoint) throws Throwable {
        TransactionScope scope = Guice.getInstance(TransactionScope.class);
        EntityManagerFactory entityManagerFactory = Guice.getInstance(EntityManagerFactory.class);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            scope.enter();
            scope.seed(EntityManager.class, entityManager);
            return thisJoinPoint.proceed();
        } finally {
            entityManager.close();
            scope.exit();
        }
    }

    @Around("transactional()")
    public Object runTransaction(final ProceedingJoinPoint thisJoinPoint) throws Throwable {
        EntityManager entityManager = Guice.getInstance(EntityManager.class);
        boolean existeTransacao = entityManager.getTransaction().isActive();
        try {
            if (!existeTransacao) {
                entityManager.getTransaction().begin();
            }
            Object o = thisJoinPoint.proceed();
            return o;
        } catch (Throwable err) {
            entityManager.getTransaction().setRollbackOnly();
            throw err;
        } finally {
            if (!existeTransacao) {
                if (entityManager.getTransaction().getRollbackOnly()) {
                    entityManager.getTransaction().rollback();
                } else {
                    entityManager.getTransaction().commit();
                }
            }
        }
    }

}
