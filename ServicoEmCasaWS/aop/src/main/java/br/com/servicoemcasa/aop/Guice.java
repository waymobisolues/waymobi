package br.com.servicoemcasa.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import com.google.inject.Injector;

@Aspect
public class Guice {

    private static Injector injector;

    public static void setGuiceFactory(Injector injector) {
        Guice.injector = injector;
    }
    
    static void injectMembers(Object o) {
        injector.injectMembers(o);
    }
    
    static <T> T getInstance(Class<T> classOf) {
        return injector.getInstance(classOf);
    }
    
    @Pointcut("execution(@javax.inject.Inject *.new()) && !within(br.com.servicoemcasa.infra.aop.*)")
    void construtoresComInjectSemParametros() {
    }

    @Before("construtoresComInjectSemParametros()")
    public void inicializaDependencias(JoinPoint thisJoinPoint) {
        injectMembers(thisJoinPoint.getTarget());
    }

}