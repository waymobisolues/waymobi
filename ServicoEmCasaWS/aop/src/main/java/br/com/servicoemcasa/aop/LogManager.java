package br.com.servicoemcasa.aop;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.WeakHashMap;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

@Aspect
public class LogManager {

    private static final ThreadLocal<WeakHashMap<Throwable, Boolean>> ERROS = new ThreadLocal<WeakHashMap<Throwable, Boolean>>();

    @Pointcut("execution(* *(..)) && !within(br.com.servicoemcasa.aop.*)")
    void todosMetodos() {
    }

    @Pointcut("execution(@br.com.servicoemcasa.aop.Loggable * *(..))")
    void loggable() {
    }

    @Around("todosMetodos()")
    public Object inicioMetodo(ProceedingJoinPoint thisJoinPoint) throws Throwable {
        Logger logger = Logger.getLogger(thisJoinPoint.getSignature().getDeclaringType());
        try {
            printLog(Level.TRACE, logger, "Entrando - %s", thisJoinPoint.getSignature().toString());
            Object result = thisJoinPoint.proceed();
            printLog(Level.TRACE, logger, "Saindo - %s", thisJoinPoint.getSignature().toString());
            return result;
        } catch (Throwable e) {
            printLog(Level.TRACE, logger, "Saindo com erro - %s {%s}", thisJoinPoint.getSignature().toString(),
                    e.toString());
            trataErro(e, thisJoinPoint, logger);
            throw e;
        }
    }

    private WeakHashMap<Throwable, Boolean> getMapaErros() {
        WeakHashMap<Throwable, Boolean> weakHashMap = ERROS.get();
        if (weakHashMap == null) {
            weakHashMap = new WeakHashMap<Throwable, Boolean>();
            ERROS.set(weakHashMap);
        }
        return weakHashMap;
    }

    private void trataErro(Throwable throwable, ProceedingJoinPoint thisJoinPoint, Logger logger) {
        WeakHashMap<Throwable, Boolean> weakHashMap = getMapaErros();
        Boolean jaFoiTratado = weakHashMap.get(throwable);
        if (jaFoiTratado == null || !jaFoiTratado.booleanValue()) {
            Level level = throwable instanceof RuntimeException ? Level.FATAL : Level.ERROR;
            printLog(level, logger, throwable, "Erro na execução do método: %s", thisJoinPoint.getSignature()
                    .toString());
            weakHashMap.put(throwable, Boolean.TRUE);
        }
    }

    @Around("loggable()")
    public Object logInfo(ProceedingJoinPoint thisJoinPoint) throws Throwable {
        Logger logger = Logger.getLogger(thisJoinPoint.getSignature().getDeclaringType());
        MethodSignature methodSignature = (MethodSignature) thisJoinPoint.getSignature();
        Loggable annotation = methodSignature.getMethod().getAnnotation(Loggable.class);
        Level level = annotation.level();
        if (isEnabledFor(level, logger)) {
            printInfoLog(level, logger, thisJoinPoint, "Entrando", null);
        }
        try {
            Object result = thisJoinPoint.proceed();
            if (isEnabledFor(level, logger)) {
                printInfoLog(level, logger, thisJoinPoint, "Saindo", toString(result));
            }
            return result;
        } catch (Throwable t) {
            printInfoLog(level, logger, thisJoinPoint, "Saindo com erro", t.toString());
            throw t;
        }
    }

    private void printInfoLog(Level level, Logger logger, ProceedingJoinPoint thisJoinPoint, String estagio,
            String resultado) {

        String mensagem;
        StringBuilder params = null;
        if (resultado == null) {
            mensagem = "[Loggable] %s - %s {%n  estado: %s,%n  parametros: [%s]%n}";
            boolean virgula = false;
            params = new StringBuilder();
            for (Object arg : thisJoinPoint.getArgs()) {
                if (virgula) {
                    params.append(",");
                }
                virgula = true;
                params.append("{").append(toString(arg)).append("}");
            }
        } else {
            mensagem = "[Loggable] %s - %s {%n  estado: %s,%n  retorno: [%s]%n}";
        }
        printLog(level, logger, mensagem, estagio, thisJoinPoint.getSignature().toString(),
                toString(thisJoinPoint.getTarget()), params != null ? params.toString() : resultado);
    }

    private String toString(Object arg) {
        try {
            if (arg == null) {
                return "null";
            }
            return isPrintable(arg.getClass()) ? arg.toString() : (arg.getClass() == Class.class) ? arg.toString()
                    : reflectionToString(arg);
        } catch (Throwable e) {
            return new StringBuilder().append("Erro ao gerar informações do bean ")
                    .append(arg.getClass().getSimpleName()).append(": ").append(e.toString()).toString();
        }
    }

    private String reflectionToString(Object arg) {
        try {
            return ReflectionToStringBuilder.toString(arg);
        } catch (Throwable e) {
            return arg.toString();
        }
    }

    private boolean isPrintable(Class<?> clazz) {
        return String.class.equals(clazz) || Boolean.class.equals(clazz) || Double.class.equals(clazz)
                || Float.class.equals(clazz) || Integer.class.equals(clazz) || Long.class.equals(clazz)
                || BigDecimal.class.equals(clazz) || BigInteger.class.equals(clazz) || clazz.isPrimitive();
    }

    private void printLog(Level level, Logger logger, String mensagem, Object... params) {
        printLog(level, logger, null, mensagem, params);
    }

    private void printLog(Level level, Logger logger, Throwable throwable, String mensagem, Object... params) {
        switch (level) {
        case TRACE:
            logger.trace(params != null ? String.format(mensagem, params) : mensagem);
            break;
        case DEBUG:
            logger.debug(params != null ? String.format(mensagem, params) : mensagem);
            break;
        case INFO:
            logger.info(params != null ? String.format(mensagem, params) : mensagem);
            break;
        case WARN:
            logger.warn(params != null ? String.format(mensagem, params) : mensagem);
            break;
        case ERROR:
            logger.error(params != null ? String.format(mensagem, params) : mensagem, throwable);
            break;
        case FATAL:
            logger.fatal(params != null ? String.format(mensagem, params) : mensagem, throwable);
            break;
        }
    }

    private boolean isEnabledFor(Level level, Logger logger) {
        switch (level) {
        case TRACE:
            return logger.isTraceEnabled();
        case DEBUG:
            return logger.isDebugEnabled();
        case INFO:
            return logger.isInfoEnabled();
        case WARN:
            return logger.isEnabledFor(org.apache.log4j.Level.WARN);
        case ERROR:
            return logger.isEnabledFor(org.apache.log4j.Level.ERROR);
        case FATAL:
            return logger.isEnabledFor(org.apache.log4j.Level.FATAL);
        }
        return false;
    }

}
