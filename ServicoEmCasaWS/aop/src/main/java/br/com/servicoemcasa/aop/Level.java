package br.com.servicoemcasa.aop;

public enum Level {
    TRACE, DEBUG, INFO, WARN, ERROR, FATAL
}
