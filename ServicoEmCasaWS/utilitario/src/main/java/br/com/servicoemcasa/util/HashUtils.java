package br.com.servicoemcasa.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtils {

    private static final String MD5 = "MD5";

    public static String createMD5Hash(String value) {
        MessageDigest messageDigest = getMessageDigest();
        byte[] md5 = messageDigest.digest(value.getBytes());
        BigInteger bigInt = new BigInteger(1, md5);
        String hashtext = bigInt.toString(16);
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }
        return hashtext;
    }

    private static MessageDigest getMessageDigest() {
        try {
            return MessageDigest.getInstance(MD5);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
}
