package br.com.servicoemcasa.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class SimpleValidator {

    public static Collection<String> validate(Object o) {
        Set<ConstraintViolation<Object>> validate = simpleValidate(o);
        Collection<String> result = new ArrayList<String>();
        for (ConstraintViolation<Object> v : validate) {
            result.add(v.getPropertyPath() + ": " + v.getMessage());
        }
        return result;
    }

    public static void hardValidate(Object o) throws ConstraintViolationException {
        Set<ConstraintViolation<Object>> constraintsViolations = simpleValidate(o);
        if (constraintsViolations.size() > 0) {
            throw new ConstraintViolationException(constraintsViolations);
        }
    }
    
    public static boolean isValid(Object o) {
        Set<ConstraintViolation<Object>> constraintsViolations = simpleValidate(o);
        if (constraintsViolations.size() > 0) {
            return false;
        }
        return true;
    }

    public static Set<ConstraintViolation<Object>> simpleValidate(Object o) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Object>> validate = validator.validate(o);
        return validate;
    }

    public static String getFirstException(Set<ConstraintViolation<?>> e) {
        for (ConstraintViolation<?> v : e) {
            return (v.getPropertyPath() + ": " + v.getMessage());
        }
        return null;
    }

}
