package br.com.servicoemcasa.util;

import java.lang.reflect.Constructor;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.servicoemcasa.util.SimpleReflection.ReflectionField;

public class MapParser {

    public static <T> T fromMap(final Map<String, String[]> map, final Class<T> classOf) {
        final T bean = getInstance(classOf);
        for (final String key : map.keySet()) {
            findFieldAndSetValue(map, classOf, bean, key);

        }
        return bean;
    }

    private static <T> void findFieldAndSetValue(final Map<String, String[]> map, final Class<T> classOf, final T bean,
            final String key) {

        String[] values = map.get(key);
        if (values == null || values.length == 0) {
            return;
        }
        for (int i = 0; i < values.length; i++) {
            ReflectionField field = findField(map, classOf, bean, key, i);
            field.set(Types.cast(values[i], field.field.getType()));
        }
    }

    private static <T> ReflectionField findField(final Map<String, String[]> map, final Class<T> classOf, final T bean,
            final String key, int index) {

        String fieldName = key;
        SimpleReflection reflectedBean = SimpleReflection.reflect(bean);
        if (key.contains(".")) {
            String[] keys = key.split("\\.");
            fieldName = keys[keys.length - 1];
            reflectedBean = findMultiField(keys, index, reflectedBean);
        }
        return reflectedBean.field(fieldName);
    }

    private static SimpleReflection findMultiField(final String[] keys, int index, SimpleReflection reflectedBean) {
        for (int i = 0; i < keys.length - 1; i++) {
            reflectedBean = findInnerField(index, reflectedBean, keys, i);
        }
        return reflectedBean;
    }

    private static SimpleReflection findInnerField(int index, SimpleReflection reflectedBean, String[] keys, int i) {
        ReflectionField field = reflectedBean.field(keys[i]);
        Object innerFieldInstance = field.get();
        if (innerFieldInstance == null) {
            innerFieldInstance = getInstance(field.field.getType());
            field.set(innerFieldInstance);
        }
        if (innerFieldInstance instanceof List) {
            reflectedBean = getAsInnerFieldList(index, field, innerFieldInstance);
        } else {
            reflectedBean = SimpleReflection.reflect(innerFieldInstance);
        }
        return reflectedBean;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static SimpleReflection getAsInnerFieldList(int index, ReflectionField field, Object innerFieldInstance) {
        SimpleReflection reflectedBean;
        List list = (List) innerFieldInstance;
        Object instance = null;
        if (list.size() > index) {
            instance = list.get(index);
        } else {
            instance = getInstance((Class<?>) field.getGenericTypes()[0]);
            list.add(instance);
        }
        reflectedBean = SimpleReflection.reflect(instance);
        return reflectedBean;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static <T> T getInstance(final Class<T> classOf) {
        if (List.class.isAssignableFrom(classOf)) {
            return (T) new ArrayList();
        } else {
            return AccessController.doPrivileged(new PrivilegedAction<T>() {
                public T run() {
                    try {
                        Constructor<?> constructor = classOf.getDeclaredConstructor();
                        constructor.setAccessible(true);
                        return (T) constructor.newInstance();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }
    }
}
