package br.com.servicoemcasa.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtils {

    private static final int BUFFER_SIZE = 8192;

    public static byte[] readStream(InputStream in) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        echo(in, bout);
        return bout.toByteArray();
    }

    public static void echo(InputStream in, OutputStream out) throws IOException {
        echo(in, out, BUFFER_SIZE);
    }

    public static void echo(InputStream in, OutputStream out, int bufferSize) throws IOException {
        byte[] buf = new byte[bufferSize];
        int length = 0;
        while ((length = in.read(buf)) > 0) {
            out.write(buf, 0, length);
        }
    }

    public static String readString(InputStream is) throws IOException {
        byte[] b = readStream(is);
        return new String(b);
    }

    public static String readString(InputStream is, String charset) throws IOException {
        byte[] b = readStream(is);
        return new String(b, charset);
    }

}