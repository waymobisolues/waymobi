package br.com.servicoemcasa.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.Header;
import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import br.com.servicoemcasa.aop.Loggable;

public class WebClient {

    private static final Charset CHARSET = Charset.forName("UTF-8");
    private static final String JSON_CONTENT_TYPE = "application/json; charset=" + CHARSET.name();

    private String serverURL;

    public WebClient(String serverURL) {
        this.serverURL = serverURL;
    }

    public class WebResponse {
        public final int responseCode;
        public final String responseDescription;
        public final String location;
        public final String responseBody;

        public WebResponse(final int responseCode, final String responseDescription, final String location,
                final String responseBody) {

            this.responseCode = responseCode;
            this.responseDescription = responseDescription;
            this.location = location;
            this.responseBody = responseBody;
        }

        @Override
        public String toString() {
            return "WebResponse [responseCode=" + responseCode + ", responseDescription=" + responseDescription + "]";
        }
    }

    private class WebClientTask {

        private Map<String, String> header;

        WebClientTask(Map<String, String> header) {
            this.header = header;
        }

        public WebResponse execute(HttpMessage message) {
            message.setHeader("Accept", "application/json");
            if (!message.containsHeader("Content-type")) {
                message.setHeader("Content-type", "application/json");
            }
            if (header != null) {
                for (Entry<String, String> entry : header.entrySet()) {
                    message.setHeader(entry.getKey(), entry.getValue());
                }
            }
            try {
                if (message instanceof HttpPost) {
                    return post((HttpPost) message);
                } else {
                    return get((HttpGet) message);
                }
            } catch (ClientProtocolException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);

            }
        }

        private WebResponse get(HttpGet httpGet) throws ClientProtocolException, IOException {
            try (CloseableHttpClient client = HttpClients.createDefault()) {
                HttpResponse response = client.execute(httpGet);
                return responseToWebResponse(response);
            }
        }

        private WebResponse post(HttpPost httpPost) throws ClientProtocolException, IOException {
            try (CloseableHttpClient client = HttpClients.createDefault()) {
                HttpResponse response = client.execute(httpPost);
                return responseToWebResponse(response);
            }
        }

        private WebResponse responseToWebResponse(HttpResponse response) throws IOException {
            String body = EntityUtils.toString(response.getEntity());
            Header[] locations = response.getHeaders("Location");
            String location = locations != null && locations.length > 0 ? locations[0].getValue() : null;
            return new WebResponse(response.getStatusLine().getStatusCode(),
                    response.getStatusLine().getReasonPhrase(), location, body);
        }

    }

    public WebResponse post(String path, String param) {
        return post(path, param, JSON_CONTENT_TYPE, null);
    }

    public WebResponse post(String path, String param, Map<String, String> header) {
        return post(path, param, JSON_CONTENT_TYPE, header);
    }

    public WebResponse postForm(String path, String param) {
        return post(path, param, "application/x-www-form-urlencoded", null);
    }

    public WebResponse postForm(String path, String param, Map<String, String> header) {
        return post(path, param, "application/x-www-form-urlencoded", header);
    }

    @Loggable
    private WebResponse post(String path, String param, String contentType, Map<String, String> header) {
        HttpPost post = new HttpPost(checkPath(path));
        StringEntity entity = new StringEntity(param, CHARSET);
        post.setEntity(entity);
        post.setHeader("Content-type", contentType);
        return new WebClientTask(header).execute(post);
    }

    public WebResponse get(String path) {
        return get(path, null, null);
    }

    public WebResponse get(String path, Map<String, String> header) {
        return get(path, null, header);
    }

    public WebResponse get(String path, String param) {
        return get(path, param, null);
    }

    @Loggable
    public WebResponse get(String path, String param, Map<String, String> header) {
        String url = checkPath(path);
        if (param != null) {
            url += "?" + param;
        }
        HttpGet get = new HttpGet(url);
        return new WebClientTask(header).execute(get);
    }

    private String checkPath(String path) {
        if (path == null) {
            return serverURL;
        }
        if (path.startsWith("http")) {
            return path;
        }
        if (!path.startsWith("/")) {
            return serverURL + "/" + path;
        }
        return serverURL + path;
    }

    public String encode(String param) {
        try {
            return URLEncoder.encode(param, Charset.defaultCharset().name());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
