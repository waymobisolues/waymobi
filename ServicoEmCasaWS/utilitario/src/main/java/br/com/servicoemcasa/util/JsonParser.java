package br.com.servicoemcasa.util;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParser {

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
//        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ"));
        mapper.setVisibilityChecker(mapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
    }

    public static <T> T fromJson(Reader reader, Class<T> classOf) {
        try {
            return mapper.readValue(reader, classOf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromJson(String json, Class<T> classOf) {
        try {
            return mapper.readValue(json, classOf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static <T> T fromJson(String json, TypeReference<T> classOf) {
        try {
            return mapper.readValue(json, classOf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void toJson(Object obj, Writer writer) {
        try {
            mapper.writeValue(writer, obj);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toJson(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
