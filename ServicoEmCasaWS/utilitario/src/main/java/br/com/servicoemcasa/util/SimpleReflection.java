package br.com.servicoemcasa.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SimpleReflection {
    private static final Cache<MethodInfo, Method> methodCache = new SimpleCache<MethodInfo, Method>();
    private static final Cache<FieldInfo, Field> fieldCache = new SimpleCache<FieldInfo, Field>();
    private static final Cache<ConstructorInfo, Constructor<?>> constructorCache = new SimpleCache<ConstructorInfo, Constructor<?>>();
    protected Class<?> clazz;
    protected Object instance;

    private SimpleReflection() {
    }

    public static SimpleReflection reflect(Object instance) {
        SimpleReflection sr = new SimpleReflection();
        sr.clazz = instance.getClass();
        sr.instance = instance;
        return sr;
    }

    public static SimpleReflection reflect(Class<?> clazz) {
        SimpleReflection sr = new SimpleReflection();
        sr.clazz = clazz;
        return sr;
    }

    public static SimpleReflection reflect(String className) {
        try {
            SimpleReflection sr = new SimpleReflection();
            sr.clazz = Class.forName(className);
            return sr;
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String toString(Object instance) {
        return ReflectionToString.toString(this, instance);
    }

    public ReflectionMethod method(String methodName) {
        return new ReflectionMethod(methodName);
    }

    public ReflectionConstructor constructor() {
        return new ReflectionConstructor();
    }

    public ReflectionField field(String fieldName) {
        return new ReflectionField(fieldName);
    }

    public ReflectionField field(Field field) {
        return new ReflectionField(field);
    }

    public Field[] getEveryDeclaredFields() {
        List<Field> lista = new ArrayList<Field>();

        Class<?> c = clazz;
        do {
            Collections.addAll(lista, c.getDeclaredFields());
        } while ((c = c.getSuperclass()) != null);

        Field[] fields = new Field[lista.size()];
        lista.toArray(fields);
        return fields;
    }

    public Field getField(String fieldName) {
        FieldInfo fieldInfo = new FieldInfo(fieldName, clazz);
        Field field = fieldCache.get(fieldInfo);
        if (field == null) {
            System.out.println("sem cache");
            Field[] fields = getEveryDeclaredFields();
            for (Field f : fields) {
                if (f.getName().equals(fieldName)) {
                    fieldCache.put(fieldInfo, f);
                    return f;
                }
            }
        }
        return field;
    }

    private Method getMethod(String methodName, Class<?>... parameterTypes) throws RuntimeException {
        MethodInfo methodInfo = new MethodInfo(methodName, clazz, parameterTypes);
        Method method = methodCache.get(methodInfo);
        if (method == null) {
            try {
                System.out.println("sem cache");
                method = clazz.getMethod(methodName, parameterTypes);
                methodCache.put(methodInfo, method);
            } catch (NoSuchMethodException ex) {
                throw new RuntimeException(ex);
            } catch (SecurityException ex) {
                throw new RuntimeException(ex);
            }
        }
        return method;
    }

    private Constructor<?> getConstructor(Class<?>... parameterTypes) throws RuntimeException {
        ConstructorInfo constructorInfo = new ConstructorInfo(parameterTypes, clazz);
        Constructor<?> constructor = constructorCache.get(constructorInfo);
        if (constructor == null) {
            try {
                System.out.println("sem cache");
                constructor = clazz.getConstructor(parameterTypes);
                constructorCache.put(constructorInfo, constructor);
            } catch (NoSuchMethodException ex) {
                throw new RuntimeException(ex);
            } catch (SecurityException ex) {
                throw new RuntimeException(ex);
            }
        }
        return constructor;
    }

    public Class<?>[] getParameterTypes(Object... args) {
        if (args == null) {
            return null;
        }
        Class<?>[] result = new Class<?>[args.length];
        for (int i = 0; i < result.length; i++) {
            if (args[i] != null) {
                result[i] = args[i].getClass();
            }
        }
        return result;
    }

    public class ReflectionMethod {
        public final String methodName;
        private Class<?>[] paramtypes;
        private Object instance;

        ReflectionMethod(String methodName) {
            this.methodName = methodName;
        }

        public ReflectionMethod withParams(Class<?>... params) {
            this.paramtypes = params;
            return this;
        }

        public ReflectionMethod useInstance(Object instance) {
            this.instance = instance;
            return this;

        }

        public Object call(Object... params) {
            try {
                Object callInstance = instance == null ? SimpleReflection.this.instance : instance;
                if (params != null) {
                    if ((paramtypes == null || paramtypes.length == 0) && params.length > 0) {
                        paramtypes = SimpleReflection.this.getParameterTypes(params);
                    }
                }
                Method method = SimpleReflection.this.getMethod(methodName, paramtypes);
                return method.invoke(callInstance, params);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public class ReflectionConstructor {
        private Class<?>[] paramtypes;

        public ReflectionConstructor withParams(Class<?>... params) {
            this.paramtypes = params;
            return this;
        }

        public Object create(Object... params) {
            try {
                if (params != null) {
                    if ((paramtypes == null || paramtypes.length == 0) && params.length > 0) {
                        paramtypes = SimpleReflection.this.getParameterTypes(params);
                    }
                }
                Constructor<?> constructor = SimpleReflection.this.getConstructor(paramtypes);
                return constructor.newInstance(params);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException e) {

                throw new RuntimeException(e);
            }
        }
    }

    public class ReflectionField {
        public final String fieldName;
        public final Field field;
        private Object instance;

        ReflectionField(String fieldName) {
            this.fieldName = fieldName;
            this.field = SimpleReflection.this.getField(fieldName);
        }

        ReflectionField(Field field) {
            this.fieldName = field.getName();
            this.field = field;
        }

        public ReflectionField useInstance(Object instance) {
            this.instance = instance;
            return this;
        }

        public Object get() {
            Object callInstance = instance == null ? SimpleReflection.this.instance : instance;
            assertAccessible();
            try {
                return field.get(callInstance);
            } catch (IllegalArgumentException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        private void assertAccessible() {
            java.security.AccessController.doPrivileged(new java.security.PrivilegedAction<Void>() {
                public Void run() {
                    ReflectionField.this.field.setAccessible(true);
                    return null;
                }
            });
        }

        public void set(Object o) {
            Object callInstance = instance == null ? SimpleReflection.this.instance : instance;
            assertAccessible();
            try {
                field.set(callInstance, o);
            } catch (IllegalArgumentException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        public Type[] getGenericTypes() {
            ParameterizedType paramType = (ParameterizedType) field.getGenericType();
            return paramType.getActualTypeArguments();
        }
    }

}

class MethodInfo {
    private String methodName;
    private Class<?> clazz;
    private Class<?>[] parameterTypes;

    public MethodInfo(String methodName, Class<?> clazz, Class<?>[] parameterTypes) {
        super();
        this.methodName = methodName;
        this.clazz = clazz;
        this.parameterTypes = parameterTypes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
        result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
        result = prime * result + Arrays.hashCode(parameterTypes);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MethodInfo other = (MethodInfo) obj;
        if (clazz == null) {
            if (other.clazz != null)
                return false;
        } else if (!clazz.equals(other.clazz))
            return false;
        if (methodName == null) {
            if (other.methodName != null)
                return false;
        } else if (!methodName.equals(other.methodName))
            return false;
        if (!Arrays.equals(parameterTypes, other.parameterTypes))
            return false;
        return true;
    }

}

class FieldInfo {
    private String fieldName;
    private Class<?> clazz;

    public FieldInfo(String fieldName, Class<?> clazz) {
        super();
        this.fieldName = fieldName;
        this.clazz = clazz;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
        result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FieldInfo other = (FieldInfo) obj;
        if (clazz == null) {
            if (other.clazz != null)
                return false;
        } else if (!clazz.equals(other.clazz))
            return false;
        if (fieldName == null) {
            if (other.fieldName != null)
                return false;
        } else if (!fieldName.equals(other.fieldName))
            return false;
        return true;
    }

}

class ConstructorInfo {
    private Class<?>[] parameterTypes;
    private Class<?> clazz;

    public ConstructorInfo(Class<?>[] parameterTypes, Class<?> clazz) {
        super();
        this.parameterTypes = parameterTypes;
        this.clazz = clazz;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
        result = prime * result + Arrays.hashCode(parameterTypes);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConstructorInfo other = (ConstructorInfo) obj;
        if (clazz == null) {
            if (other.clazz != null)
                return false;
        } else if (!clazz.equals(other.clazz))
            return false;
        if (!Arrays.equals(parameterTypes, other.parameterTypes))
            return false;
        return true;
    }

}