package br.com.servicoemcasa.util;

public class CPFValidator {

    public static boolean isValid(Long cpf) {
        br.com.caelum.stella.validation.CPFValidator cpfValidator = new br.com.caelum.stella.validation.CPFValidator();
        try {
            cpfValidator.assertValid(StringUtils.lpad(cpf, 11));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
