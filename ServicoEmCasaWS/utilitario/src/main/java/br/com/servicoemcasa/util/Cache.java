package br.com.servicoemcasa.util;

public interface Cache<K,V> {
    
    void setCacheCapacity(int cacheCapacity);
    V get(Object key);
    V put(K key, V value);
    V remove(Object key);
    boolean containsKey(Object key);
    boolean containsValue(Object value);
    boolean isEmpty();
    int size();
    void clear();
    
}
