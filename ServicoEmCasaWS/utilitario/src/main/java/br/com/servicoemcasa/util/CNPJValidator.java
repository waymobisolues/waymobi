package br.com.servicoemcasa.util;

public class CNPJValidator {

    public static boolean isValid(Long cnpj) {
        br.com.caelum.stella.validation.CNPJValidator cnpjValidator = new br.com.caelum.stella.validation.CNPJValidator();
        try {
            cnpjValidator.assertValid(StringUtils.lpad(cnpj, 14));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
