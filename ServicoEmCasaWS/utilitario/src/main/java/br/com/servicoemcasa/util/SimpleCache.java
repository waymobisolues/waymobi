package br.com.servicoemcasa.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleCache<K, V> extends LinkedHashMap<K, V> implements Cache<K, V> {

    private static final long serialVersionUID = 1089015915719949080L;

    private int cacheCapacity;

    public SimpleCache() {
        this(50);
    }

    public SimpleCache(int cacheCapacity) {
        super(cacheCapacity, 0.75f, true);
        setCacheCapacity(cacheCapacity);
    }

    public final void setCacheCapacity(int cacheCapacity) {
        this.cacheCapacity = cacheCapacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K,V> eldest) {
        return (this.size() > cacheCapacity);  
    }
    
}