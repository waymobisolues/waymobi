package br.com.servicoemcasa.util;

import java.lang.reflect.Field;

import br.com.servicoemcasa.util.SimpleReflection.ReflectionField;

public class ReflectionToString {

    public static String toString(Object o) {
        return toString(SimpleReflection.reflect(o), o);
    }

    static String toString(SimpleReflection reflect, Object o) {
        Field[] fields = reflect.getEveryDeclaredFields();
        boolean virgula = false;
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Field field : fields) {
            if (virgula) {
                sb.append(",");
            }
            ReflectionField reflectedField = reflect.field(field);
            sb.append(reflectedField.fieldName);
            sb.append("=");
            sb.append(reflectedField.get());
            virgula = true;
        }
        sb.append("]");
        return sb.toString();
    }

}
