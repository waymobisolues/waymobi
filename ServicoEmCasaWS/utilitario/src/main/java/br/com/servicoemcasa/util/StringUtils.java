package br.com.servicoemcasa.util;

public class StringUtils {

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean isValidEmail(String s) {
        return s.matches(EMAIL_PATTERN);
    }

    public static String lpad(long num, int i) {
        return String.format("%0" + i + "d", num);
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

}
