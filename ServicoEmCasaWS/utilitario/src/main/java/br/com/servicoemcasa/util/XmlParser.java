package br.com.servicoemcasa.util;

import java.io.Reader;
import java.io.Writer;

import com.thoughtworks.xstream.XStream;

public class XmlParser {

    @SuppressWarnings("unchecked")
    public static <T> T fromXml(Reader xml, Class<T> classOf) {
        return (T) getXStream(classOf).fromXML(xml);
    }

    public static void toXml(Object obj, Class<?> classOf, Writer writer) {
        getXStream(classOf).toXML(obj, writer);
    }

    private static XStream getXStream(Class<?> classOf) {
        XStream xstream = new XStream();
        xstream.alias(classOf.getSimpleName(), classOf);
        return xstream;
    }

}
