package br.com.servicoemcasa.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StringUtilsTest {

    @Test
    public void isValidEmailTest() {
        assertTrue(StringUtils.isValidEmail("teste@teste.com"));
        assertTrue(StringUtils.isValidEmail("teste@teste.net.br"));
        assertTrue(StringUtils.isValidEmail("teste@teste.co"));
        assertTrue(StringUtils.isValidEmail("teste.trp.2@t.com"));
        assertFalse(StringUtils.isValidEmail("teste.trp.2@t"));
        assertFalse(StringUtils.isValidEmail("teste.trp.2"));
        assertFalse(StringUtils.isValidEmail("teste@jacare."));
    }

    @Test
    public void lpadTest() {
        assertEquals("01", StringUtils.lpad(1, 2));
        assertEquals("002", StringUtils.lpad(2, 3));
    }

}
