package br.com.servicoemcasa.util;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ReflectionToStringTest {

    public class Teste1 {
        @SuppressWarnings("unused")
        private String nome;
        @SuppressWarnings("unused")
        private Long numero;
        @SuppressWarnings("unused")
        private double mydouble;
    }

    public class Teste2 {
        @SuppressWarnings("unused")
        private Teste1 teste1;
        @SuppressWarnings("unused")
        private String attr1;
    }

    @Test
    public void toStringTest() {
        Teste1 t = new Teste1();
        t.nome = "nome teste";
        t.numero = 23333l;
        t.mydouble = 54.9876;
        String s = ReflectionToString.toString(t);
        assertTrue(s.startsWith("["));
        assertTrue(s.contains("nome=nome teste"));
        assertTrue(s.contains("numero=23333"));
        assertTrue(s.contains("mydouble=54.9876"));
        assertTrue(s.endsWith("]"));

        Teste2 t2 = new Teste2();
        t2.teste1 = t;
        t2.attr1 = "atributo 1";
        String s2 = ReflectionToString.toString(t2);
        assertTrue(s2.startsWith("["));
        assertTrue(s2.contains("attr1=atributo 1"));
        assertTrue(s2.endsWith("]"));
    }

}
