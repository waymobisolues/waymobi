package br.com.servicoemcasa.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HashUtilsTest {

    @Test
    public void test() {
        assertEquals("698dc19d489c4e4db73e28a713eab07b", HashUtils.createMD5Hash("teste"));
        assertEquals("a5f06bc634d6650d9dea8a50bd2de680", HashUtils.createMD5Hash("jacaré"));
        assertEquals("83712e05e7af0ca9a789096b049c8936", HashUtils.createMD5Hash("md5 generator"));
    }

}
