package br.com.servicoemcasa.util;

import static org.junit.Assert.assertEquals;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.junit.Test;

public class JsonParserTest {

    Date data = new Date();
    String json = "{\"teste\": \"novo\", \"prop\":\"prop2\", \"arr\":[{\"teste\":1,\"teste2\":3}, {\"teste\":14,\"teste2\":1,\"data\":\""
            + data.getTime() + "\"}]}";

    public static class TesteClass {
        private String teste;
        private String prop;
        private List<TesteArrClass> arr;

        public static class TesteArrClass {
            private int teste;
            private int teste2;
            private Date data;
        }
    }

    @Test
    public void fromJsonReaderTest() {
        TesteClass teste = JsonParser.fromJson(new StringReader(json), TesteClass.class);
        testeClassEquals(teste);
    }

    private void testeClassEquals(TesteClass teste) {
        assertEquals("novo", teste.teste);
        assertEquals("prop2", teste.prop);
        assertEquals(2, teste.arr.size());
        assertEquals(1, teste.arr.get(0).teste);
        assertEquals(3, teste.arr.get(0).teste2);
        assertEquals(14, teste.arr.get(1).teste);
        assertEquals(1, teste.arr.get(1).teste2);
        assertEquals(data, teste.arr.get(1).data);
    }

    @Test
    public void fromJsonStringTest() {
        TesteClass teste = JsonParser.fromJson(json, TesteClass.class);
        testeClassEquals(teste);
    }

    @Test
    public void toJsonWriterTest() {
        TesteClass teste = JsonParser.fromJson(json, TesteClass.class);
        Writer writer = new StringWriter();
        JsonParser.toJson(teste, writer);
        String genJson = writer.toString();
        TesteClass genTeste = JsonParser.fromJson(genJson, TesteClass.class);
        testeClassEquals(genTeste);
    }

    @Test
    public void toJsonStringTest() {
        TesteClass teste = JsonParser.fromJson(json, TesteClass.class);
        String genJson = JsonParser.toJson(teste);
        TesteClass genTeste = JsonParser.fromJson(genJson, TesteClass.class);
        testeClassEquals(genTeste);
    }

    // @Test
    // @SuppressWarnings({ "rawtypes", "unchecked" })
    // public void toMapTest() {
    // LinkedTreeMap map = JsonParser.toMap(json);
    // assertEquals("novo", map.get("teste"));
    // assertEquals("prop2", map.get("prop"));
    // List<Map> list = (List<Map>) map.get("arr");
    // assertEquals(2, list.size());
    // assertEquals(1.0, list.get(0).get("teste"));
    // assertEquals(3.0, list.get(0).get("teste2"));
    // assertEquals(14.0, list.get(1).get("teste"));
    // assertEquals(1.0, list.get(1).get("teste2"));
    // }

}
