package br.com.servicoemcasa.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class SimpleReflectionTest {
 
    class Teste {
    	public int um;
    }
    
    class Teste1 extends Teste {
        public int dois;
    }
    
    class TesteField {
        private int campo;
    }
    
    @Test
    public void testGetDeclaredFields() throws Exception {
        Field[] fields = SimpleReflection.reflect(Teste1.class).getEveryDeclaredFields();
        List<Field> lista = Arrays.asList(fields);
        assertTrue(lista.contains(Teste1.class.getDeclaredField("dois")));
        assertTrue(lista.contains(Teste.class.getDeclaredField("um")));
    }
    
    @Test
    public void testGetField() throws Exception {
        TesteField teste = new TesteField();
        teste.campo = 12;
        assertEquals(12, SimpleReflection.reflect(teste).field("campo").get());
    }
    
    @Test
    public void testSetField() throws Exception {
        TesteField teste = new TesteField();
        SimpleReflection.reflect(teste).field("campo").set(3333);
        assertEquals(3333, teste.campo);
    }
    
}
