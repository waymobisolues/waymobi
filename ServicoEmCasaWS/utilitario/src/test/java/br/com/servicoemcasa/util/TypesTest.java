package br.com.servicoemcasa.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class TypesTest {
    @Test
    public void trim() {
        assertEquals(Types.trim("  test e "), "test e");
    }

    @Test
    public void isNullOrEmpty() {
        assertTrue(Types.isNullOrEmpty(""));
        assertTrue(Types.isNullOrEmpty(null));
        assertFalse(Types.isNullOrEmpty(" "));
    }

    @Test
    public void parseBoolean() {
        assertTrue(Types.parseBoolean("true"));
        assertTrue(Types.parseBoolean("S"));
        assertFalse(Types.parseBoolean("false"));
        assertFalse(Types.parseBoolean("N"));
        assertFalse(Types.parseBoolean("A"));
        assertNull(Types.parseBoolean(""));
        assertNull(Types.parseBoolean(null));
    }

    @Test
    public void parseByte() {
        assertEquals(Types.parseByte("127"), new Byte((byte) 127));
        assertEquals(Types.parseByte("-128"), new Byte((byte) -128));
    }

    @Test(expected = NumberFormatException.class)
    public void parseByteWithException() {
        assertEquals(Types.parseByte("-200"), new Byte((byte) -200));
    }

    @Test
    public void parseShort() {
        assertEquals(Types.parseShort("32000"), new Short((short) 32000));
        assertEquals(Types.parseShort("-250,00"), new Short((short) -25000));
    }

    @Test(expected = NumberFormatException.class)
    public void parseShortWithException() {
        assertEquals(Types.parseShort("64000"), new Short((short) 64000));
    }

    @Test
    public void parseChar() {
        assertEquals(Types.parseChar("A"), new Character('A'));
        assertEquals(Types.parseChar("z"), new Character('z'));
        assertEquals(Types.parseChar("^olá"), new Character('^'));
    }

    @Test
    public void parseInt() {
        assertEquals(Types.parseInt("32000"), new Integer(32000));
        assertEquals(Types.parseInt("-250,00"), new Integer(-25000));
        assertEquals(Types.parseInt("2147483647"), new Integer(Integer.MAX_VALUE));
    }

    @Test(expected = NumberFormatException.class)
    public void parseIntWithException() {
        assertEquals(Types.parseInt("2147483648"), new Integer(Integer.MAX_VALUE));
    }

    @Test
    public void parseBigInteger() {
        assertEquals(Types.parseBigInteger("2147483648").longValue(), new BigInteger("2147483648").longValue());
        assertEquals(Types.parseBigInteger("-2147483657").longValue(), new BigInteger("-2147483657").longValue());
    }

    @Test
    public void parseLong() {
        assertEquals(Types.parseLong("2147483648"), new Long(2147483648l));
        assertEquals(Types.parseLong("-2147483657"), new Long(-2147483657l));
    }

    @Test
    public void parseFloat() {
        assertEquals(Types.parseFloat("214.748.364,8"), new Float(214748364.8f));
        assertEquals(Types.parseFloat("214,748,364.88"), new Float(214748364.88f));
        assertEquals(Types.parseFloat("21474836488"), new Float(21474836488f));
        assertEquals(Types.parseFloat("2,147,483,648,84"), new Float(2147483648.84f));
    }

    @Test
    public void parseDouble() {
        assertEquals(Types.parseDouble("214.748.364,8"), new Double(214748364.8d));
        assertEquals(Types.parseDouble("214,748,364.88"), new Double(214748364.88d));
        assertEquals(Types.parseDouble("21474836488"), new Double(21474836488d));
        assertEquals(Types.parseDouble("2,147,483,648,84"), new Double(2147483648.84d));
    }

    @Test
    public void parseBigDecimal() {
        assertEquals(Types.parseBigDecimal("214.748.364,8").doubleValue(), 214748364.8d, 0);
        assertEquals(Types.parseBigDecimal("214,748,364.88").doubleValue(), 214748364.88d, 0);
        assertEquals(Types.parseBigDecimal("21474836488").doubleValue(), 21474836488d, 0);
        assertEquals(Types.parseBigDecimal("2,147,483,648,84").doubleValue(), 2147483648.84d, 0);
        assertNull(Types.parseBigDecimal(""));
        assertNull(Types.parseBigDecimal(null));
    }

    @Test
    public void parseDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DATE, 10);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.YEAR, 2010);
        Date d = Types.parseDate("10/10/2010");
        assertEquals(calendar.getTime(), d);
        assertNull(Types.parseDate("10-10-2010"));
        assertNull(Types.parseDate(null));
    }

    @Test
    public void parseString() {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DATE, 10);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.YEAR, 2010);
        assertEquals(Types.parseString(calendar.getTime()), "10/10/2010");
        assertNull(Types.parseString(null));
    }

    @Test
    public void byteValue() {
        assertEquals(Types.byteValue(new Byte((byte) 120)), 120);
        assertEquals(Types.byteValue(null), 0);
    }

    @Test
    public void shortValue() {
        assertEquals(Types.shortValue(new Short((short) 3215)), 3215);
        assertEquals(Types.shortValue(null), 0);
    }

    @Test
    public void intValue() {
        assertEquals(Types.intValue(new Integer(3215)), 3215);
        assertEquals(Types.intValue(null), 0);
    }

    @Test
    public void longValue() {
        assertEquals(Types.longValue(new Long(1231321)), 1231321);
        assertEquals(Types.longValue(null), 0);
    }

    @Test
    public void floatValue() {
        assertEquals(Types.floatValue(new Float(1231321.23f)), 1231321.23f, 0);
        assertEquals(Types.floatValue(null), 0f, 0);
    }

    @Test
    public void doubleValue() {
        assertEquals(Types.doubleValue(new Double(1231321.23d)), 1231321.23d, 0);
        assertEquals(Types.doubleValue(null), 0d, 0);
    }

    @Test
    public void booleanValue() {
        assertEquals(Types.booleanValue(Boolean.TRUE), true);
        assertEquals(Types.booleanValue(null), false);
    }

    @Test
    public void cast() {
        assertTrue(Types.cast("true", Boolean.class));
        assertTrue(Types.cast("S", Boolean.class));
        assertFalse(Types.cast("false", Boolean.class));
        assertFalse(Types.cast("N", Boolean.class));
        assertFalse(Types.cast("A", Boolean.class));
        assertNull(Types.cast("", Boolean.class));
        assertNull(Types.cast(null, Boolean.class));

        assertEquals(Types.cast("127", Byte.class), new Byte((byte) 127));
        assertEquals(Types.cast("-128", Byte.class), new Byte((byte) -128));

        assertEquals(Types.cast("32000", Short.class), new Short((short) 32000));
        assertEquals(Types.cast("-250,00", Short.class), new Short((short) -25000));

        assertEquals(Types.cast("A", Character.class), new Character('A'));
        assertEquals(Types.cast("z", Character.class), new Character('z'));
        assertEquals(Types.cast("^olá", Character.class), new Character('^'));

        assertEquals(Types.cast("32000", Integer.class), new Integer(32000));
        assertEquals(Types.cast("-250,00", Integer.class), new Integer(-25000));
        assertEquals(Types.cast("2147483647", Integer.class), new Integer(Integer.MAX_VALUE));

        assertEquals(Types.cast("2147483648", BigInteger.class).longValue(), new BigInteger("2147483648").longValue());
        assertEquals(Types.cast("-2147483657", BigInteger.class).longValue(), new BigInteger("-2147483657").longValue());

        assertEquals(Types.cast("2147483648", Long.class), new Long(2147483648l));
        assertEquals(Types.cast("-2147483657", Long.class), new Long(-2147483657l));

        assertEquals(Types.cast("214.748.364,8", Float.class), new Float(214748364.8f));
        assertEquals(Types.cast("214,748,364.88", Float.class), new Float(214748364.88f));
        assertEquals(Types.cast("21474836488", Float.class), new Float(21474836488f));
        assertEquals(Types.cast("2,147,483,648,84", Float.class), new Float(2147483648.84f));

        assertEquals(Types.cast("214.748.364,8", Double.class), new Double(214748364.8d));
        assertEquals(Types.cast("214,748,364.88", Double.class), new Double(214748364.88d));
        assertEquals(Types.cast("21474836488", Double.class), new Double(21474836488d));
        assertEquals(Types.cast("2,147,483,648,84", Double.class), new Double(2147483648.84d));

        assertEquals(Types.cast("214.748.364,8", BigDecimal.class).doubleValue(), 214748364.8d, 0);
        assertEquals(Types.cast("214,748,364.88", BigDecimal.class).doubleValue(), 214748364.88d, 0);
        assertEquals(Types.cast("21474836488", BigDecimal.class).doubleValue(), 21474836488d, 0);
        assertEquals(Types.cast("2,147,483,648,84", BigDecimal.class).doubleValue(), 2147483648.84d, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DATE, 10);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.YEAR, 2010);
        Date d = Types.cast("10/10/2010", Date.class);
        assertEquals(calendar.getTime(), d);
        assertNull(Types.cast("10-10-2010", Date.class));
    }

    @Test
    public void testIsInstantiable() {
        assertFalse(Types.isInstantiable(Class.class));
        assertFalse(Types.isInstantiable(Method.class));
        assertTrue(Types.isInstantiable(TypesTest.class));
    }

    @Test
    public void testIsSimpleObject() {
        assertFalse(Types.isInstantiable(Class.class));
        assertFalse(Types.isInstantiable(Method.class));
        assertTrue(Types.isInstantiable(TypesTest.class));
    }
    
    @Test
    public void testIsInteger() {
        assertTrue(Types.isInteger("123456"));
        assertTrue(Types.isInteger("987564321"));
        assertFalse(Types.isInteger("12345a6"));
        assertFalse(Types.isInteger(" 987564321"));
        assertFalse(Types.isInteger("987564321 "));
    }
}
