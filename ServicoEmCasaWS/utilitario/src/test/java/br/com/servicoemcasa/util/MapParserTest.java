package br.com.servicoemcasa.util;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class MapParserTest {

    public static class TesteClass {
        private String teste1;
        private String teste2;
        private String teste45;
        private String teste;
        private List<Teste2Class> list;

        public static class Teste2Class {
            private int num;
            private double f;
            private List<Teste2Class> lista2;
        }
    }

    @Test
    public void test() {
        Map<String, String[]> map = new HashMap<String, String[]>();
        map.put("teste1", new String[] { "valor21" });
        map.put("teste2", new String[] { "valor2" });
        map.put("teste45", new String[] { "valor8" });
        map.put("teste", new String[] { "teste" });
        map.put("list.num", new String[] { "52", "412" });
        map.put("list.f", new String[] { "12.68", "55.11" });
        map.put("list.lista2.f", new String[] { "123456.77", "321.65484", "321.11" });
        TesteClass teste = MapParser.fromMap(map, TesteClass.class);
        assertEquals("valor21", teste.teste1);
        assertEquals("valor2", teste.teste2);
        assertEquals("valor8", teste.teste45);
        assertEquals("teste", teste.teste);
        assertEquals(3, teste.list.size());
        assertEquals(52, teste.list.get(0).num);
        assertEquals(12.68d, teste.list.get(0).f, 0);
        assertEquals(123456.77d, teste.list.get(0).lista2.get(0).f, 0);
        assertEquals(412, teste.list.get(1).num);
        assertEquals(55.11d, teste.list.get(1).f, 0);
        assertEquals(321.65484, teste.list.get(1).lista2.get(0).f, 0);
        assertEquals(321.11, teste.list.get(2).lista2.get(0).f, 0);
    }

}
