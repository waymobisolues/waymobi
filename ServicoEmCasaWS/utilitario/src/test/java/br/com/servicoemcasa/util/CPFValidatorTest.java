package br.com.servicoemcasa.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CPFValidatorTest {

    @Test
    public void test() {
        assertTrue(CPFValidator.isValid(31636546730l));
        assertTrue(CPFValidator.isValid(57379331132l));
        assertTrue(CPFValidator.isValid(45315176606l));
        assertTrue(CPFValidator.isValid(67376662138l));
        assertFalse(CPFValidator.isValid(31636546731l));
        assertFalse(CPFValidator.isValid(57379431132l));
        assertFalse(CPFValidator.isValid(45315186606l));
        assertFalse(CPFValidator.isValid(67376662139l));
    }

}
