package br.com.servicoemcasa.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CNPJValidatorTest {

    @Test
    public void test() {
        assertTrue(CNPJValidator.isValid(7128339000117l));
        assertTrue(CNPJValidator.isValid(13019700000170l));
        assertTrue(CNPJValidator.isValid(46397021000193l));
        assertTrue(CNPJValidator.isValid(21041573000133L));
        assertFalse(CNPJValidator.isValid(7128337000117l));
        assertFalse(CNPJValidator.isValid(13019700000171l));
        assertFalse(CNPJValidator.isValid(46397022000193l));
        assertFalse(CNPJValidator.isValid(21041573000134L));
    }
    
}
