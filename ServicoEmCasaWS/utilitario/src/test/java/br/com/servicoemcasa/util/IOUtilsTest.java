package br.com.servicoemcasa.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

public class IOUtilsTest {

    @Test
    public void readStreamTest() throws IOException {
        byte[] b = "teste de leitura".getBytes();
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        assertTrue(Arrays.equals(b, IOUtils.readStream(in)));
    }

    @Test
    public void echoTest() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream("teste de leitura".getBytes());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.echo(in, out);
        assertEquals("teste de leitura", new String(out.toByteArray()));
    }

    @Test
    public void readStringTest() throws IOException {
        String original = "teste de leitura";
        ByteArrayInputStream in = new ByteArrayInputStream(original.getBytes());
        String lido = IOUtils.readString(in);
        assertEquals(original, lido);
    }

    @Test
    public void readStringCharsetTest() throws IOException {
        String original = "teste de leitura à ó ç â ã ü";
        ByteArrayInputStream in = new ByteArrayInputStream(original.getBytes("utf-32"));
        String lido = IOUtils.readString(in, "cp1252");
        assertFalse(original.equals(lido));
        in.reset();
        lido = IOUtils.readString(in, "utf-32");
        assertEquals(original, lido);
    }

}
