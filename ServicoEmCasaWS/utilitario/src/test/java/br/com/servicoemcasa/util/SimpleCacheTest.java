package br.com.servicoemcasa.util;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import org.junit.Test;

public class SimpleCacheTest {
    @Test
    public void testCache() {
        int capacity = 20;
        Cache<String, String> cache = new SimpleCache<String, String>(20);
        for (int i = 0; i < capacity; ) {
            cache.put("key"+i, "value"+i);
            assertEquals(cache.size(), ++i);
        }
        assertTrue(cache.containsKey("key0"));
        assertTrue(cache.containsKey("key1"));
        assertTrue(cache.containsKey("key"+(capacity-1)));
        cache.put("key", "value");
        cache.put("key-", "value-");
        assertEquals(cache.size(), capacity);
        assertFalse(cache.containsKey("key0"));
        assertFalse(cache.containsKey("key1"));
        assertTrue(cache.containsKey("key2"));
        assertTrue(cache.containsKey("key"+(capacity-1)));
        assertTrue(cache.containsKey("key"));
        assertTrue(cache.containsKey("key-"));
        cache.put("key2", "value2"); // key2 como �ltimo incluido
        cache.put("key--", "value--");
        assertTrue(cache.containsKey("key2"));
        assertFalse(cache.containsKey("key3"));
    }
    
}
