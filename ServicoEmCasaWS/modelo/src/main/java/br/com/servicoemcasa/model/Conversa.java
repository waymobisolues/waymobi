package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.servicoemcasa.model.service.BroadcastMessage;
import br.com.servicoemcasa.model.service.BroadcastService;
import br.com.servicoemcasa.model.service.Broadcastable;
import br.com.servicoemcasa.model.service.DependencyResolver;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@NamedQueries({ @NamedQuery(name = "Conversa.FindByCodigoAtendimentoPrestador", query = "select c from Conversa c where c.solicitacaoAtendimentoPrestador.codigo = :codigoSolicitacaoAtendimentoPrestador") })
@Entity
@Table(name = "Conversa")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Conversa implements Serializable, Broadcastable {

    private static final long serialVersionUID = 1L;

    private transient BroadcastService<Gcm> gcmBroadcastService;
    private transient DependencyResolver dependencyResolver;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codigo;

    @ManyToOne
    @NotNull
    @Valid
    private SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador;

    @OneToOne
    @NotNull
    @Valid
    private Gcm remetente;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;

    @NotNull
    @Size(min = 1)
    private String mensagem;

    @Inject
    Conversa() {
    }

    @Inject
    void setGcmBroadcastService(BroadcastService<Gcm> service) {
        this.gcmBroadcastService = service;
    }

    @Inject
    void setDependencyResolver(DependencyResolver dependencyResolver) {
        this.dependencyResolver = dependencyResolver;
    }

    @PrePersist
    void prePersist() {
        data = new Date();
    }

    @PostPersist
    void postPersist() {
        enviaAvisoAoDestinatario();
    }

    void atualizaAtendimento(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        this.remetente = dependencyResolver.resolve(GcmRepository.class).findByHash(remetente.getHashedKey());
        this.solicitacaoAtendimentoPrestador = solicitacaoAtendimentoPrestador;
    }

    private void enviaAvisoAoDestinatario() {
        Gcm destinatario = solicitacaoAtendimentoPrestador.getPrestador().getGcm().equals(remetente) ? solicitacaoAtendimentoPrestador
                .getSolicitacaoAtendimento().getCliente().getGcm() : solicitacaoAtendimentoPrestador.getPrestador().getGcm();
        gcmBroadcastService.broadcast(this, destinatario);
    }

    public long getCodigo() {
        return codigo;
    }

    public Gcm getRemetente() {
        return remetente;
    }

    public Date getData() {
        return data;
    }

    public String getMensagem() {
        return mensagem;
    }

    public SolicitacaoAtendimentoPrestador getSolicitacaoAtendimentoPrestador() {
        return solicitacaoAtendimentoPrestador;
    }

    public class ConversaMessage {

        public final long codigo;
        public final long codigoSolicitacao;
        public final long codigoSolicitacaoPrestador;
        public final String nomeOrigem;
        public final String mensagem;

        public ConversaMessage(long codigo, long codigoSolicitacao, long codigoSolicitacaoPrestador, String nomeOrigem, String mensagem) {
            super();
            this.codigo = codigo;
            this.codigoSolicitacao = codigoSolicitacao;
            this.codigoSolicitacaoPrestador = codigoSolicitacaoPrestador;
            this.nomeOrigem = nomeOrigem;
            this.mensagem = mensagem;
        }

    }

    @Override
    public BroadcastMessage getMessage() {
        return new BroadcastMessage(getClass().getSimpleName(), new ConversaMessage(this.codigo, this.solicitacaoAtendimentoPrestador
                .getSolicitacaoAtendimento().getCodigo(), this.solicitacaoAtendimentoPrestador.getCodigo(), this.getNomeRemetente(),
                this.mensagem));
    }

    private String getNomeRemetente() {
        if (remetente.equals(solicitacaoAtendimentoPrestador.getPrestador().getGcm())) {
            return solicitacaoAtendimentoPrestador.getPrestador().getNomeFantasia();
        } else {
            return solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCliente().getNome();
        }
    }

}
