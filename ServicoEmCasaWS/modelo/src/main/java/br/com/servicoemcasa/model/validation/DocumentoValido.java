package br.com.servicoemcasa.model.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = DocumentoValidator.class)
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DocumentoValido {

    String message() default "{br.com.servicoemcasa.model.validation.Documento.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
