package br.com.servicoemcasa.model.vo;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import br.com.servicoemcasa.model.service.BroadcastDestination;
import br.com.servicoemcasa.util.StringUtils;

@Embeddable
public class Email implements Serializable, BroadcastDestination {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Pattern(regexp = StringUtils.EMAIL_PATTERN)
    private String email;

    Email() {
    }

    public Email(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

}
