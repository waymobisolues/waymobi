package br.com.servicoemcasa.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "UF")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class UF implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Size(min = 2, max = 2)
    private String sigla;

    @NotNull
    @Size(min = 3, max = 45)
    private String nome;

    public String getSigla() {
        return sigla;
    }

    public String getNome() {
        return nome;
    }

}
