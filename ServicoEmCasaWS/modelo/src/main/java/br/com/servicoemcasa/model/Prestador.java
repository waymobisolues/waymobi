package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.servicoemcasa.model.service.DependencyResolver;
import br.com.servicoemcasa.model.validation.DocumentoValido;
import br.com.servicoemcasa.model.vo.AvaliacaoPrestador;
import br.com.servicoemcasa.model.vo.Documento;
import br.com.servicoemcasa.model.vo.DocumentoConselho;
import br.com.servicoemcasa.model.vo.Email;
import br.com.servicoemcasa.model.vo.Endereco;
import br.com.servicoemcasa.model.vo.LocalAtendimento;
import br.com.servicoemcasa.model.vo.RG;
import br.com.servicoemcasa.model.vo.RegiaoAtendimento;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@NamedQueries({
        @NamedQuery(name = "Prestador.FindByTipo", query = "select p from Prestador p inner join p.tipo t where t = :tipoPrestador"),
        @NamedQuery(name = "Prestador.FindByDocumento", query = "select p from Prestador p where p.documento.numeroDocumento = :numeroDocumento"),
        @NamedQuery(name = "Prestador.GetByGcm", query = "select p from Prestador p where p.cliente.gcm.hashedGcmKey = :gcmKey"),
        @NamedQuery(name = "Prestador.FindByEmail", query = "select p from Prestador p where p.email.email = :email") })
@Entity
@Table(name = "Prestador")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Prestador implements Serializable {

    private static final long serialVersionUID = 1L;

    private transient DependencyResolver dependencyResolver;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigo;

    @Valid
    @NotNull
    @OneToOne
    private Cliente cliente;

    @Valid
    @NotNull
    private Email email;

    @Valid
    @NotNull
    private String nomeFantasia;

    @Valid
    @ManyToMany
    @Size(min = 1)
    private List<TipoPrestador> tipo;

    @NotNull
    @Embedded
    @DocumentoValido
    private Documento documento;

    @NotNull
    @Valid
    @Embedded
    private Endereco endereco;

    @NotNull
    @Valid
    @Embedded
    private RegiaoAtendimento regiaoAtendimento;

    private String resumoProfissional;

    @Valid
    @Embedded
    private DocumentoConselho documentoConselho;

    @Valid
    @Embedded
    private RG rg;

    @Embedded
    private AvaliacaoPrestador avaliacao;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInclusao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAlteracao;

    @Inject
    Prestador() {
    }

    @Inject
    void setDependencyResolver(DependencyResolver dependencyResolver) {
        this.dependencyResolver = dependencyResolver;
    }

    @PrePersist
    void prePersist() {
        dataInclusao = new Date();
        avaliacao = new AvaliacaoPrestador();
    }

    @PreUpdate
    void preUpdate() {
        dataAlteracao = new Date();
    }

    public void atualiza(Prestador prestador) {
        email = prestador.email;
        nomeFantasia = prestador.nomeFantasia;
        tipo = prestador.tipo;
        documento = prestador.documento;
        endereco = prestador.endereco;
        regiaoAtendimento = prestador.regiaoAtendimento;
        resumoProfissional = prestador.resumoProfissional;
        documentoConselho = prestador.documentoConselho;
        rg = prestador.rg;
    }

    public boolean atende(LocalAtendimento localAtendimento, Endereco enderecoAtendimento) {
        return localAtendimento.dentroDoLimite(regiaoAtendimento.getMaximaDistanciaAtendimento(), endereco,
                enderecoAtendimento);
    }

    public void atualizaAvaliacao() {
        avaliacao.setAvaliacao(dependencyResolver.resolve(PrestadorRepository.class).getAvaliacao(this));
    }

    public List<TipoPrestador> getTipo() {
        return Collections.unmodifiableList(tipo);
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public RegiaoAtendimento getRegiaoAtendimento() {
        return regiaoAtendimento;
    }

    public String getResumoProfissional() {
        return resumoProfissional;
    }

    public Documento getDocumento() {
        return documento;
    }

    public DocumentoConselho getDocumentoConselho() {
        return documentoConselho;
    }

    public RG getRg() {
        return rg;
    }

    public Gcm getGcm() {
        return cliente.getGcm();
    }

    public int getCodigo() {
        return codigo;
    }

    public Email getEmail() {
        return email;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public AvaliacaoPrestador getAvaliacao() {
        return avaliacao;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + codigo;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Prestador other = (Prestador) obj;
        if (codigo != other.codigo)
            return false;
        return true;
    }

}
