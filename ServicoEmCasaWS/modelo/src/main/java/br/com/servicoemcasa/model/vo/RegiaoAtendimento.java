package br.com.servicoemcasa.model.vo;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.Embeddable;

import br.com.servicoemcasa.model.service.DistanceCalculatorService;

@Embeddable
public class RegiaoAtendimento implements Serializable {

    private static final long serialVersionUID = 1L;

    private transient DistanceCalculatorService distanceCalculator;

    private int maximaDistanciaAtendimento;

    private float latitudeRegiao;
    private float longitudeRegiao;

    @Inject
    RegiaoAtendimento() {
    }

    public RegiaoAtendimento(int maximaDistanciaAtendimento, float latitude, float longitude) {
        this();
        this.maximaDistanciaAtendimento = maximaDistanciaAtendimento;
        this.latitudeRegiao = latitude;
        this.longitudeRegiao = longitude;
    }

    public RegiaoAtendimento(int maximaDistanciaAtendimento, Endereco endereco) {
        this(maximaDistanciaAtendimento, endereco.getLatitude(), endereco.getLongitude());
    }

    @Inject
    void setDistanceCalculator(DistanceCalculatorService distanceCalculator) {
        this.distanceCalculator = distanceCalculator;
    }

    public int getMaximaDistanciaAtendimento() {
        return maximaDistanciaAtendimento;
    }

    public float getLatitudeRegiao() {
        return latitudeRegiao;
    }

    public float getLongitudeRegiao() {
        return longitudeRegiao;
    }

    public boolean estaDentroDoLimite(Endereco endereco) {
        return distanceCalculator.calculateDistance(latitudeRegiao, longitudeRegiao, endereco.getLatitude(),
                endereco.getLongitude()) < maximaDistanciaAtendimento;
    }

}
