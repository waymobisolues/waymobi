package br.com.servicoemcasa.model;

import java.util.List;

import br.com.servicoemcasa.model.vo.LocalAtendimento;

public interface TipoPrestadorRepository extends Repository<Integer, TipoPrestador> {

    List<TipoPrestador> findByCodigoGrupo(Integer parseInt);

    List<TipoPrestador> findByCodigoGrupoAndDistancia(int codigoGrupo, float latitude, float longitude, LocalAtendimento localAtendimento);

}
