package br.com.servicoemcasa.model;

public interface GcmRepository extends Repository<Long, Gcm> {

    Gcm findByHash(String hash);
}
