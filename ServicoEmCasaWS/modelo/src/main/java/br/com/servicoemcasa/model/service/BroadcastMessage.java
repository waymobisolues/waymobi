package br.com.servicoemcasa.model.service;

import java.io.Serializable;
import java.util.Map;

public class BroadcastMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String messageType;
    private final Object message;
    private final String subject;
    private final Map<String, String> params;

    public BroadcastMessage(String messageType, Object message, String subject, Map<String, String> params) {
        super();
        this.messageType = messageType;
        this.message = message;
        this.subject = subject;
        this.params = params;
    }

    public BroadcastMessage(String messageType, Object message) {
        this(messageType, message, null, null);
    }
    
    public BroadcastMessage(String subject, String message, Map<String, String> params) {
        this("text", message, subject, params);
    }

    public String getMessageType() {
        return messageType;
    }

    public Object getMessage() {
        return message;
    }

    public String getSubject() {
        return subject;
    }
    
    public Map<String, String> getParams() {
        return params;
    }

}
