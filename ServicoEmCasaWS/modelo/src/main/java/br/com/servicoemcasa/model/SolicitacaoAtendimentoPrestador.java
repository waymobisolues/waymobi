package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.Table;

import br.com.servicoemcasa.model.service.BroadcastMessage;
import br.com.servicoemcasa.model.service.BroadcastService;
import br.com.servicoemcasa.model.service.Broadcastable;
import br.com.servicoemcasa.model.service.DistanceCalculatorService;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "SolicitacaoAtendimentoPrestador")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class SolicitacaoAtendimentoPrestador implements Serializable, Broadcastable {

    private static final long serialVersionUID = 1L;

    private transient DistanceCalculatorService distanceCalculator;
    private transient BroadcastService<Gcm> gcmBroadcastService;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codigo;

    @ManyToOne
    private SolicitacaoAtendimento solicitacaoAtendimento;

    @OneToOne
    private Prestador prestador;

    @OneToMany(mappedBy = "solicitacaoAtendimentoPrestador", cascade = CascadeType.ALL)
    private List<Conversa> conversa;

    private int distanciaParaEnderecoDeAtendimento;
    
    private transient boolean aceitoAnterior;
    private boolean aceito;

    @Inject
    SolicitacaoAtendimentoPrestador() {
    }

    public SolicitacaoAtendimentoPrestador(SolicitacaoAtendimento solicitacaoAtendimento, Prestador prestador) {
        this();
        this.solicitacaoAtendimento = solicitacaoAtendimento;
        this.prestador = prestador;
        calculaDistanciaParaEnderecoDeAtendimento();
    }

    @PostPersist
    void postPersist() {
        gcmBroadcastService.broadcast(this, prestador.getGcm());
    }

    @PostUpdate
    void postUpdate() {
        if (isAceito() && !aceitoAnterior) {
            gcmBroadcastService.broadcast(this, this.solicitacaoAtendimento.getCliente().getGcm());
        }
    }
    
    @PostLoad
    void postLoad() {
        this.aceitoAnterior = aceito;
    }

    @Inject
    void setDistanceCalculator(DistanceCalculatorService distanceCalculator) {
        this.distanceCalculator = distanceCalculator;
    }

    @Inject
    void setGcmBroadcastService(BroadcastService<Gcm> service) {
        this.gcmBroadcastService = service;
    }

    private void calculaDistanciaParaEnderecoDeAtendimento() {
        this.distanciaParaEnderecoDeAtendimento = this.distanceCalculator.calculateDistance(solicitacaoAtendimento.getEndereco()
                .getLatitude(), solicitacaoAtendimento.getEndereco().getLongitude(), prestador.getEndereco().getLatitude(), prestador
                .getEndereco().getLongitude());
    }

    public long getCodigo() {
        return codigo;
    }

    public SolicitacaoAtendimento getSolicitacaoAtendimento() {
        return solicitacaoAtendimento;
    }

    public Prestador getPrestador() {
        return prestador;
    }

    public int getDistanciaParaEnderecoDeAtendimento() {
        return distanciaParaEnderecoDeAtendimento;
    }

    public boolean isAceito() {
        return aceito;
    }

    public void aceita() {
        this.aceito = true;
    }

    public List<Conversa> getConversa() {
        return Collections.unmodifiableList(conversa);
    }

    public void adicionaConversa(Conversa conversa) {
        conversa.atualizaAtendimento(this);
        this.conversa.add(conversa);
    }

    public class SolicitacaoAtendimentoMessage {

        public final long codigo;
        public final long codigoSolicitacao;
        public final Gcm gcmPrestador;
        public final Date data;
        public final String tipoServico;
        public final int distancia;

        public SolicitacaoAtendimentoMessage(long codigo, long codigoSolicitacao, Date data, String tipoServico, int distancia,
                Gcm gcmPrestador) {

            this.codigo = codigo;
            this.codigoSolicitacao = codigoSolicitacao;
            this.data = data;
            this.tipoServico = tipoServico;
            this.distancia = distancia;
            this.gcmPrestador = gcmPrestador;
        }

    }

    @Override
    public BroadcastMessage getMessage() {
        return new BroadcastMessage(getClass().getSimpleName(), new SolicitacaoAtendimentoMessage(this.codigo,
                this.solicitacaoAtendimento.getCodigo(), this.solicitacaoAtendimento.getData(), this.solicitacaoAtendimento
                        .getTipoPrestador().getDescricao(), distanciaParaEnderecoDeAtendimento, this.getPrestador().getGcm()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (codigo ^ (codigo >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SolicitacaoAtendimentoPrestador other = (SolicitacaoAtendimentoPrestador) obj;
        if (codigo != other.codigo)
            return false;
        return true;
    }

}
