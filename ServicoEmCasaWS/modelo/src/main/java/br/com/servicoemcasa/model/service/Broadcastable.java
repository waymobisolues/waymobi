package br.com.servicoemcasa.model.service;

public interface Broadcastable {

    BroadcastMessage getMessage();
    
}
