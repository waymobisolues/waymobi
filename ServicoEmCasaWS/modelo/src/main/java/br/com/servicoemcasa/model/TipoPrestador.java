package br.com.servicoemcasa.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "TipoPrestador")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@NamedQueries({
        @NamedQuery(name = "TipoPrestador.FindByCodigoGrupo", query = "SELECT t FROM TipoPrestador t WHERE t.grupoTipoPrestador.codigoGrupo = :codigoGrupo"),
        @NamedQuery(name = "TipoPrestador.FindByCodigoGrupoAndDistancia", query = "SELECT DISTINCT t FROM Prestador p JOIN FETCH p.tipo t WHERE t.grupoTipoPrestador.codigoGrupo = :codigoGrupo AND SQL('calculate_distance(?,?,?,?)', :latitude, :longitude, p.regiaoAtendimento.latitudeRegiao, p.regiaoAtendimento.longitudeRegiao) <= CASE WHEN p.regiaoAtendimento.maximaDistanciaAtendimento = 0 THEN :distancia ELSE p.regiaoAtendimento.maximaDistanciaAtendimento END") })
public class TipoPrestador implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigo;

    @NotNull
    @Valid
    @OneToOne
    private GrupoTipoPrestador grupoTipoPrestador;

    @NotNull
    private String descricao;

    TipoPrestador() {
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

}
