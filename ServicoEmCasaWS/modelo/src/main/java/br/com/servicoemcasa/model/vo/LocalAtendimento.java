package br.com.servicoemcasa.model.vo;

public enum LocalAtendimento {

    Casa(0), Estabelecimento(30);

    private final int distanciaAtendimento;

    LocalAtendimento(int distanciaAtendimento) {
        this.distanciaAtendimento = distanciaAtendimento;
    }

    public boolean dentroDoLimite(int distanciaPermitida, Endereco endereco, Endereco enderecoAtendimento) {
        distanciaPermitida = distanciaAtendimento > 0 ? distanciaAtendimento : distanciaPermitida;
        return new RegiaoAtendimento(distanciaPermitida, endereco).estaDentroDoLimite(enderecoAtendimento);
    }

    public int getDistanciaAtendimento() {
        return distanciaAtendimento;
    }
}
