package br.com.servicoemcasa.model;

import java.util.List;

public interface Repository<PK, T> {

    public T get(PK pk);

    public void insert(T entity);
    
//    public T save(T entity);

    public void delete(T entity);

    public List<T> list();

}
