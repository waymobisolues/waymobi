package br.com.servicoemcasa.model;

public interface AutorizacaoCadastroRepository extends Repository<String, AutorizacaoCadastro> {

    AutorizacaoCadastro getByEmailAndGcm(String email, Gcm gcm);

}
