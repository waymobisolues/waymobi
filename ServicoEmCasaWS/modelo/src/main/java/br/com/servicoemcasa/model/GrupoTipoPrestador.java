package br.com.servicoemcasa.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "GrupoTipoPrestador")
@NamedQueries({ @NamedQuery(name = "GrupoTipoPrestador.FindByDistancia", query = "SELECT DISTINCT t.grupoTipoPrestador FROM Prestador AS p JOIN FETCH p.tipo AS t WHERE SQL('calculate_distance(?,?,?,?)', :latitude, :longitude, p.regiaoAtendimento.latitudeRegiao, p.regiaoAtendimento.longitudeRegiao) <= CASE WHEN p.regiaoAtendimento.maximaDistanciaAtendimento = 0 THEN :distancia ELSE p.regiaoAtendimento.maximaDistanciaAtendimento END") })
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class GrupoTipoPrestador implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigoGrupo;

    @NotNull
    private String descricaoGrupo;

    GrupoTipoPrestador() {
    }

    public int getCodigoGrupo() {
        return codigoGrupo;
    }

    public String getDescricaoGrupo() {
        return descricaoGrupo;
    }

}
