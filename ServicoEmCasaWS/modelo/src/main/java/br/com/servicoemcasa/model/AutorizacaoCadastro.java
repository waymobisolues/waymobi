package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.servicoemcasa.model.service.BroadcastMessage;
import br.com.servicoemcasa.model.service.BroadcastService;
import br.com.servicoemcasa.model.service.Broadcastable;
import br.com.servicoemcasa.model.service.DependencyResolver;
import br.com.servicoemcasa.model.vo.Email;
import br.com.servicoemcasa.util.HashUtils;

@NamedQueries({ @NamedQuery(name = "AutorizacaoCadastro.GetByEmailAndGcm", query = "SELECT a FROM AutorizacaoCadastro a WHERE a.email.email=:email AND a.gcmAutorizado=:gcm") })
@Entity
@Table(name = "AutorizacaoCadastro")
public class AutorizacaoCadastro implements Serializable, Broadcastable {

    private static final long serialVersionUID = 1L;

    private transient BroadcastService<Email> broadcastService;
    private transient DependencyResolver dependencyResolver;

    @Id
    private String idAutorizacao;

    @NotNull
    @Embedded
    @Valid
    private Email email;

    @NotNull
    @Valid
    @OneToOne
    private Gcm gcmAutorizado;

    @NotNull
    @OneToOne
    @Valid
    private Gcm gcm;

    private boolean liberado;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInclusao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAlteracao;

    @Inject
    AutorizacaoCadastro() {
    }

    @PrePersist
    private void prePersist() {
        this.dataInclusao = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.dataAlteracao = new Date();
    }

    @Inject
    void setBroadcastService(final BroadcastService<Email> broadcastService) {
        this.broadcastService = broadcastService;
    }

    @Inject
    void setDependencyResolver(final DependencyResolver dependencyResolver) {
        this.dependencyResolver = dependencyResolver;
    }

    public AutorizacaoCadastro(final Email email, final Gcm gcm, final Gcm gcmAutorizado) {
        this();
        this.idAutorizacao = HashUtils.createMD5Hash(email + gcmAutorizado.getHashedKey());
        this.email = email;
        this.gcmAutorizado = gcmAutorizado;
        this.gcm = gcm;
    }

    public void enviaNotificacao() {
        broadcastService.broadcast(this, email);
    }

    public String getIdAutorizacao() {
        return idAutorizacao;
    }

    public Email getEmail() {
        return email;
    }

    public Gcm getGcmAutorizado() {
        return gcmAutorizado;
    }

    public boolean isLiberado() {
        return liberado;
    }

    public void setLiberado(boolean liberado) {
        this.liberado = liberado;
    }

    public Gcm getGcm() {
        return gcm;
    }

    public void liberaCadastro() {
        liberado = true;
        GcmRepository gcmRepository = dependencyResolver.resolve(GcmRepository.class);
        gcmRepository.get(gcm.getId()).mudaGcm(gcmAutorizado);
    }

    @Override
    public BroadcastMessage getMessage() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("key", idAutorizacao);
        return new BroadcastMessage(Messages.getString("AutorizacaoCadastro.0"), Messages.getString("AutorizacaoCadastro.1"), params); //$NON-NLS-1$
    }

}
