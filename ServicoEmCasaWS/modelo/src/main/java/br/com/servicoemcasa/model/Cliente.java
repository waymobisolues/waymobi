package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.servicoemcasa.model.vo.Email;
import br.com.servicoemcasa.model.vo.EstadoCivil;
import br.com.servicoemcasa.model.vo.Sexo;
import br.com.servicoemcasa.model.vo.Telefone;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@NamedQueries({
        @NamedQuery(name = "Cliente.FindByEmail", query = "select c from Cliente c where c.email.email = :email"),
        @NamedQuery(name = "Cliente.GetByGcm", query = "select c from Cliente c where c.gcm.hashedGcmKey = :gcmKey") })
@Entity
@Table(name = "Cliente")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigo;

    @OneToOne
    @NotNull
    @Valid
    private Gcm gcm;

    @NotNull
    @Valid
    private Email email;

    @NotNull
    @Size(min = 1)
    private String nome;

    @Embedded
    @Valid
    @NotNull
    private Telefone telefone;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Sexo sexo;

    @Enumerated(EnumType.STRING)
    @NotNull
    private EstadoCivil estadoCivil;

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInclusao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAlteracao;

    Cliente() {
    }

    @PrePersist
    private void prePersist() {
        dataInclusao = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        dataAlteracao = new Date();
    }

    public void atualiza(Cliente cliente) {
        email = cliente.email;
        nome = cliente.nome;
        telefone = cliente.telefone;
        sexo = cliente.sexo;
        estadoCivil = cliente.estadoCivil;
        dataNascimento = cliente.dataNascimento;
    }

    public void setGcm(Gcm gcm) {
        this.gcm = gcm;
    }

    public int getCodigo() {
        return codigo;
    }

    public Gcm getGcm() {
        return gcm;
    }

    public Email getEmail() {
        return email;
    }

    public String getNome() {
        return nome;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

}
