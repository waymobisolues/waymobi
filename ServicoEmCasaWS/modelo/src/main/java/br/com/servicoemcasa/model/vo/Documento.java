package br.com.servicoemcasa.model.vo;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class Documento {

    @Column(unique = true)
    private Long numeroDocumento;
    @Enumerated(EnumType.STRING)
    private TipoDocumento tipoDocumento;

    Documento() {
    }

    public Documento(Long numeroDocumento, TipoDocumento tipoDocumento) {
        this.numeroDocumento = numeroDocumento;
        this.tipoDocumento = tipoDocumento;
    }

    public Long getNumeroDocumento() {
        return numeroDocumento;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

}
