package br.com.servicoemcasa.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.servicoemcasa.model.vo.Documento;
import br.com.servicoemcasa.util.CNPJValidator;
import br.com.servicoemcasa.util.CPFValidator;

public class DocumentoValidator implements ConstraintValidator<DocumentoValido, Documento> {

    @Override
    public void initialize(DocumentoValido constraintAnnotation) {
    }

    @Override
    public boolean isValid(Documento value, ConstraintValidatorContext context) {
        if (value == null || value.getTipoDocumento() == null) {
            return false;
        }
        switch (value.getTipoDocumento()) {
        case CPF:
            return CPFValidator.isValid(value.getNumeroDocumento());
        case CNPJ:
            return CNPJValidator.isValid(value.getNumeroDocumento());
        default:
            return false;
        }
    }

}
