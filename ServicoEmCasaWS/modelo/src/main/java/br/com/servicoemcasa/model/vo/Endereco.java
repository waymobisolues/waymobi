package br.com.servicoemcasa.model.vo;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class Endereco implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(min = 1)
    private String descricao;

    private String complemento;
    private float latitude;
    private float longitude;

    Endereco() {
    }

    public Endereco(String descricao, String complemento, float latitude, float longitude) {
        super();
        this.descricao = descricao;
        this.complemento = complemento;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getComplemento() {
        return complemento;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

}
