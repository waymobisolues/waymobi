package br.com.servicoemcasa.model.vo;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class Telefone implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(min = 7)
    private String numeroTelefone;

    @NotNull
    @Size(min = 2, max = 3)
    private String ddd;

    Telefone() {
    }

    public Telefone(String ddd, String numero) {
        this();
        this.ddd = ddd;
        this.numeroTelefone = numero;
    }

    public String getDDD() {
        return ddd;
    }

    public String getNumeroTelefone() {
        return numeroTelefone;
    }

}
