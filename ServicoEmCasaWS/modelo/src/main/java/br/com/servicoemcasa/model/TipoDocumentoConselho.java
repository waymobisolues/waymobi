package br.com.servicoemcasa.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "TipoDocumentoConselho")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class TipoDocumentoConselho implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    private String tipoDocumentoConselho;

    @NotNull
    private String descricaoTipoDocumentoConselho;

    TipoDocumentoConselho() {
    }

    public String getTipoDocumentoConselho() {
        return tipoDocumentoConselho;
    }

    public String getDescricaoTipoDocumentoConselho() {
        return descricaoTipoDocumentoConselho;
    }

}
