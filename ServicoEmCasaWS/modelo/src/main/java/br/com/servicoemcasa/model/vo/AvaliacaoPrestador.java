package br.com.servicoemcasa.model.vo;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Embeddable
public class AvaliacaoPrestador implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final int MINIMA_QUANTIDADE_DE_AVALIACOES = 10;
    private static final double PONTUACAO_MAXIMA = 5d;
    private static final double ARREDONDAMENTO = 0.4d;

    private long quantidadeAvaliacoes;
    @JsonIgnore
    @XStreamOmitField
    private long positivos;
    private int pontuacao;

    public AvaliacaoPrestador() {
    }

    public AvaliacaoPrestador(long quantidadeAvaliacoes, long positivos) {
        this.quantidadeAvaliacoes = quantidadeAvaliacoes;
        this.positivos = positivos;
        calculaPontuacao();
    }

    public void setAvaliacao(AvaliacaoPrestador avaliacao) {
        this.quantidadeAvaliacoes = avaliacao.quantidadeAvaliacoes;
        this.positivos = avaliacao.positivos;
        calculaPontuacao();
    }

    private void calculaPontuacao() {
        if (quantidadeAvaliacoes > MINIMA_QUANTIDADE_DE_AVALIACOES) {
            this.pontuacao = (int) Math.floor((PONTUACAO_MAXIMA * positivos / quantidadeAvaliacoes) + ARREDONDAMENTO);
        } else {
            this.quantidadeAvaliacoes = 0;
            this.pontuacao = 0;
        }
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public long getQuantidadeAvaliacoes() {
        return quantidadeAvaliacoes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + pontuacao;
        result = prime * result + (int) (positivos ^ (positivos >>> 32));
        result = prime * result + (int) (quantidadeAvaliacoes ^ (quantidadeAvaliacoes >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AvaliacaoPrestador other = (AvaliacaoPrestador) obj;
        if (pontuacao != other.pontuacao)
            return false;
        if (positivos != other.positivos)
            return false;
        if (quantidadeAvaliacoes != other.quantidadeAvaliacoes)
            return false;
        return true;
    }

}
