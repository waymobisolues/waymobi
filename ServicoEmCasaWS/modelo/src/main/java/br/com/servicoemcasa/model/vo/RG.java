package br.com.servicoemcasa.model.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import br.com.servicoemcasa.model.UF;

@Embeddable
public class RG implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(unique = true)
    private Long rg;

    @NotNull
    @Past
    @Temporal(TemporalType.DATE)
    private Date dataExpedicao;

    @NotNull
    @Size(min = 1, max = 10)
    private String orgaoEmissorRG;

    @NotNull
    @Valid
    @OneToOne
    private UF uf;

    RG() {
    }

    public Long getRg() {
        return rg;
    }

    public Date getDataExpedicao() {
        return dataExpedicao;
    }

    public String getOrgaoEmissorRG() {
        return orgaoEmissorRG;
    }

    public UF getUf() {
        return uf;
    }

}
