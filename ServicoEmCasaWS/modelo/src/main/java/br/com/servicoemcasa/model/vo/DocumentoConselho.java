package br.com.servicoemcasa.model.vo;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;

import br.com.servicoemcasa.model.TipoDocumentoConselho;

@Embeddable
public class DocumentoConselho implements Serializable {

    private static final long serialVersionUID = 1L;

    private String numeroDocumentoConselho;
    
    @OneToOne
    private TipoDocumentoConselho tipoDocumentoConselho;

    DocumentoConselho() {
    }

    public String getNumeroDocumentoConselho() {
        return numeroDocumentoConselho;
    }

    public TipoDocumentoConselho getTipoDocumentoConselho() {
        return tipoDocumentoConselho;
    }

}
