package br.com.servicoemcasa.model.vo;

public enum TipoDocumento {

    CPF, CNPJ;
    
}
