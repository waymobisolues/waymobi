package br.com.servicoemcasa.model;

import java.util.List;

import br.com.servicoemcasa.model.vo.AvaliacaoPrestador;
import br.com.servicoemcasa.model.vo.Documento;

public interface PrestadorRepository extends Repository<Integer, Prestador> {

    List<Prestador> findByTipo(TipoPrestador tipoPrestador);

    Prestador getByGcm(Gcm gcm);
    Prestador getByGcm(String hashedKey);

    Prestador findByDocumento(Documento documento);

    Prestador findByEmail(String email);

    AvaliacaoPrestador getAvaliacao(Prestador prestador);
    
}
