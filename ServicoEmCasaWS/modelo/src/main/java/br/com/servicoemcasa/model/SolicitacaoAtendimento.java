package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.servicoemcasa.model.vo.Endereco;
import br.com.servicoemcasa.model.vo.LocalAtendimento;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@NamedQueries({
        @NamedQuery(name = "SolicitacaoAtendimento.FindByCodigoCliente", query = "select s from SolicitacaoAtendimento s where s.cliente.codigo = :codigoCliente"),
        // @NamedQuery(name = "SolicitacaoAtendimento.FindByCodigoPrestador", query = "select s from SolicitacaoAtendimento as s inner join s.prestadores as p where p.prestador.codigo = :codigoPrestador and (s.encerrado = false or (s.agendamento is not null and s.agendamento.solicitacaoAtendimentoPrestador.prestador.codigo = :codigoPrestador))")
        @NamedQuery(name = "SolicitacaoAtendimento.FindByCodigoPrestador", query = "SELECT s FROM SolicitacaoAtendimento AS s INNER JOIN s.prestadores AS p LEFT JOIN s.agendamento AS a LEFT JOIN a.solicitacaoAtendimentoPrestador AS sp LEFT JOIN sp.prestador AS spp WHERE p.prestador.codigo = :codigoPrestador AND (s.encerrado = false or (a IS NOT NULL AND spp.codigo = :codigoPrestador))")})
@Entity
@Table(name = "SolicitacaoAtendimento")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class SolicitacaoAtendimento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codigo;

    @Valid
    @NotNull
    @OneToOne
    private Cliente cliente;

    @NotNull
    @Enumerated(EnumType.STRING)
    private LocalAtendimento localAtendimento;

    @Valid
    @NotNull
    @Embedded
    private Endereco endereco;

    @Valid
    @NotNull
    @OneToOne
    private TipoPrestador tipoPrestador;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;

    @OneToMany(mappedBy = "solicitacaoAtendimento", cascade = CascadeType.ALL)
    @Valid
    private List<SolicitacaoAtendimentoPrestador> prestadores;

    private String observacoes;

    private boolean encerrado;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInclusao;

    @OneToOne(cascade = CascadeType.ALL)
    private Agendamento agendamento;

    SolicitacaoAtendimento() {
    }

    @PrePersist
    private void prePersist() {
        dataInclusao = new Date();
    }

    public void solicitaAtendimento(Prestador prestador) {
        if (prestador.atende(localAtendimento, endereco)) {
            adicionaPrestador(prestador);
        }
    }

    private void adicionaPrestador(Prestador prestador) {
        if (prestadores == null) {
            prestadores = new ArrayList<>();
        }
        prestadores.add(new SolicitacaoAtendimentoPrestador(this, prestador));
    }

    public void aceitaAgendamento(SolicitacaoAtendimentoPrestador prestador) {
        int sp = prestadores.indexOf(prestador);
        prestadores.get(sp).aceita();
    }

    public Agendamento agenda(SolicitacaoAtendimentoPrestador prestador) {
        int sp = prestadores.indexOf(prestador);
        agendamento = new Agendamento(prestadores.get(sp));
        encerrado = true;
        return agendamento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public TipoPrestador getTipoPrestador() {
        return tipoPrestador;
    }

    public Date getData() {
        return data;
    }

    public List<SolicitacaoAtendimentoPrestador> getPrestadores() {
        return Collections.unmodifiableList(prestadores);
    }

    public long getCodigo() {
        return codigo;
    }

    public boolean isEncerrado() {
        return encerrado;
    }

    public void encerrarSolicitacao() {
        this.encerrado = true;
    }

    public LocalAtendimento getLocalAtendimento() {
        return localAtendimento;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public Agendamento getAgendamento() {
        return agendamento;
    }

}
