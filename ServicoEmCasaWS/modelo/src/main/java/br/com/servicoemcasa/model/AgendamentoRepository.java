package br.com.servicoemcasa.model;

import java.util.List;

public interface AgendamentoRepository extends Repository<Long, Agendamento> {

    List<Agendamento> listaAgendamentosParaEncerrar();

}
