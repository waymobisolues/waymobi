package br.com.servicoemcasa.model.service;

public interface BroadcastService<D extends BroadcastDestination> {

    void broadcast(Broadcastable message, @SuppressWarnings("unchecked") D... broadcastDestination);

}
