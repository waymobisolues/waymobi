package br.com.servicoemcasa.model;

import java.util.List;

import br.com.servicoemcasa.model.vo.LocalAtendimento;

public interface GrupoTipoPrestadorRepository extends Repository<Integer, GrupoTipoPrestador> {

    List<GrupoTipoPrestador> findByDistancia(float latitude, float longitude, LocalAtendimento localAtendimento);

}
