package br.com.servicoemcasa.model.service;

public interface DistanceCalculatorService {

    public int calculateDistance(double latitudeOrigin, double longitudeOrigin, double latitudeDest,
            double longitudeDest);

}
