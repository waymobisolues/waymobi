package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.com.servicoemcasa.model.service.BroadcastDestination;
import br.com.servicoemcasa.util.HashUtils;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@Table(name = "Gcm", indexes = { @Index(unique = false, columnList = "hashedGcmKey") })
@NamedQueries({ @NamedQuery(name = "Gcm.FindByHash", query = "select g from Gcm g where g.hashedGcmKey = :hashedGcmKey and g.valido = true") })
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Gcm implements Serializable, BroadcastDestination {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @XStreamOmitField
    private long id;

    @NotNull
    private String hashedGcmKey;

    @NotNull
    private String gcmKey;

    @JsonIgnore
    @XStreamOmitField
    private boolean valido = true;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    @XStreamOmitField
    private Date dataInclusao;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    @XStreamOmitField
    private Date dataAlteracao;

    Gcm() {
    }

    public void mudaGcm(Gcm gcmAutorizado) {
        setKey(gcmAutorizado.gcmKey);
        gcmAutorizado.valido = false;
    }

    @PrePersist
    void prePersist() {
        this.dataInclusao = new Date();
        this.valido = true;
    }

    @PreUpdate
    void preUpdate() {
        this.dataAlteracao = new Date();
    }

    public long getId() {
        return id;
    }

    public Gcm(String key) {
        setKey(key);
    }

    private void setKey(String key) {
        this.gcmKey = key;
        this.hashedGcmKey = HashUtils.createMD5Hash(key);
    }

    public String getKey() {
        return gcmKey;
    }

    public String getHashedKey() {
        return hashedGcmKey;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public boolean isValido() {
        return valido;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((hashedGcmKey == null) ? 0 : hashedGcmKey.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Gcm other = (Gcm) obj;
        if (hashedGcmKey == null) {
            if (other.hashedGcmKey != null)
                return false;
        } else if (!hashedGcmKey.equals(other.hashedGcmKey))
            return false;
        return true;
    }

}
