package br.com.servicoemcasa.model.vo;

public enum EstadoCivil {

    Solteiro, Casado, Divorciado, Viuvo;

}
