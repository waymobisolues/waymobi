package br.com.servicoemcasa.model.service;

public interface DependencyResolver {

    <T> T resolve(Class<T> classOf);
    
}
