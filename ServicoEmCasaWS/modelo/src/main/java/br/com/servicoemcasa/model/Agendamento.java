package br.com.servicoemcasa.model;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.servicoemcasa.model.service.BroadcastMessage;
import br.com.servicoemcasa.model.service.BroadcastService;
import br.com.servicoemcasa.model.service.Broadcastable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@NamedQueries({ @NamedQuery(name = "Agendamento.AgendamentosParaEncerrar", query = "SELECT a FROM Agendamento a WHERE a.solicitacaoAtendimentoPrestador.solicitacaoAtendimento.data < CURRENT_TIMESTAMP AND (a.encerrado = false OR a.avaliacaoPositiva is null)") })
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Table(name = "Agendamento")
public class Agendamento implements Serializable, Broadcastable {

    private static final long serialVersionUID = 1L;

    private transient BroadcastService<Gcm> broadcastService;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codigo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInclusao;

    @OneToOne
    private SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador;

    private boolean encerrado;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    @XStreamOmitField
    private Date dataEncerramento;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    @XStreamOmitField
    private Date dataPrimeiroEncerramento;

    private Boolean avaliacaoPositiva;

    @Inject
    Agendamento() {
    }

    public void encerra() {
        if (dataEncerramento != null) {
            if (dataPrimeiroEncerramento == null) {
                dataPrimeiroEncerramento = dataEncerramento;
            }
        }
        dataEncerramento = new Date();
        this.encerrado = true;
    }

    @Inject
    void setBroadcastService(BroadcastService<Gcm> broadcastService) {
        this.broadcastService = broadcastService;
    }

    @PrePersist
    void prePersist() {
        this.dataInclusao = new Date();
    }

    @PostPersist
    void postPersist() {
        broadcastService.broadcast(this, solicitacaoAtendimentoPrestador.getPrestador().getGcm());
    }

    @PostUpdate
    void postUpdate() {
        if (encerrado && avaliacaoPositiva == null) {
            broadcastService.broadcast(this, solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCliente()
                    .getGcm());
        }
    }

    public Agendamento(SolicitacaoAtendimentoPrestador solicitacaoAtendimentoPrestador) {
        this();
        this.solicitacaoAtendimentoPrestador = solicitacaoAtendimentoPrestador;
    }

    public void avalia(boolean positivo) {
        if (avaliacaoPositiva != null) {
            throw new IllegalStateException();
        }
        avaliacaoPositiva = positivo;
        solicitacaoAtendimentoPrestador.getPrestador().atualizaAvaliacao();
    }

    public long getCodigo() {
        return codigo;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public boolean isEncerrado() {
        return encerrado;
    }

    public Boolean getAvaliacaoPositiva() {
        return avaliacaoPositiva;
    }

    public SolicitacaoAtendimentoPrestador getSolicitacaoAtendimentoPrestador() {
        return solicitacaoAtendimentoPrestador;
    }

    public class AgendamentoMessage {

        public final long codigo;
        public final long codigoSolicitacao;
        public final long codigoSolicitacaoPrestador;
        public final Date data;
        public final String tipoServico;
        public final boolean encerrado;

        public AgendamentoMessage(long codigo, long codigoSolicitacao, long codigoSolicitacaoPrestador, Date data,
                String tipoServico, boolean encerrado) {

            this.codigo = codigo;
            this.codigoSolicitacao = codigoSolicitacao;
            this.codigoSolicitacaoPrestador = codigoSolicitacaoPrestador;
            this.data = data;
            this.tipoServico = tipoServico;
            this.encerrado = encerrado;
        }

    }

    @Override
    public BroadcastMessage getMessage() {
        return new BroadcastMessage(getClass().getSimpleName(), new AgendamentoMessage(this.codigo,
                this.solicitacaoAtendimentoPrestador.getSolicitacaoAtendimento().getCodigo(),
                this.solicitacaoAtendimentoPrestador.getCodigo(), this.solicitacaoAtendimentoPrestador
                        .getSolicitacaoAtendimento().getData(), this.solicitacaoAtendimentoPrestador
                        .getSolicitacaoAtendimento().getTipoPrestador().getDescricao(), encerrado));
    }

}
