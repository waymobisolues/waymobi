package br.com.servicoemcasa.model;

public interface ClienteRepository extends Repository<Integer, Cliente> {

    Cliente findByEmail(String email);
    
    Cliente getByGcm(Gcm gcm);
    Cliente getByGcm(String hashedKey);

}
