package br.com.servicoemcasa.model;

import java.util.List;

public interface ConversaRepository extends Repository<Integer, Conversa> {

    List<Conversa> listByCodigoAtendimentoPrestador(long codigoSolicitacaoAtendimentoPrestador);

    void updateGcm(Gcm gcmAntigo, Gcm gcmNovo);

}
