package br.com.servicoemcasa.model;

import java.util.List;

public interface SolicitacaoAtendimentoRepository extends Repository<Long, SolicitacaoAtendimento> {

    List<SolicitacaoAtendimento> findByCodigoCliente(int codigoCliente);

    List<SolicitacaoAtendimento> findByCodigoPrestador(int codigoPrestador);

}
