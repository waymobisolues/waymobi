DELIMITER $$
drop function if exists calculate_distance;
$$

create function calculate_distance(latOrig float, lngOrig float, latDest float, lngDest float)
returns float
begin
    declare dist float;
    declare latDistance float;
    declare lngDistance float;
    declare AVERAGE_RADIUS_OF_EARTH float;

    set AVERAGE_RADIUS_OF_EARTH = 6371;

    set latDistance = RADIANS(latOrig - latDest);
    set lngDistance = RADIANS(lngOrig - lngDest);

    set dist = (SIN(latDistance / 2) * SIN(latDistance / 2)) + (COS(RADIANS(latOrig)))
            * (COS(RADIANS(latDest))) * (SIN(lngDistance / 2)) * (SIN(lngDistance / 2));

    set dist = 2 * ATAN2(SQRT(dist), SQRT(1 - dist));
    return (ROUND(AVERAGE_RADIUS_OF_EARTH * dist));
end;

$$
