insert into TipoDocumentoConselho values ('CBP','Conselho Brasileiro de Psicanálise');
insert into TipoDocumentoConselho values ('CNIG','Conselho Nacional de Imigração');
insert into TipoDocumentoConselho values ('CRA','Conselho Regional de Administração');
insert into TipoDocumentoConselho values ('CRAS','Conselho Regional de Assistentes Sociais');
insert into TipoDocumentoConselho values ('CRB','Conselho Regional de Biblioteconomia');
insert into TipoDocumentoConselho values ('CRBIO','Conselho Regional de Biologia');
insert into TipoDocumentoConselho values ('CRBM','Conselho Regional de Biomedicina');
insert into TipoDocumentoConselho values ('CRC','Conselho Regional de Contabilidade ');
insert into TipoDocumentoConselho values ('CRECI','Conselho Regional de Corretores de Imóveis');
insert into TipoDocumentoConselho values ('CRE','Conselho Regional de Economia');
insert into TipoDocumentoConselho values ('CRED','Conselho Regional de Economistas Domésticos');
insert into TipoDocumentoConselho values ('CREF','Conselho Regional de Educação Física');
insert into TipoDocumentoConselho values ('COREN','Conselho Regional de Enfermagem');
insert into TipoDocumentoConselho values ('CREA','Conselho Regional de Engenharia Arquitetura e Agronomia');
insert into TipoDocumentoConselho values ('CRF','Conselho Regional de Farmácia');
insert into TipoDocumentoConselho values ('CREFITO','Conselho Regional de Fisioterapia e Terapia Ocupacional');
insert into TipoDocumentoConselho values ('CRFA','Conselho Regional de Fonoaudiologia');
insert into TipoDocumentoConselho values ('CRM','Conselho Regional de Medicina');
insert into TipoDocumentoConselho values ('CRMV','Conselho Regional de Medicina Veterinária');
insert into TipoDocumentoConselho values ('COREM','Conselho Regional de Museologia');
insert into TipoDocumentoConselho values ('CRN','Conselho Regional de Nutricionistas');
insert into TipoDocumentoConselho values ('CRO','Conselho Regional de Odontologia');
insert into TipoDocumentoConselho values ('CROO','Conselho Regional de Óptica e Optometria');
insert into TipoDocumentoConselho values ('CRPRE','Conselho Regional de Profissionais de Relações Públicas');
insert into TipoDocumentoConselho values ('CONRERP','Conselho Regional de Profissionais Relações Públicas');
insert into TipoDocumentoConselho values ('CRP','Conselho Regional de Psicologia ');
insert into TipoDocumentoConselho values ('CRQ','Conselho Regional de Química');
insert into TipoDocumentoConselho values ('CONRAD','Conselho Regional de Radiodifusão Comunitária');
insert into TipoDocumentoConselho values ('CRESS','Conselho Regional de Serviço Social');
insert into TipoDocumentoConselho values ('CRTR','Conselho Regional de Técnicos em Radiologia');
insert into TipoDocumentoConselho values ('CRDD','Conselho Regional Despachantes Documentalista');
insert into TipoDocumentoConselho values ('CRRC','Conselho Regional dos Representantes Comerciais');
insert into TipoDocumentoConselho values ('OAB','Ordem dos Advogados do Brasil');
insert into TipoDocumentoConselho values ('OMB','Ordem dos Músicos do Brasil');

insert into UF values ('AC', 'Acre');
insert into UF values ('AL', 'Alagoas');
insert into UF values ('AP', 'Amapá');
insert into UF values ('AM', 'Amazonas');
insert into UF values ('BA', 'Bahia');
insert into UF values ('CE', 'Ceará');
insert into UF values ('DF', 'Distrito Federal');
insert into UF values ('ES', 'Espírito Santo');
insert into UF values ('GO', 'Goiás');
insert into UF values ('MA', 'Maranhão');
insert into UF values ('MT', 'Mato Grosso');
insert into UF values ('MS', 'Mato Grosso do Sul');
insert into UF values ('MG', 'Minas Gerais');
insert into UF values ('PA', 'Pará');
insert into UF values ('PB', 'Paraíba');
insert into UF values ('PR', 'Paraná');
insert into UF values ('PE', 'Pernambuco');
insert into UF values ('PI', 'Piauí');
insert into UF values ('RJ', 'Rio de Janeiro');
insert into UF values ('RN', 'Rio Grande do Norte');
insert into UF values ('RS', 'Rio Grande do Sul');
insert into UF values ('RO', 'Rondônia');
insert into UF values ('RR', 'Roraima');
insert into UF values ('SC', 'Santa Catarina');
insert into UF values ('SP', 'São Paulo');
insert into UF values ('SE', 'Sergipe');
insert into UF values ('TO', 'Tocantins');

insert into GrupoTipoPrestador (descricaoGrupo) values ('Arte e Cultura');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Artesã', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Capista de Livros', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Coreógrafo', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Danças', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Escritor(a)', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Ilustradora', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Jornalista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Bateria', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Canto', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Instr. de Sopro', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Piano e Teclado', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Violão e Guitarra', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) d eViolino', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Poeta', (select max(codigoGrupo) from GrupoTipoPrestador));


insert into GrupoTipoPrestador (descricaoGrupo) values ('Assistência Técnica');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Assistência Técnica Eletro e Eletrônicos', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Assistência Técnica de Linha Branca', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Consertos de Ferramentas Elétricas', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Manutenção em Ar Condicionado', (select max(codigoGrupo) from GrupoTipoPrestador));

insert into GrupoTipoPrestador (descricaoGrupo) values ('Construção e Reformas');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Arquiteto Design de Interior', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Arquiteto(a)', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Arquiteto(a) Urbanista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Arquitetura e Construção Civil', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Arquitetura e Interiores', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Carpinteiro', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Designer de Interiores', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Elétrica e Pintura', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Eletricista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Eletricista de Manutenção', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Eletricista Predial', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Eletricista Residencial', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Eletricista Residencial e Comercial', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Eletromecânico', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Empreiteiro - reformas em geral', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Estruturas Metálicas em Geral', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Instalador de Portas', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Jardineiro', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Lider de Carpintaria', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Paisagista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Pedreiro', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Pintor', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Pinturas Reformas e Gesso', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Pinturas Residencias e Texturas', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Serralheiro', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Serralheiro Artístico e Industrial', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Serviços de Manutenção em Vidro Temperado', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Técnico em Edificação', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Técnico em instalação de portões automáticos e cancelas', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Técnico em Manutenção Residencial e Comercial', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Vidraceiro', (select max(codigoGrupo) from GrupoTipoPrestador));

insert into GrupoTipoPrestador (descricaoGrupo) values ('Consultoria e Assessoria');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Administrador Contábil', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Administrador Técnico', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Advogado Cível', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Advogado Criminal', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Advogado Família', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Advogado Previdenciário', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Advogado Trabalhista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Analista de PCP', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Consultor de Vendas', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Consultor de Vendas - Projetista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Consultoria Jurídica', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Consultor Imobiliário', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Contador', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Corretor de Imóveis', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Engenheiro(a)', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Engenheiro(a) Civil', (select max(codigoGrupo) from GrupoTipoPrestador));

insert into GrupoTipoPrestador (descricaoGrupo) values ('Design e Tecnologia');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Arquitetura Computação Gráfica', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Desenhista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Desenhista Projetista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Designer Gráfico', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Eletrotécnico', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Técnico em Informática', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Técnico em Mecatrônica', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('WebDesigner', (select max(codigoGrupo) from GrupoTipoPrestador));

insert into GrupoTipoPrestador (descricaoGrupo) values ('Ensino e Educação');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Pedagoga', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor Intérprete', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Educação Física', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Física', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Geografia', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de História', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Informática', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Inglês', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Matemática', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Professor(a) de Português', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Tradutor de Libras', (select max(codigoGrupo) from GrupoTipoPrestador));

insert into GrupoTipoPrestador (descricaoGrupo) values ('Eventos');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Animação de Festas', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Bandas Cantores e DJs', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Bartenders', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Buffets', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Churrasqueiro', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Cozinha de eventos', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Doces e Salgadinhos', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Fotografia e Filmagens', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Garçons e Copeiras', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Recepcionista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Segurança Privada', (select max(codigoGrupo) from GrupoTipoPrestador));

insert into GrupoTipoPrestador (descricaoGrupo) values ('Moda e Beleza');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Cabeleireiro(a)', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Costureira', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Depiladora', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Esteticista Corporal', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Manicure', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Maquiadora', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Personal Style', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Sapateiro', (select max(codigoGrupo) from GrupoTipoPrestador));

insert into GrupoTipoPrestador (descricaoGrupo) values ('Saúde');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Acupunturista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Acupunturista Auricular', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Analista Junguiana', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Auxiliar de Enfermagem', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Cuidadora de Idosos', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Enfermeira(o) Padrão', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Fisioterapeuta', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Especialista em Psicomotricidade', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Hipnoterapeuta', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Clinico Geral', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Cirurgião Dentista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Dentista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Especialista em Dep. Química', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Especialista em Saúde Mental', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Neuropsicóloga', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Neuropsicopedagoga', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Ortopedista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Psicanalista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Psicólogo', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Psicólogo Cognitivo Comport.', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Psicólogo Infantil', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Psiquiatra', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Traumatologista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Médico(a) Veterinário', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Nutricionista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Ortodontista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Personal Trainer', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Pilates', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Quiropraxia', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('RPG', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Técnico de Enfermagem', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Terapeuta Holístico', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Sexóloga', (select max(codigoGrupo) from GrupoTipoPrestador));

insert into GrupoTipoPrestador (descricaoGrupo) values ('Servicos Domésticos');
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Adestrador de Cães', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Cozinheira', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Diarista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Manutenção em Geral', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Manutenção em Piscinas', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Motorista', (select max(codigoGrupo) from GrupoTipoPrestador));
insert into TipoPrestador (descricao, grupoTipoPrestador_codigoGrupo) values ('Passadeira', (select max(codigoGrupo) from GrupoTipoPrestador));
